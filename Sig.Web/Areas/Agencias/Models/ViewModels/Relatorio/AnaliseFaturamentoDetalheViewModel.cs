﻿using System.Collections.Generic;
using Sig.Web.Models;

namespace Sig.Web.Areas.Agencias.Models.ViewModels.Relatorio
{
    public class AnaliseFaturamentoDetalheViewModel
    {
        public Faturamento Faturamento { get; set; }
        public IEnumerable<FaturamentoItem> FaturamentoItens { get; set; }
    }
}