﻿using Sig.Web.Models;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Sig.Web.Models.ViewModels;
using Microsoft.AspNetCore.Authorization;
using AspNetCoreHero.ToastNotification.Abstractions;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Sig.Web.Areas.Agencias.Controllers
{

    [Authorize(Roles = "Administradora")]
    public class ItemFaturavelRelacionadoController : BaseController
    {
        public ItemFaturavelRelacionadoController(INotyfService notificacao) : base(notificacao)
        {
        }

        #region Index

        public IActionResult Index()
        {
            var viewModel = new ItemFaturavelRelacionadoViewModel
            {
                ItensFaturaveisRelacionados = _context.ItemFaturavelRelacionados
            };
            return View(viewModel);
        }

        [HttpPost]
        public IActionResult Index(ItemFaturavelRelacionadoViewModel viewModel)
        {
            IQueryable<ItemFaturavelRelacionado> itensFaturaveis = _context.ItemFaturavelRelacionados;

            #region Filtros

            if (!viewModel.ItemFaturavelRelacionado.ItemFaturavelId.Equals(0))
            {
                itensFaturaveis = itensFaturaveis.Where(c => c.ItemFaturavelId == viewModel.ItemFaturavelRelacionado.ItemFaturavelId);
            }

            if (!string.IsNullOrEmpty(viewModel.ItemFaturavelRelacionado.Descricao))
            {
                itensFaturaveis = itensFaturaveis.Where(c => c.Descricao.Contains(viewModel.ItemFaturavelRelacionado.Descricao));
            }

            #endregion

            viewModel.ItensFaturaveisRelacionados = itensFaturaveis;
            return View(viewModel);
        }

        #endregion

        #region Incluir

        public IActionResult Incluir()
        {
            ViewBag.ItensFaturaveis = new SelectList(_context.ItemFaturaveis, "ItemFaturavelId", "Descricao");
            return View();
        }

        [HttpPost]
        public IActionResult Incluir(ItemFaturavelRelacionado itemFaturavel)
        {
            if (itemFaturavel is null)
            {
                throw new System.ArgumentNullException(nameof(itemFaturavel));
            }

            var isDuplicado = _context.ItemFaturavelRelacionados.Any(c => c.Descricao == itemFaturavel.Descricao);

            if (isDuplicado)
                ModelState.AddModelError("Descricao", "Já existe esta descrição");

            if (ModelState.IsValid)
            {
                _context.ItemFaturavelRelacionados.Add(itemFaturavel);
                _context.SaveChanges();

                _notificacao.Success("Item Faturável Relacionado incluído com sucesso");
                return RedirectToAction("Index");
            }

            ViewBag.ItensFaturaveis = new SelectList(_context.ItemFaturaveis, "ItemFaturavelId", "Descricao");
            return View(itemFaturavel);
        }

        #endregion

        #region Alterar

        public IActionResult Alterar(int id)
        {
            var itemFaturavelRelacionado = _context.ItemFaturavelRelacionados.Find(id);
            ViewBag.ItensFaturaveis = new SelectList(_context.ItemFaturaveis, "ItemFaturavelId", "Descricao");

            return View(itemFaturavelRelacionado);
        }

        [HttpPost]
        public IActionResult Alterar(ItemFaturavelRelacionado form)
        {
            if (ModelState.IsValid)
            {
                var itemFaturavelRelacionado = _context.ItemFaturavelRelacionados.Find(form.ItemFaturavelRelacionadoId);

                itemFaturavelRelacionado.Descricao = form.Descricao;
                _context.SaveChanges();

                _notificacao.Success("Item Faturável Relacionado alterado com sucesso");
                return RedirectToAction("Index");
            }

            ViewBag.ItensFaturaveis = new SelectList(_context.ItemFaturaveis, "ItemFaturavelId", "Descricao");
            return View(form);
        }

        #endregion

        #region Excluir

        public IActionResult Excluir(int id = 0)
        {
            var itemFaturavelRelacionado = _context.ItemFaturavelRelacionados.Find(id);

            if (itemFaturavelRelacionado != null)
            {
                _context.ItemFaturavelRelacionados.Remove(itemFaturavelRelacionado);
                _context.SaveChanges();

                _notificacao.Success("Consulta relacionada excluída com sucesso");
                return RedirectToAction("Index");
            }
            else
            {
                _notificacao.Warning("Consulta não encontrada");
                return RedirectToAction("Index");
            }
        }

        #endregion

    }
}
