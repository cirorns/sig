﻿using Sig.Web.Models;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using AspNetCoreHero.ToastNotification.Abstractions;

namespace Sig.Web.Areas.Agencias.Controllers
{
    public class DiaCobrancaController : BaseController
    {
        public DiaCobrancaController(INotyfService notificacao) : base(notificacao)
        {
        }

        public IActionResult Index()
        {
            var dias = _context.DiaCobrancas.OrderBy(c => c.Dia);
            return View(dias);
        }

        public IActionResult Incluir()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Incluir(DiaCobranca diaCobranca)
        {
            if (ModelState.IsValid)
            {
                _context.DiaCobrancas.Add(diaCobranca);
                _context.SaveChanges();

                _notificacao.Success("Dia cadastrado com sucesso");
                return RedirectToAction("Index");
            }

            return View();
        }
    }
}
