﻿using AspNetCoreHero.ToastNotification.Abstractions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Sig.Web.Helpers;
using Sig.Web.Helpers.Extensoes;
using Sig.Web.Models;
using Sig.Web.Models.Enums;
using Sig.Web.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;

namespace Sig.Web.Areas.Agencias.Controllers
{
    public class AdministradoraController : BaseController
    {
        public AdministradoraController(INotyfService notificacao) : base(notificacao)
        {
        }

        #region Incluir Agência

        [AllowAnonymous]
        public IActionResult Cadastro()
        {
            if (_context.Clientes.Any(c => c.TipoCliente == eTipoCliente.Administradora))
            {
                TempData["Alertas"] = "A Administradora já foi cadastrada.";
                return RedirectToAction("Login", "Usuario");
            }
            
            RetornaAtividades();
            RetornaEstados();
            
            var viewModel = new ClienteAgenciaViewModel();
            return View(viewModel);
        }

        [HttpPost, ValidateAntiForgeryToken()]
        public IActionResult Cadastro(ClienteAgenciaViewModel viewModel)
        {
            string retorno = string.Empty;

            if (ModelState.IsValid)
            {
                using var ts = new TransactionScope();
                try
                {
                    viewModel.Cliente.TipoCliente = eTipoCliente.Administradora;
                    viewModel.Cliente.IsAtivo = true;
                    viewModel.Cliente.CPFCNPJ = viewModel.Cliente.CPFCNPJ.Trim();
                    viewModel.Cliente.Nome = viewModel.Cliente.Nome.ToUpper();

                    _context.Clientes.Add(viewModel.Cliente);

                    viewModel.Cliente.Enderecos = viewModel.Enderecos.ToList();
                    viewModel.Cliente.Telefones = viewModel.Telefones.ToList();

                    var cargo = new Cargo
                    {
                        AgenciaId = viewModel.Cliente.ClienteId,
                        Descricao = "ADMINISTRADOR",
                    };

                    _context.Cargos.Add(cargo);

                    viewModel.Colaborador.Cargo = cargo;
                    viewModel.Colaborador.AgenciaId = viewModel.Cliente.ClienteId;

                    _context.Colaboradores.Add(viewModel.Colaborador);

                    var senha = Util.ObterSenhaAleatoria(8);

                    var usuario = new Usuario
                    {
                        ColaboradorId = viewModel.Colaborador.ColaboradorId,
                        Senha = senha,
                        SenhaConfirmar = senha,
                        IsSuperUsuario = true,
                        IsAtivo = true,
                    };

                    _context.Usuarios.Add(usuario);
                    _context.SaveChanges();

                    retorno = Mensagens.OnSuccess(string.Format("<strong>Dados cadastrados com sucesso!</strong><br/><br/>Anote os dados:<br/> Cliente: <strong>{0}</strong><br/> Email: <strong>{1}</strong><br/> Senha: <strong>{2}</strong>", viewModel.Cliente.ClienteId, viewModel.Colaborador.Email, usuario.Senha), Url.Action("Login", "Usuario"));
                    ts.Complete();

                }
                catch (Exception ex)
                {
                    retorno = Javascript.OnFailure("Falha!", ex.Message);
                    ts.Dispose();
                }
            }
            else
            {
                retorno = Mensagens.OnFailure("Falha!", "Você precisa preencher todos os campos corretamente");
            }

            return new JavaScriptResult(retorno);
        }

        #endregion

        #region Métodos Privados

        private void RetornaAtividades()
        {
            try
            {
                if (!_context.Atividades.Any())
                {
                    List<Atividade> atividades = new List<Atividade>();

                    atividades.Add(new Atividade { Descricao = "AGRICULTURA, PECUÁRIA, PRODUÇÃO FLORESTAL, PESCA E AQÜICULTURA" });
                    atividades.Add(new Atividade { Descricao = "INDÚSTRIAS EXTRATIVAS" });
                    atividades.Add(new Atividade { Descricao = "INDÚSTRIAS DE TRANSFORMAÇÃO" });
                    atividades.Add(new Atividade { Descricao = "ELETRICIDADE E GÁS" });
                    atividades.Add(new Atividade { Descricao = "ÁGUA, ESGOTO, ATIVIDADES DE GESTÃO DE RESÍDUOS E DESCONTAMINAÇÃO" });
                    atividades.Add(new Atividade { Descricao = "CONSTRUÇÃO" });
                    atividades.Add(new Atividade { Descricao = "COMÉRCIO; REPARAÇÃO DE VEÍCULOS AUTOMOTORES E MOTOCICLETAS" });
                    atividades.Add(new Atividade { Descricao = "TRANSPORTE, ARMAZENAGEM E CORREIO" });
                    atividades.Add(new Atividade { Descricao = "ALOJAMENTO E ALIMENTAÇÃO" });
                    atividades.Add(new Atividade { Descricao = "INFORMAÇÃO E COMUNICAÇÃO" });
                    atividades.Add(new Atividade { Descricao = "ATIVIDADES FINANCEIRAS, DE SEGUROS E SERVIÇOS RELACIONADOS" });
                    atividades.Add(new Atividade { Descricao = "ATIVIDADES IMOBILIÁRIAS" });
                    atividades.Add(new Atividade { Descricao = "ATIVIDADES PROFISSIONAIS, CIENTÍFICAS E TÉCNICAS" });
                    atividades.Add(new Atividade { Descricao = "ATIVIDADES ADMINISTRATIVAS E SERVIÇOS COMPLEMENTARES" });
                    atividades.Add(new Atividade { Descricao = "ADMINISTRAÇÃO PÚBLICA, DEFESA E SEGURIDADE SOCIAL" });
                    atividades.Add(new Atividade { Descricao = "EDUCAÇÃO" });
                    atividades.Add(new Atividade { Descricao = "SAÚDE HUMANA E SERVIÇOS SOCIAIS" });
                    atividades.Add(new Atividade { Descricao = "ARTES, CULTURA, ESPORTE E RECREAÇÃO" });
                    atividades.Add(new Atividade { Descricao = "OUTRAS ATIVIDADES DE SERVIÇOS" });
                    atividades.Add(new Atividade { Descricao = "SERVIÇOS DOMÉSTICOS" });
                    atividades.Add(new Atividade { Descricao = "ORGANISMOS INTERNACIONAIS E OUTRAS INSTITUIÇÕES EXTRATERRITORIAIS" });
                    atividades.Add(new Atividade { Descricao = "OUTROS" });
                    
                    _context.Atividades.AddRange(atividades);
                    _context.SaveChanges();
                }

                ViewBag.Atividades = new SelectList(_context.Atividades.OrderBy(c => c.Descricao), "AtividadeId", "Descricao");

            }
            catch (Exception)
            {
                throw;
            }
            
        }

        private void RetornaEstados()
        {
            ViewBag.Estados = new SelectList(Estado.UFs, "Sigla", "Descricao");
        }

        #endregion
        
    }
}