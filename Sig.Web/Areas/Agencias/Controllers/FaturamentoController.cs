﻿using Sig.Web.Models;
using Sig.Web.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.AspNetCore.Mvc;
using Sig.Web.Models.Enums;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using AspNetCoreHero.ToastNotification.Abstractions;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;
using Dapper;

namespace Sig.Web.Areas.Agencias.Controllers
{
    //[Authorize(Roles = "Administradora")]
    public class FaturamentoController : BaseController
    {
        public FaturamentoController(INotyfService notificacao) : base(notificacao)
        {
        }

        #region Funções do Faturamento

        private int Consumo()
        {
            var itemFaturavel = _context.ItemFaturaveis.FirstOrDefault(c => c.Descricao.Equals("CONSUMO"));

            if (itemFaturavel == null)
            {
                itemFaturavel = new ItemFaturavel
                {
                    CustoUnitario = 0,
                    Descricao = "CONSUMO",
                    IsAtivo = false,
                    IsPacote = false,
                    ValorMaximo = 0,
                    ValorMinimo = 0,
                    ValorUnitario = 0,
                };

                _context.ItemFaturaveis.Add(itemFaturavel);
                _context.SaveChanges();
            }

            return itemFaturavel.ItemFaturavelId;
        }

        private int ConsultaExcedentePacote()
        {
            var itemFaturavel = _context.ItemFaturaveis.FirstOrDefault(c => c.Descricao.Equals("CONSULTA EXCEDENTE PACOTE"));

            if (itemFaturavel == null)
            {
                itemFaturavel = new ItemFaturavel
                {
                    CustoUnitario = 0,
                    Descricao = "CONSULTA EXCEDENTE PACOTE",
                    IsAtivo = false,
                    IsPacote = false,
                    ValorMaximo = 0,
                    ValorMinimo = 0,
                    ValorUnitario = 0,
                };

                _context.ItemFaturaveis.Add(itemFaturavel);
                _context.SaveChanges();
            }

            return itemFaturavel.ItemFaturavelId;
        }

        private int TaxaExtra()
        {
            var itemFaturavel = _context.ItemFaturaveis.FirstOrDefault(c => c.Descricao.Equals("TAXA EXTRA"));

            if (itemFaturavel == null)
            {
                itemFaturavel = new ItemFaturavel
                {
                    CustoUnitario = 0,
                    Descricao = "TAXA EXTRA",
                    IsAtivo = false,
                    IsPacote = false,
                    ValorMaximo = 0,
                    ValorMinimo = 0,
                    ValorUnitario = 0,
                };

                _context.ItemFaturaveis.Add(itemFaturavel);
                _context.SaveChanges();
            }

            return itemFaturavel.ItemFaturavelId;
        }

        private int Mensalidade()
        {
            var itemFaturavel = _context.ItemFaturaveis.FirstOrDefault(c => c.Descricao.Equals("MENSALIDADE"));

            if (itemFaturavel == null)
            {
                itemFaturavel = new ItemFaturavel
                {
                    CustoUnitario = 0,
                    Descricao = "MENSALIDADE",
                    IsAtivo = true,
                    IsPacote = false,
                    ValorMaximo = 9999,
                    ValorMinimo = 0,
                    ValorUnitario = 0,
                };

                _context.ItemFaturaveis.Add(itemFaturavel);
                _context.SaveChanges();
            }

            return itemFaturavel.ItemFaturavelId;
        }

        private List<FaturamentoItemConsultado> ItensConsultados(int tabelaId, int clienteId, DateTime dataInicialFaturamento, DateTime dataFinalFaturamento)
        {
            StringBuilder sql = new();
            sql.AppendLine("SELECT ItemFaturavel.Descricao,  ");
            sql.AppendLine("       ItemFaturavel.ItemFaturavelId,  ");
            sql.AppendLine("       COALESCE(TabelaItem.ValorUnitario, ItemFaturavel.ValorMinimo) AS ValorUnitario,  ");
            sql.AppendLine("       COALESCE(TabelaItem.QuantidadePacote, 0) AS QuantidadePacote,  ");
            sql.AppendLine("       ItemFaturavel.CustoUnitario,  ");
            sql.AppendLine("       ItemFaturavel.ValorMinimo,  ");
            sql.AppendLine("       COUNT(*) AS QuantidadeConsultada ");
            sql.AppendLine("FROM TransacaoConsulta T1 ");
            sql.AppendLine("INNER JOIN ItemFaturavel ON (ItemFaturavel.ItemFaturavelId = T1.ItemFaturavelId) ");
            sql.AppendLine("LEFT JOIN TabelaItem ON (TabelaItem.TabelaId = @p0 ");
            sql.AppendLine("                         AND TabelaItem.ItemFaturavelId = ItemFaturavel.ItemFaturavelId)  ");
            sql.AppendLine("WHERE (T1.ClienteId = @p1) ");
            sql.AppendLine("    AND (CONVERT(DATETIME, CONVERT(VARCHAR, T1.DataEnvio, 101)) >= @p2 ");
            sql.AppendLine("         AND CONVERT(DATETIME, CONVERT(VARCHAR, T1.DataEnvio, 101)) <= @p3) ");
            sql.AppendLine("    AND (T1.IsConfirmado = 1) ");
            sql.AppendLine("GROUP BY ItemFaturavel.Descricao,  ");
            sql.AppendLine("         ItemFaturavel.ItemFaturavelId,  ");
            sql.AppendLine("         TabelaItem.ValorUnitario,  ");
            sql.AppendLine("         TabelaItem.QuantidadePacote,  ");
            sql.AppendLine("         ItemFaturavel.CustoUnitario,  ");
            sql.AppendLine("         ItemFaturavel.ValorMinimo");

            //TODO: Corrigir
            return null;

            //return _context.Query<FaturamentoItemConsultado>(sql.ToString(),
            //    new SqlParameter("p0", tabelaId),
            //    new SqlParameter("p1", clienteId),
            //    new SqlParameter("p2", dataInicialFaturamento),
            //    new SqlParameter("p3", dataFinalFaturamento)).ToList();
        }

        private IEnumerable<FaturamentoItemConsultado> ItensMensalidade(int tabelaId)
        {
            StringBuilder sql = new();
            sql.AppendLine("SELECT  ItemFaturavel.Descricao, ");
            sql.AppendLine("		ItemFaturavel.ItemFaturavelId, ");
            sql.AppendLine("		TabelaItem.ValorUnitario, ");
            sql.AppendLine("		0 AS QuantidadePacote, ");
            sql.AppendLine("		ItemFaturavel.CustoUnitario, ");
            sql.AppendLine("		ItemFaturavel.ValorMinimo, ");
            sql.AppendLine("		1 AS QuantidadeConsultada ");
            sql.AppendLine("FROM TabelaItem INNER JOIN ItemFaturavel ");
            sql.AppendLine("ON TabelaItem.ItemFaturavelId = ItemFaturavel.ItemFaturavelId ");
            sql.AppendLine("WHERE TabelaItem.TabelaId = @p0 AND ItemFaturavel.ItemFaturavelId = @p1");

            return _context.Database.GetDbConnection().Query<FaturamentoItemConsultado>(sql.ToString(), new { p0 = tabelaId, p1 = Mensalidade() });
        }

        private void DeletarFaturamentoAntigo(DateTime dataInicialFaturamento, DateTime dataFinalFaturamento)
        {
            string sql = "DELETE FROM Faturamento WHERE DataInicialFaturamento = @p0 AND DataFinalFaturamento = @p1";

            _context.Database.ExecuteSqlRaw(sql,
                new SqlParameter("p0", dataInicialFaturamento),
                new SqlParameter("p1", dataFinalFaturamento));
        }

        private void DeletarFaturamentoAntigo(DateTime dataInicialFaturamento, DateTime dataFinalFaturamento, int clienteId)
        {
            string sql = "DELETE FROM Faturamento WHERE DataInicialFaturamento = @p0 AND DataFinalFaturamento = @p1 AND ClienteId = @p2";

            _context.Database.ExecuteSqlRaw(sql,
                new SqlParameter("p0", dataInicialFaturamento),
                new SqlParameter("p1", dataFinalFaturamento),
                new SqlParameter("p2", clienteId));
        }

        private void DeletarFaturamentoAntigoAgencia(DateTime dataInicialFaturamento, DateTime dataFinalFaturamento, int agenciaId)
        {
            string sql = "DELETE FROM Faturamento WHERE DataInicialFaturamento = @p0 AND DataFinalFaturamento = @p1 AND ClienteId IN(Select ClienteId From Cliente Where AgenciaId = @p2 OR ClienteId = @p2)";

            _context.Database.ExecuteSqlRaw(sql,
                new SqlParameter("p0", dataInicialFaturamento),
                new SqlParameter("p1", dataFinalFaturamento),
                new SqlParameter("p2", agenciaId));
        }

        private void AtualizarFaturamentoItem(int quantidadePacote, int faturamentoId, int consultaExcedentePacoteId)
        {
            string sql = "UPDATE FaturamentoItem SET Quantidade = Quantidade + @p0 WHERE FaturamentoId = @p1 AND ItemFaturavelId = @p2";


            _context.Database.ExecuteSqlRaw(sql,
                                            new SqlParameter("p0", quantidadePacote),
                                            new SqlParameter("p1", faturamentoId),
                                            new SqlParameter("p2", consultaExcedentePacoteId));
        }
        
        private void SalvarFaturamento(Faturamento faturamento)
        {
            using var db = new ContextoDados();
            db.Faturamentos.Add(faturamento);
            db.SaveChanges();
        }

        private static void SalvarFaturamentoItem(FaturamentoItem faturamentoItem)
        {
            using var db = new ContextoDados();
            db.FaturamentoItems.Add(faturamentoItem);
            db.SaveChanges();
        }

        private void DeletarFaturamento(int id)
        {
            using var db = new ContextoDados();
            var faturamento = db.Faturamentos.Find(id);

            if (faturamento != null)
            {
                db.Faturamentos.Remove(faturamento);
                db.SaveChanges();
            }
        }

        private void AtualizarFaturamento(Faturamento faturamento)
        {
            using (var db = new ContextoDados())
            {
                db.Faturamentos.Attach(faturamento);
                var entry = db.Entry<Faturamento>(faturamento);

                entry.Property(c => c.ValorOriginal).IsModified = true;
                entry.Property(c => c.ValorCusto).IsModified = true;
                entry.Property(c => c.ValorRepasseAgencia).IsModified = true;

                entry.Property(c => c.ClienteId).IsModified = false;
                entry.Property(c => c.DataEmissao).IsModified = false;
                entry.Property(c => c.DataFinalFaturamento).IsModified = false;
                entry.Property(c => c.DataInicialFaturamento).IsModified = false;
                entry.Property(c => c.DataPagamento).IsModified = false;
                entry.Property(c => c.DataVencimento).IsModified = false;
                entry.Property(c => c.FaturamentoId).IsModified = false;
                entry.Property(c => c.TipoPagamento).IsModified = false;
                entry.Property(c => c.ValorDesconto).IsModified = false;
                entry.Property(c => c.ValorJuros).IsModified = false;
                entry.Property(c => c.ValorPago).IsModified = false;

                db.SaveChanges();
            }
        }

        #endregion

        #region Faturamento

        public IActionResult Gerar()
        {
            var hoje = DateTime.Today;
            var mes = new DateTime(hoje.Year, hoje.Month, 1);
            
            var vm = new FaturamentoViewModel {
                DataInicial = mes.AddMonths(-1),
                DataFinal = mes.AddDays(-1)
            };

            return View(vm);
        }

        [HttpPost]
        public IActionResult Gerar(FaturamentoViewModel viewModel)
        {
            int QuantidadeProcessada        = 0;
            DateTime dataInicialFaturamento = viewModel.DataInicial;
            DateTime dataFinalFaturamento   = viewModel.DataFinal;
            DateTime dataVencimentoFatura;
            int DiasFaturados               = 0;
            int ConsultaExcendentePacoteId  = ConsultaExcedentePacote();

            int debugCliente = 0;

            try
            {
                using (var db = new ContextoDados())
                {
                    DeletarFaturamentoAntigo(dataInicialFaturamento, dataFinalFaturamento);

                    var contratos = db.Contratos.AsNoTracking().Where(c => !c.DataCancelamento.HasValue || c.DataCancelamento >= dataInicialFaturamento)
                                       .Where(c => c.DataInicial <= dataFinalFaturamento).Where(c => c.Cliente.TipoCliente == eTipoCliente.Cliente || c.Cliente.TipoCliente == eTipoCliente.Agencia)
                                       .OrderBy(c => c.ClienteId).OrderBy(c => c.ContratoId);

                    foreach (var contrato in contratos)
                    {

                        decimal percentualAgencia = contrato.Cliente.Agencia.ComissaoAgente.HasValue ? (contrato.Cliente.Agencia.ComissaoAgente.Value / 100) : 0.0M; //0.5M;

                        bool isFaturaConsumo = contrato.Tabela.IsConsumo;
                        bool isFaturaProporcional = false;
                        bool isPacote = false;

                        decimal ValorTotal = 0;
                        decimal CustoTotal = 0;
                        decimal CustoPacote = 0;

                        decimal ValorTotalConsumo = 0;
                        decimal CustoTotalConsumo = 0;

                        decimal ValorRepasseAgencia = 0;
                        decimal ValorRepasseAgenciaTotal = 0;

                        debugCliente = contrato.ClienteId;

                        //Debug
                        if (contrato.ClienteId.Equals(345)) //245, 247, 248
                        {
                            //Somente Debug
                        }

                        //==============================//
                        // DATA DE VENCIMENTO DA FATURA //
                        //==============================//
                        if (viewModel.DataVencimento.HasValue)
                        {
                            dataVencimentoFatura = viewModel.DataVencimento.Value;
                        }
                        else
                        {
                            dataVencimentoFatura = DateTime.Parse(string.Format("{0}/{1}/{2}", contrato.DiaCobranca.Dia, DateTime.Now.Month, DateTime.Now.Year));
                        }

                        //==============================//
                        //   FATURAMENTO DO CLIENTE     //
                        //==============================//

                        var faturamento = new Faturamento
                        {
                            ClienteId = contrato.ClienteId,
                            DataEmissao = DateTime.Now,
                            DataVencimento = dataVencimentoFatura,
                            ValorCusto = 0,
                            ValorOriginal = 0,
                            ValorRepasseAgencia = 0,
                            //TipoPagamento = Web.Models.eTipoPagamento.Nenhum,
                            DataInicialFaturamento = dataInicialFaturamento,
                            DataFinalFaturamento = dataFinalFaturamento,
                        };

                        SalvarFaturamento(faturamento);
                        //_context.Faturamentos.Add(faturamento);
                        //_context.SaveChanges();

                        //=====================================//
                        //         CONSULTAS REALIZADAS        //
                        //=====================================//
                        var itensConsultados = ItensConsultados(contrato.TabelaId, contrato.ClienteId, dataInicialFaturamento, dataFinalFaturamento);

                        foreach (var itemConsultado in itensConsultados)
                        {
                            //MUDANÇA PARA REVENDA
                            if (contrato.Cliente.Agencia.IsRevenda || contrato.Cliente.IsRevenda)
                            {
                                if (itemConsultado.CustoRevenda > 0) //? Não tem custo revenda.
                                {
                                    itemConsultado.CustoUnitario = itemConsultado.CustoRevenda;
                                }
                            }

                            QuantidadeProcessada += itemConsultado.QuantidadeConsultada;

                            //Verifica se o valor do item é menor que o custo, considero o custo neste caso.
                            if (itemConsultado.CustoUnitario > itemConsultado.ValorUnitario)
                                itemConsultado.ValorUnitario = itemConsultado.CustoUnitario;

                            //Possui pacote?
                            //Para fatura consumo não existe pacote.
                            if (itemConsultado.QuantidadePacote > 0 && !isFaturaConsumo)
                            {
                                /*if (isFaturaProporcional)
                                    itemConsultado.QuantidadePacote =  ((itemConsultado.QuantidadePacote / 30) * DiasFaturados);*/

                                if (itemConsultado.QuantidadePacote > 0)
                                {
                                    if (itemConsultado.QuantidadePacote > itemConsultado.QuantidadeConsultada)
                                        itemConsultado.QuantidadePacote = itemConsultado.QuantidadeConsultada;

                                    itemConsultado.QuantidadeConsultada = itemConsultado.QuantidadeConsultada - itemConsultado.QuantidadePacote;


                                    if (!isPacote)
                                    {
                                        var faturamentoItem = new FaturamentoItem
                                        {
                                            FaturamentoId = faturamento.FaturamentoId,
                                            ItemFaturavelId = ConsultaExcendentePacoteId,
                                            Quantidade = itemConsultado.QuantidadePacote,
                                            CustoUnitario = 0,
                                            ValorUnitario = 0,
                                            RepasseAgencia = 0
                                        };

                                        SalvarFaturamentoItem(faturamentoItem);
                                    }
                                    else
                                    {
                                        AtualizarFaturamentoItem(itemConsultado.QuantidadePacote, faturamento.FaturamentoId, ConsultaExcendentePacoteId);
                                    }

                                    //Custo do Pacote na Mensalidade
                                    CustoPacote += (itemConsultado.CustoUnitario * itemConsultado.QuantidadePacote);
                                    isPacote = true;
                                }
                            }

                            if (itemConsultado.QuantidadeConsultada > 0)
                            {
                                if (contrato.Cliente.Agencia.IsComissionamento)
                                {
                                    ValorRepasseAgencia = (itemConsultado.ValorUnitario * itemConsultado.QuantidadeConsultada) * percentualAgencia;
                                }
                                else
                                {
                                    ValorRepasseAgencia = ((itemConsultado.ValorUnitario * itemConsultado.QuantidadeConsultada) - (itemConsultado.CustoUnitario * itemConsultado.QuantidadeConsultada)) * percentualAgencia;
                                }

                                ValorRepasseAgenciaTotal += ValorRepasseAgencia;

                                var faturamentoItem = new FaturamentoItem
                                {
                                    FaturamentoId = faturamento.FaturamentoId,
                                    ItemFaturavelId = itemConsultado.ItemFaturavelId,
                                    Quantidade = itemConsultado.QuantidadeConsultada,
                                    ValorUnitario = itemConsultado.ValorUnitario,
                                    CustoUnitario = itemConsultado.CustoUnitario,
                                    RepasseAgencia = ValorRepasseAgencia,
                                };

                                SalvarFaturamentoItem(faturamentoItem);
                                //_context.FaturamentoItems.Add(faturamentoItem);
                                //_context.SaveChanges();

                                ValorTotal += (itemConsultado.ValorUnitario * itemConsultado.QuantidadeConsultada);
                                CustoTotal += (itemConsultado.CustoUnitario * itemConsultado.QuantidadeConsultada);
                            }
                        }

                        //=====================================//
                        //              MENSALIDADE            //
                        //=====================================//
                        var itensMensalidade = ItensMensalidade(contrato.TabelaId).ToList();

                        foreach (var itemMensalidade in itensMensalidade)
                        {
                            FaturamentoItem faturamentoItem;

                            //Considera a Data Inicial do Contrato ao Invés a Data de Cadastro no Sistema.
                            if (contrato.DataInicial > dataInicialFaturamento)
                            {
                                if (contrato.DataCancelamento.HasValue)
                                {
                                    contrato.DataCancelamento = DateTime.Parse(contrato.DataCancelamento.Value.ToString("dd/MM/yyyy"));

                                    if (contrato.DataCancelamento <= dataFinalFaturamento)
                                    {
                                        DiasFaturados = contrato.DataCancelamento.Value.AddDays(1).Subtract(contrato.DataInicial).Days;
                                    }
                                    else
                                    {
                                        DiasFaturados = dataFinalFaturamento.AddDays(1).Subtract(contrato.DataInicial).Days;
                                    }
                                }
                                else
                                {
                                    DiasFaturados = dataFinalFaturamento.AddDays(1).Subtract(contrato.DataInicial).Days;
                                }

                                isFaturaProporcional = true;
                            }
                            else
                            {
                                if (contrato.DataCancelamento.HasValue)
                                {
                                    contrato.DataCancelamento = DateTime.Parse(contrato.DataCancelamento.Value.ToString("dd/MM/yyyy"));

                                    if (contrato.DataCancelamento.Value <= dataFinalFaturamento)
                                    {
                                        DiasFaturados = contrato.DataCancelamento.Value.AddDays(1).Subtract(dataInicialFaturamento).Days;
                                        isFaturaProporcional = true;
                                    }
                                    else
                                    {
                                        DiasFaturados = dataFinalFaturamento.AddDays(1).Subtract(contrato.DataInicial).Days;
                                    }
                                }
                                else
                                {
                                    DiasFaturados = dataFinalFaturamento.AddDays(1).Subtract(contrato.DataInicial).Days;
                                }
                            }

                            //É aniversário de cadastro do Cliente?
                            if (contrato.IsAniversario)
                            {
                                int AnosDeUso = DiasFaturados / 365;
                                itemMensalidade.ValorUnitario = decimal.Parse((double.Parse(itemMensalidade.ValorUnitario.ToString()) * Math.Pow(1.1, double.Parse(AnosDeUso.ToString()))).ToString());
                            }

                            //É fatura consumo?
                            //Sendo fatura consumo não gravo no banco de dados um item mensalidade.
                            if (isFaturaConsumo)
                            {
                                //Taxa Extra?
                                if (contrato.IsTaxaExtra)
                                {
                                    //Taxa extra somente em dezembro.
                                    if (dataVencimentoFatura.Month.Equals(12) && DiasFaturados >= 60)
                                    {
                                        ValorTotalConsumo += itemMensalidade.ValorUnitario * 2;
                                        CustoTotalConsumo = itemMensalidade.CustoUnitario;
                                    }
                                    else
                                    {
                                        ValorTotalConsumo += itemMensalidade.ValorUnitario;
                                        CustoTotalConsumo = itemMensalidade.CustoUnitario;
                                    }
                                }
                                else
                                {
                                    ValorTotalConsumo += itemMensalidade.ValorUnitario;
                                    CustoTotalConsumo = itemMensalidade.CustoUnitario;
                                }

                            } //Não é fatura consumo
                            else
                            {
                                //Taxa Extra?
                                if (contrato.IsTaxaExtra)
                                {
                                    //Quando é fatura proporcional nunca entra aqui.
                                    if (dataVencimentoFatura.Month.Equals(12) && DiasFaturados >= 60)
                                    {

                                        if (contrato.Cliente.Agencia.IsComissionamento)
                                        {
                                            ValorRepasseAgencia = (itemMensalidade.ValorUnitario * 1) * percentualAgencia;
                                        }
                                        else
                                        {
                                            ValorRepasseAgencia = ((itemMensalidade.ValorUnitario * 1) - (0 * 1)) * percentualAgencia;
                                        }


                                        ValorRepasseAgenciaTotal += ValorRepasseAgencia;

                                        //Gravo a Taxa Extra
                                        faturamentoItem = new FaturamentoItem
                                        {
                                            FaturamentoId = faturamento.FaturamentoId,
                                            ItemFaturavelId = TaxaExtra(),
                                            Quantidade = itemMensalidade.QuantidadeConsultada,
                                            ValorUnitario = itemMensalidade.ValorUnitario,
                                        };

                                        _context.FaturamentoItems.Add(faturamentoItem);
                                        _context.SaveChanges();

                                        //CONSERTADO!
                                        ValorTotal += itemMensalidade.ValorUnitario;
                                        CustoTotal += itemMensalidade.CustoUnitario;
                                    }
                                }

                                if (isFaturaProporcional)
                                {
                                    int diasMesFaturamento = dataFinalFaturamento.AddDays(1).Subtract(dataInicialFaturamento).Days;
                                    itemMensalidade.ValorUnitario = (((itemMensalidade.ValorUnitario - itemMensalidade.CustoUnitario) / diasMesFaturamento) * DiasFaturados) + itemMensalidade.CustoUnitario;
                                }

                                //???
                                itemMensalidade.CustoUnitario = itemMensalidade.CustoUnitario + CustoPacote;

                                if (contrato.Cliente.Agencia.IsComissionamento)
                                {
                                    ValorRepasseAgencia = (itemMensalidade.ValorUnitario * 1) * percentualAgencia;
                                }
                                else
                                {
                                    ValorRepasseAgencia = ((itemMensalidade.ValorUnitario * 1) - (itemMensalidade.CustoUnitario * 1)) * percentualAgencia;
                                }

                                ValorRepasseAgenciaTotal += ValorRepasseAgencia;

                                //Gravo o Item Mensalidade
                                faturamentoItem = new FaturamentoItem
                                {
                                    FaturamentoId = faturamento.FaturamentoId,
                                    ItemFaturavelId = itemMensalidade.ItemFaturavelId,
                                    Quantidade = itemMensalidade.QuantidadeConsultada,
                                    ValorUnitario = itemMensalidade.ValorUnitario,
                                    RepasseAgencia = ValorRepasseAgencia,
                                    CustoUnitario = itemMensalidade.CustoUnitario,
                                };

                                SalvarFaturamentoItem(faturamentoItem);
                                //_context.FaturamentoItems.Add(faturamentoItem);
                                //_context.SaveChanges();


                                ValorTotal += itemMensalidade.ValorUnitario;
                                CustoTotal += itemMensalidade.CustoUnitario;
                            }
                        }

                        //=====================================//
                        //           CONSUMO MINIMO            //
                        //=====================================//
                        if (isFaturaConsumo)
                        {
                            FaturamentoItem faturamentoItem;

                            if (ValorTotalConsumo > ValorTotal)
                            {
                                decimal ValorDiferencaConsumo = ValorTotalConsumo - ValorTotal;

                                if (contrato.Cliente.Agencia.IsComissionamento)
                                {
                                    ValorRepasseAgencia = (ValorDiferencaConsumo * 1) * percentualAgencia;
                                }
                                else
                                {
                                    ValorRepasseAgencia = ((ValorDiferencaConsumo * 1) - (CustoTotalConsumo * 1)) * percentualAgencia;
                                }

                                ValorRepasseAgenciaTotal += ValorRepasseAgencia;

                                faturamentoItem = new FaturamentoItem
                                {
                                    FaturamentoId = faturamento.FaturamentoId,
                                    ItemFaturavelId = Consumo(),
                                    Quantidade = 1,
                                    ValorUnitario = ValorDiferencaConsumo,
                                    CustoUnitario = CustoTotalConsumo,
                                    RepasseAgencia = ValorRepasseAgencia,
                                };

                                SalvarFaturamentoItem(faturamentoItem);
                                //_context.FaturamentoItems.Add(faturamentoItem);
                                //_context.SaveChanges();

                                ValorTotal = ValorTotalConsumo;
                                CustoTotal += CustoTotalConsumo;
                            }
                            else
                            {
                                //Consumo zerado cobro pelo menos a manutenção do agente.
                                if (contrato.Cliente.Agencia.IsComissionamento)
                                {
                                    ValorRepasseAgencia = (0 * 1) * percentualAgencia;
                                }
                                else
                                {
                                    ValorRepasseAgencia = ((0 * 1) - (CustoTotalConsumo * 1)) * percentualAgencia;
                                }

                                ValorRepasseAgenciaTotal += ValorRepasseAgencia;


                                faturamentoItem = new FaturamentoItem
                                {
                                    FaturamentoId = faturamento.FaturamentoId,
                                    ItemFaturavelId = Consumo(),
                                    Quantidade = 1,
                                    ValorUnitario = 0,
                                    CustoUnitario = CustoTotalConsumo,
                                    RepasseAgencia = 0,
                                };

                                SalvarFaturamentoItem(faturamentoItem);
                                //_context.FaturamentoItems.Add(faturamentoItem);
                                //_context.SaveChanges();

                                ValorTotal += 0;
                                CustoTotal += CustoTotalConsumo;
                            }
                        }

                        if (ValorTotal != 0 && CustoTotal != 0)
                        {
                            faturamento.ValorOriginal = ValorTotal;
                            faturamento.ValorCusto = CustoTotal;
                            faturamento.ValorRepasseAgencia = ValorRepasseAgenciaTotal;

                            AtualizarFaturamento(faturamento);
                        }
                        else
                        {
                            DeletarFaturamento(faturamento.FaturamentoId);
                            //_context.Faturamentos.Remove(faturamento);
                        }

                        //db.SaveChanges();

                    }

                    //ts.Complete();
                    _notificacao.Success(string.Format("Faturamento gerado com sucesso.<br/> Consultas processadas: {0}", QuantidadeProcessada));
                }

            }
            catch (Exception ex)
            {
                _notificacao.Error(ex.Message);
            }


            return View();
        }

        #endregion

        #region Faturamento Por Agência

        public IActionResult GerarPorAgencia()
        {
            return View();
        }

        //[HttpPost]
        //public IActionResult GerarPorAgencia(GerarPorAgenciaViewModel vm)
        //{
        //    int QuantidadeProcessada = 0;
        //    int agenciaId = vm.AgenciaId;
        //    DateTime dataInicialFaturamento = vm.DataInicial;
        //    DateTime dataFinalFaturamento = vm.DataFinal;

        //    DateTime dataVencimentoFatura;
        //    int DiasFaturados = 0;
        //    int ConsultaExcendentePacoteId = ConsultaExcedentePacote();

        //    int debugCliente = 0;

        //    try
        //    {
        //        using (var db = new ContextoDados())
        //        {
        //            DeletarFaturamentoAntigoAgencia(dataInicialFaturamento, dataFinalFaturamento, agenciaId);

        //            //Validar Isso!
        //            var contratos = db.Contratos.AsNoTracking()
        //                               .Where(c => !c.DataCancelamento.HasValue || c.DataCancelamento >= dataInicialFaturamento)
        //                               .Where(c => c.Cliente.AgenciaId == agenciaId || c.ClienteId == agenciaId)
        //                               .Where(c => c.DataInicial <= dataFinalFaturamento)
        //                               .Where(c => c.Cliente.TipoCliente == eTipoCliente.Cliente || c.Cliente.TipoCliente == eTipoCliente.Agencia)
        //                               .OrderBy(c => c.ClienteId).OrderBy(c => c.ContratoId);

        //            foreach (var contrato in contratos)
        //            {
        //                decimal PercentualAgencia = 0M; // contrato.Cliente.Agencia.ComissaoAgente.HasValue ? (contrato.Cliente.Agencia.ComissaoAgente.Value / 100) : 0.0M; //0.5M;

        //                bool isFaturaConsumo = contrato.Tabela.IsConsumo;
        //                bool isFaturaProporcional = false;
        //                bool isPacote = false;

        //                decimal ValorTotal = 0;
        //                decimal CustoTotal = 0;
        //                decimal CustoPacote = 0;

        //                decimal ValorTotalConsumo = 0;
        //                decimal CustoTotalConsumo = 0;

        //                decimal ValorRepasseAgencia = 0;
        //                decimal ValorRepasseAgenciaTotal = 0;

        //                debugCliente = contrato.ClienteId;

        //                //Debug
        //                if (contrato.ClienteId.Equals(386))
        //                {
        //                    //Somente Debug
        //                }

        //                //==============================//
        //                // DATA DE VENCIMENTO DA FATURA //
        //                //==============================//
        //                if (vm.DataVencimento.HasValue)
        //                {
        //                    dataVencimentoFatura = vm.DataVencimento.Value;
        //                }
        //                else
        //                {
        //                    dataVencimentoFatura = DateTime.Parse(string.Format("{0}/{1}/{2}", contrato.DiaCobranca.Dia, DateTime.Now.Month, DateTime.Now.Year));
        //                }

        //                //==============================//
        //                //   FATURAMENTO DO CLIENTE     //
        //                //==============================//

        //                var faturamento = new Faturamento
        //                {
        //                    ClienteId = contrato.ClienteId,
        //                    DataEmissao = DateTime.Now,
        //                    DataVencimento = dataVencimentoFatura,
        //                    ValorCusto = 0,
        //                    ValorOriginal = 0,
        //                    ValorRepasseAgencia = 0,
        //                    //TipoPagamento = eTipoPagamento.Nenhum,
        //                    DataInicialFaturamento = dataInicialFaturamento,
        //                    DataFinalFaturamento = dataFinalFaturamento,
        //                };

        //                SalvarFaturamento(faturamento);
        //                //_context.Faturamentos.Add(faturamento);
        //                //_context.SaveChanges();

        //                //=====================================//
        //                //         CONSULTAS REALIZADAS        //
        //                //=====================================//
        //                var itensConsultados = ItensConsultados(contrato.TabelaId, contrato.ClienteId, dataInicialFaturamento, dataFinalFaturamento);

        //                foreach (var itemConsultado in itensConsultados)
        //                {
        //                    //MUDANÇA PARA REVENDA
        //                    //if (contrato.Cliente.Agencia.IsRevenda || contrato.Cliente.IsRevenda)
        //                    //{
        //                    //    //itemConsultado.CustoUnitario = itemConsultado.CustoRevenda;
        //                    //}

        //                    QuantidadeProcessada += itemConsultado.QuantidadeConsultada;

        //                    //Verifica se o valor do item é menor que o custo, considero o custo neste caso.
        //                    if (itemConsultado.CustoUnitario > itemConsultado.ValorUnitario)
        //                        itemConsultado.ValorUnitario = itemConsultado.CustoUnitario;

        //                    //Possui pacote?
        //                    //Para fatura consumo não existe pacote.
        //                    if (itemConsultado.QuantidadePacote > 0 && !isFaturaConsumo)
        //                    {
        //                        /*if (isFaturaProporcional)
        //                            itemConsultado.QuantidadePacote =  ((itemConsultado.QuantidadePacote / 30) * DiasFaturados);*/

        //                        if (itemConsultado.QuantidadePacote > 0)
        //                        {
        //                            if (itemConsultado.QuantidadePacote > itemConsultado.QuantidadeConsultada)
        //                                itemConsultado.QuantidadePacote = itemConsultado.QuantidadeConsultada;

        //                            itemConsultado.QuantidadeConsultada = itemConsultado.QuantidadeConsultada - itemConsultado.QuantidadePacote;


        //                            if (!isPacote)
        //                            {
        //                                var faturamentoItem = new FaturamentoItem
        //                                {
        //                                    FaturamentoId = faturamento.FaturamentoId,
        //                                    ItemFaturavelId = ConsultaExcendentePacoteId,
        //                                    Quantidade = itemConsultado.QuantidadePacote,
        //                                    CustoUnitario = 0,
        //                                    ValorUnitario = 0,
        //                                    RepasseAgencia = 0
        //                                };


        //                                SalvarFaturamentoItem(faturamentoItem);
        //                            }
        //                            else
        //                            {
        //                                db.Database.ExecuteSqlCommand("UPDATE FaturamentoItem SET Quantidade = Quantidade + @p0 WHERE FaturamentoId = @p1 AND ItemFaturavelId = @p2",
        //                                    new SqlParameter("p0", itemConsultado.QuantidadePacote),
        //                                    new SqlParameter("p1", faturamento.FaturamentoId),
        //                                    new SqlParameter("p2", ConsultaExcendentePacoteId));
        //                            }

        //                            //Custo do Pacote na Mensalidade
        //                            CustoPacote += (itemConsultado.CustoUnitario * itemConsultado.QuantidadePacote);

        //                            //Teste
        //                            //ValorTotal += (itemConsultado.ValorUnitario * itemConsultado.QuantidadePacote);

        //                            isPacote = true;
        //                        }
        //                    }


        //                    if (itemConsultado.QuantidadeConsultada > 0)
        //                    {
        //                        ValorRepasseAgencia = ((itemConsultado.ValorUnitario * itemConsultado.QuantidadeConsultada) - (itemConsultado.CustoUnitario * itemConsultado.QuantidadeConsultada)) * PercentualAgencia;
        //                        ValorRepasseAgenciaTotal += ValorRepasseAgencia;

        //                        var faturamentoItem = new FaturamentoItem
        //                        {
        //                            FaturamentoId = faturamento.FaturamentoId,
        //                            ItemFaturavelId = itemConsultado.ItemFaturavelId,
        //                            Quantidade = itemConsultado.QuantidadeConsultada,
        //                            ValorUnitario = itemConsultado.ValorUnitario,
        //                            CustoUnitario = itemConsultado.CustoUnitario,
        //                            RepasseAgencia = ValorRepasseAgencia,
        //                        };

        //                        SalvarFaturamentoItem(faturamentoItem);

        //                        ValorTotal += (itemConsultado.ValorUnitario * itemConsultado.QuantidadeConsultada);
        //                        CustoTotal += (itemConsultado.CustoUnitario * itemConsultado.QuantidadeConsultada);
        //                    }

        //                }

        //                //=====================================//
        //                //              MENSALIDADE            //
        //                //=====================================//
        //                var itensMensalidade = ItensMensalidade(contrato.TabelaId).ToList();

        //                foreach (var itemMensalidade in itensMensalidade)
        //                {
        //                    FaturamentoItem faturamentoItem;

        //                    //Considera a Data Inicial do Contrato ao Invés a Data de Cadastro no Sistema.
        //                    if (contrato.DataInicial > dataInicialFaturamento)
        //                    {
        //                        if (contrato.DataCancelamento.HasValue)
        //                        {
        //                            contrato.DataCancelamento = DateTime.Parse(contrato.DataCancelamento.Value.ToString("dd/MM/yyyy"));

        //                            if (contrato.DataCancelamento <= dataFinalFaturamento)
        //                            {
        //                                DiasFaturados = contrato.DataCancelamento.Value.AddDays(1).Subtract(contrato.DataInicial).Days;
        //                            }
        //                            else
        //                            {
        //                                DiasFaturados = dataFinalFaturamento.AddDays(1).Subtract(contrato.DataInicial).Days;
        //                            }
        //                        }
        //                        else
        //                        {
        //                            DiasFaturados = dataFinalFaturamento.AddDays(1).Subtract(contrato.DataInicial).Days;
        //                        }

        //                        isFaturaProporcional = true;
        //                    }
        //                    else
        //                    {
        //                        if (contrato.DataCancelamento.HasValue)
        //                        {
        //                            contrato.DataCancelamento = DateTime.Parse(contrato.DataCancelamento.Value.ToString("dd/MM/yyyy"));

        //                            if (contrato.DataCancelamento.Value <= dataFinalFaturamento)
        //                            {
        //                                DiasFaturados = contrato.DataCancelamento.Value.AddDays(1).Subtract(dataInicialFaturamento).Days;
        //                                isFaturaProporcional = true;
        //                            }
        //                            else
        //                            {
        //                                DiasFaturados = dataFinalFaturamento.AddDays(1).Subtract(contrato.DataInicial).Days;
        //                            }
        //                        }
        //                        else
        //                        {
        //                            DiasFaturados = dataFinalFaturamento.AddDays(1).Subtract(contrato.DataInicial).Days;
        //                        }
        //                    }

        //                    //É aniversário de cadastro do Cliente?
        //                    if (contrato.IsAniversario)
        //                    {
        //                        int AnosDeUso = DiasFaturados / 365;
        //                        itemMensalidade.ValorUnitario = decimal.Parse((double.Parse(itemMensalidade.ValorUnitario.ToString()) * Math.Pow(1.1, double.Parse(AnosDeUso.ToString()))).ToString());
        //                    }

        //                    //É fatura consumo?
        //                    //Sendo fatura consumo não gravo no banco de dados um item mensalidade.
        //                    if (isFaturaConsumo)
        //                    {
        //                        //Taxa Extra?
        //                        if (contrato.IsTaxaExtra)
        //                        {
        //                            //Taxa extra somente em dezembro.
        //                            if (dataVencimentoFatura.Month.Equals(12) && DiasFaturados >= 60)
        //                            {
        //                                ValorTotalConsumo += itemMensalidade.ValorUnitario * 2;
        //                                CustoTotalConsumo = itemMensalidade.CustoUnitario;
        //                            }
        //                            else
        //                            {
        //                                ValorTotalConsumo += itemMensalidade.ValorUnitario;
        //                                CustoTotalConsumo = itemMensalidade.CustoUnitario;
        //                            }
        //                        }
        //                        else
        //                        {
        //                            ValorTotalConsumo += itemMensalidade.ValorUnitario;
        //                            CustoTotalConsumo = itemMensalidade.CustoUnitario;
        //                        }

        //                    } //Não é fatura consumo
        //                    else
        //                    {
        //                        //Taxa Extra?
        //                        if (contrato.IsTaxaExtra)
        //                        {
        //                            //Quando é fatura proporcional nunca entra aqui.
        //                            if (dataVencimentoFatura.Month.Equals(12) && DiasFaturados >= 60)
        //                            {

        //                                ValorRepasseAgencia = ((itemMensalidade.ValorUnitario * 1) - (0 * 1)) * PercentualAgencia;
        //                                ValorRepasseAgenciaTotal += ValorRepasseAgencia;

        //                                //Gravo a Taxa Extra
        //                                faturamentoItem = new FaturamentoItem
        //                                {
        //                                    FaturamentoId = faturamento.FaturamentoId,
        //                                    ItemFaturavelId = TaxaExtra(),
        //                                    Quantidade = itemMensalidade.QuantidadeConsultada,
        //                                    ValorUnitario = itemMensalidade.ValorUnitario,
        //                                };

        //                                _context.FaturamentoItems.Add(faturamentoItem);
        //                                _context.SaveChanges();

        //                                //CONSERTADO!
        //                                ValorTotal += itemMensalidade.ValorUnitario;
        //                                CustoTotal += itemMensalidade.CustoUnitario;
        //                            }
        //                        }

        //                        if (isFaturaProporcional)
        //                        {
        //                            int diasMesFaturamento = dataFinalFaturamento.AddDays(1).Subtract(dataInicialFaturamento).Days;
        //                            itemMensalidade.ValorUnitario = (((itemMensalidade.ValorUnitario - itemMensalidade.CustoUnitario) / diasMesFaturamento) * DiasFaturados) + itemMensalidade.CustoUnitario;
        //                        }

        //                        //???
        //                        itemMensalidade.CustoUnitario = itemMensalidade.CustoUnitario + CustoPacote;

        //                        ValorRepasseAgencia = ((itemMensalidade.ValorUnitario * 1) - (itemMensalidade.CustoUnitario * 1)) * PercentualAgencia;
        //                        ValorRepasseAgenciaTotal += ValorRepasseAgencia;

        //                        //Gravo o Item Mensalidade
        //                        faturamentoItem = new FaturamentoItem
        //                        {
        //                            FaturamentoId = faturamento.FaturamentoId,
        //                            ItemFaturavelId = itemMensalidade.ItemFaturavelId,
        //                            Quantidade = itemMensalidade.QuantidadeConsultada,
        //                            ValorUnitario = itemMensalidade.ValorUnitario,
        //                            RepasseAgencia = ValorRepasseAgencia,
        //                            CustoUnitario = itemMensalidade.CustoUnitario,
        //                        };

        //                        SalvarFaturamentoItem(faturamentoItem);

        //                        ValorTotal += itemMensalidade.ValorUnitario;
        //                        CustoTotal += itemMensalidade.CustoUnitario;
        //                    }
        //                }

        //                //=====================================//
        //                //           CONSUMO MINIMO            //
        //                //=====================================//
        //                if (isFaturaConsumo)
        //                {
        //                    FaturamentoItem faturamentoItem;

        //                    if (ValorTotalConsumo > ValorTotal)
        //                    {
        //                        decimal ValorDiferencaConsumo = ValorTotalConsumo - ValorTotal;

        //                        ValorRepasseAgencia = ((ValorDiferencaConsumo * 1) - (CustoTotalConsumo * 1)) * PercentualAgencia;
        //                        ValorRepasseAgenciaTotal += ValorRepasseAgencia;

        //                        faturamentoItem = new FaturamentoItem
        //                        {
        //                            FaturamentoId = faturamento.FaturamentoId,
        //                            ItemFaturavelId = Consumo(),
        //                            Quantidade = 1,
        //                            ValorUnitario = ValorDiferencaConsumo,
        //                            CustoUnitario = CustoTotalConsumo,
        //                            RepasseAgencia = ValorRepasseAgencia,
        //                        };

        //                        SalvarFaturamentoItem(faturamentoItem);

        //                        ValorTotal = ValorTotalConsumo;
        //                        CustoTotal += CustoTotalConsumo;
        //                    }
        //                    else
        //                    {
        //                        //Consumo zerado cobro pelo menos a manutenção do agente.

        //                        ValorRepasseAgencia = ((0 * 1) - (CustoTotalConsumo * 1)) * PercentualAgencia;
        //                        ValorRepasseAgenciaTotal += ValorRepasseAgencia;


        //                        faturamentoItem = new FaturamentoItem
        //                        {
        //                            FaturamentoId = faturamento.FaturamentoId,
        //                            ItemFaturavelId = Consumo(),
        //                            Quantidade = 1,
        //                            ValorUnitario = 0,
        //                            CustoUnitario = CustoTotalConsumo,
        //                            RepasseAgencia = 0,
        //                        };

        //                        SalvarFaturamentoItem(faturamentoItem);

        //                        ValorTotal += 0;
        //                        CustoTotal += CustoTotalConsumo;
        //                    }
        //                }

        //                if (ValorTotal != 0 && CustoTotal != 0)
        //                {
        //                    faturamento.ValorOriginal = ValorTotal;
        //                    faturamento.ValorCusto = CustoTotal;
        //                    faturamento.ValorRepasseAgencia = ValorRepasseAgenciaTotal;

        //                    AtualizarFaturamento(faturamento);
        //                }
        //                else
        //                {
        //                    DeletarFaturamento(faturamento.FaturamentoId);
        //                }

        //            }

        //            TempData["Alertas"] = Alertas.Sucesso(string.Format("Faturamento gerado com sucesso.<br/> Consultas processadas: {0}", QuantidadeProcessada));
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        //ts.Dispose();
        //        TempData["Alertas"] = Alertas.Atencao(ex.Message);
        //    }


        //    return View();
        //}


        #endregion

        #region Faturamento Por Cliente

        public IActionResult GerarPorCliente()
        {
            return View();
        }

        [HttpPost]
        public IActionResult GerarPorCliente(FaturamentoPorClienteViewModel viewModel)
        {
            int QuantidadeProcessada = 0;
            int clienteId = viewModel.ClienteId;
            DateTime dataInicialFaturamento = viewModel.DataInicial;
            DateTime dataFinalFaturamento = viewModel.DataFinal;

            DateTime dataVencimentoFatura;
            int DiasFaturados = 0;
            int ConsultaExcendentePacoteId = ConsultaExcedentePacote();

            int debugCliente = 0;

            try
            {
                using (var db = new ContextoDados())
                {

                    DeletarFaturamentoAntigo(dataInicialFaturamento, dataFinalFaturamento, clienteId);

                    //Validar Isso!
                    var contratos = db.Contratos.AsNoTracking().Where(c => !c.DataCancelamento.HasValue
                                       || c.DataCancelamento >= dataInicialFaturamento)
                                       .Where(c => c.ClienteId == clienteId).Where(c => c.DataInicial <= dataFinalFaturamento)
                                       .Where(c => c.Cliente.TipoCliente == eTipoCliente.Cliente
                                       || c.Cliente.TipoCliente == eTipoCliente.Agencia)
                                       .OrderBy(c => c.ClienteId).OrderBy(c => c.ContratoId);

                    foreach (var contrato in contratos)
                    {

                        decimal PercentualAgencia = 0M; // contrato.Cliente.Agencia.ComissaoAgente.HasValue ? (contrato.Cliente.Agencia.ComissaoAgente.Value / 100) : 0.0M; //0.5M;

                        bool isFaturaConsumo = contrato.Tabela.IsConsumo;
                        bool isFaturaProporcional = false;
                        bool isPacote = false;

                        decimal ValorTotal = 0;
                        decimal CustoTotal = 0;
                        decimal CustoPacote = 0;

                        decimal ValorTotalConsumo = 0;
                        decimal CustoTotalConsumo = 0;

                        decimal ValorRepasseAgencia = 0;
                        decimal ValorRepasseAgenciaTotal = 0;

                        debugCliente = contrato.ClienteId;

                        //Debug
                        if (contrato.ClienteId.Equals(386))
                        {
                            //Somente Debug
                        }

                        //==============================//
                        // DATA DE VENCIMENTO DA FATURA //
                        //==============================//
                        if (viewModel.DataVencimento.HasValue)
                        {
                            dataVencimentoFatura = viewModel.DataVencimento.Value;
                        }
                        else
                        {
                            dataVencimentoFatura = DateTime.Parse(string.Format("{0}/{1}/{2}", contrato.DiaCobranca.Dia, DateTime.Now.Month, DateTime.Now.Year));
                        }

                        //==============================//
                        //   FATURAMENTO DO CLIENTE     //
                        //==============================//

                        var faturamento = new Faturamento
                        {
                            ClienteId = contrato.ClienteId,
                            DataEmissao = DateTime.Now,
                            DataVencimento = dataVencimentoFatura,
                            ValorCusto = 0,
                            ValorOriginal = 0,
                            ValorRepasseAgencia = 0,
                            //TipoPagamento = eTipoPagamento.Nenhum,
                            DataInicialFaturamento = dataInicialFaturamento,
                            DataFinalFaturamento = dataFinalFaturamento,
                        };

                        SalvarFaturamento(faturamento);
                        //_context.Faturamentos.Add(faturamento);
                        //_context.SaveChanges();

                        //=====================================//
                        //         CONSULTAS REALIZADAS        //
                        //=====================================//
                        var itensConsultados = ItensConsultados(contrato.TabelaId, contrato.ClienteId, dataInicialFaturamento, dataFinalFaturamento);

                        foreach (var itemConsultado in itensConsultados)
                        {
                            //MUDANÇA PARA REVENDA
                            //if (contrato.Cliente.Agencia.IsRevenda || contrato.Cliente.IsRevenda)
                            //{
                            //    //itemConsultado.CustoUnitario = itemConsultado.CustoRevenda;
                            //}

                            QuantidadeProcessada += itemConsultado.QuantidadeConsultada;

                            //Verifica se o valor do item é menor que o custo, considero o custo neste caso.
                            if (itemConsultado.CustoUnitario > itemConsultado.ValorUnitario)
                                itemConsultado.ValorUnitario = itemConsultado.CustoUnitario;

                            //Possui pacote?
                            //Para fatura consumo não existe pacote.
                            if (itemConsultado.QuantidadePacote > 0 && !isFaturaConsumo)
                            {
                                /*if (isFaturaProporcional)
                                    itemConsultado.QuantidadePacote =  ((itemConsultado.QuantidadePacote / 30) * DiasFaturados);*/

                                if (itemConsultado.QuantidadePacote > 0)
                                {
                                    if (itemConsultado.QuantidadePacote > itemConsultado.QuantidadeConsultada)
                                        itemConsultado.QuantidadePacote = itemConsultado.QuantidadeConsultada;

                                    itemConsultado.QuantidadeConsultada = itemConsultado.QuantidadeConsultada - itemConsultado.QuantidadePacote;


                                    if (!isPacote)
                                    {
                                        var faturamentoItem = new FaturamentoItem
                                        {
                                            FaturamentoId = faturamento.FaturamentoId,
                                            ItemFaturavelId = ConsultaExcendentePacoteId,
                                            Quantidade = itemConsultado.QuantidadePacote,
                                            CustoUnitario = 0,
                                            ValorUnitario = 0,
                                            RepasseAgencia = 0
                                        };


                                        SalvarFaturamentoItem(faturamentoItem);
                                    }
                                    else
                                    {
                                        //TODO: Corrigir
                                        //db.Database.ExecuteSqlCommand("UPDATE FaturamentoItem SET Quantidade = Quantidade + @p0 WHERE FaturamentoId = @p1 AND ItemFaturavelId = @p2",
                                        //    new SqlParameter("p0", itemConsultado.QuantidadePacote),
                                        //    new SqlParameter("p1", faturamento.FaturamentoId),
                                        //    new SqlParameter("p2", ConsultaExcendentePacoteId));
                                    }

                                    //Custo do Pacote na Mensalidade
                                    CustoPacote += (itemConsultado.CustoUnitario * itemConsultado.QuantidadePacote);

                                    //Teste
                                    //ValorTotal += (itemConsultado.ValorUnitario * itemConsultado.QuantidadePacote);

                                    isPacote = true;
                                }
                            }


                            if (itemConsultado.QuantidadeConsultada > 0)
                            {
                                ValorRepasseAgencia = ((itemConsultado.ValorUnitario * itemConsultado.QuantidadeConsultada) - (itemConsultado.CustoUnitario * itemConsultado.QuantidadeConsultada)) * PercentualAgencia;
                                ValorRepasseAgenciaTotal += ValorRepasseAgencia;

                                var faturamentoItem = new FaturamentoItem
                                {
                                    FaturamentoId = faturamento.FaturamentoId,
                                    ItemFaturavelId = itemConsultado.ItemFaturavelId,
                                    Quantidade = itemConsultado.QuantidadeConsultada,
                                    ValorUnitario = itemConsultado.ValorUnitario,
                                    CustoUnitario = itemConsultado.CustoUnitario,
                                    RepasseAgencia = ValorRepasseAgencia,
                                };

                                SalvarFaturamentoItem(faturamentoItem);

                                ValorTotal += (itemConsultado.ValorUnitario * itemConsultado.QuantidadeConsultada);
                                CustoTotal += (itemConsultado.CustoUnitario * itemConsultado.QuantidadeConsultada);
                            }

                        }

                        //=====================================//
                        //              MENSALIDADE            //
                        //=====================================//
                        var itensMensalidade = ItensMensalidade(contrato.TabelaId).ToList();

                        foreach (var itemMensalidade in itensMensalidade)
                        {
                            FaturamentoItem faturamentoItem;

                            //Considera a Data Inicial do Contrato ao Invés a Data de Cadastro no Sistema.
                            if (contrato.DataInicial > dataInicialFaturamento)
                            {
                                if (contrato.DataCancelamento.HasValue)
                                {
                                    contrato.DataCancelamento = DateTime.Parse(contrato.DataCancelamento.Value.ToString("dd/MM/yyyy"));

                                    if (contrato.DataCancelamento <= dataFinalFaturamento)
                                    {
                                        DiasFaturados = contrato.DataCancelamento.Value.AddDays(1).Subtract(contrato.DataInicial).Days;
                                    }
                                    else
                                    {
                                        DiasFaturados = dataFinalFaturamento.AddDays(1).Subtract(contrato.DataInicial).Days;
                                    }
                                }
                                else
                                {
                                    DiasFaturados = dataFinalFaturamento.AddDays(1).Subtract(contrato.DataInicial).Days;
                                }

                                isFaturaProporcional = true;
                            }
                            else
                            {
                                if (contrato.DataCancelamento.HasValue)
                                {
                                    contrato.DataCancelamento = DateTime.Parse(contrato.DataCancelamento.Value.ToString("dd/MM/yyyy"));

                                    if (contrato.DataCancelamento.Value <= dataFinalFaturamento)
                                    {
                                        DiasFaturados = contrato.DataCancelamento.Value.AddDays(1).Subtract(dataInicialFaturamento).Days;
                                        isFaturaProporcional = true;
                                    }
                                    else
                                    {
                                        DiasFaturados = dataFinalFaturamento.AddDays(1).Subtract(contrato.DataInicial).Days;
                                    }
                                }
                                else
                                {
                                    DiasFaturados = dataFinalFaturamento.AddDays(1).Subtract(contrato.DataInicial).Days;
                                }
                            }

                            //É aniversário de cadastro do Cliente?
                            if (contrato.IsAniversario)
                            {
                                int AnosDeUso = DiasFaturados / 365;
                                itemMensalidade.ValorUnitario = decimal.Parse((double.Parse(itemMensalidade.ValorUnitario.ToString()) * Math.Pow(1.1, double.Parse(AnosDeUso.ToString()))).ToString());
                            }

                            //É fatura consumo?
                            //Sendo fatura consumo não gravo no banco de dados um item mensalidade.
                            if (isFaturaConsumo)
                            {
                                //Taxa Extra?
                                if (contrato.IsTaxaExtra)
                                {
                                    //Taxa extra somente em dezembro.
                                    if (dataVencimentoFatura.Month.Equals(12) && DiasFaturados >= 60)
                                    {
                                        ValorTotalConsumo += itemMensalidade.ValorUnitario * 2;
                                        CustoTotalConsumo = itemMensalidade.CustoUnitario;
                                    }
                                    else
                                    {
                                        ValorTotalConsumo += itemMensalidade.ValorUnitario;
                                        CustoTotalConsumo = itemMensalidade.CustoUnitario;
                                    }
                                }
                                else
                                {
                                    ValorTotalConsumo += itemMensalidade.ValorUnitario;
                                    CustoTotalConsumo = itemMensalidade.CustoUnitario;
                                }

                            } //Não é fatura consumo
                            else
                            {
                                //Taxa Extra?
                                if (contrato.IsTaxaExtra)
                                {
                                    //Quando é fatura proporcional nunca entra aqui.
                                    if (dataVencimentoFatura.Month.Equals(12) && DiasFaturados >= 60)
                                    {

                                        ValorRepasseAgencia = ((itemMensalidade.ValorUnitario * 1) - (0 * 1)) * PercentualAgencia;
                                        ValorRepasseAgenciaTotal += ValorRepasseAgencia;

                                        //Gravo a Taxa Extra
                                        faturamentoItem = new FaturamentoItem
                                        {
                                            FaturamentoId = faturamento.FaturamentoId,
                                            ItemFaturavelId = TaxaExtra(),
                                            Quantidade = itemMensalidade.QuantidadeConsultada,
                                            ValorUnitario = itemMensalidade.ValorUnitario,
                                        };

                                        _context.FaturamentoItems.Add(faturamentoItem);
                                        _context.SaveChanges();

                                        //CONSERTADO!
                                        ValorTotal += itemMensalidade.ValorUnitario;
                                        CustoTotal += itemMensalidade.CustoUnitario;
                                    }
                                }

                                if (isFaturaProporcional)
                                {
                                    int diasMesFaturamento = dataFinalFaturamento.AddDays(1).Subtract(dataInicialFaturamento).Days;
                                    itemMensalidade.ValorUnitario = (((itemMensalidade.ValorUnitario - itemMensalidade.CustoUnitario) / diasMesFaturamento) * DiasFaturados) + itemMensalidade.CustoUnitario;
                                }

                                //???
                                itemMensalidade.CustoUnitario = itemMensalidade.CustoUnitario + CustoPacote;

                                ValorRepasseAgencia = ((itemMensalidade.ValorUnitario * 1) - (itemMensalidade.CustoUnitario * 1)) * PercentualAgencia;
                                ValorRepasseAgenciaTotal += ValorRepasseAgencia;

                                //Gravo o Item Mensalidade
                                faturamentoItem = new FaturamentoItem
                                {
                                    FaturamentoId = faturamento.FaturamentoId,
                                    ItemFaturavelId = itemMensalidade.ItemFaturavelId,
                                    Quantidade = itemMensalidade.QuantidadeConsultada,
                                    ValorUnitario = itemMensalidade.ValorUnitario,
                                    RepasseAgencia = ValorRepasseAgencia,
                                    CustoUnitario = itemMensalidade.CustoUnitario,
                                };

                                SalvarFaturamentoItem(faturamentoItem);

                                ValorTotal += itemMensalidade.ValorUnitario;
                                CustoTotal += itemMensalidade.CustoUnitario;
                            }
                        }

                        //=====================================//
                        //           CONSUMO MINIMO            //
                        //=====================================//
                        if (isFaturaConsumo)
                        {
                            FaturamentoItem faturamentoItem;

                            if (ValorTotalConsumo > ValorTotal)
                            {
                                decimal ValorDiferencaConsumo = ValorTotalConsumo - ValorTotal;

                                ValorRepasseAgencia = ((ValorDiferencaConsumo * 1) - (CustoTotalConsumo * 1)) * PercentualAgencia;
                                ValorRepasseAgenciaTotal += ValorRepasseAgencia;

                                faturamentoItem = new FaturamentoItem
                                {
                                    FaturamentoId = faturamento.FaturamentoId,
                                    ItemFaturavelId = Consumo(),
                                    Quantidade = 1,
                                    ValorUnitario = ValorDiferencaConsumo,
                                    CustoUnitario = CustoTotalConsumo,
                                    RepasseAgencia = ValorRepasseAgencia,
                                };

                                SalvarFaturamentoItem(faturamentoItem);

                                ValorTotal = ValorTotalConsumo;
                                CustoTotal += CustoTotalConsumo;
                            }
                            else
                            {
                                //Consumo zerado cobro pelo menos a manutenção do agente.

                                ValorRepasseAgencia = ((0 * 1) - (CustoTotalConsumo * 1)) * PercentualAgencia;
                                ValorRepasseAgenciaTotal += ValorRepasseAgencia;


                                faturamentoItem = new FaturamentoItem
                                {
                                    FaturamentoId = faturamento.FaturamentoId,
                                    ItemFaturavelId = Consumo(),
                                    Quantidade = 1,
                                    ValorUnitario = 0,
                                    CustoUnitario = CustoTotalConsumo,
                                    RepasseAgencia = 0,
                                };

                                SalvarFaturamentoItem(faturamentoItem);

                                ValorTotal += 0;
                                CustoTotal += CustoTotalConsumo;
                            }
                        }

                        if (ValorTotal != 0 && CustoTotal != 0)
                        {
                            faturamento.ValorOriginal = ValorTotal;
                            faturamento.ValorCusto = CustoTotal;
                            faturamento.ValorRepasseAgencia = ValorRepasseAgenciaTotal;

                            AtualizarFaturamento(faturamento);
                        }
                        else
                        {
                            DeletarFaturamento(faturamento.FaturamentoId);
                        }

                    }

                    _notificacao.Success(string.Format("Faturamento gerado com sucesso.<br/> Consultas processadas: {0}", QuantidadeProcessada));
                }

            }
            catch (Exception ex)
            {
                _notificacao.Error(ex.Message);
            }


            return View();
        }

        #endregion
    }


}
