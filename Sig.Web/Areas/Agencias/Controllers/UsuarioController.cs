﻿using Sig.Web.Models;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using AspNetCoreHero.ToastNotification.Abstractions;
using Sig.Web.Interfaces;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication.Cookies;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Sig.Web.ViewModels.Usuarios;
using System;

namespace Sig.Web.Areas.Agencias.Controllers
{
    [Authorize(Roles = "Administradora, Agencia")]
    public class UsuarioController : BaseController
    {
        private readonly IUsuarioLogado _usuarioLogado;

        public UsuarioController(
            INotyfService notificacao,
            IUsuarioLogado usuarioLogado) : base(notificacao)
        {
            _usuarioLogado = usuarioLogado;
        }

        #region Index

        public IActionResult Index()
        {
            var viewModel = new UsuarioViewModel
            {
                Usuarios = _context.Usuarios.Where(c => c.Colaborador.AgenciaId == _usuarioLogado.ObterClienteId()).ToList()
            };

            return View(viewModel);
        }

        [HttpPost]
        public IActionResult Index(UsuarioViewModel vm)
        {
            IQueryable<Usuario> usuarios = _context.Usuarios.Where(c => c.Colaborador.AgenciaId == _usuarioLogado.ObterClienteId());

            #region Filtros

            if (!vm.Inativos)
            {
                usuarios = usuarios.Where(c => c.IsAtivo == true);
            }

            if (!string.IsNullOrEmpty(vm.Usuario.Colaborador.Agencia.CPFCNPJ))
            {
                usuarios = usuarios.Where(c => c.Colaborador.Agencia.CPFCNPJ == vm.Usuario.Colaborador.Agencia.CPFCNPJ);
            }

            if (!string.IsNullOrEmpty(vm.Usuario.Colaborador.Agencia.Nome))
            {
                usuarios = usuarios.Where(c => c.Colaborador.Agencia.Nome.Contains(vm.Usuario.Colaborador.Agencia.Nome));
            }

            if (!string.IsNullOrEmpty(vm.Usuario.Colaborador.Agencia.RazaoSocial))
            {
                usuarios = usuarios.Where(c => c.Colaborador.Agencia.RazaoSocial.Contains(vm.Usuario.Colaborador.Agencia.RazaoSocial));
            }

            #endregion

            vm.Usuarios = usuarios;
            return View(vm);
        }

        #endregion

        #region Incluir

        public IActionResult Incluir()
        {
            #region ViewBags

            var hasUsuarios = from u in _context.Usuarios
                              select u.ColaboradorId;

            var colaboradores = (from c in _context.Colaboradores
                                 where !hasUsuarios.Contains(c.ColaboradorId) && c.AgenciaId == _usuarioLogado.ObterClienteId()
                                 select c).OrderBy(c => c.Nome);

            ViewBag.PermissaoId = new SelectList(_context.Permissoes.Where(c => c.ClienteId == _usuarioLogado.ObterClienteId())
                                                        .OrderBy(c => c.Descricao), "PermissaoId", "Descricao");

            ViewBag.Colaboradores = new SelectList(colaboradores, "ColaboradorId", "Nome");

            #endregion

            return View();
        }


        [HttpPost]
        public IActionResult Incluir(UsuarioViewModel vm)
        {
            if (ModelState.IsValid)
            {
                _context.Usuarios.Add(vm.Usuario);
                _context.SaveChanges();

                _notificacao.Success("Usuário criado com sucesso");

                return RedirectToAction("Index");
            }

            #region ViewBag

            var hasUsuarios = from u in _context.Usuarios
                              select u.ColaboradorId;

            var colaboradores = (from c in _context.Colaboradores
                                 where !hasUsuarios.Contains(c.ColaboradorId) && c.AgenciaId == _usuarioLogado.ObterClienteId()
                                 select c).OrderBy(c => c.Nome);

            ViewBag.PermissaoId = new SelectList(_context.Permissoes.Where(c => c.ClienteId == _usuarioLogado.ObterClienteId())
                                                        .OrderBy(c => c.Descricao), "PermissaoId", "Descricao");

            ViewBag.Colaboradores = new SelectList(colaboradores, "ColaboradorId", "Nome");

            #endregion

            return View();
        }

        #endregion

        #region Alterar

        public IActionResult Alterar(int id = 0)
        {
            Usuario usuario = null;

            if (_usuarioLogado.EhAdministradora())
                usuario = _context.Usuarios.SingleOrDefault(c => c.UsuarioId == id);
            else
                usuario = _context.Usuarios.SingleOrDefault(c => c.UsuarioId == id && c.Colaborador.AgenciaId == _usuarioLogado.ObterClienteId());


            if (usuario == null)
            {
                _notificacao.Warning("Usuário não pertence a esta agência");
                return RedirectToAction("Index");
            }

            #region ViewBag

            ViewBag.PermissaoId = new SelectList(_context.Permissoes.Where(c => c.ClienteId == _usuarioLogado.ObterAgenciaId()).OrderBy(c => c.Descricao), "PermissaoId", "Descricao");
            ViewBag.Colaboradores = new SelectList(_context.Colaboradores.Where(c => c.AgenciaId == _usuarioLogado.ObterClienteId()), "ColaboradorId", "Nome", usuario.ColaboradorId);

            #endregion

            var viewModel = new UsuarioViewModel()
            {
                Usuario = usuario
            };

            return View(viewModel);
        }

        [HttpPost]
        public IActionResult Alterar(UsuarioViewModel vm)
        {
            var usuario = _context.Usuarios.Find(vm.Usuario.UsuarioId);

            usuario.Senha = vm.Usuario.Senha;
            usuario.SenhaConfirmar = vm.Usuario.SenhaConfirmar;
            usuario.PermissaoId = vm.Usuario.PermissaoId;
            usuario.IsAtivo = vm.Usuario.IsAtivo;

            _context.SaveChanges();

            _notificacao.Success("Dados do usuário da administradora alterada");
            return RedirectToAction("Index");
        }

        #endregion

        #region Login

        [AllowAnonymous]
        public IActionResult Login() => View();

        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> Login(Usuario form)
        {
            var usuario = _context.Usuarios
                .FirstOrDefault(c => c.Colaborador.AgenciaId == form.Colaborador.AgenciaId
                    && c.Colaborador.Email == form.Colaborador.Email
                    && c.Senha == form.Senha);

            if (usuario != null)
            {
                var nome = usuario.Colaborador.Agencia.Nome;
                var colaboradorNome = usuario.Colaborador.Nome;
                var clienteId = usuario.Colaborador.Agencia.ClienteId;
                var agenciaId = usuario.Colaborador.Agencia.AgenciaId ?? 0;
                var usuarioId = usuario.UsuarioId;
                var grupo = usuario.Colaborador.Agencia.TipoCliente.ToString();
                var colaboradorId = usuario.ColaboradorId;
                var isSuperUsuario = usuario.IsSuperUsuario ? "true" : "false";
                var contratoId = 0;
                var pagina = "Agencias";

                var dados = $"{nome};{colaboradorNome};{clienteId};{agenciaId};{usuarioId};{colaboradorId};{isSuperUsuario};{contratoId};{pagina}";

                var claims = new List<Claim>
                    {
                        new Claim(ClaimTypes.Name, nome),
                        new Claim(ClaimTypes.Role, grupo),
                        new Claim("Dados", dados)
                    };

                var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);

                var authProperties = new AuthenticationProperties
                {
                    ExpiresUtc = DateTime.UtcNow.AddHours(8),
                    IsPersistent = true
                };

                //Loga de fato
                await HttpContext.SignInAsync(
                      CookieAuthenticationDefaults.AuthenticationScheme,
                      new ClaimsPrincipal(claimsIdentity),
                      authProperties);

                return RedirectToAction("Index", "Atendimento");
            }

            _notificacao.Error("Usuário e/ou senha inválidos");
            return View();
        }

        #endregion

        #region Logout

        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync();
            return RedirectToAction("Login");
        }

        #endregion

        #region Recuperar Senha

        [AllowAnonymous]
        public IActionResult RecuperarSenha() => View();


        //TODO: Corrigir
        //[AllowAnonymous]
        //[HttpPost]
        //public IActionResult RecuperarSenha(RecuperarSenhaViewModel vm)
        //{
        //    if (string.IsNullOrWhiteSpace(vm.CpfCnpj) && string.IsNullOrWhiteSpace(vm.Email))
        //        ModelState.AddModelError("CpfCnpj", "Informe um documento ou um email para poder continuar");

        //    if (ModelState.IsValid)
        //    {
        //        Usuario usuario;

        //        if (!string.IsNullOrWhiteSpace(vm.CpfCnpj))
        //        {
        //            vm.CpfCnpj = vm.CpfCnpj.Trim();
        //            usuario = _context.Usuarios.FirstOrDefault(c => c.Colaborador.Agencia.CPFCNPJ == vm.CpfCnpj && c.IsSuperUsuario);
        //        }
        //        else
        //        {
        //            usuario = _context.Usuarios.FirstOrDefault(c => c.Colaborador.Email == vm.Email);
        //        }

        //        if (usuario != null)
        //        {
        //            StringBuilder sb = new StringBuilder();

        //            sb.AppendLine("Reenvio de Senha (Agente).<br/><br/>");
        //            sb.AppendLine("<hr/>");
        //            sb.AppendLine("Anote os seguintes dados:<br/><br/>");
        //            sb.AppendFormat("Agência: <strong>{0}</strong><br/>", usuario.Colaborador.AgenciaId);
        //            sb.AppendFormat("Email: <strong>{0}</strong><br/>", usuario.Colaborador.Email);
        //            sb.AppendFormat("Senha: <strong>{0}</strong><br/>", usuario.Senha);
        //            sb.AppendLine("<hr/>");
        //            sb.AppendLine("Por favor não responda este email");

        //            //TODO: Corrigir
        //            //SendGridEmail.EnviarEmailRecuperarSenha(usuario.Colaborador.Email, sb.ToString());

        //            _notificacao.Success("Reenvio realizado.<br/>Um email foi disparado com seus dados de acesso");
        //        }
        //        else
        //        {
        //            _notificacao.Warning("Informações não encontradas");
        //        }
        //    }

        //    return View();
        //}

        #endregion

        #region Desativar

        public IActionResult Desativar(int id = 0)
        {
            Usuario usuario = null;

            if (_usuarioLogado.EhAdministradora())
                usuario = _context.Usuarios.Find(id);
            else
                usuario = _context.Usuarios.FirstOrDefault(c => c.UsuarioId == id && c.Colaborador.AgenciaId == _usuarioLogado.ObterClienteId());

            if (usuario != null)
            {
                if (!usuario.IsSuperUsuario)
                {
                    usuario.SenhaConfirmar = usuario.Senha;
                    usuario.IsAtivo = false;

                    _context.SaveChanges();

                    _notificacao.Success($"Usuário: {usuario.Colaborador.Nome} foi desativado");
                }
                else
                {
                    _notificacao.Warning("Você não pode desativar um Super Usuário do sistema");
                }
            }
            else
            {
                _notificacao.Warning("Usuário não encontrado");
            }

            return RedirectToAction("Index");
        }


        #endregion

        #region Ativar

        public IActionResult Ativar(int id = 0)
        {
            Usuario usuario = null;

            if (User.IsInRole("Administradora"))
                usuario = _context.Usuarios.Find(id);
            else
                usuario = _context.Usuarios.FirstOrDefault(c => c.UsuarioId == id && c.Colaborador.AgenciaId == _usuarioLogado.ObterClienteId());

            if (usuario != null)
            {
                usuario.SenhaConfirmar = usuario.Senha;
                usuario.IsAtivo = true;
                _context.SaveChanges();

                _notificacao.Success($"Status do usuário: {usuario.Colaborador.Nome} ativado");
            }
            else
            {
                _notificacao.Warning("Usuário não encontrado");
            }

            return RedirectToAction("Index");
        }

        #endregion
    }
}
