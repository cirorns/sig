﻿using Sig.Web.Models;
using Sig.Web.Models.ViewModels;
using Sig.Web.Helpers;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;
using Microsoft.AspNetCore.Mvc;
using Sig.Web.Models.Enums;
using Microsoft.AspNetCore.Authorization;
using AspNetCoreHero.ToastNotification.Abstractions;
using Sig.Web.Interfaces;
using Microsoft.AspNetCore.Mvc.Rendering;
using Sig.Web.Helpers.Extensoes;

namespace Sig.Web.Areas.Agencias.Controllers
{
    [Authorize(Roles = "Administradora, Agencia")]
    public class ContratoController : BaseController
    {
        private readonly IUsuarioLogado _usuarioLogado;

        public ContratoController(INotyfService notificacao, IUsuarioLogado usuarioLogado) : base(notificacao)
        {
            _usuarioLogado = usuarioLogado;
        }

        #region Index

        public IActionResult Index()
        {
            var viewModel = new ContratoViewModel
            {
                Contratos = _context.Contratos.Where(c => c.Cliente.AgenciaId == _usuarioLogado.ObterClienteId()).ToList()
            };
            return View(viewModel);
        }

        [HttpPost]
        public IActionResult Index(ContratoViewModel viewModel)
        {
            IQueryable<Contrato> contratos = _context.Contratos;

            #region Filtros

            if (!User.IsInRole("Administradora"))
            {
                contratos = contratos.Where(c => c.Cliente.AgenciaId == _usuarioLogado.ObterClienteId());
            }

            if (!viewModel.Cliente.ClienteId.Equals(0))
            {
                contratos = contratos.Where(c => c.ClienteId == viewModel.Cliente.ClienteId);
            }

            if (!string.IsNullOrEmpty(viewModel.Cliente.CPFCNPJ))
            {
                contratos = contratos.Where(c => c.Cliente.CPFCNPJ == viewModel.Cliente.CPFCNPJ);
            }

            if (!viewModel.Tabela.TabelaId.Equals(0))
            {
                contratos = contratos.Where(c => c.TabelaId == viewModel.Tabela.TabelaId);
            }

            if (!viewModel.Contrato.ContratoId.Equals(0))
            {
                contratos = contratos.Where(c => c.ContratoId == viewModel.Contrato.ContratoId);
            }

            if (!viewModel.Colaborador.ColaboradorId.Equals(0))
            {
                contratos = contratos.Where(c => c.VendedorId == viewModel.Colaborador.ColaboradorId);
            }

            if (!string.IsNullOrEmpty(viewModel.Cliente.Nome))
            {
                contratos = contratos.Where(c => c.Cliente.Nome.Contains(viewModel.Cliente.Nome));
            }

            if (!string.IsNullOrEmpty(viewModel.Cliente.RazaoSocial))
            {
                contratos = contratos.Where(c => c.Cliente.RazaoSocial.Contains(viewModel.Cliente.RazaoSocial));
            }

            if (viewModel.IsAtivo)
            {
                contratos = contratos.Where(c => !c.DataCancelamento.HasValue);
            }

            viewModel.Contratos = contratos.ToList();

            #endregion

            return View(viewModel);
        }

        #endregion

        #region Cancelar Contrato

        public IActionResult Cancelar(int id = 0)
        {
            var contrato = _context.Contratos.Find(id);
            return View(contrato);
        }

        [HttpPost]
        public IActionResult Cancelar(Contrato form)
        {
            if (ModelState.IsValid)
            {
                var contrato = _context.Contratos.Find(form.ContratoId);

                if (!contrato.DataCancelamento.HasValue)
                {
                    contrato.DataCancelamento = DateTime.Now;

                    _context.SaveChanges();
                    _notificacao.Success("Contrato cancelado com sucesso");
                }
                else
                {
                    _notificacao.Warning("Contrato já está cancelado");
                }

                return RedirectToAction("Index");
            }

            return View(form);
        }

        #endregion

        #region Reativar Contrato

        public async Task<ActionResult> Reativar(int id = 0)
        {
            var contrato = await _context.Contratos.FindAsync(id);
            return View(contrato);
        }

        [HttpPost]
        public async Task<ActionResult> Reativar(Contrato form)
        {
            if (ModelState.IsValid)
            {
                var contrato = await _context.Contratos.FindAsync(form.ContratoId);

                if (contrato != null)
                {
                    contrato.DataCancelamento = null;
                    await _context.SaveChangesAsync();

                    _notificacao.Success("Contrato reativado com sucesso");
                    return RedirectToAction("Index");
                }
            }

            return View();
        }

        #endregion

        #region Alterar

        [HttpPost]
        public IActionResult AlterarContrato(int id)
        {
            var contrato = _context.Contratos.SingleOrDefault(c => c.ContratoId == id);

            #region ViewBags

            ViewBag.DiasCobranca = new SelectList(_context.DiaCobrancas.OrderBy(c => c.Dia), "DiaCobrancaId", "Dia", contrato.DiaCobrancaId);
            ViewBag.Colaboradores = new SelectList(_context.Colaboradores.Where(c => c.AgenciaId == contrato.Cliente.AgenciaId), "ColaboradorId", "Nome", contrato.VendedorId);
            ViewBag.Tabelas = new SelectList(_context.TabelaAgencias.Where(c => c.AgenciaId == contrato.Cliente.AgenciaId), "TabelaId", "Tabela.Descricao", contrato.TabelaId);

            #endregion

            return PartialView("_AlterarContrato", contrato);
        }

        [HttpPost]
        [ActionName("AlterarContratoFinal")]
        public IActionResult AlterarContrato(Contrato form)
        {
            var contrato = _context.Contratos.SingleOrDefault(c => c.ContratoId == form.ContratoId);

            if (ModelState.IsValid)
            {
                contrato.DataFinal = form.DataFinal;
                contrato.IsRenovacaoAutomatica = form.IsRenovacaoAutomatica;
                contrato.DiaCobrancaId = form.DiaCobrancaId;
                contrato.TabelaId = form.TabelaId;
                contrato.VendedorId = form.VendedorId;
                contrato.DataInicial = form.DataInicial;
                contrato.DataFinal = form.DataFinal;
                contrato.IsRenovacaoAutomatica = form.IsRenovacaoAutomatica;
                contrato.IsTaxaExtra = form.IsTaxaExtra;
                contrato.IsAniversario = form.IsAniversario;
                contrato.IsEnviaEmail = form.IsEnviaEmail;
                contrato.IsDespesaBancaria = form.IsDespesaBancaria;

                _context.SaveChanges();

                return PartialView("_Index", _context.Contratos.Where(c => c.ContratoId == form.ContratoId));
            }

            #region ViewBags

            ViewBag.DiasCobranca = new SelectList(_context.DiaCobrancas.OrderBy(c => c.Dia), "DiaCobrancaId", "Dia", form.DiaCobrancaId);
            ViewBag.Colaboradores = new SelectList(_context.Colaboradores.Where(c => c.AgenciaId == contrato.Cliente.AgenciaId), "ColaboradorId", "Nome", form.VendedorId);
            ViewBag.Tabelas = new SelectList(_context.TabelaAgencias.Where(c => c.AgenciaId == contrato.Cliente.AgenciaId), "TabelaId", "Tabela.Descricao", form.TabelaId);

            #endregion

            form.Cliente = contrato.Cliente;
            return PartialView("_AlterarContrato", form);
        }

        #endregion

        #region Incluir

        public IActionResult Incluir()
        {
            var viewModel = new ContratoIncluirViewModel
            {
                Clientes = _context.Clientes.Where(c => c.AgenciaId == _usuarioLogado.ObterClienteId() && c.TipoCliente != eTipoCliente.Administradora && c.TipoCliente != eTipoCliente.PrePago).OrderBy(c => c.Nome).ToList()
            };
            return View(viewModel);
        }

        [HttpPost]
        public IActionResult Incluir(ContratoIncluirViewModel viewModel)
        {
            IQueryable<Cliente> clientes = _context.Clientes.Where(c => c.AgenciaId == _usuarioLogado.ObterClienteId() && c.TipoCliente != eTipoCliente.Administradora && c.TipoCliente != eTipoCliente.PrePago).OrderBy(c => c.Nome);

            #region Filtros

            if (!viewModel.Cliente.ClienteId.Equals(0))
            {
                clientes = clientes.Where(c => c.ClienteId == viewModel.Cliente.ClienteId);
            }

            if (User.IsInRole("Administradora"))
            {
                if (viewModel.Cliente.AgenciaId.HasValue)
                {
                    clientes = clientes.Where(c => c.AgenciaId == viewModel.Cliente.AgenciaId);
                }
            }

            if (!string.IsNullOrEmpty(viewModel.Serasa.Logon))
            {
                clientes = clientes.Where(c => c.Serasas.Any(p => p.Logon == viewModel.Serasa.Logon));
            }

            if (!string.IsNullOrEmpty(viewModel.Cliente.CPFCNPJ))
            {
                clientes = clientes.Where(c => c.CPFCNPJ == viewModel.Cliente.CPFCNPJ);
            }

            if (!viewModel.Contrato.ContratoId.Equals(0))
            {
                clientes = clientes.Where(c => c.Contratos.Any(p => p.ContratoId == viewModel.Contrato.ContratoId));
            }

            if (!string.IsNullOrEmpty(viewModel.Cliente.Nome))
            {
                clientes = clientes.Where(c => c.Nome.Contains(viewModel.Cliente.Nome));
            }

            if (!string.IsNullOrEmpty(viewModel.Cliente.RazaoSocial))
            {
                clientes = clientes.Where(c => c.RazaoSocial.Contains(viewModel.Cliente.RazaoSocial));
            }

            #endregion

            viewModel.Clientes = clientes.ToList();
            return View(viewModel);
        }

        [HttpGet]
        public IActionResult Incluir2(int id = 0)
        {
            var cliente = _context.Clientes.Find(id);
            Contrato contrato = null;

            if (cliente != null)
            {
                contrato = new Contrato
                {
                    ClienteId = cliente.ClienteId,
                    DataInicial = DateTime.Now,
                    DataFinal = DateTime.Now.AddMonths(12),
                    IsRenovacaoAutomatica = true,
                    Cliente = cliente,

                };

                #region ViewBags

                var tabelas = (from tc in _context.TabelaAgencias
                               join t in _context.Tabelas on tc.TabelaId equals t.TabelaId
                               where tc.AgenciaId == cliente.AgenciaId
                               select t);

                ViewBag.Tabelas = new SelectList(tabelas, "TabelaId", "Descricao");

                ViewBag.DiasCobranca = new SelectList(_context.DiaCobrancas.OrderBy(c => c.Dia), "DiaCobrancaId", "Dia");
                ViewBag.Colaboradores = new SelectList(_context.Colaboradores.Where(c => c.AgenciaId == cliente.AgenciaId), "ColaboradorId", "Nome");

                #endregion
            }

            return View(contrato);
        }

        [HttpPost]
        public IActionResult Incluir2(Contrato contrato)
        {
            if (ModelState.IsValid)
            {
                using var ts = new TransactionScope();
                try
                {
                    contrato.Cliente = _context.Clientes.Find(contrato.ClienteId);
                    contrato.DataCadastro = DateTime.Now;

                    _context.Contratos.Add(contrato);
                    _context.SaveChanges();

                    var senha = Util.ObterSenhaAleatoria(8);

                    var usuarioConsulta = new UsuarioConsulta()
                    {
                        ClienteId = contrato.ClienteId,
                        ContratoId = contrato.ContratoId,
                        Email = contrato.Cliente.Email,
                        IsSuperUsuario = true,
                        Senha = senha,
                        ConfirmarSenha = senha,
                    };

                    _context.UsuarioConsultas.Add(usuarioConsulta);
                    _context.SaveChanges();

                    ts.Complete();

                    _notificacao.Success($"Dados cadastrados com sucesso!, Cliente: {contrato.ClienteId} Email: {usuarioConsulta.Email} Senha: {usuarioConsulta.Senha}");
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    _notificacao.Error(ex.Message);
                    ts.Dispose();
                }
            }

            return View(contrato);
        }

        #endregion

        #region Excluir

        public IActionResult Excluir(int id = 0)
        {
            var contrato = _context.Contratos.Find(id);
            return View(contrato);
        }

        [HttpPost]
        public IActionResult Excluir(Contrato form)
        {
            try
            {

                var contrato = _context.Contratos.Find(form.ContratoId);

                if (contrato != null)
                {
                    //Verificar usuários de consulta
                    var usuariosConsulta = _context.UsuarioConsultas.Where(c => c.ContratoId == contrato.ContratoId);

                    if (usuariosConsulta != null && usuariosConsulta.Any())
                        _context.UsuarioConsultas.RemoveRange(usuariosConsulta);

                    _context.Contratos.Remove(contrato);
                    _context.SaveChanges();

                    _notificacao.Success("Contrato excluído com sucesso");
                }
                else
                {
                    _notificacao.Warning("Contrato não encontrado");
                }
            }
            catch (Exception)
            {
                _notificacao.Warning("Contrato não excluído, por favor verifique os dados relacionados");
            }

            return RedirectToAction("Index");
        }


        #endregion
    }
}
