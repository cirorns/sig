﻿using Sig.Web.Models;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using AspNetCoreHero.ToastNotification.Abstractions;

namespace Sig.Web.Areas.Agencias.Controllers
{
    public class FuncaoController : BaseController
    {
        public FuncaoController(INotyfService notificacao) : base(notificacao)
        {
        }

        public IActionResult Faturamento()
        {
            ViewBag.ItemFaturavelId = new SelectList(_context.ItemFaturaveis.Where(c => c.IsAtivo), "ItemFaturavelId", "Descricao");
            return View();
        }

        [HttpPost]
        public IActionResult Faturamento(FaturamentoItem form)
        {
            var faturamento = _context.Faturamentos.Find(form.FaturamentoId);

            if (faturamento == null)
            {
                _notificacao.Warning("Não existe esta fatura");
                return RedirectToAction("Faturamento");
            }

            if (form.CustoUnitario > form.ValorUnitario)
            {
                _notificacao.Warning("Valor de custo unitário maior que o valor unitário");
                return RedirectToAction("Faturamento");
            }

            var faturamentoItem = _context.FaturamentoItems.Where(c => c.FaturamentoId == faturamento.FaturamentoId 
                && c.ItemFaturavelId == form.ItemFaturavelId).FirstOrDefault();

            if (faturamentoItem == null)
            {
                form.RepasseAgencia = 0;

                _context.FaturamentoItems.Add(form);
                faturamento.ValorOriginal += (form.ValorUnitario * form.Quantidade);

                _context.SaveChanges();

                _notificacao.Success(string.Format("Fatura: {0} com o novo Valor de: {1} atualizada com sucesso", 
                    faturamento.FaturamentoId, faturamento.ValorOriginal.ToString("c")));

                return RedirectToAction("Faturamento");
            }
            else
            {

                faturamentoItem.Quantidade += form.Quantidade;
                faturamento.ValorOriginal += (faturamentoItem.ValorUnitario * form.Quantidade);

                _context.SaveChanges();


                _notificacao.Success(string.Format("Fatura: {0} com o novo Valor de: {1} atualizada com sucesso",
                    faturamento.FaturamentoId, faturamento.ValorOriginal.ToString("c")));

                return RedirectToAction("Faturamento");
            }

        }

    }
}
