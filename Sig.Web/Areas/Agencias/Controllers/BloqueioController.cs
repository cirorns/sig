﻿using Sig.Web.Models;
using Sig.Web.Models.ViewModels;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Sig.Web.Models.Enums;
using Microsoft.AspNetCore.Authorization;
using AspNetCoreHero.ToastNotification.Abstractions;
using Sig.Web.Interfaces;

namespace Sig.Web.Areas.Agencias.Controllers
{

    [Authorize(Roles = "Administradora, Agencia")]
    public class BloqueioController : BaseController
    {
        private IUsuarioLogado _usuarioLogado;

        public BloqueioController(INotyfService notificacao, IUsuarioLogado usuarioLogado) : base(notificacao)
        {
            _usuarioLogado = usuarioLogado;
        }

        #region Index

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Index(BloqueioViewModel viewModel)
        {

            IQueryable<Cliente> clientes = _context.Clientes;

            #region Filtros

            if (!User.IsInRole("Administradora"))
            {
                clientes = clientes.Where(c => c.AgenciaId == _usuarioLogado.ObterClienteId());
            }
            else
            {
                if (viewModel.Cliente.AgenciaId.HasValue)
                {
                    clientes = clientes.Where(c => c.AgenciaId == viewModel.Cliente.AgenciaId);
                }
            }

            if (!viewModel.Cliente.ClienteId.Equals(0))
            {
                clientes = clientes.Where(c => c.ClienteId == viewModel.Cliente.ClienteId);
            }

            if (!string.IsNullOrEmpty(viewModel.Cliente.CPFCNPJ))
            {
                clientes = clientes.Where(c => c.CPFCNPJ == viewModel.Cliente.CPFCNPJ);
            }

            if (!string.IsNullOrEmpty(viewModel.Serasa.Logon))
            {
                clientes = clientes.Where(c => c.Serasas.Any(p => p.Logon == viewModel.Serasa.Logon));
            }

            if (!viewModel.Contrato.ContratoId.Equals(0))
            {
                clientes = clientes.Where(c => c.Contratos.Any(p => p.ContratoId == viewModel.Contrato.ContratoId));
            }

            if (!string.IsNullOrEmpty(viewModel.Cliente.Nome))
            {
                clientes = clientes.Where(c => c.Nome.Contains(viewModel.Cliente.Nome));
            }

            if (!string.IsNullOrEmpty(viewModel.Cliente.RazaoSocial))
            {
                clientes = clientes.Where(c => c.RazaoSocial.Contains(viewModel.Cliente.RazaoSocial));
            }

            #endregion

            viewModel.Clientes = clientes;
            return View(viewModel);
        }

        #endregion

        #region Alterar

        public IActionResult Status(int id)
        {
            var cliente = _context.Clientes.Find(id);

            if (cliente == null)
            {
                _notificacao.Warning("Cliente não encontrado");
                return RedirectToAction("Index");
            }

            if (!User.IsInRole("Administradora"))
            {
                if (cliente.AgenciaId != _usuarioLogado.ObterClienteId())
                {
                    _notificacao.Warning("Cliente não pertence a esta agência");
                    return RedirectToAction("Index");
                }
            }


            if (cliente.TipoCliente != eTipoCliente.Administradora)
            {
                cliente.IsAtivo = !cliente.IsAtivo;

                _context.SaveChanges();
                _notificacao.Success($"Status do cliente {cliente.Nome} alterado");
            }
            else
            {
                _notificacao.Warning("Você não pode alterar o status da administradora");
            }

            return RedirectToAction("Index");
        }

        #endregion
    }
}
