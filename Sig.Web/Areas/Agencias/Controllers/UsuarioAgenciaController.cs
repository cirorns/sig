﻿using Sig.Web.Models;
using Sig.Web.Models.ViewModels;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Sig.Web.Models.Enums;
using Microsoft.AspNetCore.Authorization;
using AspNetCoreHero.ToastNotification.Abstractions;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Sig.Web.Areas.Agencias.Controllers
{

    //[Authorize(Roles = "Administradora")]
    public class UsuarioAgenciaController : BaseController
    {
        public UsuarioAgenciaController(INotyfService notificacao) : base(notificacao)
        {
        }
        #region Index

        public IActionResult Index()
        {
            var viewModel = new UsuarioViewModel
            {
                Usuarios = _context.Usuarios.Where(c => c.Colaborador.Agencia.TipoCliente == eTipoCliente.Agencia
                                        && c.IsAtivo == true).OrderByDescending(c => c.UsuarioId)
            };

            return View(viewModel);
        }

        [HttpPost]
        public IActionResult Index(UsuarioViewModel viewModel)
        {
            IQueryable<Usuario> usuarios = _context.Usuarios.Where(c => c.Colaborador.Agencia.TipoCliente == eTipoCliente.Agencia);

            #region Filtros

            if (!viewModel.Inativos)
            {
                usuarios = usuarios.Where(c => c.IsAtivo == true);
            }

            if (!string.IsNullOrEmpty(viewModel.Usuario.Colaborador.Agencia.CPFCNPJ))
            {
                usuarios = usuarios.Where(c => c.Colaborador.Agencia.CPFCNPJ == viewModel.Usuario.Colaborador.Agencia.CPFCNPJ);
            }

            if (!string.IsNullOrEmpty(viewModel.Usuario.Colaborador.Agencia.Nome))
            {
                usuarios = usuarios.Where(c => c.Colaborador.Agencia.Nome.Contains(viewModel.Usuario.Colaborador.Agencia.Nome));
            }

            if (!string.IsNullOrEmpty(viewModel.Usuario.Colaborador.Agencia.RazaoSocial))
            {
                usuarios = usuarios.Where(c => c.Colaborador.Agencia.RazaoSocial.Contains(viewModel.Usuario.Colaborador.Agencia.RazaoSocial));
            }

            #endregion

            viewModel.Usuarios = usuarios;
            return View(viewModel);
        }

        #endregion

        #region Alterar

        public IActionResult Alterar(int id = 0)
        {
            var usuario = _context.Usuarios.FirstOrDefault(c => c.UsuarioId == id);

            #region ViewBag

            ViewBag.Colaboradores = new SelectList(_context.Colaboradores.Where(c => c.AgenciaId == usuario.Colaborador.AgenciaId), "ColaboradorId", "Nome", usuario.ColaboradorId);

            #endregion

            var viewModel = new UsuarioViewModel
            {
                Usuario = usuario
            };

            return View(viewModel);
        }

        [HttpPost]
        public IActionResult Alterar(UsuarioViewModel viewModel)
        {
            var usuario = _context.Usuarios.Find(viewModel.Usuario.UsuarioId);

            usuario.Senha = viewModel.Usuario.Senha;
            usuario.SenhaConfirmar = viewModel.Usuario.SenhaConfirmar;
            usuario.IsAtivo = viewModel.Usuario.IsAtivo;

            _context.SaveChanges();

            _notificacao.Success("Senha do usuário de agência alterada");
            return RedirectToAction("Index");
        }

        #endregion

        #region Desativar

        public IActionResult Desativar(int id = 0)
        {
            var usuario = _context.Usuarios.Find(id);
            
            if (usuario != null)
            {
                usuario.SenhaConfirmar = usuario.Senha;
                usuario.IsAtivo = false;
                _context.SaveChanges();

                _notificacao.Success(string.Format("Status do usuário: {0} desativado", usuario.Colaborador.Email));
            }
            else
            {
                _notificacao.Warning("Usuário não encontrado");
            }

            return RedirectToAction("Index");
        }

        #endregion

        #region Ativar

        public IActionResult Ativar(int id = 0)
        {
            var usuario = _context.Usuarios.Find(id);

            if (usuario != null)
            {
                usuario.SenhaConfirmar = usuario.Senha;
                usuario.IsAtivo = true;
                _context.SaveChanges();

                _notificacao.Success(string.Format("Status do usuário: {0} ativado", usuario.Colaborador.Email));
            }
            else
            {
                _notificacao.Warning("Usuário não encontrado");
            }

            return RedirectToAction("Index");
        }

        #endregion
    }
}
