﻿using Sig.Web.Models;
using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using AspNetCoreHero.ToastNotification.Abstractions;
using Sig.Web.Helpers;

namespace Sig.Web.Areas.Agencias.Controllers
{
    [Authorize(Roles = "Administradora")]
    public class ConfigSendGridController : BaseController
    {
        public ConfigSendGridController(INotyfService notificacao) : base(notificacao)
        {
        }

        public IActionResult Index()
        {
            var configSendGrid = _context.ConfigSendGrids.OrderBy(c => c.ConfigSendGridId);
            return View(configSendGrid);
        }

        public IActionResult Editar(int? id)
        {
            try
            {
                ConfigSendGrid configSendGrid;

                if (!id.HasValue || id == 0)
                {
                    if (_context.ConfigSendGrids.Any())
                    {
                        _notificacao.Warning("Já existe uma conta criada, você não poderá adicionar uma nova. Tente editar");
                        return RedirectToAction(nameof(Index));
                    }

                    configSendGrid = new ConfigSendGrid();
                }
                else
                {
                    configSendGrid = _context.ConfigSendGrids.Find(id);
                }

                return View(configSendGrid);
            }
            catch (Exception ex)
            {
                _notificacao.Error(ex.Message);
                return View();
            }
        }

        [HttpPost, ValidateAntiForgeryToken]
        public IActionResult Editar(ConfigSendGrid configSendGrid)
        {
            try
            {
                string mensagem = string.Empty;

                if (configSendGrid.ConfigSendGridId == 0)
                {
                    _context.ConfigSendGrids.Add(configSendGrid);
                    mensagem = "Conta do SendGrid adicionada com sucesso.";
                }
                else
                {
                    _context.ConfigSendGrids.Attach(configSendGrid);
                    var entry = _context.Entry(configSendGrid);

                    entry.Property(c => c.ConfigSendGridId).IsModified = false;
                    entry.Property(c => c.Email).IsModified = true;
                    entry.Property(c => c.EmailBcc).IsModified = true;
                    entry.Property(c => c.Nome).IsModified = true;
                    entry.Property(c => c.TemplateIdBoleto).IsModified = true;
                    entry.Property(c => c.TemplateIdEnvioDeSenha).IsModified = true;
                    entry.Property(c => c.TemplateIdRecuperarSenha).IsModified = true;
                    entry.Property(c => c.TemplateIdPadrao).IsModified = true;
                    entry.Property(c => c.Usuario).IsModified = true;
                    entry.Property(c => c.Senha).IsModified = true;

                    mensagem = "Conta do SendGrid alterada com sucesso.";
                }

                _context.SaveChanges();
                _notificacao.Success(mensagem);

                return RedirectToAction(nameof(Index));
            }
            catch (Exception ex)
            {
                _notificacao.Error(ex.Message);
                return View(nameof(Editar), configSendGrid);
            }
        }
    }
}