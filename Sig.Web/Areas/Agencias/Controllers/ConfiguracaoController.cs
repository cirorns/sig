﻿using Sig.Web.Models;
using System.IO;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using AspNetCoreHero.ToastNotification.Abstractions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using System.Threading.Tasks;

namespace Sig.Web.Areas.Agencias.Controllers
{
    [Authorize(Roles = "Administradora")]
    public class ConfiguracaoController : BaseController
    {
        private readonly IWebHostEnvironment _webHostEnvironment;

        public ConfiguracaoController(INotyfService notificacao, IWebHostEnvironment webHostEnvironment) : base(notificacao)
        {
            _webHostEnvironment = webHostEnvironment;
        }

        public IActionResult Index()
        {
            var configuracoes = _context.Configuracoes;
            return View(configuracoes);
        }

        public IActionResult Incluir()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Incluir(Configuracao configuracao, IFormFile Logomarca)
        {
            if (!_context.Configuracoes.Any())
            {
                if (ModelState.IsValid)
                {
                    if (Logomarca != null)
                    {
                        var path = Path.Combine(_webHostEnvironment.WebRootPath, Logomarca.FileName);
                        using (Stream fileStream = new FileStream(path, FileMode.Create))
                        {
                            await Logomarca.CopyToAsync(fileStream);
                        }

                        
                        configuracao.Logomarca = Logomarca.FileName;
                    }

                    _context.Configuracoes.Add(configuracao);
                    _context.SaveChanges();

                    _notificacao.Success("Configuração adicionada com sucesso");
                    return RedirectToAction("Index");
                }
            }
            else
            {
                _notificacao.Warning("Apenas uma configuração é permitida");
            }

            return View();
        }

        public IActionResult Alterar(int id)
        {
            var configuracao = _context.Configuracoes.Find(id);
            return View(configuracao);
        }


        [HttpPost]
        public async Task<IActionResult> Alterar(Configuracao form, IFormFile Logomarca)
        {
            if (ModelState.IsValid)
            {
                var configuracao = _context.Configuracoes.Find(form.ConfiguracaoId);

                if (Logomarca != null)
                {
                    var path = Path.Combine(_webHostEnvironment.WebRootPath, Logomarca.FileName);
                    using (Stream fileStream = new FileStream(path, FileMode.Create))
                    {
                        await Logomarca.CopyToAsync(fileStream);
                    }

                    configuracao.Logomarca = Logomarca.FileName;
                }
                
                configuracao.Empresa        = form.Empresa;
                configuracao.IsMultLogon    = form.IsMultLogon;

                configuracao.CNPJInformante         = form.CNPJInformante;
                configuracao.NomeContatoInformante  = form.NomeContatoInformante;
                configuracao.DDDInformante          = form.DDDInformante;
                configuracao.TelefoneInformante     = form.TelefoneInformante;

                _context.SaveChanges();

                _notificacao.Success("Configuração alterada com sucesso");
                return RedirectToAction("Index");
            }

            return View();
        }

    }
}
