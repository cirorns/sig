﻿using Sig.Web.Models;
using Sig.Web.Models.Enums;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using AspNetCoreHero.ToastNotification.Abstractions;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Sig.Web.Areas.Agencias.Controllers
{
    [Authorize(Roles = "Administradora")]
    public class TransacaoConsultaController : BaseController
    {
        public TransacaoConsultaController(INotyfService notificacao) : base(notificacao)
        {
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Incluir()
        {
            ViewBag.ClienteId = new SelectList(_context
               .Clientes
               .Where(c => c.TipoCliente == eTipoCliente.Cliente || c.TipoCliente == eTipoCliente.Agencia)
               .AsEnumerable()
               .Select(c => new { c.ClienteId, RazaoSocial = $"{c.ClienteId} - {c.RazaoSocial}" }).OrderBy(c => c.ClienteId), "ClienteId", "RazaoSocial");

            ViewBag.ItemFaturavelId = new SelectList(_context
               .ItemFaturaveis
               .Where(c => c.IsAtivo)
               .AsEnumerable()
               .Select(c => new { c.ItemFaturavelId, Descricao = $"{c.ItemFaturavelId} - {c.Descricao}" }).OrderBy(c => c.Descricao), "ItemFaturavelId", "Descricao");
            
            return View();
        }

        [HttpPost]
        public IActionResult Incluir(TransacaoConsulta transacaoConsulta)
        {
            if (ModelState.IsValid)
            {
                for (int i = 0; i < transacaoConsulta.Quantidade; i++)
                {
                    transacaoConsulta.IsConfirmado  = true;
                    transacaoConsulta.DataResposta  = transacaoConsulta.DataEnvio;
                    transacaoConsulta.DadosEnvio    = "INCLUSÃO MANUAL SITE";
                    transacaoConsulta.DadosResposta = "INCLUSÃO MANUAL SITE";
                    
                    _context.TransacoesConsultas.Add(transacaoConsulta);
                    _context.SaveChanges();
                }
                
                _notificacao.Success("Trasações adicionadas com sucesso");
                return Redirect("Incluir");
            }
            else
            {
                _notificacao.Warning("Dados inválidos");
            }

            ViewBag.ClienteId = new SelectList(_context
               .Clientes
               .Where(c => c.TipoCliente == eTipoCliente.Cliente || c.TipoCliente == eTipoCliente.Agencia)
               .AsEnumerable()
               .Select(c => new { c.ClienteId, RazaoSocial = $"{c.ClienteId} - {c.RazaoSocial}" }).OrderBy(c => c.ClienteId), "ClienteId", "RazaoSocial");

            ViewBag.ItemFaturavelId = new SelectList(_context
               .ItemFaturaveis
               .Where(c => c.IsAtivo)
               .AsEnumerable()
               .Select(c => new { c.ItemFaturavelId, Descricao = $"{c.ItemFaturavelId} - {c.Descricao}" }).OrderBy(c => c.Descricao), "ItemFaturavelId", "Descricao");

            return View();

        }
    }
}