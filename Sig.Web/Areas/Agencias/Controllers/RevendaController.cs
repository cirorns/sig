﻿using Sig.Web.Models;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using AspNetCoreHero.ToastNotification.Abstractions;

namespace Sig.Web.Areas.Agencias.Controllers
{
    public class RevendaController : BaseController
    {
        public RevendaController(INotyfService notificacao) : base(notificacao)
        {
        }

        // GET: Agencia/Revenda
        public IActionResult Index()
        {
            var revendas = _context.Revendas.OrderBy(c => c.ClienteId);
            return View(revendas);
        }

        #region Incluir

        public IActionResult Incluir()
        {
            var revenda = new Revenda();

            foreach (var itemFaturavel in _context.ItemFaturaveis.OrderBy(c => c.Descricao))
            {
                var revendaItem = new RevendaItem
                {
                    ItemFaturavel = itemFaturavel,
                    ItemFaturavelId = itemFaturavel.ItemFaturavelId,
                };

                revenda.RevendaItens.Add(revendaItem);
            }
            
            return View(revenda);
        }

        [HttpPost]
        public IActionResult Incluir(Revenda revenda)
        {
            if (ModelState.IsValid)
            {
                _context.Revendas.Add(revenda);
                _context.SaveChanges();

                _notificacao.Success("Revenda cadastrada com sucesso");
                return RedirectToAction("Index");
            }

            return View();
        }

        #endregion

        #region Alterar

        public IActionResult Alterar(int id = 0)
        {
            var revenda = _context.Revendas.Find(id);

            if (revenda == null)
            {
                _notificacao.Warning("Revenda não encontrada");
                return RedirectToAction("Index");
            }

            return View(revenda);
        }

        [HttpPost]
        public IActionResult Alterar(Revenda revenda)
        {
            if (ModelState.IsValid)
            {
                _context.Revendas.Attach(revenda);
                var entry = _context.Entry<Revenda>(revenda);

                entry.Property(c => c.ClienteId).IsModified = true;

                _notificacao.Success("Revenda alterada com sucesso");
                return RedirectToAction("Index");
            }

            return View(revenda);
        }

        #endregion
    }
}