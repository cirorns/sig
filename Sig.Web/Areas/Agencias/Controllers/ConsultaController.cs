﻿using System;
using System.Linq;
using AspNetCoreHero.ToastNotification.Abstractions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Sig.Web.Helpers;
using Sig.Web.Interfaces;

namespace Sig.Web.Areas.Agencias.Controllers
{
    //[Authorize(Roles = "Agencia, Administradora")]
    public class ConsultaController : BaseController
    {
        private readonly IUsuarioLogado _usuarioLogado;

        public ConsultaController(INotyfService notificacao, IUsuarioLogado usuarioLogado) : base(notificacao)
        {
            _usuarioLogado = usuarioLogado;
        }

        public IActionResult Index()
        {
            var consultas = _context.Consultas
                    .Where(c => c.AgenciaId == _usuarioLogado.ObterClienteId())
                    .OrderBy(c => c.AgenciaId)
                    .OrderBy(c => c.Descricao);

            return View(consultas);
        }

        public IActionResult Editar(int id = 0)
        {
            try
            {
                Web.Models.Consulta consulta;

                if (id == 0)
                {
                    consulta = new Web.Models.Consulta();
                }
                else
                {
                    consulta = _context.Consultas.Single(c => c.AgenciaId == _usuarioLogado.ObterClienteId() && c.ItemFaturavelId == id);
                }

                ObterViewBag();
                return View(consulta);
            }
            catch (Exception ex)
            {
                _notificacao.Error(ex.Message);
                return View();
            }
        }

        [HttpPost, ValidateAntiForgeryToken]
        public IActionResult Editar(Web.Models.Consulta consulta)
        {
            try
            {
                consulta.AgenciaId = _usuarioLogado.ObterClienteId();

                string mensagem = string.Empty;

                if (!_context.Consultas.Any(c => c.AgenciaId == consulta.AgenciaId && c.ItemFaturavelId == consulta.ItemFaturavelId))
                {
                    _context.Consultas.Add(consulta);
                    mensagem = "Consulta adicionada com sucesso";
                }
                else
                {
                    _context.Consultas.Attach(consulta);
                    var entry = _context.Entry(consulta);

                    entry.Property(c => c.AgenciaId).IsModified = false;
                    entry.Property(c => c.ItemFaturavelId).IsModified = false;
                    entry.Property(c => c.Descricao).IsModified = true;
                    entry.Property(c => c.Detalhe).IsModified = true;

                    mensagem = "Consulta alterada com sucesso";
                }

                _context.SaveChanges();
                _notificacao.Success(mensagem);

                return RedirectToAction("index");
            }
            catch (Exception ex)
            {
                _notificacao.Error(ex.Message);

                ObterViewBag();
                return View("editar", consulta);
            }
        }

        private void ObterViewBag()
        {
            ViewBag.ItemFaturavelId = new SelectList(_context.ItemFaturaveis.Where(c => c.IsAtivo && !c.LinkItemFaturavel.HasValue)
                                                            .OrderBy(c => c.Descricao), "ItemFaturavelId", "Descricao");
        }
    }
}