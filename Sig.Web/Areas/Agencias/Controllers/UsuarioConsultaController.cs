﻿using Sig.Web.Models;
using Sig.Web.Models.ViewModels;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using AspNetCoreHero.ToastNotification.Abstractions;
using Sig.Web.Interfaces;

namespace Sig.Web.Areas.Agencias.Controllers
{

    [Authorize(Roles = "Administradora, Agencia")]
    public class UsuarioConsultaController : BaseController
    {
        private readonly IUsuarioLogado _usuarioLogado;

        public UsuarioConsultaController(INotyfService notificacao, IUsuarioLogado usuarioLogado) : base(notificacao)
        {
            _usuarioLogado = usuarioLogado;
        }

        #region Index

        public IActionResult Index()
        {
            var viewModel = new UsuarioConsultaViewModel();

            if (User.IsInRole("Administradora"))
            {
                viewModel.UsuarioConsultas = _context.UsuarioConsultas.OrderBy(c => c.ClienteId).ToList();
            }
            else
            {
                viewModel.UsuarioConsultas = _context.UsuarioConsultas.Where(c => c.Cliente.AgenciaId == _usuarioLogado.ObterClienteId()).OrderBy(c => c.ClienteId).ToList();
            }

            return View(viewModel);
        }

        [HttpPost]
        public IActionResult Index(UsuarioConsultaViewModel viewModel)
        {
            IQueryable<UsuarioConsulta> usuarioConsultas = _context.UsuarioConsultas;

            #region Filtros

            if (!User.IsInRole("Administradora"))
            {
                usuarioConsultas = usuarioConsultas.Where(c => c.Cliente.AgenciaId == _usuarioLogado.ObterClienteId());
            }

            if (!viewModel.UsuarioConsulta.ClienteId.Equals(0))
            {
                usuarioConsultas = usuarioConsultas.Where(c => c.ClienteId == viewModel.UsuarioConsulta.ClienteId);
            }

            if (!string.IsNullOrEmpty(viewModel.UsuarioConsulta.Cliente.Nome))
            {
                usuarioConsultas = usuarioConsultas.Where(c => c.Cliente.Nome.Contains(viewModel.UsuarioConsulta.Cliente.Nome));
            }

            if (!string.IsNullOrEmpty(viewModel.UsuarioConsulta.Cliente.RazaoSocial))
            {
                usuarioConsultas = usuarioConsultas.Where(c => c.Cliente.Nome.Contains(viewModel.UsuarioConsulta.Cliente.RazaoSocial));
            }

            #endregion

            viewModel.UsuarioConsultas = usuarioConsultas;
            return View(viewModel);
        }

        #endregion

        #region Alterar

        public IActionResult Alterar(int id = 0)
        {
            var usuarioConsulta = _context.UsuarioConsultas.Find(id);

            if (usuarioConsulta == null)
            {
                _notificacao.Warning("Usuário de consulta não encontrado");
                return RedirectToAction("Index");
            }

            return View(usuarioConsulta);
        }

        [HttpPost]
        public IActionResult Alterar(UsuarioConsulta form)
        {
            var usuarioConsulta = _context.UsuarioConsultas.Find(form.UsuarioConsultaId);

            if (ModelState.IsValid)
            {
                usuarioConsulta.Email = form.Email;
                usuarioConsulta.Senha = form.Senha;
                usuarioConsulta.ConfirmarSenha = form.Senha;
                usuarioConsulta.IsAtivo = form.IsAtivo;

                _context.SaveChanges();

                _notificacao.Success("Usuário de consulta alterado com sucesso");
                return RedirectToAction("Index");
            }
            else
            {
                _notificacao.Warning("Dados incorretos. Tente novamente");
            }


            return View(usuarioConsulta);
        }

        #endregion
    }
}
