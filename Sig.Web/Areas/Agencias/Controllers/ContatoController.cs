﻿using Sig.Web.Models;
using Sig.Web.Models.ViewModels;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Sig.Web.Models.Enums;
using Microsoft.AspNetCore.Authorization;
using AspNetCoreHero.ToastNotification.Abstractions;
using Sig.Web.Interfaces;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Sig.Web.Areas.Agencias.Controllers
{
    [Authorize(Roles = "Administradora, Agencia")]
    public class ContatoController : BaseController
    {
        private readonly IUsuarioLogado _usuarioLogado;

        public ContatoController(INotyfService notificacao, IUsuarioLogado usuarioLogado) : base(notificacao)
        {
            _usuarioLogado = usuarioLogado;
        }


        #region Index

        public IActionResult Index()
        {
            var viewModel = new ContatoViewModel
            {
                Clientes = _context.Clientes.Where(c => c.AgenciaId == _usuarioLogado.ObterClienteId())
                                                    .OrderBy(c => c.Nome).ToList()
            };

            return View(viewModel);
        }

        [HttpPost]
        public IActionResult Index(ContatoViewModel viewModel)
        {
            IQueryable<Cliente> clientes = _context.Clientes;

            #region Filtros

            if (!viewModel.Cliente.ClienteId.Equals(0))
            {
                clientes = clientes.Where(c => c.ClienteId == viewModel.Cliente.ClienteId);
            }

            if (User.IsInRole("Administradora"))
            {
                if (!viewModel.Cliente.AgenciaId.Equals(0))
                {
                    clientes = clientes.Where(c => c.AgenciaId == viewModel.Cliente.AgenciaId);
                }
            }
            else
            {
                clientes = clientes.Where(c => c.AgenciaId == _usuarioLogado.ObterAgenciaId() && c.TipoCliente != eTipoCliente.Administradora);
            }

            if (!string.IsNullOrEmpty(viewModel.Cliente.CPFCNPJ))
            {
                clientes = clientes.Where(c => c.CPFCNPJ == viewModel.Cliente.CPFCNPJ);
            }

            if (!string.IsNullOrEmpty(viewModel.Serasa.Logon))
            {
                clientes = clientes.Where(c => c.Serasas.Any(p => p.Logon == viewModel.Serasa.Logon));
            }

            if (!viewModel.Contrato.ContratoId.Equals(0))
            {
                clientes = clientes.Where(c => c.Contratos.Any(p => p.ContratoId == viewModel.Contrato.ContratoId));
            }

            if (!string.IsNullOrEmpty(viewModel.Cliente.Nome))
            {
                clientes = clientes.Where(c => c.Nome.Contains(viewModel.Cliente.Nome));
            }

            if (!string.IsNullOrEmpty(viewModel.Cliente.RazaoSocial))
            {
                clientes = clientes.Where(c => c.RazaoSocial.Contains(viewModel.Cliente.RazaoSocial));
            }

            #endregion

            viewModel.Clientes = clientes.ToList();
            return View(viewModel);
        }

        #endregion

        #region Incluir

        public IActionResult Incluir(int id)
        {
            Cliente cliente;

            if(User.IsInRole("Administradora"))
                cliente = _context.Clientes.SingleOrDefault(c => c.ClienteId == id);
            else
                cliente = _context.Clientes.SingleOrDefault(c => c.ClienteId == id && c.AgenciaId == _usuarioLogado.ObterClienteId() && c.TipoCliente != eTipoCliente.Administradora);


            if (cliente == null)
            {
                _notificacao.Warning("Cliente não pertence a esta agência");
                return RedirectToAction("Index");
            }


            ViewBag.Estados = new SelectList(Estado.UFs, "Sigla", "Descricao");
            ViewBag.Cargos  = new SelectList(_context.Cargos
                                        .Where(c => c.AgenciaId == _usuarioLogado.ObterClienteId())
                                        .OrderBy(c => c.Descricao), "CargoId", "Descricao");

            var contato = new Contato
            {
                ClienteId = cliente.ClienteId,
                Cliente = cliente
            };

            return View(contato);
        }

        [HttpPost]
        public IActionResult Incluir(Contato contato)
        {
            if (ModelState.IsValid)
            {
                _context.Contatos.Add(contato);
                _context.SaveChanges();

                _notificacao.Success("Contato incluído com sucesso");
                return RedirectToAction("Index");
            }

            ViewBag.Estados = new SelectList(Estado.UFs, "Sigla", "Descricao");
            ViewBag.Cargos  = new SelectList(_context.Cargos
                                        .Where(c => c.AgenciaId == _usuarioLogado.ObterClienteId())
                                        .OrderBy(c => c.Descricao), "CargoId", "Descricao", contato.CargoId);
            return View(contato);
        }

        #endregion

        #region Alterar

        public IActionResult Alterar(int id)
        {
            var contato = _context.Contatos.SingleOrDefault(c => c.ContatoId == id && c.Cliente.AgenciaId == _usuarioLogado.ObterClienteId());

            if (contato == null)
            {
                _notificacao.Warning("Contato não pertence a esta agência");
                return RedirectToAction("Index");
            }

            ViewBag.Estados = new SelectList(Estado.UFs, "Sigla", "Descricao");
            ViewBag.Cargos  = new SelectList(_context.Cargos
                                        .Where(c => c.AgenciaId == _usuarioLogado.ObterClienteId())
                                        .OrderBy(c => c.Descricao), "CargoId", "Descricao", contato.CargoId);

            return View(contato);
        }

        [HttpPost]
        public IActionResult Alterar(Contato form)
        {
            var contato = _context.Contatos.Find(form.ContatoId);

            if (ModelState.IsValid)
            {
                contato.Nome                = form.Nome;
                contato.CPF                 = form.CPF;
                contato.DataNascimento      = form.DataNascimento;
                contato.Email               = form.Email;
                contato.Rg                  = form.Rg;
                contato.RgOrgaoExpedidor    = form.RgOrgaoExpedidor;
                contato.RgUf                = form.RgUf;
                contato.RgDataExpedicao     = form.RgDataExpedicao;
                contato.Logradouro          = form.Logradouro;
                contato.Complemento         = form.Complemento;
                contato.Bairro              = form.Bairro;
                contato.Cidade              = form.Cidade;
                contato.CEP                 = form.CEP;
                contato.UF                  = form.UF;
                contato.DDDTelefone         = form.DDDTelefone;
                contato.Telefone            = form.Telefone;
                contato.DDDCelular          = form.DDDCelular;
                contato.Celular             = form.Celular;

                _context.SaveChanges();

                _notificacao.Success("Contato alterado com sucesso");
                return RedirectToAction("Index");
            }

            ViewBag.Estados = new SelectList(Estado.UFs, "Sigla", "Descricao");
            ViewBag.Cargos  = new SelectList(_context.Cargos
                                        .Where(c => c.AgenciaId == _usuarioLogado.ObterAgenciaId())
                                        .OrderBy(c => c.Descricao), "CargoId", "Descricao", contato.CargoId);

            return View(contato);
        }

        #endregion

    }
}
