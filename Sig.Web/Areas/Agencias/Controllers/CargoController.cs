﻿using Sig.Web.Models;
using Sig.Web.Models.ViewModels;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using AspNetCoreHero.ToastNotification.Abstractions;
using Sig.Web.Interfaces;

namespace Sig.Web.Areas.Agencias.Controllers
{
    [Authorize(Roles = "Administradora, Agencia")]
    public class CargoController : BaseController
    {
        private readonly IUsuarioLogado _usuarioLogado;

        public CargoController(INotyfService notificacao, IUsuarioLogado usuarioLogado) : base(notificacao)
        {
            _usuarioLogado = usuarioLogado;
        }

        #region Index

        public IActionResult Index()
        {
            var viewModel = new CargoViewModel
            {
                Cargos = _context.Cargos.Where(c => c.AgenciaId == _usuarioLogado.ObterClienteId())
            };

            return View(viewModel);
        }

        [HttpPost]
        public IActionResult Index(CargoViewModel viewModel)
        {
            IQueryable<Cargo> cargos = _context.Cargos.Where(c => c.AgenciaId == _usuarioLogado.ObterClienteId());

            #region Filtros

            if (!viewModel.Cargo.CargoId.Equals(0))
            {
                cargos = cargos.Where(c => c.CargoId == viewModel.Cargo.CargoId);
            }

            if (!string.IsNullOrEmpty(viewModel.Cargo.Descricao))
            {
                cargos = cargos.Where(c => c.Descricao.Contains(viewModel.Cargo.Descricao));
            }

            #endregion

            viewModel.Cargos = cargos;
            return View(viewModel);
        }

        #endregion

        #region Incluir

        public IActionResult Incluir()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Incluir(Cargo cargo)
        {
            if (ModelState.IsValid)
            {
                cargo.AgenciaId = _usuarioLogado.ObterClienteId();

                _context.Cargos.Add(cargo);
                _context.SaveChanges();

                _notificacao.Success("Cargo incluído com sucesso");

                return RedirectToAction("Index");
            }

            return View(cargo);
        }

        #endregion

        #region Alterar

        public IActionResult Alterar(int id)
        {
            var cargo = _context.Cargos.SingleOrDefault(c => c.CargoId == id
                                && c.AgenciaId == _usuarioLogado.ObterClienteId());

            if (cargo == null)
            {
                _notificacao.Warning("Cargo não pertence a esta agência");
                return RedirectToAction("Index");
            }

            return View(cargo);
        }

        [HttpPost]
        public IActionResult Alterar(Cargo form)
        {
            var cargo = _context.Cargos.Find(form.CargoId);

            if (ModelState.IsValid)
            {
                cargo.Descricao = form.Descricao;
                _context.SaveChanges();

                _notificacao.Success("Cargo alterado com sucesso");
                return RedirectToAction("Index");
            }

            return View(form);
        }

        #endregion

    }
}
