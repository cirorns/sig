﻿using Sig.Web.Models;
using Sig.Web.Models.ViewModels;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using AspNetCoreHero.ToastNotification.Abstractions;
using Sig.Web.Interfaces;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Sig.Web.Areas.Agencias.Controllers
{
    [Authorize(Roles = "Administradora, Agencia")]
    public class ColaboradorController : BaseController
    {

        private readonly IUsuarioLogado _usuarioLogado;

        public ColaboradorController(INotyfService notificacao, IUsuarioLogado usuarioLogado) : base(notificacao)
        {
            _usuarioLogado = usuarioLogado;
        }


        #region Index

        public IActionResult Index()
        {
            var viewModel = new ColaboradorViewModel();
            viewModel.Colaboradores = _context.Colaboradores.Where(c => c.AgenciaId == _usuarioLogado.ObterClienteId());

            ViewBag.Cargos = new SelectList(_context.Cargos
                                .Where(c => c.AgenciaId == _usuarioLogado.ObterClienteId())
                                .OrderBy(c => c.Descricao), 
                                "CargoId", "Descricao");

            return View(viewModel);
        }

        [HttpPost]
        public IActionResult Index(ColaboradorViewModel viewModel)
        {
            IQueryable<Colaborador> colaboradores = _context.Colaboradores;

            #region Filtros

            if (User.IsInRole("Administradora"))
            {
                if (!viewModel.Colaborador.AgenciaId.Equals(0))
                {
                    colaboradores = colaboradores.Where(c => c.AgenciaId == viewModel.Colaborador.AgenciaId);
                }
            }
            else
            {
                colaboradores = colaboradores.Where(c => c.AgenciaId == _usuarioLogado.ObterClienteId());
            }

            if (!string.IsNullOrEmpty(viewModel.Colaborador.Nome))
            {
                colaboradores = colaboradores.Where(c => c.Nome.Contains(viewModel.Colaborador.Nome));
            }

            if (!string.IsNullOrEmpty(viewModel.Colaborador.CPF))
            {
                colaboradores = colaboradores.Where(c => c.CPF == viewModel.Colaborador.CPF);
            }

            if (!viewModel.Colaborador.CargoId.Equals(0))
            {
                colaboradores = colaboradores.Where(c => c.CargoId == viewModel.Colaborador.CargoId);
            }

            #endregion

            ViewBag.Cargos = new SelectList(_context.Cargos
                                .Where(c => c.AgenciaId == _usuarioLogado.ObterClienteId())
                                .OrderBy(c => c.Descricao),
                                "CargoId", "Descricao");

            viewModel.Colaboradores = colaboradores;
            return View(viewModel);
        }

        #endregion

        #region Incluir

        public IActionResult Incluir()
        {
            ViewBag.Estados = new SelectList(Estado.UFs, "Sigla", "Descricao");
            ViewBag.Cargos  = new SelectList(_context.Cargos
                                        .Where(c => c.AgenciaId == _usuarioLogado.ObterClienteId())
                                        .OrderBy(c => c.Descricao), "CargoId", "Descricao");

            return View();
        }

        [HttpPost]
        public IActionResult Incluir(Colaborador colaborador)
        {
            if (ModelState.IsValid)
            {
                if (!_context.Colaboradores.Any(c => c.CPF == colaborador.CPF && c.AgenciaId == _usuarioLogado.ObterClienteId()))
                {
                    colaborador.AgenciaId = _usuarioLogado.ObterClienteId();

                    colaborador.Nome        = colaborador.Nome.ToUpper();
                    colaborador.Telefone    = colaborador.Telefone.Trim();
                    colaborador.Celular     = colaborador.Celular.Trim();

                    _context.Colaboradores.Add(colaborador);
                    _context.SaveChanges();

                    _notificacao.Success("Colaborador incluído com sucesso");
                    return RedirectToAction("Index");
                }
                else
                {
                    _notificacao.Warning("Já existe um colaborador com este CPF nesta agência");
                }
            }

            ViewBag.Estados = new SelectList(Estado.UFs, "Sigla", "Descricao");
            ViewBag.Cargos  = new SelectList(_context.Cargos
                                        .Where(c => c.AgenciaId == _usuarioLogado.ObterClienteId())
                                        .OrderBy(c => c.Descricao), "CargoId", "Descricao", colaborador.CargoId);

            return View(colaborador);
        }

        #endregion

        #region Alterar

        public IActionResult Alterar(int id)
        {
            Colaborador colaborador;

            if (User.IsInRole("Administradora"))
            {
                colaborador = _context.Colaboradores.Find(id);
            }
            else
            {
                colaborador = _context.Colaboradores.SingleOrDefault(c => c.ColaboradorId == id
                                                    && c.AgenciaId == _usuarioLogado.ObterClienteId());
            }

            if (colaborador == null)
            {
                _notificacao.Warning("Colaborador não pertence a esta agência");
                return RedirectToAction("Index");
            }

            ViewBag.Estados = new SelectList(Estado.UFs, "Sigla", "Descricao", colaborador.UF);
            ViewBag.Cargos  = new SelectList(_context.Cargos
                                        .Where(c => c.AgenciaId == colaborador.AgenciaId)
                                        .OrderBy(c => c.Descricao), "CargoId", "Descricao", colaborador.CargoId);

            return View(colaborador);
        }

        [HttpPost]
        public IActionResult Alterar(Colaborador form)
        {
            var colaborador = _context.Colaboradores.Find(form.ColaboradorId);

            if (ModelState.IsValid)
            {
                colaborador.CargoId             = form.CargoId;
                colaborador.Nome                = form.Nome.ToUpper();
                colaborador.CPF                 = form.CPF;
                colaborador.DataNascimento      = form.DataNascimento;
                colaborador.Email               = form.Email;
                colaborador.Rg                  = form.Rg;
                colaborador.RgOrgaoExpedidor    = form.RgOrgaoExpedidor;
                colaborador.RgUf                = form.RgUf;
                colaborador.RgDataExpedicao     = form.RgDataExpedicao;
                colaborador.Logradouro          = form.Logradouro;
                colaborador.Complemento         = form.Complemento;
                colaborador.Bairro              = form.Bairro;
                colaborador.Cidade              = form.Cidade;
                colaborador.CEP                 = form.CEP;
                colaborador.UF                  = form.UF;
                colaborador.DDDTelefone         = form.DDDTelefone;
                colaborador.Telefone            = form.Telefone.Trim();
                colaborador.DDDCelular          = form.DDDCelular;
                colaborador.Celular             = form.Celular.Trim();

                _context.SaveChanges();

                _notificacao.Success("Colaborador alterado com sucesso");
                return RedirectToAction("Index");
            }

            ViewBag.Estados = new SelectList(Estado.UFs, "Sigla", "Descricao", form.UF);
            ViewBag.Cargos  = new SelectList(_context.Cargos
                                        .Where(c => c.AgenciaId == colaborador.AgenciaId)
                                        .OrderBy(c => c.Descricao), "CargoId", "Descricao", form.CargoId);

            return View(form);
        }

        #endregion

       

    }
}
