﻿using Sig.Web.Models;
using Sig.Web.Models.ViewModels;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using AspNetCoreHero.ToastNotification.Abstractions;
using Sig.Web.Interfaces;

namespace Sig.Web.Areas.Agencias.Controllers
{
    [Authorize(Roles = "Administradora, Agencia")]
    public class RegiaoController : BaseController
    {
        private readonly IUsuarioLogado _usuarioLogado;

        public RegiaoController(INotyfService notificacao, IUsuarioLogado usuarioLogado) : base(notificacao)
        {
            _usuarioLogado = usuarioLogado;
        }

        #region Index

        public IActionResult Index()
        {
            var viewModel = new RegiaoViewModel
            {
                Regioes = _context.Regioes.Where(c => c.AgenciaId == _usuarioLogado.ObterClienteId())
            };

            return View(viewModel);
        }

        [HttpPost]
        public IActionResult Index(RegiaoViewModel viewModel)
        {
            IQueryable<Regiao> regioes = _context.Regioes.Where(c => c.AgenciaId == _usuarioLogado.ObterClienteId());

            #region Filtros

            if (!viewModel.Regiao.RegiaoId.Equals(0))
            {
                regioes = regioes.Where(c => c.RegiaoId == viewModel.Regiao.RegiaoId);
            }

            if (!string.IsNullOrEmpty(viewModel.Regiao.Descricao))
            {
                regioes = regioes.Where(c => c.Descricao.Contains(viewModel.Regiao.Descricao));
            }

            #endregion

            viewModel.Regioes = regioes;
            return View(viewModel);
        }

        #endregion

        #region Incluir

        public IActionResult Incluir()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Incluir(Regiao regiao)
        {
            if (ModelState.IsValid)
            {
                regiao.AgenciaId = _usuarioLogado.ObterClienteId();

                _context.Regioes.Add(regiao);
                _context.SaveChanges();

                _notificacao.Success("Região incluída com sucesso");
                return RedirectToAction("Index");
            }

            return View(regiao);
        }

        #endregion

        #region Alterar

        public IActionResult Alterar(int id)
        {
            var regiao = _context.Regioes.SingleOrDefault(c => c.RegiaoId == id && c.AgenciaId == _usuarioLogado.ObterClienteId());

            if (regiao == null)
            {
                _notificacao.Warning("Região não pertence a esta agência");
                return RedirectToAction("Index");
            }

            return View(regiao);
        }

        [HttpPost]
        public IActionResult Alterar(Regiao form)
        {
            var regiao = _context.Regioes.Find(form.RegiaoId);
            
            if (ModelState.IsValid)
            {
                regiao.Descricao = form.Descricao;
                _context.SaveChanges();

                _notificacao.Success("Região alterada com sucesso");
                return RedirectToAction("Index");
            }

            return View(form);
        }

        #endregion


    }
}
