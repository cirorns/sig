﻿using Sig.Web.Models;
using Sig.Web.Models.ViewModels;
using System;
using System.Linq;
using System.Text;
using System.Transactions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using AspNetCoreHero.ToastNotification.Abstractions;
using Sig.Web.Interfaces;
using Sig.Web.ViewModels.Agencias.Tabelas;
using Microsoft.EntityFrameworkCore;
using Dapper;

namespace Sig.Web.Areas.Agencias.Controllers
{
    [Authorize(Roles = "Administradora, Agencia")]
    public class TabelaController : BaseController
    {
        private readonly IUsuarioLogado _usuarioLogado;

        public TabelaController(INotyfService notificacao, IUsuarioLogado usuarioLogado) : base(notificacao)
        {
            _usuarioLogado = usuarioLogado;
        }

        #region Index

        //[Authorize(Roles = "Administradora")]
        public IActionResult Index()
        {
            var tabelas = _context.Tabelas.Where(c => c.IsAtivo).OrderByDescending(c => c.TabelaId);
            var viewModel = new TabelaPesquisaViewModel
            {
                Tabelas = tabelas
            };

            return View(viewModel);
        }

        [HttpPost]
        public IActionResult Index(TabelaPesquisaViewModel vm)
        {
            IQueryable<Tabela> tabelas = _context.Tabelas.Where(c => c.IsAtivo);

            if (!vm.Tabela.TabelaId.Equals(0))
            {
                tabelas = tabelas.Where(c => c.TabelaId == vm.Tabela.TabelaId);
            }

            if (!string.IsNullOrEmpty(vm.Tabela.Descricao))
            {
                tabelas = tabelas.Where(c => c.Descricao.Contains(vm.Tabela.Descricao));
            }

            if (vm.Tabela.IsConsumo)
            {
                tabelas = tabelas.Where(c => c.IsConsumo == true);
            }

            if (vm.Tabela.IsEspecial)
            {
                tabelas = tabelas.Where(c => c.IsEspecial == true);
            }

            if (vm.ClienteId.HasValue)
            {
                tabelas = tabelas.Where(c => c.Contratos.Any(d => d.ClienteId == vm.ClienteId));
            }

            vm.Tabelas = tabelas;
            return View(vm);
        }

        #endregion

        #region Index Agência

        [Authorize(Roles = "Agencia")]
        public IActionResult IndexAgencia()
        {
            var tabelasAgencia = _context.TabelaAgencias.Where(c => c.AgenciaId == _usuarioLogado.ObterClienteId() && c.Tabela.IsAtivo).OrderBy(c => c.TabelaId);
            var viewModel = new IndexAgenciaViewModel
            {
                TabelasAgencia = tabelasAgencia
            };

            return View(viewModel);
        }

        [HttpPost]
        public IActionResult IndexAgencia(IndexAgenciaViewModel vm)
        {
            IQueryable<TabelaAgencia> tabelasAgencia = _context.TabelaAgencias.Where(c => c.AgenciaId == _usuarioLogado.ObterClienteId() && c.Tabela.IsAtivo);

            if (!vm.Tabela.TabelaId.Equals(0))
            {
                tabelasAgencia = tabelasAgencia.Where(c => c.TabelaId == vm.Tabela.TabelaId);
            }

            if (!string.IsNullOrEmpty(vm.Tabela.Descricao))
            {
                tabelasAgencia = tabelasAgencia.Where(c => c.Tabela.Descricao.Contains(vm.Tabela.Descricao));
            }

            if (vm.Tabela.IsConsumo)
            {
                tabelasAgencia = tabelasAgencia.Where(c => c.Tabela.IsConsumo == true);
            }

            vm.TabelasAgencia = tabelasAgencia;
            return View(vm);
        }

        #endregion

        #region Tabelas Inativas

        public IActionResult Inativa()
        {
            var tabelas = _context.Tabelas.Where(c => !c.IsAtivo).OrderByDescending(c => c.TabelaId);
            var viewModel = new TabelaPesquisaViewModel
            {
                Tabelas = tabelas
            };

            return View(viewModel);
        }

        [HttpPost]
        public IActionResult Inativa(TabelaPesquisaViewModel viewModel)
        {
            IQueryable<Tabela> tabelas = _context.Tabelas.Where(c => !c.IsAtivo);

            if (!viewModel.Tabela.TabelaId.Equals(0))
            {
                tabelas = tabelas.Where(c => c.TabelaId == viewModel.Tabela.TabelaId);
            }

            if (!string.IsNullOrEmpty(viewModel.Tabela.Descricao))
            {
                tabelas = tabelas.Where(c => c.Descricao.Contains(viewModel.Tabela.Descricao));
            }

            if (viewModel.Tabela.IsConsumo)
            {
                tabelas = tabelas.Where(c => c.IsConsumo == true);
            }

            viewModel.Tabelas = tabelas;
            return View(viewModel);
        }

        #endregion

        #region Copiar

        public IActionResult Copiar(int id = 0)
        {
            if (!User.IsInRole("Administradora"))
            {
                if (!_context.TabelaAgencias.Any(c => c.TabelaId == id && c.AgenciaId == _usuarioLogado.ObterClienteId()))
                {
                    _notificacao.Warning("Esta tabela não pertence a esta agência");
                    return RedirectToAction("IndexAgencia");
                }
            }

            #region SQL

            StringBuilder sql = new();
            sql.Append("SELECT \n");
            sql.Append("                  ( \n");
            sql.Append("                           CASE \n");
            sql.Append("                                    WHEN MAX(IsAtivo) = 1 \n");
            sql.Append("                                             THEN true \n");
            sql.Append("                                             ELSE false \n");
            sql.Append("                           END \n");
            sql.Append("                  ) \n");
            sql.Append("                       AS \"IsAtivo\" \n");
            sql.Append("       , MAX(TabelaId) AS \"TabelaId\" \n");
            sql.Append("       , \"ItemFaturavelId\" \n");
            sql.Append("       , \"Descricao\" \n");
            sql.Append("       , CASE \n");
            sql.Append("                  WHEN MAX(IsAtivo) = 1 \n");
            sql.Append("                           THEN MAX(ValorUnitario2) \n");
            sql.Append("                           ELSE MAX(ValorUnitario1) \n");
            sql.Append("         END                   AS \"ValorUnitario\" \n");
            sql.Append("       , MAX(\"ValorMinimo\")    AS \"ValorMinimo\" \n");
            sql.Append("       , MAX(\"ValorMaximo\")    AS \"ValorMaximo\" \n");
            sql.Append("       , MAX(QuantidadePacote) AS \"QuantidadePacote\" \n");
            sql.Append("       , IsPacote              as \"IsPacote\" \n");
            sql.Append("FROM \n");
            sql.Append("         ( \n");
            sql.Append("                SELECT \n");
            sql.Append("                       0 AS IsAtivo \n");
            sql.Append("                     , 0 AS TabelaId \n");
            sql.Append("                     , \"ItemFaturavelId\" \n");
            sql.Append("                     , \"Descricao\" \n");
            sql.Append("                     , \"ValorUnitario\" AS ValorUnitario1 \n");
            sql.Append("                     , 0               AS ValorUnitario2 \n");
            sql.Append("                     , \"ValorMinimo\" \n");
            sql.Append("                     , \"ValorMaximo\" \n");
            sql.Append("                     , 0 AS QuantidadePacote \n");
            sql.Append("                     , CASE \n");
            sql.Append("                              WHEN \"IsPacote\" \n");
            sql.Append("                                     THEN 1 \n");
            sql.Append("                                     ELSE 0 \n");
            sql.Append("                       END as IsPacote \n");
            sql.Append("                FROM \n");
            sql.Append("                       \"ItemFaturavel\" \n");
            sql.Append("                WHERE \n");
            sql.Append("                       \"IsAtivo\" \n");
            sql.Append("                UNION ALL \n");
            sql.Append("                SELECT \n");
            sql.Append("                           1 AS IsAtivo \n");
            sql.Append("                         , TI.\"TabelaId\" \n");
            sql.Append("                         , TI.\"ItemFaturavelId\" \n");
            sql.Append("                         , I.\"Descricao\" \n");
            sql.Append("                         , 0                  AS ValorUnitario1 \n");
            sql.Append("                         , TI.\"ValorUnitario\" AS ValorUnitario2 \n");
            sql.Append("                         , I.\"ValorMinimo\" \n");
            sql.Append("                         , I.\"ValorMaximo\" \n");
            sql.Append("                         , TI.\"QuantidadePacote\" \n");
            sql.Append("                         , CASE \n");
            sql.Append("                                      WHEN I.\"IsPacote\" \n");
            sql.Append("                                                 THEN 1 \n");
            sql.Append("                                                 ELSE 0 \n");
            sql.Append("                           END as IsPacote \n");
            sql.Append("                FROM \n");
            sql.Append("                           \"TabelaItem\" TI \n");
            sql.Append("                           INNER JOIN \n");
            sql.Append("                                      \"ItemFaturavel\" I \n");
            sql.Append("                                      ON \n");
            sql.Append("                                                 TI.\"ItemFaturavelId\" = I.\"ItemFaturavelId\" \n");
            sql.Append("                WHERE \n");
            sql.Append("                           TI.\"TabelaId\" = 0 \n");
            sql.Append("         ) \n");
            sql.Append("         \"TMP\" \n");
            sql.Append("GROUP BY \n");
            sql.Append("         \"ItemFaturavelId\" \n");
            sql.Append("       , \"Descricao\" \n");
            sql.Append("       , IsPacote");

            #endregion

            var tabela = _context.Tabelas.Find(id);
            var viewModel = new TabelaViewModel();

            try
            {
                if (tabela != null)
                {
                    viewModel.Descricao = "";
                    viewModel.IsConsumo = tabela.IsConsumo;
                    viewModel.TabelaId = tabela.TabelaId;
                    viewModel.TabelaItens = _context.Database.GetDbConnection().Query<TabelaItemViewModel>(sql.ToString(), id).ToList();
                }
                else
                {
                    _notificacao.Warning("Não existe esta tabela");
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                _notificacao.Error(ex.Message);
                return RedirectToAction("Index");
            }

            return View(viewModel);
        }

        [HttpPost]
        public IActionResult Copiar(TabelaViewModel viewModel)
        {
            bool isAdicionado = false;

            using (var ts = new TransactionScope())
            {
                try
                {
                    var tabela = new Tabela
                    {
                        Descricao = viewModel.Descricao,
                        IsConsumo = viewModel.IsConsumo,
                        IsAtivo = true,
                    };

                    _context.Tabelas.Add(tabela);
                    _context.SaveChanges();

                    foreach (var ti in viewModel.TabelaItens)
                    {
                        if (ti.IsAtivo)
                        {
                            TabelaItem tabelaItem = new()
                            {
                                ItemFaturavelId = ti.ItemFaturavelId,
                                QuantidadePacote = ti.QuantidadePacote,
                                TabelaId = tabela.TabelaId,
                                ValorUnitario = ti.ValorUnitario,
                            };

                            _context.TabelaItens.Add(tabelaItem);
                            _context.SaveChanges();
                            isAdicionado = true;
                        }
                    }

                    //Adicionando tabela para a Administradora
                    var tabelaAgencia = new TabelaAgencia
                    {
                        AgenciaId = _usuarioLogado.ObterClienteId(),
                        TabelaId = tabela.TabelaId,
                    };

                    _context.TabelaAgencias.Add(tabelaAgencia);
                    _context.SaveChanges();

                    if (isAdicionado)
                    {
                        _notificacao.Success("Tabela criada com sucesso");
                        ts.Complete();
                    }
                    else
                    {
                        _notificacao.Warning("É necessário ao menos um Item Faturável");
                        ts.Dispose();
                    }
                }
                catch (Exception ex)
                {
                    _notificacao.Error(ex.Message);
                    ts.Dispose();
                }
            }

            if (User.IsInRole("Administradora"))
                return RedirectToAction("Index");
            else
                return RedirectToAction("IndexAgencia");
        }

        #endregion

        #region Incluir

        public IActionResult Incluir()
        {
            #region SQL

            StringBuilder sql = new();
            sql.Append("SELECT \n");
            sql.Append("                  ( \n");
            sql.Append("                           CASE \n");
            sql.Append("                                    WHEN MAX(IsAtivo) = 1 \n");
            sql.Append("                                             THEN true \n");
            sql.Append("                                             ELSE false \n");
            sql.Append("                           END \n");
            sql.Append("                  ) \n");
            sql.Append("                       AS \"IsAtivo\" \n");
            sql.Append("       , MAX(TabelaId) AS \"TabelaId\" \n");
            sql.Append("       , \"ItemFaturavelId\" \n");
            sql.Append("       , \"Descricao\" \n");
            sql.Append("       , CASE \n");
            sql.Append("                  WHEN MAX(IsAtivo) = 1 \n");
            sql.Append("                           THEN MAX(ValorUnitario2) \n");
            sql.Append("                           ELSE MAX(ValorUnitario1) \n");
            sql.Append("         END                   AS \"ValorUnitario\" \n");
            sql.Append("       , MAX(\"ValorMinimo\")    AS \"ValorMinimo\" \n");
            sql.Append("       , MAX(\"ValorMaximo\")    AS \"ValorMaximo\" \n");
            sql.Append("       , MAX(QuantidadePacote) AS \"QuantidadePacote\" \n");
            sql.Append("       , IsPacote              as \"IsPacote\" \n");
            sql.Append("FROM \n");
            sql.Append("         ( \n");
            sql.Append("                SELECT \n");
            sql.Append("                       0 AS IsAtivo \n");
            sql.Append("                     , 0 AS TabelaId \n");
            sql.Append("                     , \"ItemFaturavelId\" \n");
            sql.Append("                     , \"Descricao\" \n");
            sql.Append("                     , \"ValorUnitario\" AS ValorUnitario1 \n");
            sql.Append("                     , 0               AS ValorUnitario2 \n");
            sql.Append("                     , \"ValorMinimo\" \n");
            sql.Append("                     , \"ValorMaximo\" \n");
            sql.Append("                     , 0 AS QuantidadePacote \n");
            sql.Append("                     , CASE \n");
            sql.Append("                              WHEN \"IsPacote\" \n");
            sql.Append("                                     THEN 1 \n");
            sql.Append("                                     ELSE 0 \n");
            sql.Append("                       END as IsPacote \n");
            sql.Append("                FROM \n");
            sql.Append("                       \"ItemFaturavel\" \n");
            sql.Append("                WHERE \n");
            sql.Append("                       \"IsAtivo\" \n");
            sql.Append("                UNION ALL \n");
            sql.Append("                SELECT \n");
            sql.Append("                           1 AS IsAtivo \n");
            sql.Append("                         , TI.\"TabelaId\" \n");
            sql.Append("                         , TI.\"ItemFaturavelId\" \n");
            sql.Append("                         , I.\"Descricao\" \n");
            sql.Append("                         , 0                  AS ValorUnitario1 \n");
            sql.Append("                         , TI.\"ValorUnitario\" AS ValorUnitario2 \n");
            sql.Append("                         , I.\"ValorMinimo\" \n");
            sql.Append("                         , I.\"ValorMaximo\" \n");
            sql.Append("                         , TI.\"QuantidadePacote\" \n");
            sql.Append("                         , CASE \n");
            sql.Append("                                      WHEN I.\"IsPacote\" \n");
            sql.Append("                                                 THEN 1 \n");
            sql.Append("                                                 ELSE 0 \n");
            sql.Append("                           END as IsPacote \n");
            sql.Append("                FROM \n");
            sql.Append("                           \"TabelaItem\" TI \n");
            sql.Append("                           INNER JOIN \n");
            sql.Append("                                      \"ItemFaturavel\" I \n");
            sql.Append("                                      ON \n");
            sql.Append("                                                 TI.\"ItemFaturavelId\" = I.\"ItemFaturavelId\" \n");
            sql.Append("                WHERE \n");
            sql.Append("                           TI.\"TabelaId\" = 0 \n");
            sql.Append("         ) \n");
            sql.Append("         \"TMP\" \n");
            sql.Append("GROUP BY \n");
            sql.Append("         \"ItemFaturavelId\" \n");
            sql.Append("       , \"Descricao\" \n");
            sql.Append("       , IsPacote");

            #endregion

            var viewModel = new TabelaViewModel
            {
                TabelaItens = _context.Database.GetDbConnection().Query<TabelaItemViewModel>(sql.ToString()).ToList()
            };

            return View(viewModel);
        }

        [HttpPost]
        public IActionResult Incluir(TabelaViewModel viewModel)
        {
            bool isAdicionado = false;

            using (var ts = new TransactionScope())
            {
                try
                {
                    var tabela = new Tabela
                    {
                        Descricao = viewModel.Descricao,
                        IsConsumo = viewModel.IsConsumo,
                        IsAtivo = true,
                    };

                    _context.Tabelas.Add(tabela);
                    _context.SaveChanges();

                    foreach (var ti in viewModel.TabelaItens)
                    {
                        if (ti.IsAtivo)
                        {
                            TabelaItem tabelaItem = new TabelaItem
                            {
                                ItemFaturavelId = ti.ItemFaturavelId,
                                QuantidadePacote = ti.QuantidadePacote,
                                TabelaId = tabela.TabelaId,
                                ValorUnitario = ti.ValorUnitario,
                            };

                            _context.TabelaItens.Add(tabelaItem);
                            _context.SaveChanges();
                            isAdicionado = true;
                        }
                    }

                    //Adicionando tabela para a Administradora
                    var tabelaAgencia = new TabelaAgencia
                    {
                        AgenciaId = _usuarioLogado.ObterClienteId(),
                        TabelaId = tabela.TabelaId,
                    };

                    _context.TabelaAgencias.Add(tabelaAgencia);
                    _context.SaveChanges();

                    if (isAdicionado)
                    {
                        _notificacao.Success("Tabela criada com sucesso");
                        ts.Complete();
                    }
                    else
                    {
                        _notificacao.Warning("É necessário ao menos um Item Faturável");
                        ts.Dispose();
                    }
                }
                catch (Exception ex)
                {
                    _notificacao.Error(ex.Message);
                    ts.Dispose();
                }
            }

            if (User.IsInRole("Administradora"))
                return RedirectToAction("Index");
            else
                return RedirectToAction("IndexAgencia");
        }

        #endregion

        #region Alterar

        public IActionResult Alterar(int id = 0)
        {
            if (!User.IsInRole("Administradora"))
            {
                if (!_context.TabelaAgencias.Any(c => c.TabelaId == id && c.AgenciaId == _usuarioLogado.ObterClienteId()))
                {
                    _notificacao.Warning("Esta tabela não pertence a esta agência");
                    return RedirectToAction("IndexAgencia");
                }
            }


            #region SQL

            StringBuilder sql = new();
            sql.Append("SELECT \n");
            sql.Append("                  ( \n");
            sql.Append("                           CASE \n");
            sql.Append("                                    WHEN MAX(IsAtivo) = 1 \n");
            sql.Append("                                             THEN true \n");
            sql.Append("                                             ELSE false \n");
            sql.Append("                           END \n");
            sql.Append("                  ) \n");
            sql.Append("                       AS \"IsAtivo\" \n");
            sql.Append("       , MAX(TabelaId) AS \"TabelaId\" \n");
            sql.Append("       , \"ItemFaturavelId\" \n");
            sql.Append("       , \"Descricao\" \n");
            sql.Append("       , CASE \n");
            sql.Append("                  WHEN MAX(IsAtivo) = 1 \n");
            sql.Append("                           THEN MAX(ValorUnitario2) \n");
            sql.Append("                           ELSE MAX(ValorUnitario1) \n");
            sql.Append("         END                   AS \"ValorUnitario\" \n");
            sql.Append("       , MAX(\"ValorMinimo\")    AS \"ValorMinimo\" \n");
            sql.Append("       , MAX(\"ValorMaximo\")    AS \"ValorMaximo\" \n");
            sql.Append("       , MAX(QuantidadePacote) AS \"QuantidadePacote\" \n");
            sql.Append("       , IsPacote              as \"IsPacote\" \n");
            sql.Append("FROM \n");
            sql.Append("         ( \n");
            sql.Append("                SELECT \n");
            sql.Append("                       0 AS IsAtivo \n");
            sql.Append("                     , 0 AS TabelaId \n");
            sql.Append("                     , \"ItemFaturavelId\" \n");
            sql.Append("                     , \"Descricao\" \n");
            sql.Append("                     , \"ValorUnitario\" AS ValorUnitario1 \n");
            sql.Append("                     , 0               AS ValorUnitario2 \n");
            sql.Append("                     , \"ValorMinimo\" \n");
            sql.Append("                     , \"ValorMaximo\" \n");
            sql.Append("                     , 0 AS QuantidadePacote \n");
            sql.Append("                     , CASE \n");
            sql.Append("                              WHEN \"IsPacote\" \n");
            sql.Append("                                     THEN 1 \n");
            sql.Append("                                     ELSE 0 \n");
            sql.Append("                       END as IsPacote \n");
            sql.Append("                FROM \n");
            sql.Append("                       \"ItemFaturavel\" \n");
            sql.Append("                WHERE \n");
            sql.Append("                       \"IsAtivo\" \n");
            sql.Append("                UNION ALL \n");
            sql.Append("                SELECT \n");
            sql.Append("                           1 AS IsAtivo \n");
            sql.Append("                         , TI.\"TabelaId\" \n");
            sql.Append("                         , TI.\"ItemFaturavelId\" \n");
            sql.Append("                         , I.\"Descricao\" \n");
            sql.Append("                         , 0                  AS ValorUnitario1 \n");
            sql.Append("                         , TI.\"ValorUnitario\" AS ValorUnitario2 \n");
            sql.Append("                         , I.\"ValorMinimo\" \n");
            sql.Append("                         , I.\"ValorMaximo\" \n");
            sql.Append("                         , TI.\"QuantidadePacote\" \n");
            sql.Append("                         , CASE \n");
            sql.Append("                                      WHEN I.\"IsPacote\" \n");
            sql.Append("                                                 THEN 1 \n");
            sql.Append("                                                 ELSE 0 \n");
            sql.Append("                           END as IsPacote \n");
            sql.Append("                FROM \n");
            sql.Append("                           \"TabelaItem\" TI \n");
            sql.Append("                           INNER JOIN \n");
            sql.Append("                                      \"ItemFaturavel\" I \n");
            sql.Append("                                      ON \n");
            sql.Append("                                                 TI.\"ItemFaturavelId\" = I.\"ItemFaturavelId\" \n");
            sql.Append("                WHERE \n");
            sql.Append("                           TI.\"TabelaId\" = 0 \n");
            sql.Append("         ) \n");
            sql.Append("         \"TMP\" \n");
            sql.Append("GROUP BY \n");
            sql.Append("         \"ItemFaturavelId\" \n");
            sql.Append("       , \"Descricao\" \n");
            sql.Append("       , IsPacote");

            #endregion

            var tabela = _context.Tabelas.Find(id);
            var viewModel = new TabelaViewModel();

            try
            {
                if (tabela != null)
                {
                    viewModel.Descricao = tabela.Descricao;
                    viewModel.IsEspecial = tabela.IsEspecial;
                    viewModel.IsConsumo = tabela.IsConsumo;
                    viewModel.IsTabelaAtiva = tabela.IsAtivo;
                    viewModel.TabelaId = tabela.TabelaId;
                    viewModel.TabelaItens = _context.Database.GetDbConnection().Query<TabelaItemViewModel>(sql.ToString(), id).ToList();
                }
                else
                {
                    _notificacao.Warning("Não existe esta tabela");
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                _notificacao.Warning(ex.Message);
                return RedirectToAction("Index");
            }

            return View(viewModel);
        }

        [HttpPost]
        public IActionResult Alterar(TabelaViewModel viewModel)
        {
            bool isExiste;

            try
            {
                foreach (var ti in viewModel.TabelaItens)
                {
                    var tabelaItemDB = _context.TabelaItens.SingleOrDefault(c => c.ItemFaturavelId == ti.ItemFaturavelId && c.TabelaId == viewModel.TabelaId);
                    isExiste = tabelaItemDB != null;

                    if (ti.IsAtivo)
                    {
                        if (!isExiste)
                        {
                            TabelaItem tabelaItem = new TabelaItem
                            {
                                ItemFaturavelId = ti.ItemFaturavelId,
                                QuantidadePacote = ti.QuantidadePacote,
                                TabelaId = viewModel.TabelaId,
                                ValorUnitario = ti.ValorUnitario,
                            };

                            _context.TabelaItens.Add(tabelaItem);
                            _context.SaveChanges();
                        }
                        else
                        {
                            tabelaItemDB.QuantidadePacote = ti.QuantidadePacote;
                            tabelaItemDB.ValorUnitario = ti.ValorUnitario;
                            _context.SaveChanges();
                        }
                    }
                    else
                    {
                        if (isExiste)
                        {
                            _context.TabelaItens.Remove(tabelaItemDB);
                            _context.SaveChanges();
                        }
                    }
                }

                var tabela = _context.Tabelas.Find(viewModel.TabelaId);
                tabela.IsConsumo = viewModel.IsConsumo;
                tabela.IsAtivo = viewModel.IsTabelaAtiva;
                tabela.IsEspecial = viewModel.IsEspecial;
                tabela.Descricao = viewModel.Descricao.ToUpper();

                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                _notificacao.Warning(ex.Message);
            }

            _notificacao.Success(string.Format("Tabela: {0} alterada com sucesso.", viewModel.Descricao.ToUpper()));

            if (User.IsInRole("Administradora"))
                return RedirectToAction("Index");
            else
                return RedirectToAction("IndexAgencia");
        }

        #endregion

        #region Incluir - Tabela Especial

        [Authorize(Roles = "Administradora")]
        public IActionResult Especial()
        {
            #region SQL

            StringBuilder sql = new();
            sql.Append("SELECT \n");
            sql.Append("                  ( \n");
            sql.Append("                           CASE \n");
            sql.Append("                                    WHEN MAX(IsAtivo) = 1 \n");
            sql.Append("                                             THEN true \n");
            sql.Append("                                             ELSE false \n");
            sql.Append("                           END \n");
            sql.Append("                  ) \n");
            sql.Append("                       AS \"IsAtivo\" \n");
            sql.Append("       , MAX(TabelaId) AS \"TabelaId\" \n");
            sql.Append("       , \"ItemFaturavelId\" \n");
            sql.Append("       , \"Descricao\" \n");
            sql.Append("       , CASE \n");
            sql.Append("                  WHEN MAX(IsAtivo) = 1 \n");
            sql.Append("                           THEN MAX(ValorUnitario2) \n");
            sql.Append("                           ELSE MAX(ValorUnitario1) \n");
            sql.Append("         END                   AS \"ValorUnitario\" \n");
            sql.Append("       , MAX(\"ValorMinimo\")    AS \"ValorMinimo\" \n");
            sql.Append("       , MAX(\"ValorMaximo\")    AS \"ValorMaximo\" \n");
            sql.Append("       , MAX(QuantidadePacote) AS \"QuantidadePacote\" \n");
            sql.Append("       , IsPacote              as \"IsPacote\" \n");
            sql.Append("FROM \n");
            sql.Append("         ( \n");
            sql.Append("                SELECT \n");
            sql.Append("                       0 AS IsAtivo \n");
            sql.Append("                     , 0 AS TabelaId \n");
            sql.Append("                     , \"ItemFaturavelId\" \n");
            sql.Append("                     , \"Descricao\" \n");
            sql.Append("                     , \"ValorUnitario\" AS ValorUnitario1 \n");
            sql.Append("                     , 0               AS ValorUnitario2 \n");
            sql.Append("                     , \"ValorMinimo\" \n");
            sql.Append("                     , \"ValorMaximo\" \n");
            sql.Append("                     , 0 AS QuantidadePacote \n");
            sql.Append("                     , CASE \n");
            sql.Append("                              WHEN \"IsPacote\" \n");
            sql.Append("                                     THEN 1 \n");
            sql.Append("                                     ELSE 0 \n");
            sql.Append("                       END as IsPacote \n");
            sql.Append("                FROM \n");
            sql.Append("                       \"ItemFaturavel\" \n");
            sql.Append("                WHERE \n");
            sql.Append("                       \"IsAtivo\" \n");
            sql.Append("                UNION ALL \n");
            sql.Append("                SELECT \n");
            sql.Append("                           1 AS IsAtivo \n");
            sql.Append("                         , TI.\"TabelaId\" \n");
            sql.Append("                         , TI.\"ItemFaturavelId\" \n");
            sql.Append("                         , I.\"Descricao\" \n");
            sql.Append("                         , 0                  AS ValorUnitario1 \n");
            sql.Append("                         , TI.\"ValorUnitario\" AS ValorUnitario2 \n");
            sql.Append("                         , I.\"ValorMinimo\" \n");
            sql.Append("                         , I.\"ValorMaximo\" \n");
            sql.Append("                         , TI.\"QuantidadePacote\" \n");
            sql.Append("                         , CASE \n");
            sql.Append("                                      WHEN I.\"IsPacote\" \n");
            sql.Append("                                                 THEN 1 \n");
            sql.Append("                                                 ELSE 0 \n");
            sql.Append("                           END as IsPacote \n");
            sql.Append("                FROM \n");
            sql.Append("                           \"TabelaItem\" TI \n");
            sql.Append("                           INNER JOIN \n");
            sql.Append("                                      \"ItemFaturavel\" I \n");
            sql.Append("                                      ON \n");
            sql.Append("                                                 TI.\"ItemFaturavelId\" = I.\"ItemFaturavelId\" \n");
            sql.Append("                WHERE \n");
            sql.Append("                           TI.\"TabelaId\" = 0 \n");
            sql.Append("         ) \n");
            sql.Append("         \"TMP\" \n");
            sql.Append("GROUP BY \n");
            sql.Append("         \"ItemFaturavelId\" \n");
            sql.Append("       , \"Descricao\" \n");
            sql.Append("       , IsPacote");

            #endregion

            var viewModel = new TabelaViewModel
            {
                TabelaItens = _context.Database.GetDbConnection().Query<TabelaItemViewModel>(sql.ToString()).ToList()
            };

            return View(viewModel);
        }

        [HttpPost]
        public IActionResult Especial(TabelaViewModel viewModel)
        {
            bool isAdicionado = false;

            using (var ts = new TransactionScope())
            {
                try
                {
                    var tabela = new Tabela
                    {
                        Descricao = viewModel.Descricao,
                        IsConsumo = viewModel.IsConsumo,
                        IsEspecial = true,
                        IsAtivo = true,
                    };

                    _context.Tabelas.Add(tabela);
                    _context.SaveChanges();

                    foreach (var ti in viewModel.TabelaItens)
                    {
                        if (ti.IsAtivo)
                        {
                            TabelaItem tabelaItem = new TabelaItem
                            {
                                ItemFaturavelId = ti.ItemFaturavelId,
                                QuantidadePacote = ti.QuantidadePacote,
                                TabelaId = tabela.TabelaId,
                                ValorUnitario = ti.ValorUnitario,
                            };

                            _context.TabelaItens.Add(tabelaItem);
                            _context.SaveChanges();
                            isAdicionado = true;
                        }
                    }

                    //Adicionando tabela para a Administradora
                    var tabelaAgencia = new TabelaAgencia
                    {
                        AgenciaId = _usuarioLogado.ObterClienteId(),
                        TabelaId = tabela.TabelaId,
                    };

                    _context.TabelaAgencias.Add(tabelaAgencia);
                    _context.SaveChanges();

                    if (isAdicionado)
                    {
                        _notificacao.Success("Tabela criada com sucesso");
                        ts.Complete();
                    }
                    else
                    {
                        _notificacao.Warning("É necessário ao menos um Item Faturável");
                        ts.Dispose();
                    }
                }
                catch (Exception ex)
                {
                    _notificacao.Error(ex.Message);
                    ts.Dispose();
                }
            }

            return RedirectToAction("Index");
        }

        #endregion

        #region Alterar - Tabela Especial

        [Authorize(Roles = "Administradora")]
        public IActionResult AlterarEspecial(int id = 0)
        {
            #region SQL

            StringBuilder sql = new();
            sql.Append("SELECT \n");
            sql.Append("                  ( \n");
            sql.Append("                           CASE \n");
            sql.Append("                                    WHEN MAX(IsAtivo) = 1 \n");
            sql.Append("                                             THEN true \n");
            sql.Append("                                             ELSE false \n");
            sql.Append("                           END \n");
            sql.Append("                  ) \n");
            sql.Append("                       AS \"IsAtivo\" \n");
            sql.Append("       , MAX(TabelaId) AS \"TabelaId\" \n");
            sql.Append("       , \"ItemFaturavelId\" \n");
            sql.Append("       , \"Descricao\" \n");
            sql.Append("       , CASE \n");
            sql.Append("                  WHEN MAX(IsAtivo) = 1 \n");
            sql.Append("                           THEN MAX(ValorUnitario2) \n");
            sql.Append("                           ELSE MAX(ValorUnitario1) \n");
            sql.Append("         END                   AS \"ValorUnitario\" \n");
            sql.Append("       , MAX(\"ValorMinimo\")    AS \"ValorMinimo\" \n");
            sql.Append("       , MAX(\"ValorMaximo\")    AS \"ValorMaximo\" \n");
            sql.Append("       , MAX(QuantidadePacote) AS \"QuantidadePacote\" \n");
            sql.Append("       , IsPacote              as \"IsPacote\" \n");
            sql.Append("FROM \n");
            sql.Append("         ( \n");
            sql.Append("                SELECT \n");
            sql.Append("                       0 AS IsAtivo \n");
            sql.Append("                     , 0 AS TabelaId \n");
            sql.Append("                     , \"ItemFaturavelId\" \n");
            sql.Append("                     , \"Descricao\" \n");
            sql.Append("                     , \"ValorUnitario\" AS ValorUnitario1 \n");
            sql.Append("                     , 0               AS ValorUnitario2 \n");
            sql.Append("                     , \"ValorMinimo\" \n");
            sql.Append("                     , \"ValorMaximo\" \n");
            sql.Append("                     , 0 AS QuantidadePacote \n");
            sql.Append("                     , CASE \n");
            sql.Append("                              WHEN \"IsPacote\" \n");
            sql.Append("                                     THEN 1 \n");
            sql.Append("                                     ELSE 0 \n");
            sql.Append("                       END as IsPacote \n");
            sql.Append("                FROM \n");
            sql.Append("                       \"ItemFaturavel\" \n");
            sql.Append("                WHERE \n");
            sql.Append("                       \"IsAtivo\" \n");
            sql.Append("                UNION ALL \n");
            sql.Append("                SELECT \n");
            sql.Append("                           1 AS IsAtivo \n");
            sql.Append("                         , TI.\"TabelaId\" \n");
            sql.Append("                         , TI.\"ItemFaturavelId\" \n");
            sql.Append("                         , I.\"Descricao\" \n");
            sql.Append("                         , 0                  AS ValorUnitario1 \n");
            sql.Append("                         , TI.\"ValorUnitario\" AS ValorUnitario2 \n");
            sql.Append("                         , I.\"ValorMinimo\" \n");
            sql.Append("                         , I.\"ValorMaximo\" \n");
            sql.Append("                         , TI.\"QuantidadePacote\" \n");
            sql.Append("                         , CASE \n");
            sql.Append("                                      WHEN I.\"IsPacote\" \n");
            sql.Append("                                                 THEN 1 \n");
            sql.Append("                                                 ELSE 0 \n");
            sql.Append("                           END as IsPacote \n");
            sql.Append("                FROM \n");
            sql.Append("                           \"TabelaItem\" TI \n");
            sql.Append("                           INNER JOIN \n");
            sql.Append("                                      \"ItemFaturavel\" I \n");
            sql.Append("                                      ON \n");
            sql.Append("                                                 TI.\"ItemFaturavelId\" = I.\"ItemFaturavelId\" \n");
            sql.Append("                WHERE \n");
            sql.Append("                           TI.\"TabelaId\" = 0 \n");
            sql.Append("         ) \n");
            sql.Append("         \"TMP\" \n");
            sql.Append("GROUP BY \n");
            sql.Append("         \"ItemFaturavelId\" \n");
            sql.Append("       , \"Descricao\" \n");
            sql.Append("       , IsPacote");

            #endregion

            var tabela = _context.Tabelas.Find(id);
            var viewModel = new TabelaViewModel();

            try
            {
                if (tabela != null)
                {
                    viewModel.Descricao = tabela.Descricao;
                    viewModel.IsConsumo = tabela.IsConsumo;
                    viewModel.TabelaId = tabela.TabelaId;
                    viewModel.IsEspecial = tabela.IsEspecial;
                    viewModel.IsTabelaAtiva = tabela.IsAtivo;
                    viewModel.TabelaItens = _context.Database.GetDbConnection().Query<TabelaItemViewModel>(sql.ToString(), id).ToList();
                }
                else
                {
                    _notificacao.Warning("Não existe esta tabela");
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                _notificacao.Error(ex.Message);
                return RedirectToAction("Index");
            }

            return View(viewModel);
        }

        [HttpPost]
        public IActionResult AlterarEspecial(TabelaViewModel viewModel)
        {
            bool isExiste;

            try
            {
                foreach (var ti in viewModel.TabelaItens)
                {
                    var tabelaItemDB = _context.TabelaItens.SingleOrDefault(c => c.ItemFaturavelId == ti.ItemFaturavelId && c.TabelaId == viewModel.TabelaId);
                    isExiste = tabelaItemDB != null;

                    if (ti.IsAtivo)
                    {
                        if (!isExiste)
                        {
                            TabelaItem tabelaItem = new TabelaItem
                            {
                                ItemFaturavelId = ti.ItemFaturavelId,
                                QuantidadePacote = ti.QuantidadePacote,
                                TabelaId = viewModel.TabelaId,
                                ValorUnitario = ti.ValorUnitario,
                            };

                            _context.TabelaItens.Add(tabelaItem);
                            _context.SaveChanges();
                        }
                        else
                        {
                            tabelaItemDB.QuantidadePacote = ti.QuantidadePacote;
                            tabelaItemDB.ValorUnitario = ti.ValorUnitario;
                            _context.SaveChanges();
                        }
                    }
                    else
                    {
                        if (isExiste)
                        {
                            _context.TabelaItens.Remove(tabelaItemDB);
                            _context.SaveChanges();
                        }
                    }
                }

                var tabela = _context.Tabelas.Find(viewModel.TabelaId);
                tabela.IsConsumo = viewModel.IsConsumo;
                tabela.IsAtivo = viewModel.IsTabelaAtiva;
                tabela.IsEspecial = viewModel.IsEspecial;
                tabela.Descricao = viewModel.Descricao.ToUpper();

                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                _notificacao.Warning(ex.Message);
            }

            _notificacao.Success(string.Format("Tabela: {0} alterada com sucesso.", viewModel.Descricao.ToUpper()));
            return RedirectToAction("Index");
        }

        #endregion

        #region Reajuste

        public IActionResult Reajuste()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Reajuste(ReajusteViewModel vm)
        {
            if (ModelState.IsValid)
            {
                Tabela tabela = null;

                if (User.IsInRole("Administradora"))
                    tabela = _context.Tabelas.Find(vm.TabelaId);
                else
                    tabela = _context.TabelaAgencias.SingleOrDefault(c => c.TabelaId == vm.TabelaId && c.AgenciaId == _usuarioLogado.ObterClienteId()).Tabela;

                if (tabela != null)
                {
                    foreach (var tabelaItem in tabela.TabelaItens)
                    {
                        tabelaItem.ValorUnitario = tabelaItem.ValorUnitario + (tabelaItem.ValorUnitario * (vm.Percentual / 100));
                    }

                    _context.SaveChanges();
                }
                else
                {
                    _notificacao.Warning("Tabela não encontrada");
                }
            }

            return View();
        }

        [Authorize(Roles = "Administradora")]
        public IActionResult ReajustePorAgencia()
        {
            return View();
        }

        [HttpPost]
        public IActionResult ReajustePorAgencia(ReajusteViewModel vm)
        {
            if (ModelState.IsValid)
            {
                var tabelasAgencia = _context.TabelaAgencias.Where(c => c.AgenciaId == vm.AgenciaId);

                if (tabelasAgencia.Any())
                {
                    foreach (var tabelaAgencia in tabelasAgencia)
                    {
                        foreach (var tabelaItem in tabelaAgencia.Tabela.TabelaItens)
                        {
                            tabelaItem.ValorUnitario += (tabelaItem.ValorUnitario * (vm.Percentual / 100));
                        }
                    }

                    _context.SaveChanges();

                    _notificacao.Success($"Tabelas da agência: {vm.AgenciaId}, atualizadas!");
                }
                else
                {
                    _notificacao.Warning("Nenhuma tabela encontrada para esta agência");
                }
            }


            return View(vm);
        }

        #endregion

    }
}
