﻿using Sig.Web.Models.Enums;
using System;
using System.Globalization;
using System.Linq;
using System.Text;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using AspNetCoreHero.ToastNotification.Abstractions;
using Sig.Web.ViewModels.Agencia.PagarMeVm;
using PagarMe;
using TransactionStatus = PagarMe.TransactionStatus;
using Transaction = PagarMe.Transaction;
using Sig.Web.Helpers;

namespace Sig.Web.Areas.Agencias.Controllers
{
    [Authorize(Roles = "Administradora")]
    public class PagarMeController : BaseController
    {
        public PagarMeController(INotyfService notificacao) : base(notificacao)
        {
        }

        #region Actions

        #region Configuração

        public IActionResult Index()
        {
            var pagarMe = _context.PagarMes.OrderBy(c => c.Id);
            return View(pagarMe);
        }


        public IActionResult Editar(int? id)
        {
            try
            {
                Web.Models.PagarMe pagarMe;

                if (!id.HasValue || id == 0)
                {
                    if (_context.PagarMes.Any())
                    {
                        _notificacao.Information("Já existe uma conta criada, você não poderá adicionar uma nova. Tente editar");
                        return RedirectToAction(nameof(Index));
                    }

                    pagarMe = new Web.Models.PagarMe();
                }
                else
                {
                    pagarMe = _context.PagarMes.Find(id);
                }

                return View(pagarMe);
            }
            catch (Exception ex)
            {
                _notificacao.Error(ex.Message);
                return View();
            }
        }

        [HttpPost, ValidateAntiForgeryToken]
        public IActionResult Editar(Web.Models.PagarMe pagarMe)
        {
            try
            {
                string mensagem = string.Empty;

                if (pagarMe.Id == 0)
                {
                    _context.PagarMes.Add(pagarMe);
                    mensagem = "Conta do Pagar.Me adicionada com sucesso.";
                }
                else
                {
                    _context.PagarMes.Attach(pagarMe);
                    var entry = _context.Entry(pagarMe);

                    entry.Property(c => c.Id).IsModified = false;
                    entry.Property(c => c.IsAtivo).IsModified = true;
                    entry.Property(c => c.DefaultApiKey).IsModified = true;
                    entry.Property(c => c.PostBackUrl).IsModified = true;

                    mensagem = "Conta do Pagar.Me alterada com sucesso.";
                }

                _context.SaveChanges();
                _notificacao.Success(mensagem);

                return RedirectToAction("index");
            }
            catch (Exception ex)
            {
                _notificacao.Error(ex.Message);
                return View("editar", pagarMe);
            }
        }

        #endregion

        #region Envio de Faturas

        public IActionResult Envio()
        {
            var configuracao = RetornaConfiguracaoPagarMe();

            if (configuracao == null)
            {
                _notificacao.Error("Não existe configuração ativa para o Pagar.Me");
            }

            return View();
        }

        [HttpPost]
        public IActionResult Envio(EnvioViewModel vm)
        {
            var configuracao = RetornaConfiguracaoPagarMe();

            if (configuracao != null)
            {
                if (_context.Faturamentos.Where(c => c.DataInicialFaturamento == vm.DataInicial
                 && c.DataFinalFaturamento == vm.DataFinal
                 && c.Tid.HasValue).Any())
                {
                    _notificacao.Information("O Envio já foi realizado anteriormente");
                    return RedirectToAction("EnvioPorAgencia");
                }
                else
                {
                    StringBuilder sbErro = new();

                    var faturamentos = _context.Faturamentos.Where(
                                        c => c.DataInicialFaturamento == vm.DataInicial
                                        && c.DataFinalFaturamento == vm.DataFinal
                                        && c.IsEnviar == true)
                                       .OrderBy(c => c.ClienteId);

                    if (faturamentos.Any())
                    {
                        PagarMeService.DefaultApiKey = configuracao.DefaultApiKey;

                        foreach (var fatura in faturamentos)
                        {
                            try
                            {
                                decimal novoValorOriginal = fatura.ValorOriginal;

                                if (fatura.ValorDespesa.HasValue)
                                {
                                    novoValorOriginal += fatura.ValorDespesa.Value;
                                }

                                var transaction = new Transaction
                                {
                                    Amount = int.Parse(novoValorOriginal.ToString().Replace(",", string.Empty)),
                                    PaymentMethod = PaymentMethod.Boleto,
                                    BoletoExpirationDate = fatura.DataVencimento,
                                    PostbackUrl = configuracao.PostBackUrl,

                                    Customer = new Customer()
                                    {
                                        Name = fatura.Cliente.Nome.ToUpper(),
                                        DocumentNumber = fatura.Cliente.CPFCNPJ,
                                        Email = fatura.Cliente.Email,
                                        DocumentType = fatura.Cliente.CPFCNPJ.Length == 14 ? DocumentType.Cnpj : DocumentType.Cpf,
                                        Address = new Address()
                                        {
                                            Street = fatura.Cliente.Endereco.Logradouro.ToUpper(),
                                            Neighborhood = fatura.Cliente.Endereco.Bairro.ToUpper(),
                                            Zipcode = fatura.Cliente.Endereco.CEP,
                                            StreetNumber = "NAO INFORMADO", //fatura.Cliente.Endereco.Numero,
                                            Complementary = !string.IsNullOrWhiteSpace(fatura.Cliente.Endereco.Complemento) ? fatura.Cliente.Endereco.Complemento.ToUpper() : string.Empty,
                                            State = fatura.Cliente.Endereco.UF.ToUpper(),
                                        }
                                    }
                                };

                                transaction.Save();

                                if (!string.IsNullOrWhiteSpace(transaction.BoletoBarcode))
                                {
                                    fatura.LinhaDigitavel = transaction.BoletoBarcode;
                                    fatura.LinkBoleto = transaction.BoletoUrl;
                                    fatura.Tid = int.Parse(transaction.Id);
                                }

                                StringBuilder sb = new();

                                var itensDoFaturamento = fatura.FaturamentoItens.OrderBy(c => c.ItemFaturavelId);

                                decimal valorParcial = 0M;
                                decimal valorTotal = 0M;

                                var itemConsumo = itensDoFaturamento.SingleOrDefault(c => c.ItemFaturavelId == 23);

                                if (itemConsumo != null && itemConsumo.ValorUnitario != 0)
                                {
                                    valorTotal = fatura.ValorOriginal;

                                    sb.Append("<table border=\"1\">");
                                    sb.Append("<thead>");
                                    sb.Append("<tr>");
                                    sb.Append("<th>FATURA</th>");
                                    sb.Append("<th>PRODUTO</th>");
                                    sb.Append("<th>VALOR UNITÁRIO</th>");
                                    sb.Append("<th>QUANTIDADE</th>");
                                    sb.Append("<th>VALOR TOTAL</th>");
                                    sb.Append("</tr>");
                                    sb.Append("</thead>");
                                    sb.Append("<tbody>");
                                    sb.Append("<tr>");

                                    //Consumo
                                    sb.Append("<tr>");
                                    sb.Append($"<td>{itemConsumo.FaturamentoId}</td>");
                                    sb.Append($"<td>CONSUMAÇÃO MINÍMA</td>");
                                    sb.Append($"<td>{fatura.ValorOriginal:c}</td>");
                                    sb.Append($"<td>1</td>");
                                    sb.Append($"<td>{fatura.ValorOriginal:c}</td>");
                                    sb.Append("</tr>");

                                    //Despesas
                                    if (fatura.ValorDespesa.HasValue)
                                    {
                                        valorTotal += fatura.ValorDespesa.Value;

                                        sb.Append("<tr>");
                                        sb.Append($"<td>{itemConsumo.FaturamentoId}</td>");
                                        sb.Append($"<td>DESPESAS</td>");
                                        sb.Append($"<td>R$ 3,50</td>");
                                        sb.Append($"<td>1</td>");
                                        sb.Append($"<td>R$ 3,50</td>");
                                        sb.Append("</tr>");
                                    }

                                    sb.Append("<tr>");
                                    sb.Append($"<td colspan=\"4\"></td>");
                                    sb.Append($"<td>{valorTotal:c}</td>");
                                    sb.Append("</tr>");

                                    sb.Append("</tbody>");
                                    sb.Append("</table>");

                                    SendGridEmail.EnviarEmailBoleto(fatura.Cliente.Email, fatura.Cliente.RazaoSocial, fatura.LinhaDigitavel, fatura.LinkBoleto, sb.ToString(), MesAnoPorExtenso(vm.DataInicial), fatura.DataVencimento.ToShortDateString());
                                }
                                else
                                {
                                    sb.Append("<table border=\"1\">");
                                    sb.Append("<thead>");
                                    sb.Append("<tr>");
                                    sb.Append("<th>FATURA</th>");
                                    sb.Append("<th>PRODUTO</th>");
                                    sb.Append("<th>VALOR UNITÁRIO</th>");
                                    sb.Append("<th>QUANTIDADE</th>");
                                    sb.Append("<th>VALOR TOTAL</th>");
                                    sb.Append("</tr>");
                                    sb.Append("</thead>");
                                    sb.Append("<tbody>");
                                    sb.Append("<tr>");

                                    int faturamentoId = 0;

                                    foreach (var item in itensDoFaturamento)
                                    {
                                        valorParcial = item.Quantidade * item.ValorUnitario;

                                        faturamentoId = item.FaturamentoId;

                                        if (item.ItemFaturavelId != 23) //Quando for consumo = 0 não mostra.
                                        {
                                            sb.Append("<tr>");
                                            sb.Append($"<td>{item.FaturamentoId}</td>");
                                            sb.Append($"<td>{item.ItemFaturavel.Descricao}</td>");
                                            sb.Append($"<td>{item.ValorUnitario:c}</td>");
                                            sb.Append($"<td>{item.Quantidade}</td>");
                                            sb.Append($"<td>{valorParcial:c}</td>");
                                            sb.Append("</tr>");
                                        }

                                        valorTotal += valorParcial;
                                    }


                                    if (fatura.ValorDespesa.HasValue)
                                    {
                                        valorTotal += 3.50M;

                                        sb.Append("<tr>");
                                        sb.Append($"<td>{faturamentoId}</td>");
                                        sb.Append($"<td>DESPESAS</td>");
                                        sb.Append($"<td>R$ 3,50</td>");
                                        sb.Append($"<td>1</td>");
                                        sb.Append($"<td>R$ 3,50</td>");
                                        sb.Append("</tr>");

                                    }

                                    sb.Append("<tr>");
                                    sb.Append($"<td colspan=\"4\"></td>");
                                    sb.Append($"<td>{valorTotal:c}</td>");
                                    sb.Append("</tr>");

                                    sb.Append("</tbody>");
                                    sb.Append("</table>");

                                    SendGridEmail.EnviarEmailBoleto(fatura.Cliente.Email, fatura.Cliente.RazaoSocial, fatura.LinhaDigitavel, fatura.LinkBoleto, sb.ToString(), MesAnoPorExtenso(vm.DataInicial), fatura.DataVencimento.ToShortDateString());

                                }

                            }
                            catch (PagarMeException ex)
                            {
                                sbErro.AppendLine($"Cliente: {fatura.ClienteId}<br/>");
                                foreach (var error in ex.Error.Errors)
                                {
                                    sbErro.AppendLine($"Motivo: {error.Message}<br/>");
                                }
                                sbErro.AppendLine("<br/>");
                            }
                        }

                        _context.SaveChanges();

                        if (sbErro.Length > 0)
                            _notificacao.Information($"OS SEGUINTE(S) CLIENTE(S) NÃO TIVERAM FATURAS ENVIADAS:<br/><br/>{sbErro}");

                        _notificacao.Success($"Faturamento enviado com sucesso");
                        return RedirectToAction("Envio");
                    }
                    else
                    {
                        _notificacao.Information($"Nenhuma fatura foi encontrada");
                        return RedirectToAction("Envio");
                    }
                }
            }
            else
            {
                _notificacao.Error($"Não existe configuração ativa para o Pagar.Me");
                return RedirectToAction("Envio");
            }

        }

        #endregion

        #region Envio de Faturas Por Agência

        public IActionResult EnvioPorAgencia()
        {
            var configuracao = RetornaConfiguracaoPagarMe();

            if (configuracao == null)
                _notificacao.Error($"Não existe configuração ativa para o Pagar.Me");


            return View();
        }

        [HttpPost]
        public IActionResult EnvioPorAgencia(EnvioPorAgenciaViewModel vm)
        {
            var configuracao = RetornaConfiguracaoPagarMe();

            if (configuracao != null)
            {
                StringBuilder sbErro = new StringBuilder();

                var faturamentos = _context.Faturamentos.Where(
                                    c => c.DataInicialFaturamento == vm.DataInicial
                                    && c.DataFinalFaturamento == vm.DataFinal
                                    && (c.Cliente.AgenciaId == vm.AgenciaId || c.ClienteId == vm.AgenciaId)
                                    && !c.ValorPago.HasValue
                                    && !c.Tid.HasValue)
                                   .OrderBy(c => c.ClienteId);

                if (faturamentos.Any())
                {
                    PagarMeService.DefaultApiKey = configuracao.DefaultApiKey;

                    foreach (var fatura in faturamentos)
                    {
                        try
                        {
                            decimal novoValorOriginal = fatura.ValorOriginal; //fatura.ValorOriginal + 3.50M;

                            Transaction transaction = new();
                            transaction.Amount = int.Parse(novoValorOriginal.ToString().Replace(",", string.Empty));
                            transaction.PaymentMethod = PaymentMethod.Boleto;
                            transaction.BoletoExpirationDate = fatura.DataVencimento;
                            transaction.PostbackUrl = configuracao.PostBackUrl;

                            transaction.Customer = new Customer()
                            {
                                Name = fatura.Cliente.Nome.ToUpper(),
                                DocumentNumber = fatura.Cliente.CPFCNPJ,
                                Email = fatura.Cliente.Email,
                                DocumentType = fatura.Cliente.CPFCNPJ.Length == 14 ? DocumentType.Cnpj : DocumentType.Cpf,
                                Address = new Address()
                                {
                                    Street = fatura.Cliente.Endereco.Logradouro.ToUpper(),
                                    Neighborhood = fatura.Cliente.Endereco.Bairro.ToUpper(),
                                    Zipcode = fatura.Cliente.Endereco.CEP,
                                    StreetNumber = "NAO INFORMADO", //fatura.Cliente.Endereco.Numero,
                                    Complementary = !string.IsNullOrWhiteSpace(fatura.Cliente.Endereco.Complemento) ? fatura.Cliente.Endereco.Complemento.ToUpper() : string.Empty,
                                    State = fatura.Cliente.Endereco.UF.ToUpper(),
                                }
                            };

                            transaction.Save();

                            if (!string.IsNullOrWhiteSpace(transaction.BoletoBarcode))
                            {
                                fatura.LinhaDigitavel = transaction.BoletoBarcode;
                                fatura.LinkBoleto = transaction.BoletoUrl;
                                fatura.Tid = int.Parse(transaction.Id);
                            }

                            StringBuilder sb = new();
                            var faturamentoItens = fatura.FaturamentoItens.OrderBy(c => c.ItemFaturavelId);

                            decimal valorParcial = 0M;
                            decimal valorTotal = 0M;

                            var consumo = faturamentoItens.SingleOrDefault(c => c.ItemFaturavelId == 23);

                            if (consumo != null && consumo.ValorUnitario != 0)
                            {
                                sb.Append("<table border=\"1\">");
                                sb.Append("<thead>");
                                sb.Append("<tr>");
                                sb.Append("<th>FATURA</th>");
                                sb.Append("<th>PRODUTO</th>");
                                sb.Append("<th>VALOR UNITÁRIO</th>");
                                sb.Append("<th>QUANTIDADE</th>");
                                sb.Append("<th>VALOR TOTAL</th>");
                                sb.Append("</tr>");
                                sb.Append("</thead>");
                                sb.Append("<tbody>");
                                sb.Append("<tr>");

                                //Consumo
                                sb.Append("<tr>");
                                sb.Append($"<td>{consumo.FaturamentoId}</td>");
                                sb.Append($"<td>CONSUMAÇÃO MINÍMA</td>");
                                sb.Append($"<td>{fatura.ValorOriginal:c}</td>");
                                sb.Append($"<td>1</td>");
                                sb.Append($"<td>{fatura.ValorOriginal:c}</td>");
                                sb.Append("</tr>");

                                //Despesas
                                //valorTotal = fatura.ValorOriginal + 3.50M;

                                //sb.Append("<tr>");
                                //sb.Append($"<td>{consumo.FaturamentoId}</td>");
                                //sb.Append($"<td>DESPESAS</td>");
                                //sb.Append($"<td>R$ 3,50</td>");
                                //sb.Append($"<td>1</td>");
                                //sb.Append($"<td>R$ 3,50</td>");
                                //sb.Append("</tr>");

                                sb.Append("<tr>");
                                sb.Append($"<td colspan=\"4\"></td>");
                                sb.Append($"<td>{valorTotal:c}</td>");
                                sb.Append("</tr>");

                                sb.Append("</tbody>");
                                sb.Append("</table>");

                                SendGridEmail.EnviarEmailBoleto(fatura.Cliente.Email, fatura.Cliente.RazaoSocial, fatura.LinhaDigitavel, fatura.LinkBoleto, sb.ToString(), MesAnoPorExtenso(vm.DataInicial), fatura.DataVencimento.ToShortDateString());

                            }
                            else
                            {
                                sb.Append("<table border=\"1\">");
                                sb.Append("<thead>");
                                sb.Append("<tr>");
                                sb.Append("<th>FATURA</th>");
                                sb.Append("<th>PRODUTO</th>");
                                sb.Append("<th>VALOR UNITÁRIO</th>");
                                sb.Append("<th>QUANTIDADE</th>");
                                sb.Append("<th>VALOR TOTAL</th>");
                                sb.Append("</tr>");
                                sb.Append("</thead>");
                                sb.Append("<tbody>");
                                sb.Append("<tr>");

                                int faturamentoId = 0;

                                foreach (var item in faturamentoItens)
                                {
                                    valorParcial = item.Quantidade * item.ValorUnitario;

                                    faturamentoId = item.FaturamentoId;

                                    if (item.ItemFaturavelId != 23) //Quando for consumo = 0 não mostra.
                                    {
                                        sb.Append("<tr>");
                                        sb.Append($"<td>{item.FaturamentoId}</td>");
                                        sb.Append($"<td>{item.ItemFaturavel.Descricao}</td>");
                                        sb.Append($"<td>{item.ValorUnitario:c}</td>");
                                        sb.Append($"<td>{item.Quantidade}</td>");
                                        sb.Append($"<td>{valorParcial:c}</td>");
                                        sb.Append("</tr>");
                                    }

                                    valorTotal += valorParcial;
                                }

                                //Despesas
                                //valorTotal += 3.50M;

                                //sb.Append("<tr>");
                                //sb.Append($"<td>{faturamentoId}</td>");
                                //sb.Append($"<td>DESPESAS</td>");
                                //sb.Append($"<td>R$ 3,50</td>");
                                //sb.Append($"<td>1</td>");
                                //sb.Append($"<td>R$ 3,50</td>");
                                //sb.Append("</tr>");
                                //Fim Despesas

                                sb.Append("<tr>");
                                sb.Append($"<td colspan=\"4\"></td>");
                                sb.Append($"<td>{valorTotal:c}</td>");
                                sb.Append("</tr>");

                                sb.Append("</tbody>");
                                sb.Append("</table>");

                                SendGridEmail.EnviarEmailBoleto(fatura.Cliente.Email, fatura.Cliente.RazaoSocial, fatura.LinhaDigitavel, fatura.LinkBoleto, sb.ToString(), MesAnoPorExtenso(vm.DataInicial), fatura.DataVencimento.ToShortDateString());

                            }

                        }
                        catch (PagarMeException ex)
                        {
                            sbErro.AppendLine($"Cliente: {fatura.ClienteId}<br/>");
                            foreach (var error in ex.Error.Errors)
                            {
                                sbErro.AppendLine($"Motivo: {error.Message}<br/>");
                            }
                            sbErro.AppendLine("<br/>");
                        }
                    }

                    _context.SaveChanges();

                    if (sbErro.Length > 0)
                        _notificacao.Information($"OS SEGUINTE(S) CLIENTE(S) NÃO TIVERAM FATURAS ENVIADAS:<br/><br/>{sbErro}");

                    
                    _notificacao.Success($"Faturamento enviado com sucesso");
                    return RedirectToAction("EnvioPorAgencia");
                }
                else
                {
                    _notificacao.Information($"Nenhuma fatura foi encontrada");
                    return RedirectToAction("EnvioPorAgencia");
                }
            }
            else
            {
                _notificacao.Error($"Não existe configuração ativa para o Pagar.Me");
                return RedirectToAction("Envio");
            }

        }

        #endregion

        #region Retorno Pagar.Me

        [HttpPost, AllowAnonymous]
        public IActionResult Retorno()
        {
            var configuracao = RetornaConfiguracaoPagarMe();

            if (configuracao != null)
            {
                PagarMeService.DefaultApiKey = configuracao.DefaultApiKey;

                var postback_body = $"{Request.Form.ToString()}";
                var signature = $"{Request.Headers["X-Hub-Signature"]}";

                if (Utils.ValidateRequestSignature(postback_body, signature))
                {
                    var current_status = Request.Form["current_status"];
                    var transaction_id = Request.Form["id"];

                    if (current_status == "paid")
                    {
                        var tid = int.Parse(transaction_id);

                        var fatura = _context.Faturamentos.Find(new { Tid = tid });

                        var transaction = PagarMeService.GetDefaultService().Transactions.Find(tid);

                        if (fatura != null && transaction != null)
                        {
                            fatura.DataPagamento = DateTime.Now;
                            fatura.ValorPago = Convert.ToDecimal(transaction.PaidAmount / 100.00);
                            fatura.TipoPagamento = eTipoPagamento.PagamentoEmBanco;

                            _context.SaveChanges();
                        }
                    }
                }
            }

            return View();
        }

        #endregion

        #region Cancelar Fatura

        //public IActionResult Cancelar()
        //{
        //    var configuracao = RetornaConfiguracaoPagarMe();

        //    if (configuracao != null)
        //    {
        //        PagarMeService.DefaultApiKey = configuracao.DefaultApiKey;

        //        var dataInicio = DateTime.Parse("01/02/2017");
        //        var dataFinal = DateTime.Parse("28/02/2017");

        //        var faturamentos = _context.Faturamentos.Where(
        //                            c => c.DataInicialFaturamento == dataInicio
        //                            && c.DataFinalFaturamento == dataFinal
        //                            && c.Cliente.AgenciaId == 439)
        //                           .OrderBy(c => c.ClienteId);

        //        foreach (var item in faturamentos)
        //        {
        //            if (item.Tid.HasValue)
        //            {
        //                var transaction = PagarMeService.GetDefaultService().Transactions.Find(item.Tid.Value);
        //                transaction.Refund();
        //            }

        //            Thread.Sleep(2000);
        //        }
        //    }

        //    return View();
        //}

        #endregion

        #region Atualizar Fatura

        public IActionResult AtualizarFatura()
        {
            return View();
        }

        [HttpPost]
        public IActionResult AtualizarFatura(AtualizarFaturaViewModel vm)
        {
            if (vm.DataVencimento < DateTime.Now.Date)
                ModelState.AddModelError("DataVencimento", "A data de vencimento não pode ser menor que a data de hoje");

            if (ModelState.IsValid)
            {
                var configuracao = RetornaConfiguracaoPagarMe();

                if (configuracao != null)
                {
                    var fatura = _context.Faturamentos.Find(vm.FaturamentoId);

                    if (fatura != null)
                    {
                        PagarMeService.DefaultApiKey = configuracao.DefaultApiKey;

                        decimal novoValor = fatura.ValorOriginal;

                        //Taxa do Boleto
                        if (vm.IsTaxaBoleto)
                            novoValor += 3.50M;

                        //Desconto
                        if (vm.ValorDesconto.HasValue && vm.ValorDesconto > 0)
                        {
                            novoValor -= vm.ValorDesconto.Value;
                            fatura.ValorDesconto = vm.ValorDesconto;
                        }

                        //Juros
                        if (vm.ValorJuros.HasValue && vm.ValorJuros > 0)
                        {
                            novoValor += vm.ValorJuros.Value;
                            fatura.ValorJuros = vm.ValorJuros;
                        }

                        Transaction transaction = new()
                        {
                            Amount = int.Parse(novoValor.ToString().Replace(",", string.Empty)),
                            PaymentMethod = PaymentMethod.Boleto,
                            BoletoExpirationDate = vm.DataVencimento,

                            /*if (!string.IsNullOrWhiteSpace(vm.Observacao))
                                transaction.SoftDescriptor = vm.Observacao;*/

                            PostbackUrl = configuracao.PostBackUrl,

                            Customer = new Customer()
                            {
                                Name = fatura.Cliente.Nome.ToUpper(),
                                DocumentNumber = fatura.Cliente.CPFCNPJ,
                                Email = fatura.Cliente.Email,
                                DocumentType = fatura.Cliente.CPFCNPJ.Length == 14 ? DocumentType.Cnpj : DocumentType.Cpf,
                                Address = new Address()
                                {
                                    Street = fatura.Cliente.Endereco.Logradouro.ToUpper(),
                                    Neighborhood = fatura.Cliente.Endereco.Bairro.ToUpper(),
                                    Zipcode = fatura.Cliente.Endereco.CEP,
                                    StreetNumber = "NAO INFORMADO",
                                    Complementary = !string.IsNullOrWhiteSpace(fatura.Cliente.Endereco.Complemento) ? fatura.Cliente.Endereco.Complemento.ToUpper() : string.Empty,
                                    State = fatura.Cliente.Endereco.UF.ToUpper(),
                                }
                            }
                        };

                        transaction.Save();

                        if (!string.IsNullOrWhiteSpace(transaction.BoletoBarcode))
                        {
                            fatura.LinhaDigitavel = transaction.BoletoBarcode;
                            fatura.LinkBoleto = transaction.BoletoUrl;
                            fatura.Tid = int.Parse(transaction.Id);

                            _context.SaveChanges();
                            _notificacao.Success($"Boleto enviado: {fatura.LinkBoleto}<br/> Linha digitável: {fatura.LinhaDigitavel}");
                        }

                        StringBuilder sb = new();

                        if (!string.IsNullOrWhiteSpace(vm.Observacao))
                        {
                            sb.Append("<table border=\"1\">");
                            sb.Append("<tbody>");
                            sb.Append("<tr>");
                            sb.Append("<td><strong>Observações</strong></td>");
                            sb.Append("</tr>");
                            sb.Append("<tr>");
                            sb.Append($"<td>{vm.Observacao}</td>");
                            sb.Append("</tr>");
                            sb.Append("</tbody>");
                            sb.Append("</table>");
                            sb.Append("<br/><br/>");
                        }


                        var faturaItens = fatura.FaturamentoItens.OrderBy(c => c.ItemFaturavelId);

                        decimal valorParcial = 0M;
                        decimal valorTotal = 0M;

                        var consumo = faturaItens.SingleOrDefault(c => c.ItemFaturavelId == 23);

                        if (consumo != null && consumo.ValorUnitario != 0)
                        {
                            sb.Append("<table border=\"1\">");
                            sb.Append("<thead>");
                            sb.Append("<tr>");
                            sb.Append("<th>FATURA</th>");
                            sb.Append("<th>PRODUTO</th>");
                            sb.Append("<th>VALOR UNITÁRIO</th>");
                            sb.Append("<th>QUANTIDADE</th>");
                            sb.Append("<th>VALOR TOTAL</th>");
                            sb.Append("</tr>");
                            sb.Append("</thead>");
                            sb.Append("<tbody>");
                            sb.Append("<tr>");

                            //Consumo
                            sb.Append("<tr>");
                            sb.Append($"<td>{consumo.FaturamentoId}</td>");
                            sb.Append($"<td>CONSUMAÇÃO MINÍMA</td>");
                            sb.Append($"<td>{fatura.ValorOriginal:c}</td>");
                            sb.Append($"<td>1</td>");
                            sb.Append($"<td>{fatura.ValorOriginal:c}</td>");
                            sb.Append("</tr>");

                            if (vm.IsTaxaBoleto)
                            {
                                valorTotal = fatura.ValorOriginal + 3.50M;

                                sb.Append("<tr>");
                                sb.Append($"<td>{consumo.FaturamentoId}</td>");
                                sb.Append($"<td>DESPESAS</td>");
                                sb.Append($"<td>R$ 3,50</td>");
                                sb.Append($"<td>1</td>");
                                sb.Append($"<td>R$ 3,50</td>");
                                sb.Append("</tr>");
                            }

                            sb.Append("<tr>");
                            sb.Append($"<td colspan=\"4\"></td>");
                            sb.Append($"<td>{valorTotal:c}</td>");
                            sb.Append("</tr>");

                            sb.Append("</tbody>");
                            sb.Append("</table>");

                            SendGridEmail.EnviarEmailBoleto(fatura.Cliente.Email, fatura.Cliente.RazaoSocial, fatura.LinhaDigitavel, fatura.LinkBoleto, sb.ToString(), $"{fatura.DataInicialFaturamento.Value.ToShortDateString()} a {fatura.DataFinalFaturamento.Value.ToShortDateString()}", fatura.DataVencimento.ToShortDateString());

                        }
                        else
                        {
                            sb.Append("<table border=\"1\">");
                            sb.Append("<thead>");
                            sb.Append("<tr>");
                            sb.Append("<th>FATURA</th>");
                            sb.Append("<th>PRODUTO</th>");
                            sb.Append("<th>VALOR UNITÁRIO</th>");
                            sb.Append("<th>QUANTIDADE</th>");
                            sb.Append("<th>VALOR TOTAL</th>");
                            sb.Append("</tr>");
                            sb.Append("</thead>");
                            sb.Append("<tbody>");
                            sb.Append("<tr>");

                            int faturamentoId = 0;

                            foreach (var item in faturaItens)
                            {
                                valorParcial = item.Quantidade * item.ValorUnitario;

                                faturamentoId = item.FaturamentoId;

                                if (item.ItemFaturavelId != 23) //Quando for consumo = 0 não mostra.
                                {
                                    sb.Append("<tr>");
                                    sb.Append($"<td>{item.FaturamentoId}</td>");
                                    sb.Append($"<td>{item.ItemFaturavel.Descricao}</td>");
                                    sb.Append($"<td>{item.ValorUnitario:c}</td>");
                                    sb.Append($"<td>{item.Quantidade}</td>");
                                    sb.Append($"<td>{valorParcial:c}</td>");
                                    sb.Append("</tr>");
                                }

                                valorTotal += valorParcial;
                            }

                            //Despesas
                            valorTotal += 3.50M;

                            sb.Append("<tr>");
                            sb.Append($"<td>{faturamentoId}</td>");
                            sb.Append($"<td>DESPESAS</td>");
                            sb.Append($"<td>R$ 3,50</td>");
                            sb.Append($"<td>1</td>");
                            sb.Append($"<td>R$ 3,50</td>");
                            sb.Append("</tr>");
                            //Fim Despesas

                            sb.Append("<tr>");
                            sb.Append($"<td colspan=\"4\"></td>");
                            sb.Append($"<td>{valorTotal:c}</td>");
                            sb.Append("</tr>");

                            sb.Append("</tbody>");
                            sb.Append("</table>");

                            SendGridEmail.EnviarEmailBoleto(fatura.Cliente.Email, fatura.Cliente.RazaoSocial, fatura.LinhaDigitavel, fatura.LinkBoleto, sb.ToString(), $"{fatura.DataInicialFaturamento.Value.ToShortDateString()} a {fatura.DataFinalFaturamento.Value.ToShortDateString()}", fatura.DataVencimento.ToShortDateString());

                        }
                    }
                    else
                    {
                        _notificacao.Error("Fatura não encontrada");
                    }
                }
            }

            return View();
        }

        #endregion

        #region Verificar Faturas e Baixar

        public IActionResult VerificarFatura()
        {
            return View();
        }

        [HttpPost]
        public IActionResult VerificarFatura(VerificarFaturaViewModel vm)
        {
            var configuracao = RetornaConfiguracaoPagarMe();

            if (configuracao != null)
            {
                StringBuilder sb = new StringBuilder();

                PagarMeService.DefaultApiKey = configuracao.DefaultApiKey;

                var faturamentos = _context.Faturamentos.Where(
                                    c => c.DataInicialFaturamento == vm.DataInicial
                                    && c.DataFinalFaturamento == vm.DataFinal
                                    && c.Tid.HasValue
                                    && !c.DataPagamento.HasValue)
                                   .OrderBy(c => c.ClienteId);

                if (faturamentos.Any())
                {
                    foreach (var faturamento in faturamentos)
                    {
                        var transaction = PagarMeService.GetDefaultService().Transactions.Find(faturamento.Tid.Value);

                        if (transaction.Status == TransactionStatus.Paid)
                        {
                            faturamento.ValorPago = Convert.ToDecimal(transaction.PaidAmount / 100.00);
                            faturamento.DataPagamento = transaction.DateUpdated;
                            faturamento.TipoPagamento = eTipoPagamento.PagamentoEmBanco;

                            sb.Append($"Fatura: {faturamento.FaturamentoId} - Data: {transaction.DateUpdated} - PAGO<br/>");
                        }
                    }

                    if (sb.Length > 0)
                    {

                        _notificacao.Success($"Faturas baixadas: <br/> {sb}");
                        _context.SaveChanges();
                    }
                    else
                    {
                        _notificacao.Information("Nenhuma fatura para baixar");
                    }
                }
                else
                {
                    _notificacao.Information("Nenhuma fatura para baixar");
                }
            }

            return View();
        }

        #endregion

        #region Correção

        //public IActionResult ProcurarTransacoes()
        //{
        //    var configuracao = RetornaConfiguracaoPagarMe();

        //    if (configuracao != null)
        //    {
        //        PagarMeService.DefaultApiKey = configuracao.DefaultApiKey;

        //        var dataInicial = DateTime.Parse("01/02/2017");
        //        var dataFinal = DateTime.Parse("28/02/2017");

        //        var faturamentos = _context.Faturamentos.Where(
        //                            c => c.DataInicialFaturamento == dataInicial
        //                            && c.DataFinalFaturamento == dataFinal
        //                            && !c.Tid.HasValue
        //                            && !c.DataPagamento.HasValue)
        //                           .OrderBy(c => c.ClienteId);

        //        foreach (var faturamento in faturamentos)
        //        {
        //            int amountOriginal = int.Parse(faturamento.ValorOriginal.ToString().Replace(",", string.Empty));

        //            var search = new Transaction
        //            {
        //                Customer = new Customer { DocumentNumber = faturamento.Cliente.CPFCNPJ }
        //            };

        //            var transaction = PagarMeService.GetDefaultService().Transactions.Find(search);

        //            if (transaction != null
        //                && transaction.BoletoExpirationDate.Value.Month == faturamento.DataVencimento.Month
        //                && transaction.Amount == amountOriginal)
        //            {
        //                faturamento.Tid = int.Parse(transaction.Id);
        //                faturamento.LinkBoleto = transaction.BoletoUrl;
        //                faturamento.LinhaDigitavel = transaction.BoletoBarcode;
        //            }

        //        }

        //        _context.SaveChanges();
        //    }

        //    return View();
        //}

        #endregion

        #endregion

        #region Métodos Privados

        private Web.Models.PagarMe RetornaConfiguracaoPagarMe()
        {
            var pagarMe = _context.PagarMes.FirstOrDefault(c => c.IsAtivo == true);
            return pagarMe;
        }

        private string MesAnoPorExtenso(DateTime data)
        {
            CultureInfo culture = new CultureInfo("pt-BR");
            DateTimeFormatInfo dtfi = culture.DateTimeFormat;

            int ano = data.Year;
            string mes = culture.TextInfo.ToTitleCase(dtfi.GetMonthName(data.Month));

            return $"{mes.ToLower()} de {ano}";
        }

        #endregion
    }
}