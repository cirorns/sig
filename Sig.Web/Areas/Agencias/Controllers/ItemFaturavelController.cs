﻿using Sig.Web.Models;
using Sig.Web.Models.ViewModels;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using AspNetCoreHero.ToastNotification.Abstractions;

namespace Sig.Web.Areas.Agencias.Controllers
{

    //[Authorize(Roles = "Administradora")]
    public class ItemFaturavelController : BaseController
    {
        public ItemFaturavelController(INotyfService notificacao) : base(notificacao)
        {
        }
        #region Index

        public IActionResult Index()
        {
            var viewModel = new ItemFaturavelViewModel();
            viewModel.ItensFaturaveis = _context.ItemFaturaveis.OrderByDescending(c => c.ItemFaturavelId);
            return View(viewModel);
        }

        [HttpPost]
        public IActionResult Index(ItemFaturavelViewModel viewModel)
        {
            IQueryable<ItemFaturavel> itensFaturaveis = _context.ItemFaturaveis;

            #region Filtros

            if (!viewModel.ItemFaturavel.ItemFaturavelId.Equals(0))
            {
                itensFaturaveis = itensFaturaveis.Where(c => c.ItemFaturavelId == viewModel.ItemFaturavel.ItemFaturavelId);
            }

            if (!string.IsNullOrEmpty(viewModel.ItemFaturavel.Descricao))
            {
                itensFaturaveis = itensFaturaveis.Where(c => c.Descricao.Contains(viewModel.ItemFaturavel.Descricao));
            }

            #endregion

            viewModel.ItensFaturaveis = itensFaturaveis;
            return View(viewModel);
        }

        #endregion

        #region Incluir

        public IActionResult Incluir()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Incluir(ItemFaturavel itemFaturavel)
        {
            if (ModelState.IsValid)
            {
                _context.ItemFaturaveis.Add(itemFaturavel);
                _context.SaveChanges();

                _notificacao.Success("Item Faturável incluído com sucesso");
                return RedirectToAction("Index");


            }

            return View(itemFaturavel);
        }

        #endregion

        #region Alterar

        public IActionResult Alterar(int id)
        {
            var itemFaturavel = _context.ItemFaturaveis.Find(id);
            return View(itemFaturavel);
        }

        [HttpPost]
        public IActionResult Alterar(ItemFaturavel form)
        {
            var itemFaturavel = _context.ItemFaturaveis.Find(form.ItemFaturavelId);

            if (ModelState.IsValid)
            {
                itemFaturavel.ItemFaturavelId = form.ItemFaturavelId;
                itemFaturavel.Descricao = form.Descricao;
                itemFaturavel.CustoUnitario = form.CustoUnitario;
                itemFaturavel.ValorUnitario = form.ValorUnitario;
                itemFaturavel.ValorMinimo = form.ValorMinimo;
                itemFaturavel.ValorMaximo = form.ValorMaximo;
                itemFaturavel.IsAtivo = form.IsAtivo;
                itemFaturavel.IsPacote = form.IsPacote;
                itemFaturavel.IsConsulta = form.IsConsulta;

                _context.SaveChanges();

               _notificacao.Success("Item Faturável alterado com sucesso");
                return RedirectToAction("Index");

            }

            return View(itemFaturavel);
        }

        #endregion

    }
}
