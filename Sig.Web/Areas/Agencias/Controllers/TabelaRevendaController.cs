﻿using Sig.Web.Models;
using Sig.Web.Models.ViewModels.TabelaRevendaVM;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using AspNetCoreHero.ToastNotification.Abstractions;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Sig.Web.Areas.Agencias.Controllers
{
    [Authorize(Roles = "Administradora")]
    public class TabelaRevendaController : BaseController
    {
        public TabelaRevendaController(INotyfService notificacao) : base(notificacao)
        {
        }

        #region Index

        public IActionResult Index()
        {
            var tabelasRevendas = _context.TabelasRevendas.Where(c => c.Cliente.IsRevenda == true).OrderBy(c => c.ClienteId);
            var viewModel = new IndexViewModel
            {
                TabelasRevendas = tabelasRevendas
            };

            return View(viewModel);
        }

        [HttpPost]
        public IActionResult Index(IndexViewModel viewModel)
        {
            IQueryable<TabelaRevenda> tabelasRevendas = _context.TabelasRevendas.Where(c => c.Cliente.IsRevenda == true);

            if (viewModel.Cliente.ClienteId != 0)
                tabelasRevendas = tabelasRevendas.Where(c => c.ClienteId == viewModel.Cliente.ClienteId);

            if (!viewModel.Tabela.TabelaId.Equals(0))
                tabelasRevendas = tabelasRevendas.Where(c => c.TabelaId == viewModel.Tabela.TabelaId);

            if (!string.IsNullOrEmpty(viewModel.Tabela.Descricao))
                tabelasRevendas = tabelasRevendas.Where(c => c.Tabela.Descricao.Contains(viewModel.Tabela.Descricao));

            viewModel.TabelasRevendas = tabelasRevendas;
            return View(viewModel);
        }

        #endregion

        #region Incluir

        public IActionResult Incluir()
        {
            AjusteContextoIncluir();
            return View();
        }

        [HttpPost]
        public IActionResult Incluir(TabelaRevenda tabelaRevenda)
        {
            if (ModelState.IsValid)
            {
                //Verifico se NÃO existe a tabela para este cliente
                if (!_context.TabelasRevendas.Any(c => c.TabelaId == tabelaRevenda.TabelaId && c.Cliente.ClienteId == tabelaRevenda.ClienteId))
                {
                    _context.TabelasRevendas.Add(tabelaRevenda);
                    _context.SaveChanges();

                    _notificacao.Success("Tabela cadastrada na agência com sucesso");
                    return RedirectToAction("Index");
                }

                _notificacao.Warning("Esta tabela já está cadastrada para este cliente");
            }

            AjusteContextoIncluir();
            return View();
        }

        #endregion

        #region Métodos Privadados

        private void AjusteContextoIncluir()
        {
            var clientes = _context.Clientes
                .Where(c => c.IsRevenda)
                .Select(x => new { x.ClienteId, Nome = x.ClienteId + " - " + x.Nome })
                .OrderBy(c => c.ClienteId).ToList();

            var tabelas = _context.Tabelas
                .Select(x => new { x.TabelaId, Descricao = x.TabelaId + " - " + x.Descricao })
                .OrderBy(c => c.TabelaId).ToList();

            ViewBag.Clientes = new SelectList(clientes, "ClienteId", "Nome");
            ViewBag.Tabelas  = new SelectList(tabelas, "TabelaId", "Descricao");
        }

        #endregion
    }
}