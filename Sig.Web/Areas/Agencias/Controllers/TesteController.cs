﻿using AspNetCoreHero.ToastNotification.Abstractions;
using Microsoft.AspNetCore.Mvc;

namespace Sig.Web.Areas.Agencias.Controllers
{
    public class TesteController : BaseController
    {
        public TesteController(INotyfService notificacao) : base(notificacao)
        {
        }

        public IActionResult Index()
        {
            _notificacao.Success("Opaa, deu certo!");
            _notificacao.Error("Mensagem de erro");
            _notificacao.Information("Mensagem de informação");
            _notificacao.Warning("Mensagem de atenção");

            return View();
        }
    }
}
