﻿using Sig.Web.Models;
using Sig.Web.Models.ViewModels;
using Sig.Web.Models.ViewModels.Relatorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.AspNetCore.Mvc;
using Sig.Web.Models.Enums;
using AspNetCoreHero.ToastNotification.Abstractions;
using Sig.Web.Interfaces;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Dapper;

namespace Sig.Web.Areas.Agencias.Controllers
{

    public class RelatorioController : BaseController
    {
        private readonly IUsuarioLogado _usuarioLogado;

        public RelatorioController(INotyfService notificacao, IUsuarioLogado usuarioLogado) : base(notificacao)
        {
            _usuarioLogado = usuarioLogado;
        }

        #region Relatório de Clientes

        public IActionResult Cliente()
        {
            #region ViewBag Atividades

            ViewBag.Atividades = new SelectList(_context.Atividades.OrderBy(c => c.Descricao), "AtividadeId", "Descricao");

            #endregion

            #region ViewBag Regiões

            IEnumerable<Regiao> regioes;

            if (User.IsInRole("Administradora"))
                regioes = _context.Regioes.OrderBy(c => c.AgenciaId);
            else
                regioes = _context.Regioes.Where(c => c.AgenciaId == _usuarioLogado.ObterClienteId()).OrderBy(c => c.AgenciaId);
            
            ViewBag.Regioes = new SelectList(regioes.AsEnumerable().Select(c => new { RegiaoId = c.RegiaoId, Descricao = $"{c.AgenciaId}-{c.Descricao}" }), "RegiaoId", "Descricao");

            #endregion

            #region ViewBag Colaboradores

            IEnumerable<Colaborador> colaboradores;

            if (User.IsInRole("Administradora"))
                colaboradores = _context.Colaboradores.OrderBy(c => c.AgenciaId).OrderBy(c => c.Nome);
            else
                colaboradores = _context.Colaboradores.Where(c => c.AgenciaId == _usuarioLogado.ObterClienteId()).OrderBy(c => c.AgenciaId).OrderBy(c => c.Nome);

            ViewBag.Colaboradores = new SelectList(colaboradores.AsEnumerable().Select(c => new { ColaboradorId = c.ColaboradorId, Nome = $"{c.ColaboradorId}-{c.Nome}" }), "ColaboradorId", "Nome");



            #endregion

            #region ViewBag Status

            ViewBag.Status = new SelectList(new[] { "Sim", "Não" });

            #endregion

            return View();
        }

        [HttpPost]
        public IActionResult Cliente(RelatorioClienteViewModel viewModel)
        {
            #region ViewBag Atividades

            ViewBag.Atividades = new SelectList(_context.Atividades.OrderBy(c => c.Descricao), "AtividadeId", "Descricao");

            #endregion

            #region ViewBag Regiões

            IEnumerable<Regiao> regioes;

            if (User.IsInRole("Administradora"))
                regioes = _context.Regioes.OrderBy(c => c.AgenciaId);
            else
                regioes = _context.Regioes.Where(c => c.AgenciaId == _usuarioLogado.ObterClienteId()).OrderBy(c => c.AgenciaId);

            ViewBag.Regioes = new SelectList(regioes.AsEnumerable().Select(c => new { RegiaoId = c.RegiaoId, Descricao = $"{c.AgenciaId}-{c.Descricao}" }), "RegiaoId", "Descricao");

            #endregion

            #region ViewBag Colaboradores

            IEnumerable<Colaborador> colaboradores;

            if (User.IsInRole("Administradora"))
                colaboradores = _context.Colaboradores.OrderBy(c => c.AgenciaId).OrderBy(c => c.Nome);
            else
                colaboradores = _context.Colaboradores.Where(c => c.AgenciaId == _usuarioLogado.ObterClienteId()).OrderBy(c => c.AgenciaId).OrderBy(c => c.Nome);

            ViewBag.Colaboradores = new SelectList(colaboradores.AsEnumerable().Select(c => new { ColaboradorId = c.ColaboradorId, Nome = $"{c.ColaboradorId}-{c.Nome}" }), "ColaboradorId", "Nome");

            #endregion

            #region ViewBag Status

            ViewBag.Status = new SelectList(new[] { "Sim", "Não" });

            #endregion


            #region Filtros

            IQueryable<Cliente> clientes;

            if (User.IsInRole("Administradora"))
                clientes = _context.Clientes.Where(c => c.TipoCliente == eTipoCliente.Cliente || c.TipoCliente == eTipoCliente.PrePago).OrderBy(c => c.Nome);
            else
                clientes = _context.Clientes.Where(c => c.ClienteId == _usuarioLogado.ObterClienteId());


            if (!viewModel.Cliente.ClienteId.Equals(0))
            {
                clientes = clientes.Where(c => c.ClienteId == viewModel.Cliente.ClienteId);
            }

            if (viewModel.Cliente.AgenciaId.HasValue)
            {
                if (User.IsInRole("Administradora"))
                {
                    clientes = clientes.Where(c => c.AgenciaId == viewModel.Cliente.AgenciaId);
                }
            }

            if (!string.IsNullOrEmpty(viewModel.Cliente.Nome))
            {
                clientes = clientes.Where(c => c.Nome.Contains(viewModel.Cliente.Nome));
            }

            if (!string.IsNullOrEmpty(viewModel.Cliente.RazaoSocial))
            {
                clientes = clientes.Where(c => c.RazaoSocial.Contains(viewModel.Cliente.RazaoSocial));
            }

            if (!string.IsNullOrEmpty(viewModel.Endereco.Bairro))
            {
                clientes = clientes.Where(c => c.Enderecos.Any(d => d.Bairro.Contains(viewModel.Endereco.Bairro)));
            }

            if (!string.IsNullOrEmpty(viewModel.Endereco.Cidade))
            {
                clientes = clientes.Where(c => c.Enderecos.Any(d => d.Cidade.Contains(viewModel.Endereco.Cidade)));
            }

            if (!viewModel.Cliente.AtividadeId.Equals(0))
            {
                clientes = clientes.Where(c => c.AtividadeId == viewModel.Cliente.AtividadeId);
            }

            if (viewModel.Cliente.RegiaoId.HasValue && viewModel.Cliente.RegiaoId != 0)
            {
                clientes = clientes.Where(c => c.RegiaoId == viewModel.Cliente.RegiaoId);
            }

            if (!viewModel.Contrato.VendedorId.Equals(0))
            {
                clientes = clientes.Where(c => c.Colaboradores.Any(d => d.ColaboradorId == viewModel.Contrato.VendedorId));
            }

            if (viewModel.DataInicioCadastro.HasValue)
            {
                clientes = clientes.Where(c => c.Contratos.Any(d => d.DataCadastro >= viewModel.DataInicioCadastro));
            }

            if (viewModel.DataFinalCadastro.HasValue)
            {
                clientes = clientes.Where(c => c.Contratos.Any(d => d.DataCadastro <= viewModel.DataFinalCadastro));
            }

            if (viewModel.DataInicioCancelamento.HasValue)
            {
                clientes = clientes.Where(c => c.Contratos.Any(d => d.DataCancelamento >= viewModel.DataInicioCancelamento));
            }

            if (viewModel.DataFinalCancelamento.HasValue)
            {
                clientes = clientes.Where(c => c.Contratos.Any(d => d.DataCancelamento <= viewModel.DataFinalCancelamento));
            }

            if (viewModel.Cliente.IsAtivo)
            {
                clientes = clientes.Where(c => c.IsAtivo);
            }
            else
            {
                clientes = clientes.Where(c => !c.IsAtivo);
            }

            #endregion

            viewModel.Clientes = clientes;

            return View(viewModel);
        }

        #endregion

        #region Relatório de Clientes Bloqueados

        public IActionResult ClienteBloqueado()
        {
            #region ViewBag Atividades

            ViewBag.Atividades = new SelectList(_context.Atividades.OrderBy(c => c.Descricao), "AtividadeId", "Descricao");

            #endregion

            #region ViewBag Regiões

            IEnumerable<Regiao> regioes;

            if (User.IsInRole("Administradora"))
                regioes = _context.Regioes.OrderBy(c => c.AgenciaId);
            else
                regioes = _context.Regioes.Where(c => c.AgenciaId == _usuarioLogado.ObterClienteId()).OrderBy(c => c.AgenciaId);

            ViewBag.Regioes = new SelectList(regioes.AsEnumerable().Select(c => new { RegiaoId = c.RegiaoId, Descricao = $"{c.AgenciaId}-{c.Descricao}" }), "RegiaoId", "Descricao");

            #endregion

            #region ViewBag Colaboradores

            IEnumerable<Colaborador> colaboradores;

            if (User.IsInRole("Administradora"))
                colaboradores = _context.Colaboradores.OrderBy(c => c.AgenciaId).OrderBy(c => c.Nome);
            else
                colaboradores = _context.Colaboradores.Where(c => c.AgenciaId == _usuarioLogado.ObterClienteId()).OrderBy(c => c.AgenciaId).OrderBy(c => c.Nome);

            ViewBag.Colaboradores = new SelectList(colaboradores.AsEnumerable().Select(c => new { ColaboradorId = c.ColaboradorId, Nome = $"{c.ColaboradorId}-{c.Nome}" }), "ColaboradorId", "Nome");

            #endregion

            return View();
        }

        [HttpPost]
        public IActionResult ClienteBloqueado(RelatorioClienteViewModel viewModel)
        {
            #region ViewBag Atividades

            ViewBag.Atividades = new SelectList(_context.Atividades.OrderBy(c => c.Descricao), "AtividadeId", "Descricao");

            #endregion

            #region ViewBag Regiões

            IEnumerable<Regiao> regioes;

            if (User.IsInRole("Administradora"))
                regioes = _context.Regioes.OrderBy(c => c.AgenciaId);
            else
                regioes = _context.Regioes.Where(c => c.AgenciaId == _usuarioLogado.ObterClienteId()).OrderBy(c => c.AgenciaId);

            ViewBag.Regioes = new SelectList(regioes.AsEnumerable().Select(c => new { RegiaoId = c.RegiaoId, Descricao = $"{c.AgenciaId}-{c.Descricao}" }), "RegiaoId", "Descricao");

            #endregion

            #region ViewBag Colaboradores

            IEnumerable<Colaborador> colaboradores;

            if (User.IsInRole("Administradora"))
                colaboradores = _context.Colaboradores.OrderBy(c => c.AgenciaId).OrderBy(c => c.Nome);
            else
                colaboradores = _context.Colaboradores.Where(c => c.AgenciaId == _usuarioLogado.ObterClienteId()).OrderBy(c => c.AgenciaId).OrderBy(c => c.Nome);

            ViewBag.Colaboradores = new SelectList(colaboradores.AsEnumerable().Select(c => new { ColaboradorId = c.ColaboradorId, Nome = $"{c.ColaboradorId}-{c.Nome}" }), "ColaboradorId", "Nome");

            #endregion

            #region Filtros

            IQueryable<Cliente> clientes;

            if (User.IsInRole("Administradora"))
                clientes = _context.Clientes.Where(c => c.TipoCliente == eTipoCliente.Cliente || c.TipoCliente == eTipoCliente.PrePago).OrderBy(c => c.Nome);
            else
                clientes = _context.Clientes.Where(c => c.ClienteId == _usuarioLogado.ObterClienteId());


            if (!viewModel.Cliente.ClienteId.Equals(0))
            {
                clientes = clientes.Where(c => c.ClienteId == viewModel.Cliente.ClienteId);
            }

            if (viewModel.Cliente.AgenciaId.HasValue)
            {
                if (User.IsInRole("Administradora"))
                {
                    clientes = clientes.Where(c => c.AgenciaId == viewModel.Cliente.AgenciaId);
                }
            }

            if (!string.IsNullOrEmpty(viewModel.Cliente.Nome))
            {
                clientes = clientes.Where(c => c.Nome.Contains(viewModel.Cliente.Nome));
            }

            if (!string.IsNullOrEmpty(viewModel.Cliente.RazaoSocial))
            {
                clientes = clientes.Where(c => c.RazaoSocial.Contains(viewModel.Cliente.RazaoSocial));
            }

            if (!string.IsNullOrEmpty(viewModel.Endereco.Bairro))
            {
                clientes = clientes.Where(c => c.Enderecos.Any(d => d.Bairro.Contains(viewModel.Endereco.Bairro)));
            }

            if (!string.IsNullOrEmpty(viewModel.Endereco.Cidade))
            {
                clientes = clientes.Where(c => c.Enderecos.Any(d => d.Cidade.Contains(viewModel.Endereco.Cidade)));
            }

            if (!viewModel.Cliente.AtividadeId.Equals(0))
            {
                clientes = clientes.Where(c => c.AtividadeId == viewModel.Cliente.AtividadeId);
            }

            if (viewModel.Cliente.RegiaoId.HasValue && viewModel.Cliente.RegiaoId != 0)
            {
                clientes = clientes.Where(c => c.RegiaoId == viewModel.Cliente.RegiaoId);
            }

            if (!viewModel.Contrato.VendedorId.Equals(0))
            {
                clientes = clientes.Where(c => c.Colaboradores.Any(d => d.ColaboradorId == viewModel.Contrato.VendedorId));
            }

            if (viewModel.DataInicioCadastro.HasValue)
            {
                clientes = clientes.Where(c => c.Contratos.Any(d => d.DataCadastro >= viewModel.DataInicioCadastro));
            }

            if (viewModel.DataFinalCadastro.HasValue)
            {
                clientes = clientes.Where(c => c.Contratos.Any(d => d.DataCadastro <= viewModel.DataFinalCadastro));
            }

            #endregion

            viewModel.Clientes = clientes.Where(c => !c.IsAtivo && c.Contratos.Any(e => !e.DataCancelamento.HasValue));

            return View(viewModel);
        }

        #endregion

        #region Relatório de Agencias

        public IActionResult Agencia()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Agencia(RelatorioClienteViewModel viewModel)
        {
            IQueryable<Cliente> clientes = _context.Clientes.Where(c => c.TipoCliente == eTipoCliente.Agencia);

            #region Filtros

            if (!viewModel.Cliente.ClienteId.Equals(0))
            {
                clientes = clientes.Where(c => c.ClienteId == viewModel.Cliente.ClienteId);
            }

            if (!string.IsNullOrEmpty(viewModel.Cliente.Nome))
            {
                clientes = clientes.Where(c => c.Nome.Contains(viewModel.Cliente.Nome));
            }

            if (!string.IsNullOrEmpty(viewModel.Cliente.RazaoSocial))
            {
                clientes = clientes.Where(c => c.RazaoSocial.Contains(viewModel.Cliente.RazaoSocial));
            }

            if (!string.IsNullOrEmpty(viewModel.Endereco.Bairro))
            {
                clientes = clientes.Where(c => c.Enderecos.Any(p => p.Bairro.Contains(viewModel.Endereco.Bairro)));
            }

            if (!string.IsNullOrEmpty(viewModel.Endereco.Cidade))
            {
                clientes = clientes.Where(c => c.Enderecos.Any(p => p.Cidade.Contains(viewModel.Endereco.Cidade)));
            }

            #endregion

            viewModel.Clientes = clientes;
            return View(viewModel);
        }

        #endregion

        #region Consultas Realizadas

        public IActionResult ConsultasRealizadas()
        {
            var hoje = DateTime.Today;
            var mes = new DateTime(hoje.Year, hoje.Month, 1);
            
            var vm = new ConsultasRealizadasViewModel();

            vm.DataInicial = mes.AddMonths(-1);
            vm.DataFinal = mes.AddDays(-1);

            return View(vm);
        }

        [HttpPost]
        public IActionResult ConsultasRealizadas(ConsultasRealizadasViewModel viewModel)
        {

            #region Filtros

            IQueryable<TransacaoConsulta> transacaoConsultas;

            if (User.IsInRole("Administradora"))
            {
                transacaoConsultas = _context.TransacoesConsultas.Where(c => c.IsConfirmado).OrderByDescending(c => c.NSU);

                if (viewModel.ClienteId.HasValue)
                {
                    transacaoConsultas = transacaoConsultas.Where(c => c.ClienteId == viewModel.ClienteId);
                }
            }
            else
            {
                transacaoConsultas = _context.TransacoesConsultas.Where(c => c.Cliente.AgenciaId == _usuarioLogado.ObterClienteId() && c.IsConfirmado).OrderByDescending(c => c.NSU);
            }

            if (viewModel.ClienteId.HasValue)
            {
                transacaoConsultas = transacaoConsultas.Where(c => c.ClienteId == viewModel.ClienteId);
            }

            if (viewModel.DataInicial.HasValue && viewModel.DataFinal.HasValue)
            {
                transacaoConsultas = transacaoConsultas.Where(c => c.DataEnvio.Date >= viewModel.DataInicial && c.DataEnvio.Date <= viewModel.DataFinal);
            }


            #endregion

            viewModel.TransacoesConsultas = transacaoConsultas.Take(100);

            return View(viewModel);
        }

        [HttpPost]
        public IActionResult ConsultasRealizadasPdf(ConsultasRealizadasViewModel viewModel)
        {
            #region Filtros

            IQueryable<TransacaoConsulta> transacaoConsultas;

            if (User.IsInRole("Administradora"))
            {
                transacaoConsultas = _context.TransacoesConsultas.Where(c => c.IsConfirmado).OrderByDescending(c => c.NSU);

                if (viewModel.ClienteId.HasValue)
                {
                    transacaoConsultas = transacaoConsultas.Where(c => c.ClienteId == viewModel.ClienteId);
                }
            }
            else
            {
                transacaoConsultas = _context.TransacoesConsultas.Where(c => c.Cliente.AgenciaId == _usuarioLogado.ObterClienteId() && c.IsConfirmado).OrderByDescending(c => c.NSU);
            }

            if (viewModel.ClienteId.HasValue)
            {
                transacaoConsultas = transacaoConsultas.Where(c => c.ClienteId == viewModel.ClienteId);
            }

            if (viewModel.DataInicial.HasValue && viewModel.DataFinal.HasValue)
            {
                transacaoConsultas = transacaoConsultas.Where(c => c.DataEnvio.Date >= viewModel.DataInicial && c.DataEnvio.Date <= viewModel.DataFinal);
            }

            #endregion

            //TODO: Corrigir
            //var bytes = transacaoConsultas.AsEnumerable().Select(o => new TransacaoConsulta { NSU = o.NSU, ItemFaturavelId = o.ItemFaturavelId, ItemFaturavelDescricao = o.ItemFaturavel.Descricao, ClienteId = o.ClienteId, IsConfirmado = o.IsConfirmado, DataEnvio = o.DataEnvio, DataResposta = o.DataResposta, ChaveConsulta = o.ChaveConsulta }).ToList().ToExcelBytes();
            //return File(bytes, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "ConsultasRealizadas.xlsx");

            return null;
        }

        public IActionResult ConsultasRealizadasDetalhe(int id = 0)
        {
            var transacaoConsulta = _context.TransacoesConsultas.Find(id);

            if (transacaoConsulta == null)
            {
                _notificacao.Warning("Consulta não encontrada");
                return RedirectToAction("Index", "Produto");
            }

            return View(transacaoConsulta);
        }

        #endregion

        #region Relatório de Contas a Receber

        public IActionResult ContasReceber()
        {
            return View();
        }

        [HttpPost]
        public IActionResult ContasReceber(ContasReceberViewModel viewModel)
        {
            IQueryable<Faturamento> faturamentos = _context.Faturamentos.Where(c => !c.DataPagamento.HasValue);

            if (!User.IsInRole("Administradora"))
            {
                faturamentos = faturamentos.Where(c => c.Cliente.AgenciaId == _usuarioLogado.ObterClienteId());
            }
            else
            {
                if (viewModel.AgenciaId.HasValue)
                {
                    faturamentos = faturamentos.Where(c => c.Cliente.AgenciaId == viewModel.AgenciaId);
                }
                else
                {
                    faturamentos = faturamentos.Where(c => c.Cliente.TipoCliente == eTipoCliente.Agencia);
                }
            }


            if (viewModel.DataInicialFaturamento.HasValue)
            {
                faturamentos = faturamentos.Where(c => c.DataInicialFaturamento == viewModel.DataInicialFaturamento);
            }


            viewModel.Faturamentos = faturamentos.OrderByDescending(c => c.FaturamentoId);
            return View(viewModel);
        }

        #endregion

        #region Contas Recebidas

        public IActionResult ContasRecebidas()
        {
            return View();
        }

        [HttpPost]
        public IActionResult ContasRecebidas(ContasRecebidasViewModel viewModel)
        {
            IQueryable<Faturamento> faturamentos = _context.Faturamentos.Where(c => c.DataPagamento.HasValue);

            if (!User.IsInRole("Administradora"))
            {
                faturamentos = faturamentos.Where(c => c.Cliente.AgenciaId == _usuarioLogado.ObterClienteId());
            }
            else
            {
                if (viewModel.AgenciaId.HasValue)
                {
                    faturamentos = faturamentos.Where(c => c.Cliente.AgenciaId == viewModel.AgenciaId);
                }
                else
                {
                    faturamentos = faturamentos.Where(c => c.Cliente.TipoCliente == eTipoCliente.Agencia);
                }
            }


            if (viewModel.DataInicialFaturamento.HasValue)
            {
                faturamentos = faturamentos.Where(c => c.DataInicialFaturamento == viewModel.DataInicialFaturamento);
            }


            viewModel.Faturamentos = faturamentos.OrderByDescending(c => c.FaturamentoId);
            return View(viewModel);
        }

        #endregion

        #region Análise de Faturamento

        public IActionResult AnaliseFaturamentoConsulta()
        {
            var hoje = DateTime.Today;
            var mes = new DateTime(hoje.Year, hoje.Month, 1);

            var vm = new AnaliseFaturamentoViewModel
            {
                DataInicial = mes.AddMonths(-1),
                DataFinal = mes.AddDays(-1)
            };

            return View(vm);
        }

        [HttpPost]
        public IActionResult AnaliseFaturamentoResposta(AnaliseFaturamentoViewModel viewModel)
        {
            #region SQL

            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT f.FaturamentoId, \n");
            sql.Append("	   c.ClienteId, \n");
            sql.Append("	   c.Nome AS Cliente, \n");
            sql.Append("	   c.AgenciaId, \n");
            sql.Append("	   cp.Nome AS Agencia, \n");
            sql.Append("	   cp.IsRevenda, \n");
            sql.Append("	   f.DataVencimento, \n");
            sql.Append("	   f.DataPagamento, \n");
            sql.Append("	    \n");
            sql.Append("	   -- LUCRO ADMINISTRADORA \n");
            sql.Append("	   CASE WHEN cp.IsComissionamento = 1 THEN \n");
            sql.Append("			(COALESCE(SUM(fi.ValorUnitario * fi.Quantidade), 0) - SUM(fi.CustoUnitario * fi.Quantidade)) - (COALESCE(SUM(fi.ValorUnitario * fi.Quantidade),0) * (COALESCE(cp.comissaoagente, 0) / 100)) \n");
            sql.Append("	   WHEN cp.IsRevenda = 1 THEN \n");
            sql.Append("			(COALESCE(SUM(fi.ValorUnitario * fi.Quantidade), 0) - SUM(fi.CustoUnitario * fi.Quantidade)) - (COALESCE(SUM(fi.ValorUnitario * fi.Quantidade),0) - COALESCE(SUM(ti.ValorUnitario * fi.Quantidade),0)) \n");
            sql.Append("	   ELSE \n");
            sql.Append("			(COALESCE(SUM(fi.ValorUnitario * fi.Quantidade), 0) - SUM(fi.CustoUnitario * fi.Quantidade)) - ((COALESCE(SUM(fi.ValorUnitario * fi.Quantidade),0) - (SUM(fi.CustoUnitario * fi.Quantidade))) * (COALESCE(cp.comissaoagente, 0) / 100)) \n");
            sql.Append("	   END AS LucroAdministradora, \n");
            sql.Append("	   -- END LUCRO ADMINISTRADORA \n");
            sql.Append("	    \n");
            sql.Append("	   -- FATURAMENTO CUSTO \n");
            sql.Append("	   SUM(fi.CustoUnitario * fi.Quantidade) AS FaturamentoCusto, \n");
            sql.Append("	   -- END FATURAMENTO CUSTO \n");
            sql.Append("	    \n");
            sql.Append("	    \n");
            sql.Append("		-- FATURAMENTO CUSTO REVENDA -- SE FOR ADMINISTRADORA ENXERGA TUDO, SE FOR REVENDA NAO ENXERGA FATURAMENTOCUSTO \n");
            sql.Append("		CASE WHEN cp.IsRevenda = 1 THEN \n");
            sql.Append("			COALESCE(SUM(ti.ValorUnitario * fi.Quantidade),0) \n");
            sql.Append("		ELSE \n");
            sql.Append("			SUM(fi.CustoUnitario * fi.Quantidade) \n");
            sql.Append("		END As FaturamentoCustoRevenda, \n");
            sql.Append("		-- END FATURAMENTO CUSTO REVENDA \n");
            sql.Append("	 \n");
            sql.Append("		-- REPASSE REAL \n");
            sql.Append("		CASE WHEN cp.IsComissionamento = 1 THEN \n");
            sql.Append("			CASE WHEN ((SUM(fi.ValorUnitario * fi.Quantidade) * COALESCE(cp.ComissaoAgente, 0) / 100) - SUM(fi.ValorUnitario * fi.Quantidade) > 0) THEN \n");
            sql.Append("				(SUM(fi.ValorUnitario * fi.Quantidade) * COALESCE(cp.ComissaoAgente, 0) / 100) - SUM(fi.ValorUnitario * fi.Quantidade) \n");
            sql.Append("			ELSE \n");
            sql.Append("				0 \n");
            sql.Append("			END \n");
            sql.Append("	   ELSE \n");
            sql.Append("		    CASE WHEN cp.IsRevenda = 1 THEN \n");
            sql.Append("				COALESCE(f.ValorPago, 0) - COALESCE(SUM(ti.ValorUnitario * fi.Quantidade),0) \n");
            sql.Append("		    ELSE \n");
            sql.Append("				(COALESCE(CASE WHEN (f.TipoPagamento IN(1,3,5)) THEN f.ValorPago ELSE 0 END, 0)) - (CASE WHEN cp.IsComissionamento = 1 THEN SUM(fi.CustoUnitario * fi.Quantidade) + (COALESCE(SUM(fi.ValorUnitario * fi.Quantidade), 0) - SUM(fi.CustoUnitario * fi.Quantidade)) - (SUM(fi.ValorUnitario * fi.Quantidade) * COALESCE(cp.ComissaoAgente, 0) / 100) WHEN cp.IsRevenda = 1 THEN COALESCE(SUM(ti.ValorUnitario * fi.Quantidade),0) ELSE SUM(fi.CustoUnitario * fi.Quantidade) + (COALESCE(SUM(fi.ValorUnitario * fi.Quantidade), 0) - SUM(fi.CustoUnitario * fi.Quantidade)) - ((COALESCE(SUM(fi.ValorUnitario * fi.Quantidade),0) - (SUM(fi.CustoUnitario * fi.Quantidade))) * (COALESCE(cp.comissaoagente, 0) / 100)) END) \n");
            sql.Append("		    END \n");
            sql.Append("	   END AS RepasseReal, \n");
            sql.Append("		-- END REPASSE REAL \n");
            sql.Append("	    \n");
            sql.Append("	   -- FATURAMENTO BRUTO \n");
            sql.Append("	   SUM(fi.ValorUnitario * fi.Quantidade) AS FaturamentoBruto, \n");
            sql.Append("	   -- FATURAMENTO BRUTO \n");
            sql.Append("	    \n");
            sql.Append("	   -- COMISSAO AGENTE \n");
            sql.Append("	   COALESCE(cp.ComissaoAgente, 0) AS ComissaoAgente, \n");
            sql.Append("	   -- END COMISSAO AGENTE \n");
            sql.Append("	    \n");
            sql.Append("	   -- COMISSAO ADMINISTRADORA \n");
            sql.Append("	   (100 - (COALESCE(cp.ComissaoAgente, 0))) AS ComissaoAdministradora, \n");
            sql.Append("	   -- END COMISSAO ADMINISTRADORA \n");
            sql.Append("	   	 \n");
            sql.Append("	   -- RETENCAO AGENTE  ************************ REPASSE PONTENCIAL SOMENTE QUANDO FOR COMISSIONADO		 \n");
            sql.Append("	   CASE WHEN cp.IsComissionamento = 1 THEN \n");
            sql.Append("			(COALESCE(SUM(fi.ValorUnitario * fi.Quantidade),0) * (COALESCE(cp.comissaoagente, 0) / 100)) \n");
            sql.Append("	   WHEN cp.IsRevenda = 1 THEN \n");
            sql.Append("			(COALESCE(SUM(fi.ValorUnitario * fi.Quantidade),0) - COALESCE(SUM(ti.ValorUnitario * fi.Quantidade),0)) \n");
            sql.Append("	   ELSE \n");
            sql.Append("			((COALESCE(SUM(fi.ValorUnitario * fi.Quantidade),0) - (SUM(fi.CustoUnitario * fi.Quantidade))) * (COALESCE(cp.comissaoagente, 0) / 100)) \n");
            sql.Append("	   END AS RetencaoAgente, \n");
            sql.Append("	   -- FIM RETENCAO AGENTE 2  ******************* \n");
            sql.Append("	    \n");
            sql.Append("	   -- RETENCAO ADMINISTRADORA \n");
            sql.Append("	   (SUM(fi.ValorUnitario * fi.Quantidade) - SUM(fi.CustoUnitario * fi.Quantidade)) * ((100 - (COALESCE(cp.comissaoagente, 0))) / 100) AS RetencaoAdministradora, \n");
            sql.Append("	   -- END RETENCAO ADMINISTRADORA \n");
            sql.Append("	    \n");
            sql.Append("	   -- RETENCAO TOTAL \n");
            sql.Append("	   CASE WHEN cp.IsComissionamento = 1 THEN \n");
            sql.Append("			SUM(fi.CustoUnitario * fi.Quantidade) + (COALESCE(SUM(fi.ValorUnitario * fi.Quantidade), 0) - SUM(fi.CustoUnitario * fi.Quantidade)) - (SUM(fi.ValorUnitario * fi.Quantidade) * COALESCE(cp.ComissaoAgente, 0) / 100) \n");
            sql.Append("	   WHEN cp.IsRevenda = 1 THEN \n");
            sql.Append("			COALESCE(SUM(ti.ValorUnitario * fi.Quantidade),0) \n");
            sql.Append("	   ELSE \n");
            sql.Append("			SUM(fi.CustoUnitario * fi.Quantidade) + (COALESCE(SUM(fi.ValorUnitario * fi.Quantidade), 0) - SUM(fi.CustoUnitario * fi.Quantidade)) - ((COALESCE(SUM(fi.ValorUnitario * fi.Quantidade),0) - (SUM(fi.CustoUnitario * fi.Quantidade))) * (COALESCE(cp.comissaoagente, 0) / 100)) \n");
            sql.Append("	   END AS RetencaoTotal, \n");
            sql.Append("	   -- END RETENCAO TOTAL \n");
            sql.Append("		 \n");
            sql.Append("	   -- VALOR PAGO \n");
            sql.Append("	   COALESCE(CASE WHEN (f.TipoPagamento IN(1,3,5)) THEN \n");
            sql.Append("		f.ValorPago \n");
            sql.Append("	   ELSE 0 \n");
            sql.Append("	   END, 0) AS ValorPago, \n");
            sql.Append("	   -- END VALOR PAGO \n");
            sql.Append("	   f.LinkBoleto \n");
            sql.Append("	    \n");
            sql.Append("FROM   cliente c \n");
            sql.Append("	   INNER JOIN Cliente cp \n");
            sql.Append("			   ON cp.ClienteId = c.AgenciaId \n");
            sql.Append("	   INNER JOIN Faturamento f \n");
            sql.Append("			   ON f.ClienteId = c.ClienteId \n");
            sql.Append("	   INNER JOIN FaturamentoItem fi \n");
            sql.Append("			   ON fi.FaturamentoId = f.FaturamentoId \n");
            sql.Append("	   INNER JOIN ItemFaturavel ifa \n");
            sql.Append("			   ON fi.ItemFaturavelId = ifa.ItemFaturavelId \n");
            sql.Append("	   LEFT JOIN TabelaRevenda tr \n");
            sql.Append("			  ON cp.ClienteId = tr.ClienteId \n");
            sql.Append("	   LEFT JOIN Tabela t \n");
            sql.Append("			  ON tr.TabelaId = t.TabelaId \n");
            sql.Append("	   LEFT JOIN TabelaItem ti \n");
            sql.Append("			  ON t.TabelaId = ti.TabelaId \n");
            sql.Append("				 AND ifa.ItemFaturavelId = ti.ItemFaturavelId \n");
            sql.Append("WHERE  f.DataInicialFaturamento	= {0} \n");
            sql.Append("	   AND f.DataFinalFaturamento	= {1} \n");

            if (!User.IsInRole("Administradora"))
            {
                sql.Append(string.Format("AND f.ClienteId IN(Select ClienteId From Cliente Where AgenciaId = {0} OR ClienteId = {1}) \n",
                    _usuarioLogado.ObterClienteId(), _usuarioLogado.ObterClienteId()));
            }

            sql.Append(" \n");
            sql.Append("GROUP  BY f.FaturamentoId, \n");
            sql.Append("		  f.ValorPago, \n");
            sql.Append("		  f.LinkBoleto, \n");
            sql.Append("		  c.ClienteId, \n");
            sql.Append("		  c.IsRevenda, \n");
            sql.Append("		  c.IsComissionamento, \n");
            sql.Append("		  cp.ComissaoAgente, \n");
            sql.Append("		  cp.IsRevenda, \n");
            sql.Append("		  cp.IsComissionamento, \n");
            sql.Append("		  c.Nome, \n");
            sql.Append("		  cp.Nome, \n");
            sql.Append("		  c.AgenciaId, \n");
            sql.Append("		  f.DataVencimento, \n");
            sql.Append("		  f.TipoPagamento, \n");
            sql.Append("		  f.DataPagamento \n");
            sql.Append("ORDER  BY c.AgenciaId, \n");
            sql.Append("		  cp.Nome");

            #endregion

            var result = _context.Database.GetDbConnection().Query<AnaliseFaturamentoViewModel>(sql.ToString(), new { viewModel.DataInicial, viewModel.DataFinal });

            TempData["DataInicial"] = viewModel.DataInicial;
            TempData["DataFinal"] = viewModel.DataFinal;

            return View(result);
        }

        [HttpPost]
        public IActionResult AnaliseFaturamentoPdf(DateTime dataInicial, DateTime dataFinal)
        {
            #region SQL

            StringBuilder sql = new();
            sql.AppendLine("Select f.FaturamentoId,  ");
            sql.AppendLine("       c.ClienteId,  ");
            sql.AppendLine("       c.Nome As Cliente,  ");
            sql.AppendLine("       c.AgenciaId,  ");
            sql.AppendLine("       cp.Nome As Agencia,  ");
            sql.AppendLine("       COALESCE(CASE WHEN cp.IsRevenda = 1 OR c.IsRevenda = 1 THEN SUM(ifa.CustoRevenda * fi.Quantidade) - SUM(ifa.CustoUnitario * fi.Quantidade) END,0)  AS LucroAdministradora, ");
            sql.AppendLine("       SUM(fi.CustoUnitario * fi.Quantidade) As FaturamentoCusto,  ");
            sql.AppendLine("       SUM(fi.ValorUnitario * fi.Quantidade) As FaturamentoBruto,  ");
            sql.AppendLine("       COALESCE(cp.ComissaoAgente,0) As ComissaoAgente,  ");
            sql.AppendLine("       (100 - (COALESCE(cp.ComissaoAgente,0))) As ComissaoAdministradora,  ");
            sql.AppendLine("       (SUM(fi.ValorUnitario * fi.Quantidade) - SUM(fi.CustoUnitario * fi.Quantidade)) * (COALESCE(cp.ComissaoAgente,0) / 100) As RetencaoAgente,  ");
            sql.AppendLine("       (SUM(fi.ValorUnitario * fi.Quantidade) - SUM(fi.CustoUnitario * fi.Quantidade)) * ((100 - (COALESCE(cp.ComissaoAgente,0)))/100) As RetencaoAdministradora,  ");
            sql.AppendLine("       SUM(fi.CustoUnitario * fi.Quantidade) + (SUM(fi.ValorUnitario * fi.Quantidade) - SUM(fi.CustoUnitario * fi.Quantidade)) * ((100 - (COALESCE(cp.ComissaoAgente,0)))/100) As RetencaoTotal,  ");
            sql.AppendLine("       f.DataVencimento,  ");
            sql.AppendLine("       f.DataPagamento, ");
            sql.AppendLine("       COALESCE(f.ValorPago,0) AS ValorPago ");
            sql.AppendLine("From Cliente c ");
            sql.AppendLine("Inner join Cliente cp On cp.ClienteId = c.AgenciaId ");
            sql.AppendLine("Inner join Faturamento f On f.ClienteId = c.ClienteId ");
            sql.AppendLine("Inner join FaturamentoItem fi On fi.FaturamentoId = f.FaturamentoId ");
            sql.AppendLine("Inner join ItemFaturavel ifa On fi.ItemFaturavelId = ifa.ItemFaturavelId");
            sql.AppendLine("Where f.DataInicialFaturamento = {0} And f.DataFinalFaturamento = {1} ");

            if (!User.IsInRole("Administradora"))
            {
                sql.AppendLine(string.Format("And f.ClienteId IN(Select ClienteId From Cliente Where AgenciaId = {0} OR ClienteId = {1}) ",
                    _usuarioLogado.ObterClienteId(), _usuarioLogado.ObterClienteId()));
            }

            sql.AppendLine("Group By f.FaturamentoId,  ");
            sql.AppendLine("         f.ValorPago,  ");
            sql.AppendLine("         c.ClienteId,  ");
            sql.AppendLine("         c.IsRevenda, ");
            sql.AppendLine("         cp.ComissaoAgente,  ");
            sql.AppendLine("         cp.IsRevenda, ");
            sql.AppendLine("         c.Nome,  ");
            sql.AppendLine("         cp.Nome,  ");
            sql.AppendLine("         c.AgenciaId,  ");
            sql.AppendLine("         f.DataVencimento,  ");
            sql.AppendLine("         f.DataPagamento ");
            sql.AppendLine("Order By c.AgenciaId,  ");
            sql.AppendLine("         cp.Nome");

            #endregion

            //TODO: Corrigir

            //var bytes = _context.Database.GetDbConnection().Query<AnaliseFaturamentoViewModel>(sql.ToString(), new { dataInicial, dataFinal }).ToList().ToExcelBytes();
            //return File(bytes, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "AnaliseFaturamento.xlsx");

            return null;
        }

        

        //[HttpGet]
        //public IActionResult AnaliseFaturamentoDetalhe(int id = 0)
        //{
        //    var viewModel = new AnaliseFaturamentoDetalheViewModel
        //    {
        //        Faturamento = _context.Faturamentos.Find(id),
        //        FaturamentoItens = _context.FaturamentoItems.Where(c => c.FaturamentoId == id)
        //    };

        //    return View(viewModel);
        //}

        #endregion

        #region Clientes Devedores

        public IActionResult ClientesDevedores()
        {
            return View();
        }

        [HttpPost]
        public IActionResult ClientesDevedores(ClientesDevedoresViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                IQueryable<Faturamento> faturamentos = _context.Faturamentos.Where(c => c.DataPagamento.HasValue == false);

                if (User.IsInRole("Administradora"))
                {
                    if (viewModel.AgenciaId.HasValue)
                    {
                        faturamentos = faturamentos.Where(c => c.Cliente.AgenciaId == viewModel.AgenciaId.Value);
                    }
                }
                else
                {
                    faturamentos = faturamentos.Where(c => c.Cliente.AgenciaId == _usuarioLogado.ObterClienteId());
                }

                if (viewModel.ClienteId.HasValue)
                {
                    faturamentos = faturamentos.Where(c => c.ClienteId == viewModel.ClienteId.Value);
                }

                faturamentos = faturamentos.Where(c =>
                c.DataVencimento.Date >= viewModel.DataInicial
                && c.DataVencimento.Date <= viewModel.DataFinal);

                viewModel.Faturamentos = faturamentos.OrderBy(c => c.ClienteId);
            }

            
            return View(viewModel);
        }

        #endregion

        #region Clientes Prospectos
        public IActionResult Prospectos()
        {
            var viewModel = new ProspectosViewModel
            {
                Clientes = _context.Clientes.Where(c => c.Contratos.Count == 0 && c.TipoCliente == eTipoCliente.Cliente)
            };

            ViewBag.Estados = new SelectList(Estado.UFs, "Sigla", "Descricao");

            return View(viewModel);
        }

        [HttpPost]
        public IActionResult Prospectos(ProspectosViewModel viewModel)
        {
            IQueryable<Cliente> query = _context.Clientes.Where(c => c.Contratos.Count == 0 && c.TipoCliente == eTipoCliente.Cliente);

            if (ModelState.IsValid)
            {
                if (viewModel.AgenciaId.HasValue)
                {
                    query = query.Where(c => c.AgenciaId == viewModel.AgenciaId.Value);
                }

                if (!string.IsNullOrEmpty(viewModel.Cidade))
                {
                    query = query.Where(c => c.Enderecos.Any(x => x.Cidade.Contains(viewModel.Cidade)));
                }

                if (!string.IsNullOrEmpty(viewModel.UF))
                {
                    query = query.Where(c => c.Enderecos.Any(x => x.UF.Contains(viewModel.UF)));
                }
            }

            viewModel.Clientes = query;
            ViewBag.Estados = new SelectList(Estado.UFs, "Sigla", "Descricao");

            return View(viewModel);
        }

        #endregion

    }
}