﻿using Sig.Web.Models;
using Sig.Web.Models.ViewModels;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using AspNetCoreHero.ToastNotification.Abstractions;
using Microsoft.EntityFrameworkCore;

namespace Sig.Web.Areas.Agencias.Controllers
{
    public class CreditoController : BaseController
    {
        public CreditoController(INotyfService notificacao) : base(notificacao)
        {
        }


        #region Index

        public IActionResult Index()
        {
            var viewModel = new CreditoViewModel
            {
                Creditos = _context.Creditos
            };

            return View(viewModel);
        }

        [HttpPost]
        public IActionResult Index(CreditoViewModel viewModel)
        {
            IQueryable<Credito> creditos = _context.Creditos;

            #region Filtros

            if (!viewModel.Credito.CreditoId.Equals(0))
            {
                creditos = creditos.Where(c => c.CreditoId == viewModel.Credito.CreditoId);
            }

            if (!string.IsNullOrEmpty(viewModel.Credito.Descricao))
            {
                creditos = creditos.Where(c => c.Descricao.Contains(viewModel.Credito.Descricao));
            }

            if (!viewModel.Credito.Valor.Equals(0))
            {
                creditos = creditos.Where(c => c.Valor == viewModel.Credito.Valor);
            }
            #endregion

            viewModel.Creditos = creditos;
            return View(viewModel);
        }

        #endregion

        #region Incluir

        public IActionResult Incluir()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Incluir(Credito credito)
        {
            if (ModelState.IsValid)
            {
                _context.Creditos.Add(credito);
                _context.SaveChanges();

                _notificacao.Success("Crédito incluído com sucesso");
                return RedirectToAction("Index");
            }
            return View(credito);
        }

        #endregion

        #region Alterar

        public IActionResult Alterar(int id)
        {
            var credito = _context.Creditos.Find(id);
            return View(credito);
        }

        [HttpPost]
        public IActionResult Alterar(Credito credito)
        {
            if (ModelState.IsValid)
            {
                _context.Entry(credito).State = EntityState.Modified;
                _context.SaveChanges();

                _notificacao.Success("Crédito Alterado com sucesso");
                return RedirectToAction("Index");

            }
            return View(credito);
        }

        #endregion
    }
}
