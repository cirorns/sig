﻿using Sig.Web.Models;
using Sig.Web.Models.ViewModels;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Sig.Web.Helpers;
using System.Transactions;
using System;
using System.Text;
using AspNetCoreHero.ToastNotification.Abstractions;

namespace Sig.Web.Areas.Agencias.Controllers
{
    
    public class PedidoCreditoController : BaseController
    {
        public PedidoCreditoController(INotyfService notificacao) : base(notificacao)
        {
        }
        #region Index

        public IActionResult Index()
        {
            var viewModel = new PedidoCreditoViewModel();
            viewModel.PedidoCreditos = _context.PedidoCreditos.OrderByDescending(c => c.PedidoCreditoId);

            return View(viewModel);
        }

        [HttpPost]
        public IActionResult Index(PedidoCreditoViewModel viewModel)
        {
            IQueryable<PedidoCredito> pedidoCreditos = _context.PedidoCreditos;

            #region Filtros

            if (!viewModel.PedidoCredito.ClienteId.Equals(0))
            {
                pedidoCreditos = pedidoCreditos.Where(c => c.ClienteId == viewModel.PedidoCredito.ClienteId);
            }

            if (!viewModel.PedidoCredito.PedidoCreditoId.Equals(0))
            {
                pedidoCreditos = pedidoCreditos.Where(c => c.PedidoCreditoId == viewModel.PedidoCredito.PedidoCreditoId);
            }

            if (!string.IsNullOrEmpty(viewModel.PedidoCredito.Cliente.Nome))
            {
                pedidoCreditos = pedidoCreditos.Where(c => c.Cliente.Nome.Contains(viewModel.PedidoCredito.Cliente.Nome));
            }

            pedidoCreditos = pedidoCreditos.OrderByDescending(c => c.PedidoCreditoId);

            #endregion

            viewModel.PedidoCreditos = pedidoCreditos;
            return View(viewModel);
        }

        #endregion

        #region Confirmação do Pedido

        public IActionResult ConfirmacaoManual(int id = 0)
        {
            var pedidoCredito = _context.PedidoCreditos.Find(id);

            if (pedidoCredito == null)
            {
                _notificacao.Warning("Não foi encontrado o pedido");
                return RedirectToAction("Index");
            }

            return View(pedidoCredito);
        }

        [HttpPost]
        public IActionResult ConfirmacaoManual(PedidoCredito form)
        {
            var pedidoCredito = _context.PedidoCreditos.Find(form.PedidoCreditoId);

            if (!pedidoCredito.IsConfirmado)
            {
                if (ModelState.IsValid)
                {
                    using (var ts = new TransactionScope())
                    {
                        pedidoCredito.IsConfirmado = true;
                        pedidoCredito.DataConfirmacao = DateTime.Now;
                        pedidoCredito.DataVencimento = DateTime.Now.AddDays(30);

                        _context.SaveChanges();

                        #region Cria Usuário de Consulta

                        var usuarioConsulta = _context.UsuarioConsultas.FirstOrDefault(c => c.Email == pedidoCredito.Cliente.Email 
                        && c.ClienteId == pedidoCredito.ClienteId);

                        if (usuarioConsulta == null)
                        {
                            string senha = Util.ObterSenhaAleatoria(8);

                            usuarioConsulta = new UsuarioConsulta
                            {
                                ClienteId = pedidoCredito.ClienteId,
                                Email = pedidoCredito.Cliente.Email,
                                IsSuperUsuario = true,
                                Senha = senha,
                                ConfirmarSenha = senha,
                            };

                            _context.UsuarioConsultas.Add(usuarioConsulta);
                            _context.SaveChanges();
                        }

                        #endregion

                        #region Dispara Email

                        StringBuilder sb = new StringBuilder();

                        sb.AppendLine("Seu pedido foi confirmado.<br/><br/>");
                        sb.AppendLine("<hr/>");
                        sb.AppendLine($"Olá, {usuarioConsulta.Cliente.Nome.ToUpper()} Seja bem vindo!");
                        sb.AppendLine("<hr/>");
                        sb.AppendLine("Anote os seguintes dados:<br/><br/>");
                        sb.AppendLine($"Cliente: <strong>{usuarioConsulta.ClienteId}</strong><br/>");
                        sb.AppendLine($"Email:   <strong>{usuarioConsulta.Email}</strong><br/>");
                        sb.AppendLine($"Senha:   <strong>{usuarioConsulta.Senha}</strong><br/>");
                        sb.AppendLine("<hr/>");
                        sb.AppendLine("Por favor não responda este email");

                        //TODO: Corrigir
                        //Commons.EnviaEmail(usuarioConsulta.Email, "Confirmação efetivação de pagamento", sb.ToString());

                        _notificacao.Success("Pedido confirmado.<br/>Um email foi disparado com seus dados de acesso. <br/>Dados do usuário criado com sucesso");

                        ts.Complete();

                        #endregion
                    }
                }
            }
            else
            {
                _notificacao.Warning("Este pedido já foi confirmado anteriormente");
                return RedirectToAction("Index");
            }

            return RedirectToAction("Index");
        }

        #endregion

    }
}
