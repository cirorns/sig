﻿using AspNetCoreHero.ToastNotification.Abstractions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Sig.Web.Models;
using System;
using System.Data.SqlClient;
using System.IO;
using Dapper;
using System.Text;
using System.Linq;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using OfficeOpenXml;
using Sig.Web.Helpers;

namespace Sig.Web.Areas.Agencias.Controllers
{
    public class ImportarController : BaseController
    {
        private readonly IWebHostEnvironment _webHostEnvironment;

        public ImportarController(INotyfService notificacao, IWebHostEnvironment webHostEnvironment) : base(notificacao)
        {
            _webHostEnvironment = webHostEnvironment;
        }

        #region Importar Planilha Serasa

        private void DeletarImportacaoAntiga(DateTime dataInicial, DateTime dataFinal, int? clienteId)
        {
            if (clienteId.HasValue)
            {
                string sql = "DELETE FROM TransacaoConsulta WHERE DataInicial = @p0 AND DataFinal = @p1 AND ClienteId = @p2";

                _context.Database.ExecuteSqlRaw(sql,
                    new SqlParameter("p0", dataInicial),
                    new SqlParameter("p1", dataFinal),
                    new SqlParameter("p2", clienteId));
            }
            else
            {
                string sql = "DELETE FROM TransacaoConsulta WHERE DataInicial = @p0 AND DataFinal = @p1";

                _context.Database.ExecuteSqlRaw(sql,
                    new SqlParameter("p0", dataInicial),
                    new SqlParameter("p1", dataFinal));
            }
        }

        private ItemFaturavel GetItemFaturavel(string descricao)
        {
            StringBuilder sql = new();
            sql.AppendLine("SELECT TOP 1  i.* FROM ItemFaturavel i INNER JOIN ItemFaturavelRelacionado r ON i.ItemFaturavelId = r.ItemFaturavelId WHERE r.Descricao = @p0");

            return _context.Database.GetDbConnection().QuerySingle<ItemFaturavel>(sql.ToString(),
                new SqlParameter("p0", descricao));
        }

        public IActionResult Planilha()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Planilha(IFormFile Arquivo, PlanilhaSerasa form)
        {
            StringBuilder sb = new();
            List<ItemFaturavel> itensFaturaveisEncontrados = new();
            List<Serasa> serasasEncontrados = new();

            int ValorTeste = 0;
            int j = 0;
            int quantidadeProcessada = 0;

            if (ModelState.IsValid)
            {
                if (Arquivo != null)
                {
                    //Deleta Importação Transações Antigas
                    DeletarImportacaoAntiga(form.DataInicial, form.DataFinal, form.ClienteId);

                    string uploads = Path.Combine(_webHostEnvironment.WebRootPath, "Uploads");
                    string filePath = Path.Combine(uploads, Arquivo.FileName);

                    using (Stream fileStream = new FileStream(filePath, FileMode.Create))
                    {
                        Arquivo.CopyTo(fileStream);
                    }

                    var bytes = System.IO.File.ReadAllBytes(filePath);
                    using var stream = new MemoryStream(bytes);
                    using var package = new ExcelPackage(stream);

                    var worksheet = package.Workbook.Worksheets[0];
                    var rowCount = worksheet.Dimension.Rows;
                    var colCount = worksheet.Dimension.Columns;
                    var header = ExcelUtils.CellsToArray(worksheet, 1, colCount);

                    for (var row = 2; row <= rowCount; row++)
                    {
                        var data = ExcelUtils.CellsToArray(worksheet, row, colCount);

                        var posicao = int.Parse(ExcelUtils.GetData("POSICAO", header, data));
                        var cnpj = ExcelUtils.GetData("CNPJ", header, data);
                        var razaosocial = ExcelUtils.GetData("RAZAOSOCIAL", header, data);
                        var dataConsultaLogon = DateTime.Parse(ExcelUtils.GetData("DATACONSULTA", header, data));
                        var produto = ExcelUtils.GetData("PRODUTO", header, data);
                        var logon = ExcelUtils.GetData("LOGON", header, data);
                        var quantidadeConsultadaLogon = int.Parse(ExcelUtils.GetData("QUANTIDADE", header, data));

                        logon ??= "0";

                        if (int.TryParse(logon, out ValorTeste))
                        {
                            logon = logon.Length < 8 ? logon.PadLeft(8, '0') : logon;
                        }

                        Serasa serasa;
                        ItemFaturavel consulta;

                        if (!serasasEncontrados.Any(c => c.Logon == logon))
                        {
                            serasa = _context.Serasas.AsNoTracking().FirstOrDefault(c => c.Logon == logon);

                            if (serasa != null)
                            {
                                if (serasa.IsInterno)
                                {
                                    if (!sb.ToString().Contains(logon))
                                        sb.AppendLine(string.Format("POSICAO: {0} LOGON SENDO USADO INTERNAMENTE: {1}<br/>", posicao == null ? 0 : posicao, logon));

                                    logon = "00000000";
                                }
                                else
                                {
                                    serasasEncontrados.Add(serasa);
                                }
                            }
                            else
                            {
                                if (!sb.ToString().Contains(logon))
                                    sb.AppendLine($"POSICAO: {posicao} LOGON NAO ENCONTRADO: {logon}<br/>");
                            }
                        }
                        else
                        {
                            serasa = serasasEncontrados.FirstOrDefault(c => c.Logon == logon);
                        }


                        if (!itensFaturaveisEncontrados.Any(c => c.Descricao == produto))
                        {
                            consulta = _context.ItemFaturaveis.AsNoTracking().FirstOrDefault(c => c.Descricao == produto);

                            if (consulta == null)
                            {
                                var relacionado = GetItemFaturavel(produto);

                                if (relacionado == null)
                                {
                                    if (!sb.ToString().Contains(produto))
                                        sb.AppendLine($"POSICAO: {posicao} CONSULTA NAO ENCONTRADA: {produto}<br/>");
                                }
                                else
                                {
                                    consulta = relacionado;
                                    itensFaturaveisEncontrados.Add(consulta);
                                }
                            }
                            else
                            {
                                itensFaturaveisEncontrados.Add(consulta);
                            }
                        }
                        else
                        {
                            consulta = itensFaturaveisEncontrados.FirstOrDefault(c => c.Descricao == produto);
                        }

                        if (logon != "00000000")
                        {
                            //Laço
                            for (int i = 0; i < quantidadeConsultadaLogon; i++)
                            {
                                if (serasa != null && consulta != null)
                                {
                                    var transacaoConsulta = new TransacaoConsulta
                                    {
                                        ClienteId = serasa.ClienteId,
                                        DadosEnvio = string.Format("IMPORTACAO {0}", logon),
                                        DadosResposta = "SEM RESPOSTA",
                                        DataEnvio = dataConsultaLogon,
                                        DataResposta = dataConsultaLogon,
                                        IsConfirmado = true,
                                        ItemFaturavelId = consulta.ItemFaturavelId,
                                        DataInicial = form.DataInicial,
                                        DataFinal = form.DataFinal,
                                    };

                                    _context.TransacoesConsultas.Add(transacaoConsulta);

                                    j++;
                                }
                            }

                            quantidadeProcessada += quantidadeConsultadaLogon;
                        }

                        if (j > 50)
                        {
                            _context.SaveChanges();
                            j = 0;
                        }
                    }

                    _context.SaveChanges();
                }
            }

            sb.AppendLine(string.Format("<br/><br/>QUANTIDADE PROCESSADA: {0} CONSULTA(S)<br/>", quantidadeProcessada));

            if (sb.Length > 0)
                _notificacao.Error(sb.ToString());

            return View();
        }

        #endregion

    }
}
