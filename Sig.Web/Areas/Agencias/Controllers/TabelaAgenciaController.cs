﻿using Sig.Web.Models;
using Sig.Web.Models.ViewModels;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Sig.Web.Models.Enums;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Rendering;
using AspNetCoreHero.ToastNotification.Abstractions;

namespace Sig.Web.Areas.Agencias.Controllers
{

    [Authorize(Roles = "Administradora")]
    public class TabelaAgenciaController : BaseController
    {
        public TabelaAgenciaController(INotyfService notificacao) : base(notificacao)
        {
        }

        #region Index

        public IActionResult Index()
        {
            var tabelaAgencias = _context.TabelaAgencias.OrderBy(c => c.AgenciaId).ToList();
            var viewModel = new TabelaAgenciaViewModel
            {
                TabelasAgencia = tabelaAgencias
            };

            return View(viewModel);
        }

        [HttpPost]
        public IActionResult Index(TabelaAgenciaViewModel viewModel)
        {
            IQueryable<TabelaAgencia> tabelaAgencias = _context.TabelaAgencias;

            if (viewModel.Agencia.AgenciaId.HasValue)
                tabelaAgencias = tabelaAgencias.Where(c => c.AgenciaId == viewModel.Agencia.AgenciaId);

            if (!viewModel.Tabela.TabelaId.Equals(0))
                tabelaAgencias = tabelaAgencias.Where(c => c.TabelaId == viewModel.Tabela.TabelaId);

            if (!string.IsNullOrEmpty(viewModel.Tabela.Descricao))
                tabelaAgencias = tabelaAgencias.Where(c => c.Tabela.Descricao.Contains(viewModel.Tabela.Descricao));


            viewModel.TabelasAgencia = tabelaAgencias;
            return View(viewModel);
        }

        #endregion

        #region Incluir

        public IActionResult Incluir()
        {
            ViewBag.Agencias = new SelectList(_context.Clientes.Where(c => c.TipoCliente == eTipoCliente.Agencia || c.TipoCliente == eTipoCliente.Administradora).Select(c => new { ClienteId = c.ClienteId, Nome = c.ClienteId + " - " + c.Nome }).OrderBy(c => c.ClienteId), "ClienteId", "Nome");
            ViewBag.Tabelas  = new SelectList(_context.Tabelas.Select(c => new { TabelaId = c.TabelaId, Descricao = c.TabelaId + " - " + c.Descricao }).OrderBy(c => c.Descricao), "TabelaId", "Descricao");
            
            return View();
        }

        [HttpPost]
        public IActionResult Incluir(TabelaAgencia tabelaAgencia)
        {
            if (ModelState.IsValid)
            {
                //Verifico se NÃO existe a tabela para esta agência
                if (!_context.TabelaAgencias.Any(c => c.TabelaId == tabelaAgencia.TabelaId && c.Agencia.TipoCliente == eTipoCliente.Agencia))
                {
                    _context.TabelaAgencias.Add(tabelaAgencia);
                    _context.SaveChanges();

                    _notificacao.Success("Tabela cadastrada na agência com sucesso");
                    return RedirectToAction("Index");
                }

                _notificacao.Warning("Esta tabela já está cadastrada para uma agência");

            }

            ViewBag.Agencias = new SelectList(_context.Clientes.Where(c => c.TipoCliente == eTipoCliente.Agencia || c.TipoCliente == eTipoCliente.Administradora).Select(c => new { ClienteId = c.ClienteId, Nome = c.ClienteId + " - " + c.Nome }).OrderBy(c => c.ClienteId), "ClienteId", "Nome");
            ViewBag.Tabelas = new SelectList(_context.Tabelas.Select(c => new { TabelaId = c.TabelaId, Descricao = c.TabelaId + " - " + c.Descricao }).OrderBy(c => c.Descricao), "TabelaId", "Descricao");

            return View();
        }

        #endregion

    }
}
