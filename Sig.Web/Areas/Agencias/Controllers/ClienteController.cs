﻿using Sig.Web.Models;
using Sig.Web.Models.ViewModels;
using Sig.Web.Helpers;
using System;
using System.Linq;
using System.Transactions;
using Microsoft.AspNetCore.Mvc;
using Sig.Web.Models.Enums;
using Microsoft.AspNetCore.Authorization;
using AspNetCoreHero.ToastNotification.Abstractions;
using Sig.Web.Interfaces;
using Sig.Web.Helpers.Extensoes;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Sig.Web.Areas.Agencias.Controllers
{
    [Authorize(Roles = "Administradora, Agencia")]
    public class ClienteController : BaseController
    {
        private readonly IUsuarioLogado _usuarioLogado;

        public ClienteController(INotyfService notificacao, IUsuarioLogado usuarioLogado) : base(notificacao)
        {
            _usuarioLogado = usuarioLogado;
        }

        #region Index

        public IActionResult Index()
        {
            var viewModel = new ClientePesquisaViewModel
            {
                Clientes = _context.Clientes.Where(c => c.AgenciaId == _usuarioLogado.ObterClienteId()).OrderByDescending(c => c.ClienteId)
            };

            return View(viewModel);
        }

        [HttpPost]
        public IActionResult Index(ClientePesquisaViewModel viewModel)
        {
            IQueryable<Cliente> clientes = _context.Clientes;

            #region Filtros

            if (User.IsInRole("Administradora"))
            {
                if (viewModel.Cliente.AgenciaId.HasValue)
                {
                    clientes = clientes.Where(c => c.AgenciaId == viewModel.Cliente.AgenciaId);
                }
            }
            else
            {
                clientes = clientes.Where(c => c.AgenciaId == _usuarioLogado.ObterClienteId());
            }

            if (!viewModel.Cliente.ClienteId.Equals(0))
            {
                clientes = clientes.Where(c => c.ClienteId == viewModel.Cliente.ClienteId);
            }

            if (!string.IsNullOrEmpty(viewModel.Cliente.CPFCNPJ))
            {
                clientes = clientes.Where(c => c.CPFCNPJ == viewModel.Cliente.CPFCNPJ);
            }

            if (!string.IsNullOrEmpty(viewModel.Serasa.Logon))
            {
                clientes = clientes.Where(c => c.Serasas.Any(p => p.Logon == viewModel.Serasa.Logon));
            }

            if (!viewModel.Contrato.ContratoId.Equals(0))
            {
                clientes = clientes.Where(c => c.Contratos.Any(p => p.ContratoId == viewModel.Contrato.ContratoId));
            }

            if (!string.IsNullOrEmpty(viewModel.Cliente.Nome))
            {
                clientes = clientes.Where(c => c.Nome.Contains(viewModel.Cliente.Nome));
            }

            if (!string.IsNullOrEmpty(viewModel.Cliente.RazaoSocial))
            {
                clientes = clientes.Where(c => c.RazaoSocial.Contains(viewModel.Cliente.RazaoSocial));
            }

            #endregion

            viewModel.Clientes = clientes;
            return View(viewModel);
        }

        #endregion

        #region Incluir Cliente

        public IActionResult Incluir()
        {
            RetornaViewBags();

            var viewModel = new ClienteViewModel
            {
                Contrato = new Contrato
                {
                    DataInicial = DateTime.Now,
                    DataFinal = DateTime.Now.AddMonths(12),
                    IsRenovacaoAutomatica = true
                }
            };

            return View(viewModel);
        }

        [HttpPost, ValidateAntiForgeryToken()]
        public IActionResult Incluir(ClienteViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                using var ts = new TransactionScope();
                try
                {
                    viewModel.Cliente.AgenciaId = _usuarioLogado.ObterClienteId();
                    viewModel.Cliente.TipoCliente = eTipoCliente.Cliente;
                    viewModel.Cliente.IsAtivo = true;
                    viewModel.Cliente.CPFCNPJ = viewModel.Cliente.CPFCNPJ.Trim();
                    viewModel.Cliente.Nome = viewModel.Cliente.Nome.ToUpper();
                    viewModel.Contrato.DataCadastro = DateTime.Now;

                    viewModel.Cliente.Contratos.Add(viewModel.Contrato);
                    viewModel.Cliente.Contatos.Add(viewModel.Contato);

                    viewModel.Cliente.Enderecos = viewModel.Enderecos.ToList();
                    viewModel.Cliente.Telefones = viewModel.Telefones.ToList();

                    var senha = Util.ObterSenhaAleatoria(8);
                    var usuarioConsulta = new UsuarioConsulta()
                    {
                        Email = viewModel.Cliente.Email,
                        IsSuperUsuario = true,
                        Senha = senha,
                        ConfirmarSenha = senha,
                        IsAtivo = true
                    };

                    viewModel.Cliente.UsuariosConsultas.Add(usuarioConsulta);

                    _context.Clientes.Add(viewModel.Cliente);
                    _context.SaveChanges();

                    ts.Complete();

                    _notificacao.Success($"Dados cadastrados com sucesso! Cliente: {viewModel.Cliente.ClienteId}, Email: {usuarioConsulta.Email}, Senha: {usuarioConsulta.Senha}", 360);
                    return RedirectToAction("Index");

                }
                catch (Exception ex)
                {
                    _notificacao.Error(ex.Message);
                    ts.Dispose();
                }
            }
            else
            {
                _notificacao.Error("Você precisar preencher todos os campos corretamente");
            }

            RetornaViewBags();
            return View(viewModel);
        }

        #endregion

        #region Cadastro Agência

        [Authorize(Roles = "Administradora")]
        public IActionResult IncluirAgencia()
        {
            RetornaViewBags();

            var viewModel = new ClienteAgenciaViewModel();
            return View(viewModel);
        }

        [HttpPost, ValidateAntiForgeryToken()]
        public IActionResult IncluirAgencia(ClienteAgenciaViewModel viewModel)
        {
            
            if (ModelState.IsValid)
            {
                using var ts = new TransactionScope();
                try
                {

                    viewModel.Cliente.AgenciaId = _usuarioLogado.ObterClienteId();
                    viewModel.Cliente.TipoCliente = eTipoCliente.Agencia;
                    viewModel.Cliente.IsAtivo = true;
                    viewModel.Cliente.CPFCNPJ = viewModel.Cliente.CPFCNPJ.Trim();
                    viewModel.Cliente.Nome = viewModel.Cliente.Nome.ToUpper();
                    
                    viewModel.Cliente.Enderecos = viewModel.Enderecos.ToList();
                    viewModel.Cliente.Telefones = viewModel.Telefones.ToList();

                    var cargo = new Cargo
                    {
                        Descricao = "ADMINISTRADOR",
                    };

                    viewModel.Colaborador.Cargo = cargo;
                    viewModel.Colaborador.AgenciaId = viewModel.Cliente.ClienteId;

                    viewModel.Cliente.Colaboradores.Add(viewModel.Colaborador);

                    var senha = Util.ObterSenhaAleatoria(8);
                    var usuario = new Usuario()
                    {
                        ColaboradorId = viewModel.Colaborador.ColaboradorId,
                        Senha = senha,
                        SenhaConfirmar = senha,
                        IsSuperUsuario = true,
                        IsAtivo = true,
                    };

                    //TODO:
                    //viewModel.Cliente.Usuarios.Add(usuario);

                    _context.Clientes.Add(viewModel.Cliente);
                    _context.SaveChanges();

                    ts.Complete();

                    _notificacao.Success($"Dados cadastrados com sucesso! Cliente: {viewModel.Cliente.ClienteId}, Email: {viewModel.Colaborador.Email}, Senha: {usuario.Senha}", 360);
                    return RedirectToAction("Index");

                }
                catch (Exception ex)
                {
                    _notificacao.Error(ex.Message);
                    ts.Dispose();
                }
            }

            RetornaViewBags();
            return View(viewModel);
        }

        #endregion

        #region Alterar

        public IActionResult Alterar(int id = 0)
        {
            Cliente cliente;

            if (User.IsInRole("Administradora"))
            {
                cliente = _context.Clientes.Find(id);
            }
            else
            {
                cliente = _context.Clientes.SingleOrDefault(c => c.ClienteId == id && c.AgenciaId == _usuarioLogado.ObterClienteId());
            }

            if (cliente == null)
            {
                _notificacao.Warning("Cliente não pertence a esta agência");
                return RedirectToAction("Index");
            }

            RetornaViewBags();
            return View(cliente);
        }

        [HttpPost]
        public IActionResult Alterar(Cliente form)
        {
            var cliente = _context.Clientes.Find(form.ClienteId);

            if (ModelState.IsValid)
            {
                cliente.Nome = form.Nome.ToUpper();
                cliente.RazaoSocial = form.RazaoSocial;
                cliente.CPFCNPJ = form.CPFCNPJ;
                cliente.InscricaoEstadual = form.InscricaoEstadual;
                cliente.InscricaoMunicipal = form.InscricaoMunicipal;
                cliente.AtividadeId = form.AtividadeId;
                cliente.RegiaoId = form.RegiaoId;
                cliente.Email = form.Email;
                cliente.Website = form.Website;
                cliente.Rg = form.Rg;
                cliente.RgDataExpedicao = form.RgDataExpedicao;
                cliente.RgOrgaoExpedidor = form.RgOrgaoExpedidor;
                cliente.RgUf = form.RgUf;
                cliente.ComissaoAgente = form.ComissaoAgente;
                cliente.IsRevenda = form.IsRevenda;
                cliente.IsComissionamento = form.IsComissionamento;

                foreach (var endereco in form.Enderecos)
                {
                    _context.Enderecos.Attach(endereco);
                    _context.Entry(endereco).State = EntityState.Modified;
                }

                foreach (var telefone in form.Telefones)
                {
                    _context.Telefones.Attach(telefone);
                    _context.Entry(telefone).State = EntityState.Modified;
                }

                foreach (var contato in form.Contatos)
                {
                    _context.Contatos.Attach(contato);
                    _context.Entry(contato).State = EntityState.Modified;
                }

                _context.SaveChanges();

                _notificacao.Success("Cliente salvo com sucesso");
                return RedirectToAction("Index");
            }

            RetornaViewBags();
            return View(form);
        }

        #endregion

        #region Excluir

        public IActionResult Excluir()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Excluir(FormCollection fc)
        {
            int[] ids = fc["Ids"].ToString().Split(',')
                .Select(s => int.Parse(s))
                .ToArray();

            if (ids != null)
            {
                var clientes = _context.Clientes.Where(c => ids.Contains(c.ClienteId)).ToList();
                clientes.ForEach(c => _context.Clientes.Remove(c));
                _context.SaveChanges();

                _notificacao.Success("Cliente(s) removido(s) com sucesso");
                return RedirectToAction("Excluir");
            }
            else
            {
                _notificacao.Warning("Problema ao remover cliente(s)");
            }

            return View();
        }

        #endregion

        #region Migrar Cliente

        public IActionResult Migrar()
        {
            ViewBag.ClienteId = new SelectList(_context
                .Clientes
                .Where(c => c.TipoCliente == eTipoCliente.Cliente)
                .AsEnumerable()
                .Select(c => new { c.ClienteId, RazaoSocial = $"{c.ClienteId} - {c.RazaoSocial}" }).OrderBy(c => c.ClienteId), "ClienteId", "RazaoSocial");

            ViewBag.AgenciaId = new SelectList(_context
                .Clientes
                .Where(c => c.TipoCliente == eTipoCliente.Agencia)
                .AsEnumerable()
                .Select(c => new { c.ClienteId, RazaoSocial = $"{c.ClienteId} - {c.RazaoSocial}" }).OrderBy(c => c.ClienteId), "ClienteId", "RazaoSocial");

            return View();
        }

        //[HttpPost]
        //public IActionResult Migrar(MigrarViewModel vm)
        //{
        //    var cliente = _context.Clientes.Find(vm.ClienteId);

        //    if (cliente != null)
        //    {
        //        cliente.AgenciaId = vm.AgenciaId;
        //        _context.SaveChanges();

        //        _notificacao.Success("Cliente migrado com sucesso");
        //        return RedirectToAction("Migrar");
        //    }
        //    else
        //    {
        //        _notificacao.Warning("Cliente não encontrado");
        //        return RedirectToAction("Migrar");
        //    }
        //}


        #endregion

        #region Propriedades Privadas

        private void RetornaViewBags()
        {
            ViewBag.Cargos = new SelectList(_context.Cargos.Where(c => c.AgenciaId == _usuarioLogado.ObterClienteId()), "CargoId", "Descricao");
            ViewBag.Atividades = new SelectList(_context.Atividades.OrderBy(c => c.Descricao), "AtividadeId", "Descricao");
            ViewBag.Regioes = new SelectList(_context.Regioes.Where(c => c.AgenciaId == _usuarioLogado.ObterClienteId()).OrderBy(c => c.Descricao), "RegiaoId", "Descricao");
            ViewBag.DiasCobranca = new SelectList(_context.DiaCobrancas.OrderBy(c => c.Dia), "DiaCobrancaId", "Dia");
            ViewBag.Colaboradores = new SelectList(_context.Colaboradores.Where(c => c.AgenciaId == _usuarioLogado.ObterClienteId()), "ColaboradorId", "Nome");
            ViewBag.Estados = new SelectList(Estado.UFs, "Sigla", "Descricao");

            var tabelas = (from tc in _context.TabelaAgencias
                           join t in _context.Tabelas on tc.TabelaId equals t.TabelaId
                           where tc.AgenciaId == _usuarioLogado.ObterClienteId()
                           select t);

            ViewBag.Tabelas = new SelectList(tabelas, "TabelaId", "Descricao");
        }

        #endregion
    }
}
