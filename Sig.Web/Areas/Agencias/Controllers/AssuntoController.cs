﻿using Sig.Web.Models;
using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using AspNetCoreHero.ToastNotification.Abstractions;

namespace Sig.Web.Areas.Agencias.Controllers
{
    public class AssuntoController : BaseController
    {
        public AssuntoController(INotyfService notificacao) : base(notificacao)
        {
        }

        public IActionResult Index()
        {
            var assuntos = _context.Assuntos.OrderBy(c => c.Descricao);
            return View(assuntos);
        }

        public IActionResult Editar(int? id)
        {
            try
            {
                Assunto assunto;

                if (!id.HasValue || id == 0)
                {
                    assunto = new Assunto();
                }
                else
                {
                    assunto = _context.Assuntos.Find(id);
                }

                return View(assunto);
            }
            catch (Exception ex)
            {
                _notificacao.Error(ex.Message);
                return View();
            }
        }

        [HttpPost, ValidateAntiForgeryToken]
        public IActionResult Editar(Assunto assunto)
        {
            try
            {
                string mensagem = string.Empty;

                if (assunto.AssuntoId == 0)
                {
                    _context.Assuntos.Add(assunto);
                    mensagem = "Assunto adicionado com sucesso.";
                }
                else
                {
                    _context.Assuntos.Attach(assunto);
                    var entry = _context.Entry(assunto);

                    entry.Property(c => c.AssuntoId).IsModified = false;
                    entry.Property(c => c.Descricao).IsModified = true;

                    mensagem = "Assunto alterado com sucesso.";
                }

                _context.SaveChanges();
                _notificacao.Success(mensagem);

                return RedirectToAction(nameof(Index));
            }
            catch (Exception ex)
            {
                _notificacao.Error(ex.Message);
                return View(nameof(Editar), assunto);
            }
        }
    }
}