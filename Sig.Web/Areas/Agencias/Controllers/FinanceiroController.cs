﻿using System.Linq;
using AspNetCoreHero.ToastNotification.Abstractions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Sig.Web.Models;
using Sig.Web.Models.ViewModels.Financeiro;

namespace Sig.Web.Areas.Agencias.Controllers
{
    [Authorize(Roles = "Administradora")]
    public class FinanceiroController : BaseController
    {
        public FinanceiroController(INotyfService notificacao) : base(notificacao)
        {
        }

        #region Index

        public IActionResult Index()
        {
            return View();
        }

        #endregion

        #region Faturamento

        public IActionResult Fatura()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Fatura(FaturaViewModel viewModel)
        {
            IQueryable<Faturamento> query = _context.Faturamentos;

            #region Filtros

            if (viewModel.ClienteId.HasValue)
            {
                query = query.Where(c => c.ClienteId == viewModel.ClienteId);
            }

            if (viewModel.FaturamentoId.HasValue)
            {
                query = query.Where(c => c.FaturamentoId == viewModel.FaturamentoId);
            }

            if (viewModel.DataInicialFatura.HasValue)
            {
                query = query.Where(c => c.DataInicialFaturamento >= viewModel.DataInicialFatura);
            }

            if (viewModel.DataFinalFatura.HasValue)
            {
                query = query.Where(c => c.DataFinalFaturamento <= viewModel.DataFinalFatura);
            }

            viewModel.Faturamentos = query;

            #endregion

            return View(viewModel);
        }

        public IActionResult Baixar(int id = 0)
        {
            var faturamento = _context.Faturamentos.Find(id);

            if (faturamento == null)
            {
                _notificacao.Warning("Não foi possível encontrar a fatura");
                return RedirectToAction("Fatura");
            }

            faturamento.ValorPago = faturamento.ValorOriginal;

            return View(faturamento);
        }

        [HttpPost]
        public IActionResult Baixar(Faturamento form)
        {
            var faturamento = _context.Faturamentos.Find(form.FaturamentoId);

            if (form.DataPagamento.HasValue && form.ValorPago.HasValue)
            {
                faturamento.DataPagamento   = form.DataPagamento;
                faturamento.ValorPago       = form.ValorPago;
                faturamento.TipoPagamento   = form.TipoPagamento;

                //Funcoes.GravaAtendimento(UsuarioLogado.AgenciaId, UsuarioLogado.ColaboradorId,
                //    string.Format("BAIXA FATURA: {0}", form.FaturamentoId));

                _context.SaveChanges();

                _notificacao.Success("Fatura baixada com sucesso");
                return RedirectToAction("Fatura");
            }

            _notificacao.Warning("Preencha os dados corretamente");
            return RedirectToAction("Baixar", new { id = form.FaturamentoId });
        }

        public IActionResult Estornar(int id = 0)
        {
            var faturamento = _context.Faturamentos.Find(id);

            if (faturamento == null)
            {
                _notificacao.Warning("Não foi possível encontrar a fatura");
                return RedirectToAction("Fatura");
            }

            return View(faturamento);
        }

        [HttpPost]
        public IActionResult Estornar(Faturamento form)
        {
            var faturamento = _context.Faturamentos.Find(form.FaturamentoId);

            faturamento.DataPagamento = null;
            faturamento.ValorPago = null;
            faturamento.TipoPagamento = null;
            //faturamento.TipoPagamento = eTipoPagamento.Nenhum;

            _context.SaveChanges();

            //Funcoes.GravaAtendimento(UsuarioLogado.AgenciaId, UsuarioLogado.ColaboradorId,
            //        string.Format("ESTORNO FATURA: {0}", form.FaturamentoId));

            _notificacao.Success("Fatura estornada com sucesso");
            return RedirectToAction("Fatura");
        }

        #endregion
        
    }
}
