﻿using Sig.Web.Models;
using System;
using Microsoft.AspNetCore.Mvc;
using AspNetCoreHero.ToastNotification.Abstractions;

namespace Sig.Web.Areas.Agencias.Controllers
{
    [Area("Agencias")]
    public class BaseController : Controller, IDisposable
    {
        public readonly ContextoDados _context;
        public readonly INotyfService _notificacao;

        public BaseController(INotyfService notificacao)
        {
            _context = new ContextoDados();
            _notificacao = notificacao;
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
            base.Dispose(disposing);
        }
    }
}