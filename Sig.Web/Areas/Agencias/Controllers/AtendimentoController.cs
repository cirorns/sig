﻿using Sig.Web.Models;
using Sig.Web.Models.ViewModels;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Sig.Web.Models.Enums;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using AspNetCoreHero.ToastNotification.Abstractions;
using Sig.Web.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc.Rendering;
using Sig.Web.Helpers;

namespace Sig.Web.Areas.Agencias.Controllers
{
    [Authorize(Roles = "Administradora, Agencia")]
    public class AtendimentoController : BaseController
    {
        private readonly IUsuarioLogado _usuarioLogado;

        public AtendimentoController(INotyfService notificacao, IUsuarioLogado usuarioLogado) : base(notificacao)
        {
            _usuarioLogado = usuarioLogado;
        }

        #region Cadastro da Administradora

        [AllowAnonymous]
        public IActionResult Primeiro()
        {
            try
            {
                List<Atividade> atividades = new()
                {
                    new Atividade { Descricao = "AGRICULTURA, PECUÁRIA, PRODUÇÃO FLORESTAL, PESCA E AQÜICULTURA" },
                    new Atividade { Descricao = "INDÚSTRIAS EXTRATIVAS" },
                    new Atividade { Descricao = "INDÚSTRIAS DE TRANSFORMAÇÃO" },
                    new Atividade { Descricao = "ELETRICIDADE E GÁS" },
                    new Atividade { Descricao = "ÁGUA, ESGOTO, ATIVIDADES DE GESTÃO DE RESÍDUOS E DESCONTAMINAÇÃO" },
                    new Atividade { Descricao = "CONSTRUÇÃO" },
                    new Atividade { Descricao = "COMÉRCIO; REPARAÇÃO DE VEÍCULOS AUTOMOTORES E MOTOCICLETAS" },
                    new Atividade { Descricao = "TRANSPORTE, ARMAZENAGEM E CORREIO" },
                    new Atividade { Descricao = "ALOJAMENTO E ALIMENTAÇÃO" },
                    new Atividade { Descricao = "INFORMAÇÃO E COMUNICAÇÃO" },
                    new Atividade { Descricao = "ATIVIDADES FINANCEIRAS, DE SEGUROS E SERVIÇOS RELACIONADOS" },
                    new Atividade { Descricao = "ATIVIDADES IMOBILIÁRIAS" },
                    new Atividade { Descricao = "ATIVIDADES PROFISSIONAIS, CIENTÍFICAS E TÉCNICAS" },
                    new Atividade { Descricao = "ATIVIDADES ADMINISTRATIVAS E SERVIÇOS COMPLEMENTARES" },
                    new Atividade { Descricao = "ADMINISTRAÇÃO PÚBLICA, DEFESA E SEGURIDADE SOCIAL" },
                    new Atividade { Descricao = "EDUCAÇÃO" },
                    new Atividade { Descricao = "SAÚDE HUMANA E SERVIÇOS SOCIAIS" },
                    new Atividade { Descricao = "ARTES, CULTURA, ESPORTE E RECREAÇÃO" },
                    new Atividade { Descricao = "OUTRAS ATIVIDADES DE SERVIÇOS" },
                    new Atividade { Descricao = "SERVIÇOS DOMÉSTICOS" },
                    new Atividade { Descricao = "ORGANISMOS INTERNACIONAIS E OUTRAS INSTITUIÇÕES EXTRATERRITORIAIS" },
                    new Atividade { Descricao = "OUTROS" }
                };

                foreach (Atividade atividade in atividades)
                {
                    _context.Atividades.Add(atividade);
                    _context.SaveChanges();
                }

                var endereco = new Endereco
                {
                    Bairro = "CENTRO",
                    CEP = "30170121",
                    Cidade = "BELO HORIZONTE",
                    Logradouro = "RUA CURITIBA",
                    Complemento = "SALA 701",
                    TipoEndereco = eTipoEndereco.Comercial,
                    UF = "MG",
                    Numero = "1269"
                };

                var telefone = new Telefone
                {
                    DDD = "62",
                    Numero = "981568760",

                };

                var regiao = new Regiao
                {
                    Descricao = "SUDESTE",
                };

                var cliente = new Cliente
                {
                    IsAtivo = true,
                    Nome = "NOME DA ADMINISTRADORA",
                    RazaoSocial = "RAZÃO SOCIAL DA ADMINISTRADORA",
                    TipoCliente = eTipoCliente.Administradora,
                    Email = "contato@administradora.com",
                    CPFCNPJ = "01234567890",
                    Atividade = atividades.FirstOrDefault(),
                    Regiao = regiao,
                };

                cliente.Enderecos.Add(endereco);
                cliente.Telefones.Add(telefone);

                _context.Clientes.Add(cliente);
                _context.SaveChanges();

                cliente.AgenciaId = cliente.ClienteId;
                _context.SaveChanges();

                var cargo = new Cargo
                {
                    Agencia = cliente,
                    Descricao = "ADMINISTRADOR",
                };

                var colaborador = new Colaborador
                {
                    Agencia = cliente,
                    Bairro = "CENTRO",
                    Celular = "982677820",
                    CEP = "30170121",
                    Cidade = "BELO HORIZONTE",
                    Logradouro = "RUA CURITIBA",
                    Complemento = "SALA 701",
                    Nome = "Glauber - Desenvolvedor",
                    Telefone = "981568760",
                    UF = "GO",
                    Email = "glaubervictor@gmail.com",
                    CPF = "01234567890",
                    DataNascimento = DateTime.Parse("22/10/1953"),
                    DDDCelular = "62",
                    DDDTelefone = "62",
                    Cargo = cargo,
                };

                _context.Colaboradores.Add(colaborador);
                _context.SaveChanges();

                List<DiaCobranca> diasCobranca = new()
                {
                    new DiaCobranca { Dia = 10 }
                };

                foreach (DiaCobranca diaCobranca in diasCobranca)
                {
                    _context.DiaCobrancas.Add(diaCobranca);
                    _context.SaveChanges();
                }

                var usuario = new Usuario
                {
                    Colaborador = colaborador,
                    Senha = "123456",
                    SenhaConfirmar = "123456",
                    IsSuperUsuario = true,
                    IsAtivo = true
                };

                _context.Usuarios.Add(usuario);
                _context.SaveChanges();

                List<Assunto> assuntos = new()
                {
                    new Assunto { Descricao = "ALTERACAO DE SENHA" },
                    new Assunto { Descricao = "ASSISTENCIA TECNICA" },
                    new Assunto { Descricao = "ATENDIMENTO TELEFONICO" },
                    new Assunto { Descricao = "BLOQUEIO DO SISTEMA" },
                    new Assunto { Descricao = "CARTA DE ANUÊNCIA CARTORIO DE PROTESTO" },
                    new Assunto { Descricao = "COBRANCA" },
                    new Assunto { Descricao = "CÓDIGO LIBERADO SISTEMA" },
                    new Assunto { Descricao = "CONTRATO RECEBIDO ADMINISTRADORA" },
                    new Assunto { Descricao = "COPIA DE CONTRATO" },
                    new Assunto { Descricao = "DEVOLUCAO DE EQUIPAMENTOS" },
                    new Assunto { Descricao = "EMPRESTIMO DE EQUIPAMENTO" },
                    new Assunto { Descricao = "ENVIO DE DOCUMENTOS" },
                    new Assunto { Descricao = "ENVIO PARA SERASA / AGUARDANDO LIBERAÇÃO" },
                    new Assunto { Descricao = "EXTRATO DE CONSULTAS" },
                    new Assunto { Descricao = "FATURAS - PAGAMENTO E DUVIDAS" },
                    new Assunto { Descricao = "HISTORICO" },
                    new Assunto { Descricao = "MANUTENCAO DE CLIENTES - POS VENDA" },
                    new Assunto { Descricao = "PAGAMENTO DE TAXA DE ADESAO" },
                    new Assunto { Descricao = "PEDIDO DE CANCELAMENTO DE CONTRATO" },
                    new Assunto { Descricao = "PROPOSTA GERADA SERASA" },
                    new Assunto { Descricao = "PROSPECCAO - FUTURO CLIENTE" },
                    new Assunto { Descricao = "PROVIDENCIAS" },
                    new Assunto { Descricao = "RESTRICAO COMERCIAL INCLUSAO" },
                    new Assunto { Descricao = "RESULTADO DE CONSULTAS" },
                    new Assunto { Descricao = "RETIFICACAO DE ASSUNTO" },
                    new Assunto { Descricao = "SOLICITACAO DE VISITA COMERCIAL" },
                    new Assunto { Descricao = "SOLICITACAO DE VISITA TECNICA" },
                    new Assunto { Descricao = "SUGESTAO" },
                    new Assunto { Descricao = "VISITA REALIZADA - ASSIST. COMERCIAL" },
                    new Assunto { Descricao = "VISITA REALIZADA - ASSIST. TECNICA" }
                };

                foreach (Assunto assunto in assuntos)
                {
                    _context.Assuntos.Add(assunto);
                    _context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                _notificacao.Error(ex.Message);
            }

            return View();
        }

        #endregion

        #region Index

        public IActionResult Index()
        {
            var viewModel = new AtendimentoViewModel();

            if (User.IsInRole("Administradora"))
            {
                viewModel.Clientes = _context.Clientes.OrderByDescending(c => c.ClienteId);
            }
            else
            {
                viewModel.Clientes = _context.Clientes.Where(c => c.AgenciaId == _usuarioLogado.ObterClienteId()).OrderByDescending(c => c.ClienteId);
            }

            return View(viewModel);
        }

        [HttpPost]
        public async Task<ActionResult> Index(AtendimentoViewModel viewModel)
        {
            IQueryable<Cliente> query = _context.Clientes;

            #region Filtros

            if (User.IsInRole("Administradora"))
            {
                if (viewModel.Cliente.AgenciaId.HasValue)
                {
                    query = query.Where(c => c.AgenciaId == viewModel.Cliente.AgenciaId.Value);
                }
            }
            else
            {
                query = query.Where(c => c.AgenciaId == _usuarioLogado.ObterClienteId());
            }

            if (!viewModel.Cliente.ClienteId.Equals(0))
            {
                query = query.Where(c => c.ClienteId == viewModel.Cliente.ClienteId);
            }

            if (!string.IsNullOrEmpty(viewModel.Cliente.CPFCNPJ))
            {
                query = query.Where(c => c.CPFCNPJ == viewModel.Cliente.CPFCNPJ);
            }

            if (!string.IsNullOrEmpty(viewModel.Serasa.Logon))
            {
                query = query.Where(c => c.Serasas.Any(p => p.Logon == viewModel.Serasa.Logon));
            }

            if (!viewModel.Contrato.ContratoId.Equals(0))
            {
                query = query.Where(c => c.Contratos.Any(p => p.ContratoId == viewModel.Contrato.ContratoId));
            }

            if (!string.IsNullOrEmpty(viewModel.Cliente.Nome))
            {
                query = query.Where(c => c.Nome.Contains(viewModel.Cliente.Nome));
            }

            if (!string.IsNullOrEmpty(viewModel.Cliente.RazaoSocial))
            {
                query = query.Where(c => c.RazaoSocial.Contains(viewModel.Cliente.RazaoSocial));
            }

            #endregion

            viewModel.Clientes = await query.ToListAsync();
            return View("Index", viewModel);
        }

        #endregion

        #region Incluir

        [HttpGet]
        public IActionResult Incluir(int id)
        {
            ObterViewBags();

            var viewModel = new AtendimentoIncluirViewModel
            {
                Cliente = _context.Clientes.Find(id),
                Atendimento = new Atendimento()
            };

            viewModel.Atendimento.ClienteId = viewModel.Cliente.ClienteId;

            return View("Incluir", viewModel);
        }

        [HttpPost]
        public IActionResult Incluir(AtendimentoIncluirViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                viewModel.Atendimento.ColaboradorId = _context.Colaboradores.Find(_usuarioLogado.ObterColaboradorId()).ColaboradorId;
                viewModel.Atendimento.Data = DateTime.Now;

                _context.Atendimentos.Add(viewModel.Atendimento);
                _context.SaveChanges();
            }

            ObterViewBags();
            viewModel.Cliente = _context.Clientes.Find(viewModel.Atendimento.ClienteId);
            return View("Incluir", viewModel);
        }

        #endregion

        #region Excluir Cliente

        public IActionResult Excluir(int id = 0)
        {
            try
            {
                var cliente = _context.Clientes.Find(id);

                if (cliente == null)
                {
                    _notificacao.Warning("Cliente não encontrado");
                    return RedirectToAction("Index");
                }

                _context.Clientes.Remove(cliente);
                _context.SaveChanges();

                _notificacao.Success("Cliente excluído do sistema");
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                _notificacao.Error("Erro ao excluir o cliente");
            }

            return View();
        }

        #endregion

        #region Alterar para Pré-Pago

        public IActionResult AlterarPrePago(int id = 0)
        {
            try
            {
                var cliente = _context.Clientes.Find(id);


                if (cliente == null)
                {
                    _notificacao.Warning("Cliente não encontrado");
                    return RedirectToAction("Index");
                }
                else
                {
                    //Seta Cliente

                    cliente.TipoCliente = eTipoCliente.PrePago;

                    //Remove Contratos dos Usuários de Consulta

                    var usuariosConsulta = _context.UsuarioConsultas.Where(c => c.ClienteId == id);

                    if (usuariosConsulta != null && usuariosConsulta.Any())
                    {
                        foreach (var uc in usuariosConsulta)
                        {
                            uc.Contrato = null;
                            uc.ContratoId = null;
                            uc.ConfirmarSenha = uc.Senha;
                        }
                    }

                    //Salva
                    _context.SaveChanges();

                    //Remove Contratos

                    var contratos = _context.Contratos.Where(c => c.ClienteId == id);
                    _context.Contratos.RemoveRange(contratos);

                    //Salva
                    _context.SaveChanges();
                    _notificacao.Success("Cliente alterado para Pré-Pago");
                }

            }
            catch (Exception)
            {
                _notificacao.Error("Erro ao alterar o cliente");
            }

            return RedirectToAction("Index");
        }

        #endregion

        #region Tabelas

        [HttpPost]
        public async Task<string> Tabela(int id)
        {
            StringBuilder sb = new();

            sb.Append("<table class=\"table table-striped table-hover\" width=\"100%\">");
            sb.Append("<tr>");
            sb.Append("<th><b>Item</b></th>");
            sb.Append("<th><b>Valor</b></th>");
            sb.Append("<th><b>Qtde Pcte</b></th>");
            sb.Append("</tr>");

            foreach (var item in await _context.TabelaItens.Where(c => c.TabelaId == id).ToListAsync())
            {
                sb.Append("<tr>");
                sb.Append($"<td>{item.ItemFaturavel.Descricao}</td>");
                sb.Append($"<td>{item.ValorUnitario:c}</td>");
                sb.Append($"<td>{item.QuantidadePacote}</td>");
                sb.Append("</tr>");
            }

            sb.Append("</table>");

            return sb.ToString();
        }

        #endregion


        private void ObterViewBags()
        {
            #region ViewBags

            ViewBag.Assuntos = new SelectList(_context.Assuntos.OrderBy(c => c.Descricao), "AssuntoId", "Descricao");
            ViewBag.Meios = new SelectList((Enum.GetValues(typeof(eMeio))
                                               .Cast<eMeio>()
                                               .Select(m => new SelectListItem
                                               {
                                                   Value = ((int)m).ToString(),
                                                   Text = m.ObterDescricaoDoEnum()
                                               })), "Value", "Text");

            #endregion
        }

    }
}
