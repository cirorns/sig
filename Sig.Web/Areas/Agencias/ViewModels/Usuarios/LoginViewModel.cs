﻿using Sig.Web.Helpers;
using System.ComponentModel.DataAnnotations;

namespace Sig.Web.Areas.Agencias.ViewModels.Usuarios
{
    public class LoginViewModel
    {
        [Required(ErrorMessage = Mensagens.Requerido)]
        public int AgenciaId { get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        [EmailAddress(ErrorMessage = Mensagens.Email)]
        public string Email { get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        public string Senha { get; set; }
    }
}
