﻿using AspNetCoreHero.ToastNotification.Abstractions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Sig.Web.Areas.Consulta.Models.ViewModels;
using Sig.Web.Interfaces;
using Sig.Web.Models;
using System;
using System.Linq;

namespace Sig.Web.Areas.Consultas.Controllers
{
    public class TransacaoConsultaController : BaseController
    {
        private readonly IUsuarioLogado _usuarioLogado;

        public TransacaoConsultaController(INotyfService notificacao, IUsuarioLogado usuarioLogado) : base(notificacao)
        {
            _usuarioLogado = usuarioLogado;
        }

        #region Index

        public IActionResult Index()
        {
            ViewBag.UsuarioConsultaId = new SelectList(_context.UsuarioConsultas.Where(c => c.ClienteId == _usuarioLogado.ObterClienteId() && c.ContratoId == _usuarioLogado.ObterContratoId()), "UsuarioConsultaId", "Email");

            TransacaoConsultaViewModel vm = new();

            var data = DateTime.Now.Date;

            var transacaoConsultas = _context.TransacoesConsultas
                .Where(c => c.ClienteId == _usuarioLogado.ObterClienteId()
                && c.IsConfirmado == true
                && c.DataResposta.Value.Date >= data
                && c.DataResposta.Value.Date <= data);

            vm.TransacaoConsultas = transacaoConsultas;
            vm.DataInicial = data;
            vm.DataFinal = data;

            return View(vm);
        }

        [HttpPost]
        public IActionResult Index(TransacaoConsultaViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                IQueryable<TransacaoConsulta> transacaoConsultas = _context.TransacoesConsultas.Where(c => c.ClienteId == _usuarioLogado.ObterClienteId() && c.IsConfirmado == true);

                #region Filtros

                if (viewModel.UsuarioConsultaId.HasValue)
                {
                    transacaoConsultas = transacaoConsultas.Where(c => c.UsuarioConsultaId == viewModel.UsuarioConsultaId.Value);
                }

                transacaoConsultas = transacaoConsultas.Where(c => c.DataResposta.Value.Date >= viewModel.DataInicial
                && c.DataResposta.Value.Date <= viewModel.DataFinal);

                #endregion

                viewModel.TransacaoConsultas = transacaoConsultas;
            }

            ViewBag.UsuarioConsultaId = new SelectList(_context.UsuarioConsultas.Where(c => c.ClienteId == _usuarioLogado.ObterClienteId() && c.ContratoId == _usuarioLogado.ObterContratoId()), "UsuarioConsultaId", "Email");
            return View(viewModel);
        }


        #endregion

        #region Detalhe

        public IActionResult Detalhe(int id = 0)
        {
            var transacaoConsulta = _context.TransacoesConsultas.Find(id);

            if (transacaoConsulta == null)
            {
                _notificacao.Error("Consulta não encontrada");
                return RedirectToAction("Index", "Produto");
            }

            return View(transacaoConsulta);
        }

        #endregion
    }
}
