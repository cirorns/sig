﻿using AspNetCoreHero.ToastNotification.Abstractions;
using Microsoft.AspNetCore.Mvc;
using Sig.Web.Models;
using System;

namespace Sig.Web.Areas.Consultas.Controllers
{
    [Area("Consultas")]
    public class BaseController : Controller, IDisposable
    {
        public readonly ContextoDados _context;
        public readonly INotyfService _notificacao;

        public BaseController(INotyfService notificacao)
        {
            _context = new ContextoDados();
            _notificacao = notificacao;
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
            base.Dispose(disposing);
        }
    }
}
