﻿using AspNetCoreHero.ToastNotification.Abstractions;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Sig.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Sig.Web.Areas.Consultas.Controllers
{
    public class UsuarioConsultaController : BaseController
    {
        public UsuarioConsultaController(INotyfService notificacao) : base(notificacao)
        {
        }

        [AllowAnonymous]
        public ActionResult Login()
        {
            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> Login(UsuarioConsulta form)
        {
            var usuarioConsulta = _context.UsuarioConsultas
                .FirstOrDefault(c => c.ClienteId == form.ClienteId && c.Email == form.Email && c.Senha == form.Senha);

            if (usuarioConsulta != null)
            {
                if (usuarioConsulta.IsAtivo)
                {
                    if (usuarioConsulta.Cliente.IsAtivo)
                    {
                        if (usuarioConsulta.Contrato != null)
                        {
                            //Verifica se o contrato não está cancelado
                            if (usuarioConsulta.Contrato.DataCancelamento.HasValue)
                            {
                                _notificacao.Error("Contrato cancelado. Por favor entre em contato com sua agência");
                                return RedirectToAction("Login", "UsuarioConsulta");
                            }
                        }

                        var configuracao = _context.Configuracoes.FirstOrDefault();

                        if (configuracao != null)
                        {
                            var nome = usuarioConsulta.Cliente.Nome;
                            var colaboradorNome = string.Empty;
                            var clienteId = usuarioConsulta.ClienteId;
                            var agenciaId = usuarioConsulta.Cliente.AgenciaId ?? 0;
                            var usuarioId = usuarioConsulta.UsuarioConsultaId;
                            var grupo = "Cliente";
                            var colaboradorId = 0;
                            var isSuperUsuario = usuarioConsulta.IsSuperUsuario ? "true" : "false";
                            var contratoId = usuarioConsulta.ContratoId;
                            var pagina = "Consultas";

                            var dados = $"{nome};{colaboradorNome};{clienteId};{agenciaId};{usuarioId};{colaboradorId};{isSuperUsuario};{contratoId};{pagina}";

                            var claims = new List<Claim>
                            {
                                new Claim(ClaimTypes.Name, nome),
                                new Claim(ClaimTypes.Role, grupo),
                                new Claim("Dados", dados)
                            };

                            var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);

                            var authProperties = new AuthenticationProperties
                            {
                                ExpiresUtc = DateTime.UtcNow.AddHours(8),
                                IsPersistent = true
                            };

                            await HttpContext.SignInAsync(
                              CookieAuthenticationDefaults.AuthenticationScheme,
                              new ClaimsPrincipal(claimsIdentity),
                              authProperties);

                            return RedirectToAction("Index", "Consulta");

                        }
                        else
                        {
                            _notificacao.Error("Arquivo de configuração não definido. Por favor entre em contato com sua agência.");
                        }

                    }
                    else
                    {
                        _notificacao.Error("Cliente bloqueado, por favor entre em contato com sua agência.");
                    }
                }
                else
                {
                    _notificacao.Error("Usuário desativado.");
                }
            }
            else
            {
                _notificacao.Error("Cliente, usuário ou senha inválidos.");
            }

            return View();
        }
    }
}
