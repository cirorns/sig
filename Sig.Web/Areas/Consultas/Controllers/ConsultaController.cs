﻿using AspNetCoreHero.ToastNotification.Abstractions;
using Microsoft.AspNetCore.Mvc;

namespace Sig.Web.Areas.Consultas.Controllers
{
    public class ConsultaController : BaseController
    {
        public ConsultaController(INotyfService notificacao) : base(notificacao)
        {
        }

        public IActionResult Index()
        {
            return View();
        }
    }
}
