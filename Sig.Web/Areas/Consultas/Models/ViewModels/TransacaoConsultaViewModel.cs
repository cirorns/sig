﻿using Sig.Web.Helpers;
using Sig.Web.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Sig.Web.Areas.Consulta.Models.ViewModels
{
    public class TransacaoConsultaViewModel
    {
        public int? UsuarioConsultaId { get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        public DateTime DataInicial { get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        public DateTime DataFinal { get; set; }

        public IEnumerable<TransacaoConsulta> TransacaoConsultas { get; set; }
    }
}