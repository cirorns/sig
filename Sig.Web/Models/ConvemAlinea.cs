﻿using System.Collections.Generic;

namespace Sig.Web.Models
{
    public class Alinea
    {
        public string Sigla { get; set; }
        public string Descricao { get; set; }
    }

    public class ConvemAlinea
    {
        private static readonly List<Alinea> alineas = new()
        {
            new Alinea { Descricao = "Selecione...", Sigla = "0" },
            new Alinea { Descricao = "12 - Insuficiência de Fundos, 2ª Apresentação", Sigla = "12" },
            new Alinea { Descricao = "13 - Conta Encerrada", Sigla = "13" },
            new Alinea { Descricao = "14 - Prática Espúria (Compromisso Pronto Acolhimento)", Sigla = "14" },
        };
        public static List<Alinea> Alineas = alineas;
    }
}