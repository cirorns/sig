﻿using Sig.Web.Helpers;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sig.Web.Models
{
    [Table(nameof(Tabela))]
    public class Tabela
    {
        [Key]
        public int TabelaId { get; set; }

        [StringLength(100, ErrorMessage = Mensagens.Maximo)]
        [Required(ErrorMessage = Mensagens.Requerido)]
        public string Descricao { get; set; }

        public bool IsConsumo { get; set; }

        public bool IsAtivo { get; set; }

        public bool IsEspecial { get; set; }

        public virtual ICollection<TabelaItem> TabelaItens { get; set; }

        public virtual ICollection<Contrato> Contratos { get; set; }

        public Tabela()
        {
            TabelaItens = new List<TabelaItem>();
            Contratos = new List<Contrato>();
        }
    }
}