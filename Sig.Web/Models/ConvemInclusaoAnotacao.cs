﻿using Sig.Web.Helpers;
using Sig.Web.Helpers.ValidadoresCustomizados;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace Sig.Web.Models
{

    [Table(nameof(ConvemInclusaoAnotacao))]
    public class ConvemInclusaoAnotacao
    {
        [Key]
        public int ConvemInclusaoAnotacaoId { get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        public int ConvemCredorId { get; set; }

        [ForeignKey("ConvemCredorId")]
        public virtual ConvemCredor ConvemCredor { get; set; }

        //Devedor

        [StringLength(70, ErrorMessage = Mensagens.Maximo)]
        [Required(ErrorMessage = Mensagens.Requerido)]
        public string DevedorNome { get; set; }

        [CpfCnpjValidador(ErrorMessage = "CPF/CNPJ Inválidos.")]
        [StringLength(14, ErrorMessage = "O campo não pode exceder 14 caracteres.")]
        [Required(ErrorMessage = Mensagens.Requerido)]
        public string DevedorCPFCNPJ { get; set; }

        [StringLength(70, ErrorMessage = "O campo não pode exceder 70 caracteres.")]
        public string DevedorNomeMae { get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        [StringLength(8, ErrorMessage = "O campo não pode exceder 8 dígitos.")]
        public string DevedorCEP { get; set; }

        [StringLength(45, ErrorMessage = "O campo não pode exceder 45 caracteres.")]
        [Required(ErrorMessage = Mensagens.Requerido)]
        public string DevedorEndereco { get; set; }

        [StringLength(20, ErrorMessage = "O campo não pode exceder 20 caracteres.")]
        [Required(ErrorMessage = Mensagens.Requerido)]
        public string DevedorBairro { get; set; }

        [StringLength(25, ErrorMessage = "O campo não pode exceder 25 caracteres.")]
        [Required(ErrorMessage = Mensagens.Requerido)]
        public string DevedorCidade { get; set; }

        [StringLength(2, ErrorMessage = "O campo não pode exceder 2 caracteres.")]
        [Required(ErrorMessage = Mensagens.Requerido)]
        public string DevedorUF { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public Nullable<DateTime> DevedorDataNascimento { get; set; }

        [StringLength(2, ErrorMessage = "O campo não pode exceder 2 caracteres.")]
        public string DevedorDDD { get; set; }

        [StringLength(9, ErrorMessage = "O campo não pode exceder 9 caracteres.")]
        public string DevedorTelefone { get; set; }

        //Anotação

        [Required(ErrorMessage = Mensagens.Requerido)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime AnotacaoDataCompromisso { get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime AnotacaoDataVencimento { get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        [StringLength(2, ErrorMessage = "O campo não pode exceder 2 caracteres.")]
        public string AnotacaoNatureza { get; set; }

        [StringLength(16, ErrorMessage = "O campo não pode exceder 16 caracteres.")]
        [Required(ErrorMessage = Mensagens.Requerido)]
        public string AnotacaoNumeroContrato { get; set; }

        [StringLength(9, ErrorMessage = "O campo não pode exceder 9 caracteres.")]
        [Required(ErrorMessage = Mensagens.Requerido)]
        public string AnotacaoNossoNumero { get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        public decimal AnotacaoValor { get; set; }

        //Cheque
        [StringLength(4, ErrorMessage = "O campo não pode exceder 4 caracteres.")]
        public string ChequeNumeroBanco { get; set; }

        [StringLength(4, ErrorMessage = "O campo não pode exceder 4 caracteres.")]
        public string ChequeNumeroAgencia { get; set; }

        [StringLength(6, ErrorMessage = "O campo não pode exceder 6 caracteres.")]
        public string ChequeNumero { get; set; }

        [StringLength(9, ErrorMessage = "O campo não pode exceder 9 caracteres.")]
        public string ChequeNumeroContaCorrente { get; set; }

        public int ChequeAlinea { get; set; }

        //Número da Remessa Fins de Relatório
        public int? NumeroRemessa { get; set; }

        //Data de Confirmação da Remessa
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? DataAlteracao { get; set; }

        public int? ConvemErroPefinId { get; set; }

        public string ErroPefinDescricao
        {
            get
            {
                if (ConvemErroPefinId.HasValue)
                {
                    return ConvemErroPefin.Erros.Single(c => c.Id == ConvemErroPefinId.Value).Descricao;
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        public string Status { get; set; }

        //Aplicar na geração do arquivo.
        [NotMapped]
        public bool IsAplicar { get; set; }

        public virtual ICollection<ConvemExclusaoAnotacao> ConvemExclusaoAnotacoes { get; set; }

        public bool IsExcluido
        {
            get
            {
                return ConvemExclusaoAnotacoes.Any(c => c.Status == "C");
            }
        }

        public ConvemInclusaoAnotacao()
        {
            ConvemExclusaoAnotacoes = new List<ConvemExclusaoAnotacao>();
        }
    }
}