﻿using Sig.Web.Helpers;
using Sig.Web.Models.Enums;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sig.Web.Models
{
    [Table(nameof(DocumentoPendente))]
    public class DocumentoPendente
    {
        [Key]
        public int DocumentoPendenteId { get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        public int ContratoId { get; set; }

        [ForeignKey(nameof(ContratoId))]
        public virtual Contrato Contrato { get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        public int UsuarioId { get; set; }

        [ForeignKey(nameof(UsuarioId))]
        public virtual Usuario Usuario { get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        public eTipoDocumentoPendente TipoDocumento { get; set; }

        public DateTime? DataBaixa { get; set; }
    }
}