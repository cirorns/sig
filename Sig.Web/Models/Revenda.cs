﻿using Sig.Web.Helpers;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sig.Web.Models
{
    [Table(nameof(Revenda))]
    public class Revenda : BaseModel
    {
        public Revenda()
        {
            RevendaItens = new List<RevendaItem>();
        }

        [Required(ErrorMessage = Mensagens.Requerido)]
        public int ClienteId { get; set; }

        [ForeignKey("ClienteId")]
        public virtual Cliente Cliente { get; set; }
        public virtual List<RevendaItem> RevendaItens { get; set; }
    }
}