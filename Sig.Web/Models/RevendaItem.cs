﻿using Sig.Web.Helpers;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sig.Web.Models
{
    [Table(nameof(RevendaItem))]
    public class RevendaItem : BaseModel
    {
        [Required(ErrorMessage = Mensagens.Requerido)]
        public int RevendaId { get; set; }

        [ForeignKey("RevendaId")]
        public virtual Revenda Revenda { get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        public int ItemFaturavelId { get; set; }

        public virtual ItemFaturavel ItemFaturavel { get; set; }

        public decimal? Valor { get; set; }
    }
}