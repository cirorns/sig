﻿using Sig.Web.Helpers;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sig.Web.Models
{
    [Table(nameof(DiaCobranca))]
    public class DiaCobranca
    {
        [Key]
        public int DiaCobrancaId { get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        public int Dia { get; set; }
    }
}
