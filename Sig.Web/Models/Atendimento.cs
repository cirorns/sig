﻿using Sig.Web.Helpers;
using Sig.Web.Models.Enums;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sig.Web.Models
{
    [Table(nameof(Atendimento))]
    public class Atendimento
    {
        [Key]
        public int AtendimentoId { get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        public int AssuntoId { get; set; }

        [ForeignKey(nameof(AssuntoId))]
        public virtual Assunto Assunto { get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        public int ClienteId { get; set; }

        [ForeignKey(nameof(ClienteId))]
        public virtual Cliente Cliente { get; set; }

        public int? ContratoId { get; set; }

        [ForeignKey(nameof(ContratoId))]
        public virtual Contrato Contrato { get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        public int ColaboradorId { get; set; }

        [ForeignKey(nameof(ColaboradorId))]
        public virtual Colaborador Colaborador { get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        public eMeio Meio { get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        public string Descricao { get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        public DateTime Data { get; set; }

    }
}