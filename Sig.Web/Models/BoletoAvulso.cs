﻿using Sig.Web.Helpers;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sig.Web.Models
{
    [Table(nameof(BoletoAvulso))]
    public class BoletoAvulso
    {
        [Key]
        public int BoletoAvulsoId { get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        public int ClienteId { get; set; }

        [ForeignKey(nameof(ClienteId))]
        public virtual Cliente Cliente { get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        [StringLength(100, ErrorMessage = Mensagens.Maximo)]
        public string NossoNumero { get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        public DateTime DataVencimento { get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        public decimal ValorOriginal { get; set; }

        public decimal? ValorPago { get; set; }

        public decimal? ValorJuro { get; set; }

        public decimal? ValorDesconto { get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        public string Demonstrativo { get; set; }

        public DateTime? DataPagamento { get; set; }

        public DateTime? DataBaixa { get; set; }
    }
}
