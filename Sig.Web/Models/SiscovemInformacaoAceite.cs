﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sig.Web.Models
{
    public class InformacaoAceite
    {
        public string Sigla { get; set; }
        public string Descricao { get; set; }
    }

    public class SiscovemInformacaoAceite
    {
        public static List<InformacaoAceite> InformacaoAceites = new List<InformacaoAceite>
        {
            new InformacaoAceite{ Descricao = "NÃO ACEITOS", Sigla = "N" },
            new InformacaoAceite{ Descricao = "ACEITOS", Sigla = "A" },
        };
    }
}