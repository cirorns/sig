﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sig.Web.Models
{
    public class FaturamentoItemConsultado
    {
        public string Descricao { get; set; }
        public int ItemFaturavelId { get; set; }
        public decimal ValorUnitario { get; set; }
        public int QuantidadePacote { get; set; }
        public decimal CustoUnitario { get; set; }
        public decimal CustoRevenda { get; set; }
        public decimal ValorMinimo { get; set; }
        public int QuantidadeConsultada { get; set; }
    }
}