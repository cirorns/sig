﻿using Sig.Web.Helpers;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sig.Web.Models
{
    [Table(nameof(TabelaRevenda))]
    public class TabelaRevenda
    {
        [Key, Column(Order = 0)]
        [Required(ErrorMessage = Mensagens.Requerido)]
        public int ClienteId { get; set; }

        [ForeignKey(nameof(ClienteId))]
        public virtual Cliente Cliente { get; set; }

        [Key, Column(Order = 1)]
        [Required(ErrorMessage = Mensagens.Requerido)]
        public int TabelaId { get; set; }

        [ForeignKey(nameof(TabelaId))]
        public virtual Tabela Tabela { get; set; }
    }
}