﻿using Sig.Web.Helpers;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sig.Web.Models
{
    [Table(nameof(Pagina))]
    public class Pagina
    {
        public int PaginaId { get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        [StringLength(250, ErrorMessage = "Máximo permitido 250 caracters.")]
        public string Controller { get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        [StringLength(250, ErrorMessage = "Máximo permitido 250 caracters.")]
        public string View { get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        [StringLength(250, ErrorMessage = "Máximo permitido 250 caracters.")]
        public string Descricao { get; set; }
        
    }
}