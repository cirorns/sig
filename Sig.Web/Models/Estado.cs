﻿using System.Collections.Generic;

namespace Sig.Web.Models
{
    public class UnidadeFederativa
    {
        public string Sigla { get; set; }
        public string Descricao { get; set; }
    }

    public static class Estado
    {
        public static List<UnidadeFederativa> UFs = new List<UnidadeFederativa>
        {
            new UnidadeFederativa {Descricao = "ACRE", Sigla = "AC"},
            new UnidadeFederativa {Descricao = "ALAGOAS", Sigla = "AL"},
            new UnidadeFederativa {Descricao = "AMAPÁ", Sigla = "AP"},
            new UnidadeFederativa {Descricao = "AMAZONAS", Sigla = "AM"},
            new UnidadeFederativa {Descricao = "BAHIA", Sigla = "BA"},
            new UnidadeFederativa {Descricao = "CEARÁ", Sigla = "CE"},
            new UnidadeFederativa {Descricao = "DISTRITO FEDERAL", Sigla = "DF"},
            new UnidadeFederativa {Descricao = "ESPÍRITO SANTOS", Sigla = "ES"},
            new UnidadeFederativa {Descricao = "GOIÁS", Sigla = "GO"},
            new UnidadeFederativa {Descricao = "MARANHÃO", Sigla = "MA"},
            new UnidadeFederativa {Descricao = "MATO GROSSO", Sigla = "MT"},
            new UnidadeFederativa {Descricao = "MATO GROSSO DO SUL", Sigla = "MS"},
            new UnidadeFederativa {Descricao = "MINAS GERAIS", Sigla = "MG"},
            new UnidadeFederativa {Descricao = "PARÁ", Sigla = "PA"},
            new UnidadeFederativa {Descricao = "PARAÍBA", Sigla = "PB"},
            new UnidadeFederativa {Descricao = "PARANÁ", Sigla = "PR"},
            new UnidadeFederativa {Descricao = "PERNAMBUCO", Sigla = "PE"},
            new UnidadeFederativa {Descricao = "PIAUÍ", Sigla = "PI"},
            new UnidadeFederativa {Descricao = "RIO DE JANEIRO", Sigla = "RJ"},
            new UnidadeFederativa {Descricao = "RIO GRANDE DO NORTE", Sigla = "RN"},
            new UnidadeFederativa {Descricao = "RIO GRANDE DO SUL", Sigla = "RS"},
            new UnidadeFederativa {Descricao = "RONDÔNIA", Sigla = "RO"},
            new UnidadeFederativa {Descricao = "RORAIMA", Sigla = "RR"},
            new UnidadeFederativa {Descricao = "SANTA CATARINA", Sigla = "SC"},
            new UnidadeFederativa {Descricao = "SÃO PAULO", Sigla = "SP"},
            new UnidadeFederativa {Descricao = "SERGIPE", Sigla = "SE"},
            new UnidadeFederativa {Descricao = "TOCATINS", Sigla = "TO"}
            
        };
    }
}