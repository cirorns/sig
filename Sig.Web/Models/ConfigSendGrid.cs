﻿using Sig.Web.Helpers;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sig.Web.Models
{
    [Table(nameof(ConfigSendGrid))]
    public class ConfigSendGrid
    {
        [Key]
        public int ConfigSendGridId { get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        [StringLength(250, ErrorMessage = Mensagens.Maximo)]
        [EmailAddress(ErrorMessage = Mensagens.Email)]
        public string Email { get; set; }

        [StringLength(250, ErrorMessage = Mensagens.Maximo)]
        [EmailAddress(ErrorMessage = Mensagens.Email)]
        public string EmailBcc { get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        [StringLength(250, ErrorMessage = Mensagens.Maximo)]
        public string Nome { get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        [StringLength(100, ErrorMessage = Mensagens.Maximo)]
        public string Usuario { get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        [StringLength(20, ErrorMessage = Mensagens.Maximo)]
        public string Senha { get; set; }

        [StringLength(100, ErrorMessage = Mensagens.Maximo)]
        public string TemplateIdBoleto { get; set; }

        [StringLength(100, ErrorMessage = Mensagens.Maximo)]
        public string TemplateIdEnvioDeSenha { get; set; }
        
        [StringLength(100, ErrorMessage = Mensagens.Maximo)]
        public string TemplateIdRecuperarSenha { get; set; }

        [StringLength(100, ErrorMessage = Mensagens.Maximo)]
        public string TemplateIdPadrao { get; set; }
    }
}