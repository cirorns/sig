﻿using Sig.Web.Helpers;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sig.Web.Models
{
    [Table(nameof(Consulta))]
    public class Consulta
    {
        [Key, Column(Order = 0)]
        public int AgenciaId { get; set; }

        [ForeignKey(nameof(AgenciaId))]
        public virtual Cliente Agencia { get; set; }

        [Key, Column(Order = 1)]
        [Required(ErrorMessage = Mensagens.Requerido)]
        public int ItemFaturavelId { get; set; }

        [ForeignKey(nameof(ItemFaturavelId))]
        public virtual ItemFaturavel ItemFaturavel { get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        [StringLength(100, ErrorMessage = Mensagens.Maximo)]
        public string Descricao { get; set; }

        public string Detalhe { get; set; }

    }
}