﻿using Sig.Web.Helpers;
using Sig.Web.Helpers.ValidadoresCustomizados;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sig.Web.Models
{
    [Table(nameof(ConvemCredor))]
    public class ConvemCredor
    {
        //Chaves

        [Key]
        public int ConvemCredorId { get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        public int ClienteId { get; set; }

        [ForeignKey("ClienteId")]
        public virtual Cliente Cliente { get; set; }

        //Credor
        [StringLength(1, ErrorMessage = "O campo não pode exceder 1 caractere.")]
        [Required(ErrorMessage = Mensagens.Requerido)]
        public string CredorTipo { get; set; }

        [StringLength(27, ErrorMessage = "O campo não pode exceder 27 caracteres.")]
        [Required(ErrorMessage = Mensagens.Requerido)]
        public string CredorRazaoSocial { get; set; }
        
        [StringLength(30, ErrorMessage = "O campo não pode exceder 30 caracteres.")]
        public string CredorContato { get; set; }

        [EmailAddress(ErrorMessage = "Email inválido")]
        [StringLength(100, ErrorMessage = "O campo não pode exceder 100 caracteres.")]
        [Required(ErrorMessage = Mensagens.Requerido)]
        public string CredorEmail { get; set; }

        [StringLength(23, ErrorMessage = "O campo não pode exceder 23 caracteres.")]
        [Required(ErrorMessage = Mensagens.Requerido)]
        public string CredorNomeFantasia { get; set; }

        [CnpjValidador(ErrorMessage = "CNPJ Inválido.")]
        [StringLength(14, ErrorMessage = "O campo não pode exceder 14 caracteres.")]
        [Required(ErrorMessage = Mensagens.Requerido)]
        public string CredorCNPJ { get; set; }

        [StringLength(35, ErrorMessage = "O campo não pode exceder 35 caracteres.")]
        [Required(ErrorMessage = Mensagens.Requerido)]
        public string CredorEndereco { get; set; }

        [StringLength(15, ErrorMessage = "O campo não pode exceder 15 caracteres.")]
        [Required(ErrorMessage = Mensagens.Requerido)]
        public string CredorBairro { get; set; }

        [StringLength(15, ErrorMessage = "O campo não pode exceder 15 caracteres.")]
        [Required(ErrorMessage = Mensagens.Requerido)]
        public string CredorCidade { get; set; }

        [StringLength(8, ErrorMessage = "O campo não pode exceder 8 caracteres.")]
        [Required(ErrorMessage = Mensagens.Requerido)]
        public string CredorCEP { get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        public string CredorUF { get; set; }

        [StringLength(2, ErrorMessage = "O campo não pode exceder 2 dígitos.")]
        [Required(ErrorMessage = Mensagens.Requerido)]
        public string CredorDDD { get; set; }

        [StringLength(9, ErrorMessage = "O campo não pode exceder 9 dígitos.")]
        [Required(ErrorMessage = Mensagens.Requerido)]
        public string CredorTelefone { get; set; }

        public bool IsConfirmado { get; set; }

        //Número da Remessa Fins de Relatório
        public Nullable<int> NumeroRemessa { get; set; }

        //Data de Confirmação da Remessa
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public Nullable<DateTime> DataConfirmacao { get; set; }

        //Aplicar na geração do arquivo.
        [NotMapped]
        public bool IsAplicar { get; set; }
    }
}