﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Sig.Web.Models
{
    
    public class PlanilhaSerasa
    {
        public Nullable<int> Posicao { get; set; }
        public string CNPJCliente { get; set; }
        public string RazaoSocial { get; set; }
        public DateTime DataConsultaLogon { get; set; }
        public string Produto { get; set; }
        public string Logon { get; set; }
        public int QuantidadeConsultaLogon { get; set; }

        public Nullable<int> ClienteId { get; set; }

        [Required(ErrorMessage="Campo requerido.")]
        public string Arquivo { get; set; }

        [Required(ErrorMessage = "Campo requerido.")]
        public DateTime DataInicial { get; set; }

        [Required(ErrorMessage = "Campo requerido.")]
        public DateTime DataFinal { get; set; }
    }
}