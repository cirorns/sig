﻿using Sig.Web.Helpers;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sig.Web.Models
{
    [Table(nameof(Usuario))]
    public class Usuario
    {
        [Key]
        public int UsuarioId { get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        public int ColaboradorId { get; set; }

        [ForeignKey("ColaboradorId")]
        public virtual Colaborador Colaborador { get; set; }

        [StringLength(8, ErrorMessage = Mensagens.Senha)]
        [Required(ErrorMessage = Mensagens.Requerido)]
        public string Senha { get; set; }

        [NotMapped]
        [StringLength(8, ErrorMessage = Mensagens.Senha)]
        [Required(ErrorMessage = Mensagens.Requerido)]
        [Compare("Senha", ErrorMessage = Mensagens.SenhaConfirmacao)]
        public string SenhaConfirmar { get; set; }

        public int? PermissaoId { get; set; }

        [ForeignKey("PermissaoId")]
        public virtual Permissao Permissao { get; set; }

        public bool IsSuperUsuario { get; set; }
        public bool IsAtivo { get; set; }
    }
}