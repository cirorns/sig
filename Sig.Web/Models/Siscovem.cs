﻿using Sig.Web.Helpers;
using Sig.Web.Helpers.ValidadoresCustomizados;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sig.Web.Models
{
    [Table(nameof(Siscovem))]
    public class Siscovem
    {
        private string logon;
        private string senha;

        private string credorDocumento;
        private string credorRazaoSocial;
        private string credorNomeFantasia;
        private string credorEndereco;
        private string credorBairro;
        private string credorMunicipio;
        private string credorUF;
        private string credorCEP;
        private string credorTelefone;

        private string distribuidorDocumento;

        private string devedorDocumento;
        //private string devedorTipo;
        private string devedorNome;
        private string devedorEndereco;
        private string devedorComplemento;
        private string devedorBairro;
        private string devedorMunicipio;
        private string devedorUF;
        private string devedorCEP;
        private string devedorTelefone;
        private string devedorDataNascimento;
        private string devedorRG;
        private string devedorRGUF;
        private string devedorNomePai;
        private string devedorNomeMae;

        private string anotacaoNossoNumero;
        private string anotacaoEspecieTitulo;
        private string anotacaoNumeroTitulo;
        private string anotacaoUF;
        private string anotacaoCidade;
        private string anotacaoDataEmissao;
        private string anotacaoDataVencimento;
        private string anotacaoValorTitulo;
        private string anotacaoValorSaldoTitulo;
        private string anotacaoTipoEndosso;
        private string anotacaoInformacaoAceite;
        private string anotacaoNumeroControle;
        private string anotacaoUsoInstituicaoConveniada;

        [Key]
        public int SiscovemId { get; set; }

        public int ClienteId { get; set; }

        [ForeignKey("ClienteId")]
        public virtual Cliente Cliente { get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        public string Logon
        {
            get { return logon; }
            set { logon = value; }
        }

        [Required(ErrorMessage = Mensagens.Requerido)]
        public string Senha
        {
            get { return senha; }
            set { senha = value; }
        }

        [Required(ErrorMessage = Mensagens.Requerido)]
        [CnpjValidador(ErrorMessage = Mensagens.Cnpj)]
        public string CredorDocumento
        {
            get
            {
                if (!string.IsNullOrEmpty(credorDocumento))
                    return credorDocumento.SomenteNumeros();
                else
                    return string.Empty;
            }

            set { credorDocumento = value; }
        }

        [Required(ErrorMessage = Mensagens.Requerido)]
        [StringLength(70, ErrorMessage = "Máximo permitido 70 caracteres.")]
        public string CredorRazaoSocial
        {
            get
            {
                if (!string.IsNullOrEmpty(credorRazaoSocial))
                    return credorRazaoSocial.CortaTexto(70).RemoveAcentos().ToUpper();
                else
                    return string.Empty;
            }

            set { credorRazaoSocial = value; }
        }

        [Required(ErrorMessage = Mensagens.Requerido)]
        [StringLength(25, ErrorMessage = "Máximo permitido 25 caracteres.")]
        public string CredorNomeFantasia
        {
            get
            {
                if (!string.IsNullOrEmpty(credorNomeFantasia))
                    return credorNomeFantasia.CortaTexto(25).RemoveAcentos().ToUpper();
                else
                    return string.Empty;
            }

            set { credorNomeFantasia = value; }
        }

        [Required(ErrorMessage = Mensagens.Requerido)]
        [StringLength(45, ErrorMessage = "Máximo permitido 45 caracteres.")]
        public string CredorEndereco
        {
            get
            {
                if (!string.IsNullOrEmpty(credorEndereco))
                    return credorEndereco.CortaTexto(45).RemoveAcentos().ToUpper();
                else
                    return string.Empty;
            }

            set { credorEndereco = value; }
        }

        [Required(ErrorMessage = Mensagens.Requerido)]
        [StringLength(20, ErrorMessage = "Máximo permitido 20 caracteres.")]
        public string CredorBairro
        {
            get
            {
                if (!string.IsNullOrEmpty(credorBairro))
                    return credorBairro.CortaTexto(20).RemoveAcentos().ToUpper();
                else
                    return string.Empty;
            }

            set { credorBairro = value; }
        }

        [Required(ErrorMessage = Mensagens.Requerido)]
        [StringLength(25, ErrorMessage = "Máximo permitido 25 caracteres.")]
        public string CredorMunicipio
        {
            get
            {
                if (!string.IsNullOrEmpty(credorMunicipio))
                    return credorMunicipio.CortaTexto(25).RemoveAcentos().ToUpper();
                else
                    return string.Empty;
            }

            set { credorMunicipio = value; }
        }

        [Required(ErrorMessage = Mensagens.Requerido)]
        public string CredorUF
        {
            get
            {
                if (!string.IsNullOrEmpty(credorUF))
                    return credorUF.RemoveAcentos();
                else
                    return string.Empty;
            }

            set { credorUF = value; }
        }

        [Required(ErrorMessage = Mensagens.Requerido)]
        [StringLength(8, ErrorMessage = "Máximo permitido 8 dígitos.")]
        public string CredorCEP
        {
            get
            {
                if (!string.IsNullOrEmpty(credorCEP))
                    return credorCEP.SomenteNumeros();
                else
                    return string.Empty;
            }

            set { credorCEP = value; }
        }

        [Required(ErrorMessage = Mensagens.Requerido)]
        [StringLength(11, ErrorMessage = "Máximo permitido 11 dígitos.")]
        public string CredorTelefone
        {
            get
            {
                if (!string.IsNullOrEmpty(credorTelefone))
                    return credorTelefone.SomenteNumeros();
                else
                    return string.Empty;
            }

            set { credorTelefone = value; }
        }

        [Required(ErrorMessage = Mensagens.Requerido)]
        public string DistribuidorDocumento
        {
            get
            {
                if (!string.IsNullOrEmpty(distribuidorDocumento))
                    return distribuidorDocumento.SomenteNumeros();
                else
                    return string.Empty;
            }

            set { distribuidorDocumento = value; }
        }

        [Required(ErrorMessage = Mensagens.Requerido)]
        [CpfCnpjValidador(ErrorMessage = "CPF ou CNPJ inválidos.")]
        public string DevedorDocumento
        {
            get
            {
                if (!string.IsNullOrEmpty(devedorDocumento))
                    return devedorDocumento.Trim().SomenteNumeros();
                else
                    return string.Empty;
            }

            set { devedorDocumento = value; }
        }

        public string DevedorTipo
        {
            get
            {
                if (DevedorDocumento.Length == 11)
                    return "F";
                else
                    return "J";
            }
        }

        [Required(ErrorMessage = Mensagens.Requerido)]
        [StringLength(70, ErrorMessage = "Máximo permitido 70 caracteres.")]
        public string DevedorNome
        {
            get
            {
                if (!string.IsNullOrEmpty(devedorNome))
                    return devedorNome.CortaTexto(70).RemoveAcentos().ToUpper();
                else
                    return string.Empty;
            }
            set { devedorNome = value; }
        }

        [Required(ErrorMessage = Mensagens.Requerido)]
        [StringLength(45, ErrorMessage = "Máximo permitido 45 caracteres.")]
        public string DevedorEndereco
        {
            get
            {
                if (!string.IsNullOrEmpty(devedorEndereco))
                    return devedorEndereco.CortaTexto(45).RemoveAcentos().ToUpper();
                else
                    return string.Empty;
            }
            set { devedorEndereco = value; }
        }

        [StringLength(25, ErrorMessage = "Máximo permitido 25 caracteres.")]
        public string DevedorComplemento
        {
            get
            {
                if (!string.IsNullOrEmpty(devedorComplemento))
                    return devedorComplemento.CortaTexto(25).RemoveAcentos();
                else
                    return string.Empty;
            }
            set { devedorComplemento = value; }
        }

        [Required(ErrorMessage = Mensagens.Requerido)]
        [StringLength(20, ErrorMessage = "Máximo permitido 20 caracteres.")]
        public string DevedorBairro
        {
            get
            {
                if (!string.IsNullOrEmpty(devedorBairro))
                    return devedorBairro.CortaTexto(20).RemoveAcentos().ToUpper();
                else
                    return string.Empty;
            }
            set { devedorBairro = value; }
        }

        [Required(ErrorMessage = Mensagens.Requerido)]
        [StringLength(25, ErrorMessage = "Máximo permitido 25 caracteres.")]
        public string DevedorMunicipio
        {
            get
            {
                if (!string.IsNullOrEmpty(devedorMunicipio))
                    return devedorMunicipio.CortaTexto(25).RemoveAcentos().ToUpper();
                else
                    return string.Empty;
            }
            set { devedorMunicipio = value; }
        }

        [Required(ErrorMessage = Mensagens.Requerido)]
        public string DevedorUF
        {
            get { return devedorUF; }
            set { devedorUF = value; }
        }

        [Required(ErrorMessage = Mensagens.Requerido)]
        [StringLength(8, ErrorMessage = "Máximo permitido 8 dígitos.")]
        public string DevedorCEP
        {
            get
            {
                if (!string.IsNullOrEmpty(devedorCEP))
                    return devedorCEP.SomenteNumeros();
                else
                    return string.Empty;
            }
            set { devedorCEP = value; }
        }

        public string DevedorTelefone
        {
            get
            {
                if (!string.IsNullOrEmpty(devedorTelefone))
                    return devedorTelefone.SomenteNumeros();
                else
                    return string.Empty;
            }
            set { devedorTelefone = value; }
        }


        public string DevedorDataNascimento
        {
            get { return devedorDataNascimento; }
            set { devedorDataNascimento = value; }
        }

        [StringLength(15, ErrorMessage = "Máximo permitido 15 caracteres.")]
        public string DevedorRG
        {
            get
            {
                if (!string.IsNullOrEmpty(devedorRG))
                    return devedorRG.CortaTexto(15);
                else
                    return string.Empty;
            }
            set { devedorRG = value; }
        }

        public string DevedorRGUF
        {
            get { return devedorRGUF; }
            set { devedorRGUF = value; }
        }

        [StringLength(70, ErrorMessage = "Máximo permitido 70 caracteres.")]
        public string DevedorNomePai
        {
            get
            {
                if (!string.IsNullOrEmpty(devedorNomePai))
                    return devedorNomePai.CortaTexto(70).RemoveAcentos().ToUpper();
                else
                    return string.Empty;
            }
            set { devedorNomePai = value; }
        }

        [StringLength(70, ErrorMessage = "Máximo permitido 70 caracteres.")]
        public string DevedorNomeMae
        {
            get
            {
                if (!string.IsNullOrEmpty(devedorNomeMae))
                    return devedorNomeMae.CortaTexto(70).RemoveAcentos().ToUpper();
                else
                    return string.Empty;
            }
            set { devedorNomeMae = value; }
        }

        [Required(ErrorMessage = Mensagens.Requerido)]
        [StringLength(15, ErrorMessage = "Máximo permitido 15 caracteres.")]
        public string AnotacaoNossoNumero
        {
            get
            {
                if (!string.IsNullOrEmpty(anotacaoNossoNumero))
                    return anotacaoNossoNumero.CortaTexto(15).RemoveAcentos();
                else
                    return string.Empty;
            }
            set { anotacaoNossoNumero = value; }
        }

        [Required(ErrorMessage = Mensagens.Requerido)]
        public string AnotacaoEspecieTitulo
        {
            get { return anotacaoEspecieTitulo; }
            set { anotacaoEspecieTitulo = value; }
        }

        [Required(ErrorMessage = Mensagens.Requerido)]
        [StringLength(16, ErrorMessage = "Máximo permitido 16 dígitos.")]
        public string AnotacaoNumeroTitulo
        {
            get
            {
                if (!string.IsNullOrEmpty(anotacaoNumeroTitulo))
                    return anotacaoNumeroTitulo.CortaTexto(16).RemoveAcentos();
                else
                    return string.Empty;
            }
            set { anotacaoNumeroTitulo = value; }
        }

        [Required(ErrorMessage = Mensagens.Requerido)]
        public string AnotacaoUF
        {
            get { return anotacaoUF; }
            set { anotacaoUF = value; }
        }

        [Required(ErrorMessage = Mensagens.Requerido)]
        public string AnotacaoCidade
        {
            get
            {
                if (!string.IsNullOrEmpty(anotacaoCidade))
                    return anotacaoCidade.RemoveAcentos().ToUpper();
                else
                    return string.Empty;
            }
            set { anotacaoCidade = value; }
        }

        [Required(ErrorMessage = Mensagens.Requerido)]
        public string AnotacaoDataEmissao
        {
            get { return anotacaoDataEmissao; }
            set { anotacaoDataEmissao = value; }
        }

        [Required(ErrorMessage = Mensagens.Requerido)]
        public string AnotacaoDataVencimento
        {
            get { return anotacaoDataVencimento; }
            set { anotacaoDataVencimento = value; }
        }

        [Required(ErrorMessage = Mensagens.Requerido)]
        public string AnotacaoValorTitulo
        {
            get
            {
                if (!string.IsNullOrEmpty(anotacaoValorTitulo))
                    return anotacaoValorTitulo.SomenteNumeros();
                else
                    return string.Empty;
            }
            set { anotacaoValorTitulo = value; }
        }

        [Required(ErrorMessage = Mensagens.Requerido)]
        public string AnotacaoValorSaldoTitulo
        {
            get
            {
                if (!string.IsNullOrEmpty(anotacaoValorSaldoTitulo))
                    return anotacaoValorSaldoTitulo.SomenteNumeros();
                else
                    return string.Empty;
            }
            set { anotacaoValorSaldoTitulo = value; }
        }

        [Required(ErrorMessage = Mensagens.Requerido)]
        public string AnotacaoTipoEndosso
        {
            get { return anotacaoTipoEndosso; }
            set { anotacaoTipoEndosso = value; }
        }

        [Required(ErrorMessage = Mensagens.Requerido)]
        public string AnotacaoInformacaoAceite
        {
            get { return anotacaoInformacaoAceite; }
            set { anotacaoInformacaoAceite = value; }
        }

        [Required(ErrorMessage = Mensagens.Requerido)]
        public string AnotacaoNumeroControle
        {
            get { return anotacaoNumeroControle; }
            set { anotacaoNumeroControle = value; }
        }

        public string AnotacaoUsoInstituicaoConveniada
        {
            get
            {
                if (!string.IsNullOrEmpty(anotacaoUsoInstituicaoConveniada))
                    return anotacaoUsoInstituicaoConveniada.CortaTexto(20).RemoveAcentos();
                else
                    return string.Empty;
            }
            set { anotacaoUsoInstituicaoConveniada = value; }
        }


        public bool IsCredorSerasa { get; set; }
        public string DadosEnvio { get; set; }

    }
}