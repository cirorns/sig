﻿using Sig.Web.Helpers;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sig.Web.Models
{
    [Table(nameof(ItemFaturavel))]
    public class ItemFaturavel
    {
        [Key]
        public int ItemFaturavelId { get; set; }

        [StringLength(100, ErrorMessage = Mensagens.Maximo)]
        [Required(ErrorMessage = Mensagens.Requerido)]
        public string Descricao { get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        public decimal CustoUnitario { get; set; }
        
        [Required(ErrorMessage = Mensagens.Requerido)]
        public decimal ValorUnitario { get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        public decimal ValorMinimo { get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        public decimal ValorMaximo { get; set; }

        public bool IsAtivo { get; set; }

        public bool IsPacote { get; set; }

        public int? LinkItemFaturavel { get; set; }

        public bool IsConsulta { get; set; }

        [NotMapped]
        public bool IsSelecionado { get; set; }
        
    }
}
