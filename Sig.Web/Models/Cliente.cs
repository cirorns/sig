﻿using Sig.Web.Helpers;
using Sig.Web.Helpers.ValidadoresCustomizados;
using Sig.Web.Models.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace Sig.Web.Models
{
    [Table(nameof(Cliente))]
    public class Cliente
    {
        [Key]
        public int ClienteId { get; set; }

        public int? AgenciaId { get; set; }

        [ForeignKey(nameof(AgenciaId))]
        public virtual Cliente Agencia { get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        public int AtividadeId { get; set; }

        [ForeignKey(nameof(AtividadeId))]
        public virtual Atividade Atividade { get; set; }

        public int? RegiaoId { get; set; }

        [ForeignKey(nameof(RegiaoId))]
        public virtual Regiao Regiao { get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        public eTipoCliente TipoCliente { get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        public bool IsAtivo { get; set; }

        [StringLength(14, ErrorMessage = Mensagens.Maximo)]
        public string Rg { get; set; }

        [StringLength(10, ErrorMessage = Mensagens.Maximo)]
        public string RgOrgaoExpedidor { get; set; }

        [StringLength(2, ErrorMessage = Mensagens.Maximo)]
        public string RgUf { get; set; }

        public DateTime? RgDataExpedicao { get; set; }

        [CpfCnpjValidador(ErrorMessage = Mensagens.CpfCnpj)]
        [StringLength(14, ErrorMessage = Mensagens.Maximo)]
        [Required(ErrorMessage = Mensagens.Requerido)]
        public string CPFCNPJ { get; set; }

        [StringLength(100, ErrorMessage = Mensagens.Maximo)]
        public string InscricaoEstadual { get; set; }

        [StringLength(100, ErrorMessage = Mensagens.Maximo)]
        public string InscricaoMunicipal { get; set; }

        [StringLength(50, ErrorMessage = Mensagens.Maximo)]
        [Required(ErrorMessage = Mensagens.Requerido)]
        public string Nome { get; set; }

        [StringLength(100, ErrorMessage = Mensagens.Maximo)]
        public string RazaoSocial { get; set; }

        [StringLength(100, ErrorMessage = Mensagens.Maximo)]
        [Required(ErrorMessage = Mensagens.Requerido)]
        [EmailAddress(ErrorMessage = Mensagens.Email)]
        public string Email { get; set; }

        [StringLength(250, ErrorMessage = Mensagens.Maximo)]
        public string Website { get; set; }


        //TODO: Excluir
        public bool IsRevenda { get; set; }

        public decimal? ComissaoAgente { get; set; }

        public bool IsComissionamento { get; set; }
        //****


        public virtual List<Telefone> Telefones { get; set; }
        public virtual List<Endereco> Enderecos { get; set; }
        public virtual List<Contrato> Contratos { get; set; }
        public virtual List<Colaborador> Colaboradores { get; set; }
        public virtual List<Serasa> Serasas { get; set; }
        public virtual List<Atendimento> Atendimentos { get; set; }
        public virtual List<Contato> Contatos { get; set; }
        public virtual List<Faturamento> Faturamentos { get; set; }
        public virtual List<UsuarioConsulta> UsuariosConsultas { get; set; } = new List<UsuarioConsulta>();


        public Contrato ObtemContratoAtivo()
        {
            return Contratos.SingleOrDefault(c => !c.DataCancelamento.HasValue);
        }

        public Endereco ObtemEndereco()
        {
            return Enderecos.FirstOrDefault();
        }

        public Telefone ObtemTelefone()
        {
            return Telefones.FirstOrDefault();
        }


        [NotMapped]
        public Endereco Endereco
        {
            get
            {
                return Enderecos.FirstOrDefault();
            }
        }

        [NotMapped]
        public Telefone Telefone
        {
            get
            {
                return Telefones.FirstOrDefault();
            }
        }

        public Cliente()
        {
            Telefones = new List<Telefone>();
            Enderecos = new List<Endereco>();
            Contratos = new List<Contrato>();
            Colaboradores = new List<Colaborador>();
            Serasas = new List<Serasa>();
            Atendimentos = new List<Atendimento>();
            Contatos = new List<Contato>();
            Faturamentos = new List<Faturamento>();
        }

    }
}