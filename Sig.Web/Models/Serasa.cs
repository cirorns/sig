﻿using Sig.Web.Helpers;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sig.Web.Models
{
    [Table(nameof(Serasa))]
    public class Serasa
    {
        [Key]
        public int SerasaId { get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        public int ClienteId { get; set; }

        [ForeignKey("ClienteId")]
        public virtual Cliente Cliente { get; set; }

        [StringLength(50, ErrorMessage = "O campo não pode exceder 50 caracteres.")]
        [Required(ErrorMessage = Mensagens.Requerido)]
        public string Logon { get; set; }

        [StringLength(50, ErrorMessage = "O campo não pode exceder 50 caracteres.")]
        [Required(ErrorMessage = Mensagens.Requerido)]
        public string Senha { get; set; }

        public string Observacao { get; set; }
        public bool IsAtivo { get; set; }
        public bool IsInclusao { get; set; }
        public bool IsInterno { get; set; }
    }
}