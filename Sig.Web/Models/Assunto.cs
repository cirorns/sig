﻿using Sig.Web.Helpers;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sig.Web.Models
{
    [Table(nameof(Assunto))]
    public class Assunto
    {
        [Key]
        public int AssuntoId { get; set; }

        [StringLength(250, ErrorMessage = Mensagens.Maximo)]
        [Required(ErrorMessage = Mensagens.Requerido)]
        public string Descricao { get; set; }
    }
}