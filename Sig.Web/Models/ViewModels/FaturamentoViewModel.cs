﻿using Sig.Web.Helpers;
using System;
using System.ComponentModel.DataAnnotations;

namespace Sig.Web.Models.ViewModels
{
    public class FaturamentoViewModel
    {
        [Required(ErrorMessage = Mensagens.Requerido)]
        public DateTime DataInicial { get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        public DateTime DataFinal { get; set; }

        public DateTime? DataVencimento { get; set; }
    }
}