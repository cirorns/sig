﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sig.Web.Models.ViewModels
{
    public class RetornoBancarioViewModel
    {
        public string NossoNumero { get; set; }
        public string CodigoOcorrencia { get; set; }
        public string DataOcorrencia { get; set; }
        public bool Pagamento { get; set; }
        public string DataCredito { get; set; }
        public double ValorPago { get; set; }
        public double ValorMultaPaga { get; set; }
        public double ValorJurosPago { get; set; }
        public double ValorTaxaCobranca { get; set; }
        public double ValorCredito { get; set; }
        public string NumeroDocumento { get; set; }
        public string MotivoOcorrenciaCodigo { get; set; }
        public string MotivoOcorrenciaDescricao { get; set; }
        public string NomeSacado { get; set; }
    }
}