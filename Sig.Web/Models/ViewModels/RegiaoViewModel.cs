﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sig.Web.Models.ViewModels
{
    public class RegiaoViewModel
    {
        public Regiao Regiao { get; set; }
        public IEnumerable<Regiao> Regioes { get; set; }
    }
}