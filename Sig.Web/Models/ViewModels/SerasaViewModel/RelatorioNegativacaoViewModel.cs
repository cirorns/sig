﻿namespace Sig.Web.Models.ViewModels.SerasaViewModel
{
    public class RelatorioNegativacaoViewModel
    {
        public int? ClienteId { get; set; }
        public string DataInicial { get; set; }
        public string DataFinal { get; set; }
    }
}