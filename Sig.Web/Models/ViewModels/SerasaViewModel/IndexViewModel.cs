﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sig.Web.Models.ViewModels.SerasaViewModel
{
    public class IndexViewModel
    {
        public Serasa Serasa { get; set; }
        public IEnumerable<Serasa> Serasas { get; set; }
    }
}