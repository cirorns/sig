﻿using System.Collections.Generic;

namespace Sig.Web.Models.ViewModels.SerasaViewModel
{
    public class RemessaInclusaoAnotacaoNotificarViewModel
    {
        public int? ClienteId { get; set; }
        public int? AgenciaId { get; set; }
        public string DevedorDocumento { get; set; }
        public string CredorNome { get; set; }
        public string DevedorNome { get; set; }
        public bool IsConfirmado { get; set; }
        public List<ConvemInclusaoAnotacao> ConvemInclusaoAnotacoes { get; set; }
    }
}