﻿using Sig.Web.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Sig.Web.Models.ViewModels.SerasaViewModel
{
    public class RemessaExclusaoAnotacaoViewModel
    {
        public Nullable<int> ClienteId { get; set; }
        public Nullable<int> AgenciaId { get; set; }
        public string DevedorDocumento { get; set; }
        public string CredorNome { get; set; }
        public string DevedorNome { get; set; }

        //***//

        [Required(ErrorMessage = Mensagens.Requerido)]
        public int SequenciaAtual { get; set; }
        public List<ConvemExclusaoAnotacao> ConvemExclusaoAnotacoes { get; set; }
    }
}