﻿using Sig.Web.Helpers;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Sig.Web.Models.ViewModels.SerasaViewModel
{
    public class RemessaCredorViewModel
    {
        [Required(ErrorMessage = Mensagens.Requerido)]
        public int SequenciaAtual{ get; set; }
        public List<ConvemCredor> ConvemCredores { get; set; }
    }
}