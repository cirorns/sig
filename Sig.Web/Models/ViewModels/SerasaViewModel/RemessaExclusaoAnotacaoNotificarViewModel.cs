﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sig.Web.Models.ViewModels.SerasaViewModel
{
    public class RemessaExclusaoAnotacaoNotificarViewModel
    {
        public Nullable<int> ClienteId { get; set; }
        public Nullable<int> AgenciaId { get; set; }
        public string DevedorDocumento { get; set; }
        public string CredorNome { get; set; }
        public string DevedorNome { get; set; }
        public bool IsConfirmado { get; set; }
        public List<ConvemExclusaoAnotacao> ConvemExclusaoAnotacoes { get; set; }
    }
}