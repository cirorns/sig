﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sig.Web.Models.ViewModels.Financeiro
{
    public class FaturaViewModel
    {
        public Nullable<int> FaturamentoId { get; set; }
        public Nullable<int> ClienteId { get; set; }
        public Nullable<DateTime> DataInicialFatura { get; set; }
        public Nullable<DateTime> DataFinalFatura { get; set; }
        public IEnumerable<Faturamento> Faturamentos { get; set; }

    }
}