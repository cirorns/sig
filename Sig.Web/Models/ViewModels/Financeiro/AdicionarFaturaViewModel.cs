﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sig.Web.Models.ViewModels.Financeiro
{
    public class AdicionarFaturaViewModel
    {
        public int FaturamentoId { get; set; }
        public int ItemFaturavelId { get; set; }
        public int Quantidade { get; set; }   
    }
}