﻿using Sig.Web.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Sig.Web.Models.ViewModels.Financeiro
{
    public class BoletoViewModel
    {
        [Required(ErrorMessage = Mensagens.Requerido)]
        public DateTime DataInicialFaturamento { get; set; }
        [Required(ErrorMessage = Mensagens.Requerido)]
        public DateTime DataFinalFaturamento { get; set; }
    }
}