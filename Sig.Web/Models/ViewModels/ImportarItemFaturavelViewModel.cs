﻿using Sig.Web.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Sig.Web.Models.ViewModels
{
    public class ImportarItemFaturavelViewModel
    {
        [Required(ErrorMessage = Mensagens.Requerido)]
        public string Arquivo { get; set; }
    }
}