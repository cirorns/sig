﻿using Sig.Web.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Sig.Web.Models.ViewModels
{
    public class ClienteCreditoViewModel
    {
        public Cliente Cliente { get; set; }
        
        [Required(ErrorMessage = Mensagens.Requerido)]
        public int CreditoId { get; set; }
        
        public IList<Endereco> Enderecos { get; set; }
        public IList<Telefone> Telefones { get; set; }

    }
}