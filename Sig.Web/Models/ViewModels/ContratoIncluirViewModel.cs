﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sig.Web.Models.ViewModels
{
    public class ContratoIncluirViewModel
    {
        public Cliente Cliente { get; set; }
        public IEnumerable<Cliente> Clientes { get; set; }
        public Serasa Serasa { get; set; }
        public Contrato Contrato { get; set; }
    }
}