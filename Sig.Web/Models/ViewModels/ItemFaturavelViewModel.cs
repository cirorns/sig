﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sig.Web.Models.ViewModels
{
    public class ItemFaturavelViewModel
    {
        public ItemFaturavel ItemFaturavel { get; set; }
        public IEnumerable<ItemFaturavel> ItensFaturaveis { get; set; }
    }
}