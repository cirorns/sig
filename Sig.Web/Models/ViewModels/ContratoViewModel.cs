﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sig.Web.Models.ViewModels
{
    public class ContratoViewModel
    {
        public Cliente Cliente { get; set; }
        public Colaborador Colaborador { get; set; }
        public Tabela Tabela { get; set; }
        public Contrato Contrato { get; set; }
        public bool IsAtivo { get; set; }
        public IEnumerable<Contrato> Contratos { get; set; }
    }
}