﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sig.Web.Models.ViewModels
{
    public class RelatorioClienteViewModel
    {
        public Faturamento Faturamento { get; set; }
        public Cliente Cliente { get; set; }
        public Contrato Contrato { get; set; }
        public Endereco Endereco { get; set; }
        public Nullable<DateTime> DataInicioCancelamento { get; set; }
        public Nullable<DateTime> DataFinalCancelamento { get; set; }
        public Nullable<DateTime> DataInicioCadastro { get; set; }
        public Nullable<DateTime> DataFinalCadastro { get; set; }
        public IEnumerable<Cliente> Clientes { get; set; }
        public Atividade Atividade { get; set; }
        public IEnumerable<Telefone> Telefone { get; set; }
        public IEnumerable<Faturamento> Faturamentos { get; set; }
        public TransacaoConsulta TransacaoConsulta { get; set; }
        public IEnumerable<TransacaoConsulta> TransacaoConsultas { get; set; }
        public ItemFaturavel ItemFaturavel { get; set; }
    }

}