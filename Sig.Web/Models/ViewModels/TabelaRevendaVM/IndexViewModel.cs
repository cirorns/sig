﻿using System.Collections.Generic;

namespace Sig.Web.Models.ViewModels.TabelaRevendaVM
{
    public class IndexViewModel
    {
        public Cliente Cliente { get; set; }
        public Tabela Tabela { get; set; }
        public IEnumerable<TabelaRevenda> TabelasRevendas { get; set; }
    }
}