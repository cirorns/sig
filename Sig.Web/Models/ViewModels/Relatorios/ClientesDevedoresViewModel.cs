﻿using Sig.Web.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Sig.Web.Models.ViewModels.Relatorios
{
    public class ClientesDevedoresViewModel
    {
        public int? AgenciaId { get; set; }

        public int? ClienteId { get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        public DateTime DataInicial { get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        public DateTime DataFinal { get; set; }

        public IEnumerable<Faturamento> Faturamentos { get; set; }
    }
}