﻿using Sig.Web.Helpers;
using System;
using System.ComponentModel.DataAnnotations;

namespace Sig.Web.Models.ViewModels.Relatorios
{
    public class AnaliseFaturamentoViewModel
    {
        public int FaturamentoId { get; set; }
        public int ClienteId { get; set; }
        public string Cliente { get; set; }
        public int AgenciaId { get; set; }
        public string Agencia { get; set; }
        public decimal FaturamentoCusto { get; set; }
        public decimal FaturamentoCustoRevenda { get; set; }
        public decimal FaturamentoBruto { get; set; }
        public decimal ComissaoAgente { get; set; }
        public decimal ComissaoAdministradora { get; set; }
        public decimal RetencaoAgente { get; set; }
        public decimal RetencaoAdministradora { get; set; }
        public decimal RetencaoTotal { get; set; }
        public decimal RepasseReal { get; set; }
        public DateTime DataVencimento { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? DataPagamento { get; set; }

        public decimal ValorPago { get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        public DateTime DataInicial { get; set; }
        [Required(ErrorMessage = Mensagens.Requerido)]
        public DateTime DataFinal { get; set; }
        public decimal LucroAdministradora { get; set; }
        public bool IsRevenda { get; set; }
        public string LinkBoleto { get; set; }
    }
}