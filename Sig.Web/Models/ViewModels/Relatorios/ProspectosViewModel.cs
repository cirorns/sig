﻿using System;
using System.Collections.Generic;

namespace Sig.Web.Models.ViewModels.Relatorios
{
    public class ProspectosViewModel
    {
        public int? AgenciaId { get; set; }
        public string Cidade { get; set; }
        public string UF { get; set; }
        public IEnumerable<Cliente> Clientes { get; set; }
    }
}