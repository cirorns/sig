﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sig.Web.Models.ViewModels.Relatorios
{
    public class ContasRecebidasViewModel
    {
        public Nullable<DateTime> DataInicialFaturamento { get; set; }
        public Nullable<int> AgenciaId { get; set; }
        public IEnumerable<Faturamento> Faturamentos { get; set; }
    }
}