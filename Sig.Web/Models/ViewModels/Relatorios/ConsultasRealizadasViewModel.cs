﻿using System;
using System.Collections.Generic;

namespace Sig.Web.Models.ViewModels.Relatorios
{
    public class ConsultasRealizadasViewModel
    {
        public int? ClienteId { get; set; }
        public DateTime? DataInicial { get; set; }
        public DateTime? DataFinal { get; set; }

        public IEnumerable<TransacaoConsulta> TransacoesConsultas { get; set; }
    }
}