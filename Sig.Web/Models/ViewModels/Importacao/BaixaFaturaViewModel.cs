﻿using Sig.Web.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Sig.Web.Models.ViewModels.Importacao
{
    public class BaixaFaturaViewModel
    {
        public int FaturamentoId { get; set; }
        public DateTime DataPagamento { get; set; }
        public decimal ValorPago { get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        public string Arquivo { get; set; }
    }
}