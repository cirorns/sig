﻿using Sig.Web.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Sig.Web.Models.ViewModels.Importacao
{
    public class ExcluiFaturaViewModel
    {
        [Required(ErrorMessage = Mensagens.Requerido)]
        public string FaturasId { get; set; }
    }
}