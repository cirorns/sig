﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sig.Web.Models.ViewModels
{
    public class ClienteAgenciaViewModel
    {
        public Cliente Cliente { get; set; }
        public IList<Endereco> Enderecos { get; set; }
        public IList<Telefone> Telefones { get; set; }
        public Colaborador Colaborador { get; set; }
    }
}