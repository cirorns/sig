﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sig.Web.Models.ViewModels
{
    public class ColaboradorViewModel
    {
        public Colaborador Colaborador { get; set; }
        public IEnumerable<Colaborador> Colaboradores { get; set; }
    }
}