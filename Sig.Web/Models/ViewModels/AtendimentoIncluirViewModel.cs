﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sig.Web.Models.ViewModels
{
    public class AtendimentoIncluirViewModel
    {
        public Atendimento Atendimento { get; set; }
        public Cliente Cliente { get; set; }
    }
}