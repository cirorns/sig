﻿using Sig.Web.Helpers;
using Sig.Web.Helpers.ValidadoresCustomizados;
using System.ComponentModel.DataAnnotations;

namespace Sig.Web.Models.ViewModels.Siscovem
{
    public class ExcluirViewModel
    {
        [Required(ErrorMessage = Mensagens.Requerido)]
        [CnpjValidador(ErrorMessage= Mensagens.Cnpj)]
        public string CredorCNPJ { get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        [CpfCnpjValidador(ErrorMessage=Mensagens.CpfCnpj)]
        public string DevedorCPFCNPJ { get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        public string MotivoBaixa { get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        public string DistribuidorCNPJ { get; set; }
    }
}