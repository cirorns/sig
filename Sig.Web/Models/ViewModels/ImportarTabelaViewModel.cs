﻿using Sig.Web.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Sig.Web.Models.ViewModels
{
    public class ImportarTabelaViewModel
    {
        public string DescricaoItemFaturavel { get; set; }
        public decimal ValorUnitario { get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        public string Arquivo { get; set; }
    }
}