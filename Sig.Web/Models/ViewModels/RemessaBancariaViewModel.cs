﻿using Sig.Web.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Sig.Web.Models.ViewModels
{
    public class RemessaBancariaViewModel
    {
        [Required(ErrorMessage = Mensagens.Requerido)]
        public DateTime DataInicial { get; set; }
        [Required(ErrorMessage = Mensagens.Requerido)]
        public DateTime DataFinal { get; set; }
    }
}