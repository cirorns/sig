﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sig.Web.Models.ViewModels
{
    public class TabelaItemViewModel
    {
        public int TabelaId { get; set; }
        public int ItemFaturavelId { get; set; }
        public string Descricao { get; set; }
        public decimal ValorUnitario { get; set; }
        public decimal ValorMinimo { get; set; }
        public decimal ValorMaximo { get; set; }
        public int QuantidadePacote { get; set; }
        public bool IsPacote { get; set; }
        public bool IsAtivo { get; set; }
    }
}