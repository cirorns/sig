﻿using Sig.Web.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Sig.Web.Models.ViewModels
{
    public class UsuarioConsultaAlterarSenhaViewModel
    {
        [Required(ErrorMessage = Mensagens.Requerido)]
        public string SenhaAtual{ get; set; }
        
        [Required(ErrorMessage = Mensagens.Requerido)]
        public string SenhaNova { get; set; }

        [Compare("SenhaNova", ErrorMessage=Mensagens.SenhaConfirmacao)]
        [Required(ErrorMessage = Mensagens.Requerido)]
        public string SenhaConfirmar { get; set; }

    }
}