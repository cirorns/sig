﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sig.Web.Models.ViewModels
{
    public class CargoViewModel
    {
        public Cargo Cargo { get; set; }
        public IEnumerable<Cargo> Cargos { get; set; }
    }
}