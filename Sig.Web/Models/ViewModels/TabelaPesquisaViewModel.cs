﻿using System.Collections.Generic;

namespace Sig.Web.Models.ViewModels
{
    public class TabelaPesquisaViewModel
    {
        public int? ClienteId { get; set; }
        public Tabela Tabela { get; set; }
        public IEnumerable<Tabela> Tabelas { get; set; }
    }
}