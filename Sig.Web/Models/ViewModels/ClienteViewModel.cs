﻿using System.Collections.Generic;

namespace Sig.Web.Models.ViewModels
{
    public class ClienteViewModel
    {
        public Cliente Cliente { get; set; }
        public IList<Endereco> Enderecos { get; set; }
        public IList<Telefone> Telefones { get; set; }
        public Contrato Contrato { get; set; }
        public Contato Contato { get; set; }
    }
}