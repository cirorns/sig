﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sig.Web.Models.ViewModels
{
    public class CreditoViewModel
    {
        public Credito Credito { get; set; }
        public IEnumerable<Credito> Creditos { get; set; }
    }
}