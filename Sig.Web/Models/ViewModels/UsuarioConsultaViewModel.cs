﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sig.Web.Models.ViewModels
{
    public class UsuarioConsultaViewModel
    {
        public UsuarioConsulta UsuarioConsulta { get; set; }
        public IEnumerable<UsuarioConsulta> UsuarioConsultas { get; set; }
    }
}