﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sig.Web.Models.ViewModels
{
    public class RelatorioAnaliseFaturamentoViewModel
    {
        public int ClienteId { get; set; }
        public string Cliente { get; set; }
        public int AgenciaId { get; set; }
        public string Agencia { get; set; }
        public decimal FaturamentoCusto { get; set; }
        public decimal FaturamentoBruto { get; set; }
        public decimal ComissaoAgente { get; set; }
        public decimal ComissaoAdministradora { get; set; }
        public decimal RetencaoAgente { get; set; }
        public decimal RetencaoAdministradora { get; set; }
        public decimal RetencaoTotal { get; set; }
        public DateTime DataVencimento { get; set; }
        public Nullable<DateTime> DataPagamento { get; set; }
    }
}