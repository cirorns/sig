﻿using Sig.Web.Helpers;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Sig.Web.Models.ViewModels
{
    public class TabelaViewModel
    {
        public int TabelaId { get; set; }
        [Required(ErrorMessage = Mensagens.Requerido)]
        public string Descricao { get; set; }
        public bool IsConsumo { get; set; }
        public bool IsTabelaAtiva { get; set; }
        public bool IsEspecial { get; set; }
        public List<TabelaItemViewModel> TabelaItens { get; set; }
    }
}