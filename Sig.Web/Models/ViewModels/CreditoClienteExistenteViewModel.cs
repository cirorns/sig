﻿using Sig.Web.Helpers;
using System.ComponentModel.DataAnnotations;

namespace Sig.Web.Models.ViewModels
{
    public class CreditoClienteExistenteViewModel
    {
        [Required(ErrorMessage = Mensagens.Requerido)]
        public int CreditoId { get; set; }
    }
}