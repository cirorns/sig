﻿using System.Collections.Generic;

namespace Sig.Web.Models.ViewModels
{
    public class TabelaAgenciaViewModel
    {
        public Cliente Agencia { get; set; }
        public Tabela Tabela { get; set; }
        public IEnumerable<TabelaAgencia> TabelasAgencia { get; set; }
    }
}