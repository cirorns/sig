﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sig.Web.Models.ViewModels
{
    public class MultLogonViewModel
    {
        public MultLogon MultLogon { get; set; }
        public IEnumerable<MultLogon> MultLogons { get; set; }
    }
}