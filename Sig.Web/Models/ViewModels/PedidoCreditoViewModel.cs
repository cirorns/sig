﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sig.Web.Models.ViewModels
{
    public class PedidoCreditoViewModel
    {
        public PedidoCredito PedidoCredito { get; set; }
        public IEnumerable<PedidoCredito> PedidoCreditos { get; set; }
    }
}