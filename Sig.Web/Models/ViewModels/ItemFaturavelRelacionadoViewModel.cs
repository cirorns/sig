﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sig.Web.Models.ViewModels
{
    public class ItemFaturavelRelacionadoViewModel
    {
        public ItemFaturavelRelacionado ItemFaturavelRelacionado { get; set; }
        public IEnumerable<ItemFaturavelRelacionado> ItensFaturaveisRelacionados { get; set; }
    }
}