﻿using System.Collections.Generic;

namespace Sig.Web.Models.ViewModels
{
    public class AtendimentoViewModel
    {
        public Cliente Cliente { get; set; }
        public Serasa Serasa { get; set; }
        public Contrato Contrato { get; set; }
        public IEnumerable<Cliente> Clientes { get; set; }
    }
}