﻿using Sig.Web.Helpers;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sig.Web.Models
{
    [Table(nameof(ConfiguracaoEmpresa))]
    public class ConfiguracaoEmpresa : BaseModel
    {
        [Required(ErrorMessage = Mensagens.Requerido)]
        [StringLength(250, ErrorMessage = Mensagens.Maximo)]
        public string Nome { get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        [StringLength(100, ErrorMessage = Mensagens.Maximo)]
        public string Email { get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        [StringLength(250, ErrorMessage = Mensagens.Maximo)]
        public string SiteConsulta { get; set; }
    }
}