﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sig.Web.Models
{
    [Table(nameof(Configuracao))]
    public class Configuracao
    {
        [Key]
        public int ConfiguracaoId { get; set; }        

        //LOGOMARCA EMPRESA

        [StringLength(250)]
        public string Logomarca { get; set; }

        //SISTEMA

        [StringLength(250)]
        public string Empresa { get; set; }

        public bool IsMultLogon { get; set; }

        //CONVEM CREDOR DADOS DO INFORMANTE

        [StringLength(15, ErrorMessage = "O campo não pode exceder 15 dígitos.")]
        public string CNPJInformante { get; set; } //003885106 + 000108

        [StringLength(4, ErrorMessage = "O campo não pode exceder 4 dígitos.")]
        public string DDDInformante { get; set; } //0081

        [StringLength(8, ErrorMessage = "O campo não pode exceder 8 dígitos.")]
        public string TelefoneInformante { get; set; } //32240419

        [StringLength(70, ErrorMessage = "O campo não pode exceder 70 caracteres.")]
        public string NomeContatoInformante { get; set; } //KARYNE MONTEIRO

    }
}