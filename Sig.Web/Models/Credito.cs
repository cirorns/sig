﻿using Sig.Web.Helpers;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sig.Web.Models
{
    [Table(nameof(Credito))]
    public class Credito
    {
        [Key]
        [Required(ErrorMessage = Mensagens.Requerido)]
        public int CreditoId { get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        [StringLength(250, ErrorMessage = "O campo não pode exceder 250 caracters.")]
        public string Descricao { get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        public decimal Valor { get; set; }

        public bool IsAtivo { get; set; }
    }
}