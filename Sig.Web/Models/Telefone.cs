﻿using Sig.Web.Helpers;
using Sig.Web.Models.Enums;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sig.Web.Models
{
    [Table(nameof(Telefone))]
    public class Telefone
    {
        [Key]
        public int TelefoneId { get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        public int ClienteId { get; set; }

        [ForeignKey(nameof(ClienteId))]
        public virtual Cliente Cliente { get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        public eTipoTelefone TipoTelefone { get; set; }

        [StringLength(2, ErrorMessage = Mensagens.Maximo)]
        [Required(ErrorMessage = Mensagens.Requerido)]
        public string DDD { get; set; }

        [StringLength(9, MinimumLength = 8, ErrorMessage = Mensagens.MaximoMinimo)]
        [Required(ErrorMessage = Mensagens.Requerido)]
        public string Numero { get; set; }

    }
}