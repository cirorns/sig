﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sig.Web.Models
{
    public class EspecieTitulo
    {
        public string Sigla { get; set; }
        public string Descricao { get; set; }
    }

    public class SiscovemEspecieTitulo
    {
        public static List<EspecieTitulo> EspecieTitulos = new List<EspecieTitulo>
        {
            new EspecieTitulo{ Descricao = "BC-COBRANCA CAUCIONADA", Sigla = "BC" },
            new EspecieTitulo{ Descricao = "BD-COBRANCA DESCONTADA", Sigla = "BD" },
            new EspecieTitulo{ Descricao = "BS-COBRANCA SIMPLES", Sigla = "BS" },
            new EspecieTitulo{ Descricao = "CC-CONTRATO DE CAMBIO", Sigla = "CC" },
            new EspecieTitulo{ Descricao = "CCC-CEDULA DE CREDITO COMERCIAL", Sigla = "CCC" },
            new EspecieTitulo{ Descricao = "CCE-CEDULA DE CREDITO A EXPORTACAO", Sigla = "CCE" },
            new EspecieTitulo{ Descricao = "CCI-CEDULA DE CREDITO INDUSTRIAL", Sigla = "CCI" },
            new EspecieTitulo{ Descricao = "CCR-CEDULA DE CREDITO RURAL", Sigla = "CCR" },
            new EspecieTitulo{ Descricao = "CD -CONFISSAO DE DIVIDA", Sigla = "CD" },
            new EspecieTitulo{ Descricao = "CDE-CERT EN76 JE", Sigla = "CDE" },
            new EspecieTitulo{ Descricao = "CH -CHEQUE", Sigla = "CH" },
            new EspecieTitulo{ Descricao = "CHP-CEDULA HIPOTECARIA", Sigla = "CHP" },
            new EspecieTitulo{ Descricao = "CJV-CONTA JUDICIALMENTE VERIFICADA", Sigla = "CJV" },
            new EspecieTitulo{ Descricao = "CM -CONTRATO DE MUTUO", Sigla = "CM" },
            new EspecieTitulo{ Descricao = "CPH-CEDULA RURAL PIGNORATICIA HIPOTECARIA", Sigla = "CPH" },
            new EspecieTitulo{ Descricao = "CRH-CEDULA RURAL HIPOTECARIA", Sigla = "CRH" },
            new EspecieTitulo{ Descricao = "CRP-CEDULA RURAL PIGNORATICIA", Sigla = "CRP" },
            new EspecieTitulo{ Descricao = "CS -CONTA DE PRESTACAO DE SERVICO", Sigla = "CS" },
            new EspecieTitulo{ Descricao = "DM -DUPLICATA DE VENDA MERCANTIL", Sigla = "DM" },
            new EspecieTitulo{ Descricao = "DMI-DUPLICATA DE VENDA MERCANTIL POR INDICACAO", Sigla = "DMI" },
            new EspecieTitulo{ Descricao = "DR -DUPLICATA RURAL", Sigla = "DR" },
            new EspecieTitulo{ Descricao = "DRI-DUPLICATA RURAL DE INDICACAO", Sigla = "DRI" },
            new EspecieTitulo{ Descricao = "DS -DUPLICATA DE PRESTACAO DE SERVICOS", Sigla = "DS" },
            new EspecieTitulo{ Descricao = "DSI-DUPLICATA DE PRESTACAO DE SERVICOS POR INDICACAO", Sigla = "DSI" },
            new EspecieTitulo{ Descricao = "DV -DIVERSOS QUANDO NAO ENQUADRAR NA RELACAO", Sigla = "DV" },
            new EspecieTitulo{ Descricao = "LC -LETRA DE CAMBIO", Sigla = "LC" },
            new EspecieTitulo{ Descricao = "LCN-LETRA DE CAMBIO SEM ACEITE", Sigla = "LCN" },
            new EspecieTitulo{ Descricao = "NCC-NOTA DE CREDITO COMERCIAL", Sigla = "NCC" },
            new EspecieTitulo{ Descricao = "NCE-NOTA DE CREDITO A EXPORTACAO", Sigla = "NCE" },
            new EspecieTitulo{ Descricao = "NCI-NOTA DE CREDITO INDUSTRIAL", Sigla = "NCI" },
            new EspecieTitulo{ Descricao = "NCR-NOTA DE CREDITO RURAL", Sigla = "NCR" },
            new EspecieTitulo{ Descricao = "NP -NOTA PROMISSORIA", Sigla = "NP" },
            new EspecieTitulo{ Descricao = "NPR-NOTA PROMISSORIA RURAL", Sigla = "NPR" },
            new EspecieTitulo{ Descricao = "SJ -SENTENCA JUDICIAL", Sigla = "SJ" },
            new EspecieTitulo{ Descricao = "TA -TERMO DE ACORDO", Sigla = "TA" },
            new EspecieTitulo{ Descricao = "TM -TRIPLICATA DE VENDA MERCANTIL", Sigla = "TM" },
            new EspecieTitulo{ Descricao = "TS -TRIPLICATA DE PRESTACAO DE SERVICOS", Sigla = "TS" },
            new EspecieTitulo{ Descricao = "W  -WARRANT", Sigla = "W" },
        };
    }
}