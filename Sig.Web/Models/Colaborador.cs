﻿using Sig.Web.Helpers;
using Sig.Web.Helpers.ValidadoresCustomizados;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sig.Web.Models
{
    [Table(nameof(Colaborador))]
    public class Colaborador
    {
        [Key]
        public int ColaboradorId { get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        public int AgenciaId { get; set; }

        [ForeignKey(nameof(AgenciaId))]
        public virtual Cliente Agencia { get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        public int CargoId { get; set; }

        [ForeignKey(nameof(CargoId))]
        public virtual Cargo Cargo { get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        [StringLength(11, ErrorMessage = Mensagens.Maximo)]
        [CpfValidador(ErrorMessage=Mensagens.Cpf)]
        public string CPF { get; set; }
        
        [StringLength(14, ErrorMessage = Mensagens.Maximo)]
        public string Rg { get; set; }

        [StringLength(10, ErrorMessage = Mensagens.Maximo)]
        public string RgOrgaoExpedidor { get; set; }

        [StringLength(2, ErrorMessage = Mensagens.Maximo)]
        public string RgUf { get; set; }

        public DateTime? RgDataExpedicao { get; set; }

        [StringLength(50, ErrorMessage = Mensagens.Maximo)]
        [Required(ErrorMessage = Mensagens.Requerido)]
        public string Nome { get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        public DateTime DataNascimento { get; set; }

        [StringLength(2, ErrorMessage = Mensagens.Maximo)]
        [Required(ErrorMessage = Mensagens.Requerido)]
        public string DDDTelefone { get; set; }

        [StringLength(9, MinimumLength = 8, ErrorMessage = Mensagens.MaximoMinimo)]
        [Required(ErrorMessage = Mensagens.Requerido)]
        public string Telefone { get; set; }

        [StringLength(2, ErrorMessage = Mensagens.Maximo)]
        [Required(ErrorMessage = Mensagens.Requerido)]
        public string DDDCelular { get; set; }

        [StringLength(9, MinimumLength = 8, ErrorMessage = Mensagens.MaximoMinimo)]
        [Required(ErrorMessage = Mensagens.Requerido)]
        public string Celular { get; set; }

        [StringLength(250, ErrorMessage = Mensagens.Maximo)]
        [Required(ErrorMessage = Mensagens.Requerido)]
        public string Logradouro { get; set; }

        [StringLength(100, ErrorMessage = Mensagens.Maximo)]
        public string Complemento { get; set; }

        [StringLength(100, ErrorMessage = Mensagens.Maximo)]
        [Required(ErrorMessage = Mensagens.Requerido)]
        public string Bairro { get; set; }

        [StringLength(100, ErrorMessage = Mensagens.Maximo)]
        [Required(ErrorMessage = Mensagens.Requerido)]
        public string Cidade { get; set; }

        [StringLength(8, ErrorMessage = Mensagens.Maximo)]
        [Required(ErrorMessage = Mensagens.Requerido)]
        public string CEP { get; set; }

        [StringLength(2, ErrorMessage = Mensagens.Maximo)]
        [Required(ErrorMessage = Mensagens.Requerido)]
        public string UF { get; set; }

        [StringLength(100, ErrorMessage = Mensagens.Maximo)]
        [Required(ErrorMessage = Mensagens.Requerido)]
        [EmailAddress(ErrorMessage = Mensagens.Email)]
        public string Email { get; set; }
    }
}
