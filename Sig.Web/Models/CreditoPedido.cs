﻿using Sig.Web.Helpers;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sig.Web.Models
{
    [Table(nameof(CreditoPedido))]
    public class CreditoPedido : BaseModel
    {
        [Required(ErrorMessage = Mensagens.Requerido)]
        public int ClienteId { get; set; }

        [ForeignKey(nameof(ClienteId))]
        public virtual Cliente Cliente { get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        public int CreditoId { get; set; }

        [ForeignKey(nameof(CreditoId))]
        public virtual Credito Credito { get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        public DateTime Data { get; set; }

        public DateTime? DataConfirmacao { get; set; }

        public DateTime? DataVencimento { get; set; }

        [StringLength(250, ErrorMessage = Mensagens.Maximo)]
        public string CodigoTransacao { get; set; }

        [StringLength(250, ErrorMessage = Mensagens.Maximo)]
        public string Referencia { get; set; }

        [StringLength(250, ErrorMessage = Mensagens.Maximo)]
        public string Token { get; set; }

        public bool IsConfirmado { get; set; }
    }
}