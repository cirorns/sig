﻿using Sig.Web.Helpers;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sig.Web.Models
{
    [Table(nameof(UsuarioConsulta))]
    public class UsuarioConsulta
    {
        [Key]
        public int UsuarioConsultaId { get; set; }

        public int? ContratoId { get; set; }

        [ForeignKey("ContratoId")]
        public virtual Contrato Contrato { get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        public int ClienteId { get; set; }

        [ForeignKey("ClienteId")]
        public virtual Cliente Cliente { get; set; }

        [StringLength(100, ErrorMessage = "O campo não pode exceder 100 caracteres.")]
        [Required(ErrorMessage = Mensagens.Requerido)]
        public string Email { get; set; }

        [StringLength(8, ErrorMessage = "O campo não pode exceder 8 caracteres.")]
        [Required(ErrorMessage = Mensagens.Requerido)]
        public string Senha { get; set; }

        [NotMapped]
        [StringLength(8, ErrorMessage = "O campo não pode exceder 8 caracteres.")]
        [Required(ErrorMessage = Mensagens.Requerido)]
        [Compare("Senha", ErrorMessage = "A Confirmação de senha não confere.")]
        public string ConfirmarSenha { get; set; }

        public bool IsSuperUsuario { get; set; }

        public bool IsAtivo { get; set; }
    }
}