﻿using Sig.Web.Helpers;
using System.ComponentModel.DataAnnotations;

namespace Sig.Web.Models
{
    public class PagarMe : BaseModel
    {
        [StringLength(250, ErrorMessage = Mensagens.Maximo)]
        [Required(ErrorMessage = Mensagens.Requerido)]
        public string DefaultApiKey { get; set; }

        [StringLength(250, ErrorMessage = Mensagens.Maximo)]
        [Required(ErrorMessage = Mensagens.Requerido)]
        public string PostBackUrl { get; set; }

        public bool IsAtivo { get; set; }
    }
}