﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sig.Web.Models
{
    public class TipoEndosso
    {
        public string Descricao { get; set; }
        public string Sigla { get; set; }
    }

    public class SiscovemTipoEndosso
    {
        public static List<TipoEndosso> TipoEndossos = new List<TipoEndosso>
        {
            new TipoEndosso{ Descricao = "MANDATO", Sigla = "M" },
            new TipoEndosso{ Descricao = "TRANSLATIVO", Sigla = "T" },
            new TipoEndosso{ Descricao = "SEM ENDOSSO", Sigla = " " },
        };
            
    }
}