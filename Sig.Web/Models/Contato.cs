﻿using Sig.Web.Helpers;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sig.Web.Models
{
    [Table(nameof(Contato))]
    public class Contato
    {
        [Key]
        public int ContatoId { get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        public int ClienteId { get; set; }

        [ForeignKey("ClienteId")]
        public virtual Cliente Cliente { get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        public int CargoId { get; set; }

        [ForeignKey("CargoId")]
        public virtual Cargo Cargo { get; set; }

        [StringLength(11, ErrorMessage = "O campo não pode exceder 11 dígitos.")]
        [Required(ErrorMessage = Mensagens.Requerido)]
        public string CPF { get; set; }

        #region Dados RG

        [StringLength(14, ErrorMessage = "O campo não pode exceder 14 dígitos.")]
        public string Rg { get; set; }

        [StringLength(10, ErrorMessage = "O campo não pode exceder 10 caracteres.")]
        public string RgOrgaoExpedidor { get; set; }

        [StringLength(2, ErrorMessage = "O campo não pode exceder 2 caracteres.")]
        public string RgUf { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public Nullable<DateTime> RgDataExpedicao { get; set; }

        #endregion

        [StringLength(50, ErrorMessage = "O campo não pode exceder 50 caracters.")]
        [Required(ErrorMessage = Mensagens.Requerido)]
        public string Nome { get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime DataNascimento { get; set; }

        [StringLength(2, ErrorMessage = "O campo não pode exceder 2 dígitos.")]
        public string DDDTelefone { get; set; }

        [StringLength(9, MinimumLength = 8, ErrorMessage = "O campo deve conter 8 ou 9 dígitos.")]
        public string Telefone { get; set; }

        [StringLength(2, ErrorMessage = "O campo não pode exceder 2 dígitos.")]
        public string DDDCelular { get; set; }

        [StringLength(9, MinimumLength = 8, ErrorMessage = "O campo deve conter 8 ou 9 dígitos.")]
        public string Celular { get; set; }

        [StringLength(250, ErrorMessage = "O campo não pode exceder 250 caracteres.")]
        public string Logradouro { get; set; }

        [StringLength(100, ErrorMessage = "O campo não pode exceder 100 caracteres.")]
        public string Complemento { get; set; }

        [StringLength(100, ErrorMessage = "O campo não pode exceder 100 caracteres.")]
        public string Bairro { get; set; }

        [StringLength(100, ErrorMessage = "O campo não pode exceder 100 caracteres.")]
        public string Cidade { get; set; }

        [StringLength(8, ErrorMessage = "O campo não pode exceder 8 dígitos.")]
        public string CEP { get; set; }

        [StringLength(2, ErrorMessage = "O campo não pode exceder 2 caracteres.")]
        public string UF { get; set; }

        [StringLength(100, ErrorMessage = "O campo não pode exceder 100 caracteres.")]
        [Required(ErrorMessage = Mensagens.Requerido)]
        public string Email { get; set; }

    }
}
