﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sig.Web.Models
{
    public class CredorTipo
    {
        public string Sigla { get; set; }
        public string Descricao { get; set; }
    }

    public class ConvemCredorTipo
    {
        public static List<CredorTipo> CredorTipos = new List<CredorTipo>
        {
            new CredorTipo{ Descricao = "Inclusão", Sigla = "A" },
            new CredorTipo{ Descricao = "Cancelamento/Exclusão", Sigla = "C" },
            new CredorTipo{ Descricao = "Reativação/Reinclusão", Sigla = "R" },
        };
    }
}