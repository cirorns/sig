﻿using Sig.Web.Helpers;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sig.Web.Models
{
    [Table(nameof(TransacaoConsulta))]
    public class TransacaoConsulta
    {
        [Key]
        public int NSU { get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        public int ItemFaturavelId { get; set; }

        [ForeignKey("ItemFaturavelId")]
        public virtual ItemFaturavel ItemFaturavel { get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        public int ClienteId { get; set; }

        [ForeignKey("ClienteId")]
        public virtual Cliente Cliente { get; set; }

        public int? UsuarioConsultaId { get; set; }

        [ForeignKey("UsuarioConsultaId")]
        public virtual UsuarioConsulta UsuarioConsulta { get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        public DateTime DataEnvio { get; set; }

        public DateTime? DataResposta { get; set; }

        public string DadosEnvio { get; set; }

        public string DadosResposta { get; set; }

        public string StringResposta { get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        public bool IsConfirmado { get; set; }

        public int? NSUOrigem { get; set; }

        public DateTime? DataInicial { get; set; }

        public DateTime? DataFinal { get; set; }

        [StringLength(250, ErrorMessage = Mensagens.Maximo)]
        public string ChaveConsulta { get; set; }

        [NotMapped]
        public string ItemFaturavelDescricao { get; set; }

        [NotMapped]
        public int Quantidade { get; set; }

    }
}
