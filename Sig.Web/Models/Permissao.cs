﻿using Sig.Web.Helpers;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sig.Web.Models
{
    [Table(nameof(Permissao))]
    public class Permissao
    {
        [Key]
        public int PermissaoId { get; set; }

        public int ClienteId { get; set; }

        [ForeignKey("ClienteId")]
        public virtual Cliente Cliente { get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        [StringLength(250, ErrorMessage="Máximo permitido 250 caracters.")]
        public string Descricao { get; set; }
    }
}