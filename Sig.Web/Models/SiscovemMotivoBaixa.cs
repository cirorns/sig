﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sig.Web.Models
{
    public class MotivoBaixa
    {
        public string Descricao { get; set; }
        public string Sigla { get; set; }

    }
    public class SiscovemMotivoBaixa
    {
        public static List<MotivoBaixa> MotivoBaixas = new List<MotivoBaixa>
        {
            new MotivoBaixa{ Sigla ="01", Descricao ="01 - PAGAMENTO DA DIVIDA" },
            new MotivoBaixa{ Sigla ="02", Descricao ="02 - RENEGOCIACAO DA DIVIDA" },
            new MotivoBaixa{ Sigla ="03", Descricao ="03 - POR SOLICITACAO DO CLIENTE" },
            new MotivoBaixa{ Sigla ="04", Descricao ="04 - ORDEM JUDICIAL" },
            new MotivoBaixa{ Sigla ="05", Descricao ="05 - CORRECAO DO ENDERECO" },
            new MotivoBaixa{ Sigla ="06", Descricao ="06 - ATUALIZACAO DO VALOR - VALORIZACAO" },
            new MotivoBaixa{ Sigla ="07", Descricao ="07 - ATUALIZACAO DO VALOR - PAGAMENTO PARCIAL" },
            new MotivoBaixa{ Sigla ="08", Descricao ="08 - ATUALIZACAO DA DATA" },
            new MotivoBaixa{ Sigla ="09", Descricao ="09 - CORRECAO DO NOME" },
            new MotivoBaixa{ Sigla ="10", Descricao ="10 - CORRECAO DO NUMERO DO CONTRATO" },
            new MotivoBaixa{ Sigla ="11", Descricao ="11 - CORRECAO DE VARIOS VALORES (VALOR+DATA+ETC)" },
            new MotivoBaixa{ Sigla ="12", Descricao ="12 - BAIXA POR PERDA DE CONTROLE DA BASE" },
            new MotivoBaixa{ Sigla ="13", Descricao ="13 - MOTIVO NAO IDENTIFICADO" },
            new MotivoBaixa{ Sigla ="14", Descricao ="14 - PONTUALIZACAO DA DIVIDA" },
            new MotivoBaixa{ Sigla ="15", Descricao ="15 - BAIXA POR CONCESSAO DE CREDITO" },
            new MotivoBaixa{ Sigla ="16", Descricao ="16 - INCORPORACAO/MUDANCA DE TITULARIDADE" },
            new MotivoBaixa{ Sigla ="17", Descricao ="17 - COMUNICADO DEVOLVIDO DO CORREIO" },
            new MotivoBaixa{ Sigla ="18", Descricao ="18 - CORRECAO DE DADOS DO COOBRIGADO/AVALISTA" },
            new MotivoBaixa{ Sigla ="19", Descricao ="19 - RENEGOCIACAO DA DIVIDA POR ACORDO" },
            new MotivoBaixa{ Sigla ="20", Descricao ="20 - PAGAMENTO DA DIVIDA POR DEPOSITO BANCARIO" },
            new MotivoBaixa{ Sigla ="21", Descricao ="21 - ANALISE DE DOCUMENTOS" },
            new MotivoBaixa{ Sigla ="22", Descricao ="22 - CORRECAO DE DADOS PELA LOJA/FILIAL" },
            new MotivoBaixa{ Sigla ="23", Descricao ="23 - PGTO DA DIVIDA POR EMISSAO DE NOTA PROMISSORIA" },
            new MotivoBaixa{ Sigla ="24", Descricao ="24 - ANALISE DE DOCUMENTO PELO SEGURO" },
            new MotivoBaixa{ Sigla ="25", Descricao ="25 - DEVOLUCAO OU TROCA DE BEM FINANCIADO" },
            
        };
    }
}