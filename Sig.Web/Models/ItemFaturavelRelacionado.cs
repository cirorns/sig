﻿using Sig.Web.Helpers;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sig.Web.Models
{
    [Table(nameof(ItemFaturavelRelacionado))]
    public class ItemFaturavelRelacionado
    {
        [Key]
        public int ItemFaturavelRelacionadoId { get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        public int ItemFaturavelId { get; set; }

        [ForeignKey("ItemFaturavelId")]
        public virtual ItemFaturavel ItemFaturavel { get; set; }

        [StringLength(100, ErrorMessage = "O campo não pode exceder 100 caracteres.")]
        [Required(ErrorMessage = Mensagens.Requerido)]
        public string Descricao { get; set; }
    }
}