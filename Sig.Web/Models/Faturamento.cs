﻿using Sig.Web.Helpers;
using Sig.Web.Models.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sig.Web.Models
{
    [Table(nameof(Faturamento))]
    public class Faturamento
    {
        [Key]
        public int FaturamentoId { get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        public int ClienteId { get; set; }

        [ForeignKey(nameof(ClienteId))]
        public virtual Cliente Cliente { get; set; }

        public eTipoPagamento? TipoPagamento { get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        public DateTime DataEmissao { get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        public DateTime DataVencimento { get; set; }

        public DateTime? DataPagamento { get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        public decimal ValorOriginal { get; set; }

        public decimal? ValorPago { get; set; }

        public decimal? ValorDesconto { get; set; }

        public decimal? ValorJuros { get; set; }

        public decimal ValorCusto { get; set; }

        public decimal? ValorRepasseAgencia { get; set; }

        public DateTime? DataInicialFaturamento { get; set; }

        public DateTime? DataFinalFaturamento { get; set; }
        
        public int? Tid { get; set; }

        [StringLength(250, ErrorMessage = Mensagens.Maximo)]
        public string LinhaDigitavel { get; set; }

        [StringLength(250, ErrorMessage = Mensagens.Maximo)]
        public string LinkBoleto { get; set; }
        public decimal? ValorDespesa { get; set; }
        public bool IsEnviar { get; set; }


        public virtual ICollection<FaturamentoItem> FaturamentoItens { get; set; }
        

        public Faturamento()
        {
            FaturamentoItens = new List<FaturamentoItem>();
        }

    }
}