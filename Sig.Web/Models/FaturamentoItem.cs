﻿using Sig.Web.Helpers;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sig.Web.Models
{
    [Table(nameof(FaturamentoItem))]
    public class FaturamentoItem
    {
        [Key, Column(Order = 0)]
        [Required(ErrorMessage = Mensagens.Requerido)]
        public int FaturamentoId { get; set; }

        [ForeignKey(nameof(FaturamentoId))]
        public virtual Faturamento Faturamento { get; set; }

        [Key, Column(Order = 1)]
        [Required(ErrorMessage = Mensagens.Requerido)]
        public int ItemFaturavelId { get; set; }

        [ForeignKey(nameof(ItemFaturavelId))]
        public virtual ItemFaturavel ItemFaturavel { get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        public int Quantidade { get; set; }

        public decimal? CustoUnitario { get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        public decimal ValorUnitario { get; set; }

        public decimal? RepasseAgencia { get; set; }
    }
}