﻿using Sig.Web.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Sig.Web.Models
{
    public class ImportarCliente
    {
        //Cliente
        public int AgenciaId { get; set; }
        public int AtividadeId { get; set; }
        public int RegiaoId { get; set; }        
        public bool IsAtivo { get; set; }
        public string CPFCNPJ { get; set; }
        public string InscricaoEstadual { get; set; }
        public string InscricaoMunicipal { get; set; }
        public string Nome { get; set; }
        public string RazaoSocial { get; set; }
        public string Email { get; set; }
        public string Website { get; set; }

        //Endereco
        public string Logradouro { get; set; }
        public string Complemento { get; set; }
        public string Bairro { get; set; }
        public string Cidade { get; set; }
        public string CEP { get; set; }
        public string UF { get; set; }

        //Telefone
        public string DDD { get; set; }
        public string Numero { get; set; }


        [Required(ErrorMessage = Mensagens.Requerido)]
        public string Arquivo { get; set; }

        //Serasa
        public string Logon1 { get; set; }
        public string Logon2 { get; set; }
        public string LogonNegativacao { get; set; }

        public string Contato { get; set; }
        public string CPF { get; set; }


    }
}