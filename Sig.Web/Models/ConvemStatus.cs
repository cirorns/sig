﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sig.Web.Models
{
    public class Posicao
    {
        public string Sigla { get; set; }
        public string Descricao { get; set; }
    }

    public class ConvemStatus
    {
        public static List<Posicao> Posicoes = new()
        {
            new Posicao{ Descricao = "Nenhum", Sigla = "" },
            new Posicao{ Descricao = "Rejeitado", Sigla = "R" },
            new Posicao{ Descricao = "Confirmado", Sigla = "C" }
        };
    }
}