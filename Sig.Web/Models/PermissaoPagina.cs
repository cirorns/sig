﻿using Sig.Web.Helpers;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sig.Web.Models
{
    [Table(nameof(PermissaoPagina))]
    public class PermissaoPagina
    {
        [Key, Column(Order = 0)]
        [Required(ErrorMessage = Mensagens.Requerido)]
        public int PermissaoId { get; set; }

        [ForeignKey("PermissaoId")]
        public virtual Permissao Permissao { get; set; }

        [Key, Column(Order = 1)]
        [Required(ErrorMessage = Mensagens.Requerido)]
        public int PaginaId { get; set; }

        [ForeignKey("PaginaId")]
        public virtual Pagina Pagina { get; set; }
        
    }
}