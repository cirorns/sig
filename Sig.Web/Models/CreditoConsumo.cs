﻿using Sig.Web.Helpers;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sig.Web.Models
{
    [Table(nameof(CreditoConsumo))]
    public class CreditoConsumo
    {
        [Key]
        public int CreditoConsumoId { get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        public int ClienteId { get; set; }

        [ForeignKey("ClienteId")]
        public virtual Cliente Cliente { get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        public decimal Valor { get; set; }
    }
}