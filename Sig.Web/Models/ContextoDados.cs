﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Sig.Web.Models
{
    public class ContextoDados : DbContext
    {
        private static readonly ILoggerFactory _loggerFactory = LoggerFactory.Create(builder => { builder.AddConsole(); });

        public DbSet<Telefone> Telefones { get; set; }
        public DbSet<Atividade> Atividades { get; set; }
        public DbSet<Endereco> Enderecos { get; set; }
        public DbSet<Cliente> Clientes { get; set; }
        public DbSet<ItemFaturavel> ItemFaturaveis { get; set; }
        public DbSet<Tabela> Tabelas { get; set; }
        public DbSet<TabelaItem> TabelaItens { get; set; }
        public DbSet<DiaCobranca> DiaCobrancas { get; set; }
        public DbSet<Colaborador> Colaboradores { get; set; }
        public DbSet<Contato> Contatos { get; set; }
        public DbSet<Contrato> Contratos { get; set; }
        public DbSet<Serasa> Serasas { get; set; }
        public DbSet<Regiao> Regioes { get; set; }
        public DbSet<Usuario> Usuarios { get; set; }
        public DbSet<UsuarioConsulta> UsuarioConsultas { get; set; }
        public DbSet<TabelaAgencia> TabelaAgencias { get; set; }
        public DbSet<Faturamento> Faturamentos { get; set; }
        public DbSet<FaturamentoItem> FaturamentoItems { get; set; }
        public DbSet<DocumentoPendente> DocumentoPendentes { get; set; }
        public DbSet<ItemFaturavelRelacionado> ItemFaturavelRelacionados { get; set; }
        public DbSet<Assunto> Assuntos { get; set; }
        public DbSet<Atendimento> Atendimentos { get; set; }
        public DbSet<BoletoAvulso> BoletosAvulsos { get; set; }
        public DbSet<TransacaoConsulta> TransacoesConsultas { get; set; }
        public DbSet<Cargo> Cargos { get; set; }
        public DbSet<Configuracao> Configuracoes { get; set; }
        public DbSet<MultLogon> MultLogons { get; set; }
        public DbSet<Credito> Creditos { get; set; }
        public DbSet<PedidoCredito> PedidoCreditos { get; set; }
        public DbSet<CreditoConsumo> CreditoConsumos { get; set; }
        public DbSet<ConvemCredor> ConvemCredores { get; set; }
        public DbSet<ConvemInclusaoAnotacao> ConvemInclusaoAnotacoes { get; set; }
        public DbSet<ConvemExclusaoAnotacao> ConvemExclusaoAnotacoes { get; set; }
        public DbSet<Siscovem> Siscovems { get; set; }
        public DbSet<Pagina> Paginas { get; set; }
        public DbSet<PermissaoPagina> PermissoesPaginas { get; set; }
        public DbSet<Permissao> Permissoes { get; set; }
        public DbSet<Revenda> Revendas { get; set; }
        public DbSet<RevendaItem> RevendaItens { get; set; }
        public DbSet<TabelaRevenda> TabelasRevendas { get; set; }
        public DbSet<PagarMe> PagarMes { get; set; }
        public DbSet<ConfigSendGrid> ConfigSendGrids { get; set; }
        public DbSet<PermissaoConsulta> PermissaoConsultas { get; set; }
        public DbSet<Consulta> Consultas { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (optionsBuilder.IsConfigured) return;
            optionsBuilder.UseLoggerFactory(_loggerFactory);

            optionsBuilder.UseNpgsql("Server=localhost;Port=5432;User Id=postgres;Password=a1228ds;Database=sig;Pooling=true;")
                .UseLazyLoadingProxies();

            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            
            #region Relacionamentos em Cascata

            modelBuilder.Entity<Contrato>()
                        .HasOne(t => t.Vendedor)
                        .WithMany()
                        .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<Regiao>()
                        .HasOne(t => t.Agencia)
                        .WithMany()
                        .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<Cargo>()
                        .HasOne(t => t.Agencia)
                        .WithMany()
                        .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<DocumentoPendente>()
                        .HasOne(t => t.Usuario)
                        .WithMany()
                        .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<TransacaoConsulta>()
                        .HasOne(t => t.UsuarioConsulta)
                        .WithMany()
                        .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<Regiao>()
                        .HasOne(t => t.Agencia)
                        .WithMany()
                        .HasForeignKey(t => t.AgenciaId)
                        .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<Atendimento>()
                        .HasOne(t => t.Colaborador)
                        .WithMany()
                        .OnDelete(DeleteBehavior.NoAction);


            //Chaves compostas

            modelBuilder.Entity<Consulta>()
                    .HasKey(t => new { t.AgenciaId, t.ItemFaturavelId });

            modelBuilder.Entity<FaturamentoItem>()
                    .HasKey(t => new { t.FaturamentoId, t.ItemFaturavelId });

            modelBuilder.Entity<PermissaoConsulta>()
                    .HasKey(t => new { t.UsuarioConsultaId, t.ItemFaturavelId });

            modelBuilder.Entity<PermissaoPagina>()
                   .HasKey(t => new { t.PermissaoId, t.PaginaId });

            modelBuilder.Entity<TabelaAgencia>()
                   .HasKey(t => new { t.AgenciaId, t.TabelaId });

            modelBuilder.Entity<TabelaItem>()
                   .HasKey(t => new { t.TabelaId, t.ItemFaturavelId });

            modelBuilder.Entity<TabelaRevenda>()
                   .HasKey(t => new { t.ClienteId, t.TabelaId });

            #endregion
        }

    }
}