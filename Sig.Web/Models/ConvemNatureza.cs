﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sig.Web.Models
{
    public class Natureza
    {
        public string Sigla { get; set; }
        public string Descricao { get; set; }
    }

    public class ConvemNatureza
    {
        public static List<Natureza> Naturezas = new List<Natureza>
        {
            new Natureza{ Descricao = "Adiantamento a Depositantes - Conta Corrente Devedora", Sigla = "AD" },
            new Natureza{ Descricao = "Empréstimo/Financiamento Agricola/Rural-Comercial, Industrial e Custeio", Sigla = "AG" },
            new Natureza{ Descricao = "Aluguel", Sigla = "AL" },
            new Natureza{ Descricao = "Arrendamento, Inclusive Leasing", Sigla = "AR" },
            new Natureza{ Descricao = "Consórcio Imóvel", Sigla = "C1" },
            new Natureza{ Descricao = "Consórcio Veículos Pesados", Sigla = "C2" },
            new Natureza{ Descricao = "Consórcio Veículos", Sigla = "C3" },
            new Natureza{ Descricao = "Consórcio Motocicletas e Motonetas", Sigla = "C4" },
            new Natureza{ Descricao = "Consórcio Outros Bens Móveis", Sigla = "C5" },
            new Natureza{ Descricao = "Consórcio Passagens Aéreas", Sigla = "C6" },
            new Natureza{ Descricao = "Operações de Financiamento de Câmbio em Geral ", Sigla = "CA" },
            new Natureza{ Descricao = "Cota Condominial", Sigla = "CC" },
            new Natureza{ Descricao = "Crédito Direto - Crediário", Sigla = "CD" },
            new Natureza{ Descricao = "Consórcio Contemplado", Sigla = "CO" },
            new Natureza{ Descricao = "Crédito Pessoal", Sigla = "CP" },
            new Natureza{ Descricao = "Impedidos de Crédito Rural pelo Bacen", Sigla = "CR" },
            new Natureza{ Descricao = "Operações com Cartão de Crédito", Sigla = "CT" },
            new Natureza{ Descricao = "Dívidas Originárias de Cheque", Sigla = "DC" },
            new Natureza{ Descricao = "Duplicata", Sigla = "DP" },
            new Natureza{ Descricao = "Empréstimos em Conta, Conta Corrente Garantida, Capital de Giro, Programa Especial", Sigla = "EC" },
            new Natureza{ Descricao = "Energia Elétrica - Faturas de Fornecimento de Luz", Sigla = "EE" },
            new Natureza{ Descricao = "Empréstimo Consignado", Sigla = "EG" },
            new Natureza{ Descricao = "Gás - Faturas de Fornecimento de Gás", Sigla = "FG" },
            new Natureza{ Descricao = "Créditos e Financiamentos", Sigla = "FI" },
            new Natureza{ Descricao = "Despesas com Hospitais", Sigla = "HO" },
            new Natureza{ Descricao = "Instituição de Ensino - Bolsa Restituível", Sigla = "IE" },
            new Natureza{ Descricao = "Operações Imobiliárias", Sigla = "IM" },
            new Natureza{ Descricao = "Mensalidades Escolares", Sigla = "ME" },
            new Natureza{ Descricao = "Notas Fiscais", Sigla = "NF" },
            new Natureza{ Descricao = "Operações Agrícolas - Negociações Produtos Rurais, Insumos, Sementes, etc.", Sigla = "OA" },
            new Natureza{ Descricao = "Operações Ajuizadas", Sigla = "OJ" },
            new Natureza{ Descricao = "Outras Operações", Sigla = "OO" },
            new Natureza{ Descricao = "Operações de Repasse, Finame, 63, Recon etc.", Sigla = "RE" },
            new Natureza{ Descricao = "Arrecadadores - Prestadores de Serviço", Sigla = "RR" },
            new Natureza{ Descricao = "Saneamento Básico - Faturas de Fornecimento de Água", Sigla = "SB" },
            new Natureza{ Descricao = "Seguro Fiança Locatícia", Sigla = "SF" },
            new Natureza{ Descricao = "Seguro Garantia", Sigla = "SG" },
            new Natureza{ Descricao = "Seguro Quebra de Garantia", Sigla = "SQ" },
            new Natureza{ Descricao = "Seguro de Risco Decorrido", Sigla = "SR" },
            new Natureza{ Descricao = "Mensalidade de Plano/Seguro de Saúde", Sigla = "SS" },
            new Natureza{ Descricao = "Termo de Confissão de Dívida", Sigla = "TC" },
            new Natureza{ Descricao = "Títulos Descontados, Duplicatas, Promissórias etc.", Sigla = "TD" },
            new Natureza{ Descricao = "Serviços de Telefonia Móvel Pessoal/Celular – Outras Cntas Após Rescisão do Contrato.", Sigla = "TE" },
            new Natureza{ Descricao = "Serviços de Telefonia Fixa – Outras Contas Após Rescisão do Contrato ", Sigla = "TF" },
            new Natureza{ Descricao = "Serviços de Dados e Internet (Speedy, Aluguel de Canais de Dados e Fibra Ótica)", Sigla = "TI" },
            new Natureza{ Descricao = "Serviços de Telefonia Móvel Pessoal/Celular – Res221/00", Sigla = "TM" },
            new Natureza{ Descricao = "Serviços de Telefonia/Vendas de Produtos  – Vendas de Aparelhos, Instalação, Aluguel", Sigla = "TP" },
            new Natureza{ Descricao = "Serviços de Telefonia – Renegociação da Dívida", Sigla = "TR" },
            new Natureza{ Descricao = "Serviços de Telefonia Fixa – Res 085/98", Sigla = "TT" },
            new Natureza{ Descricao = "Prestação de Serviços - Vendas de Mercadorias", Sigla = "VM" },
        };
    }
}