﻿using System.ComponentModel;

namespace Sig.Web.Models.Enums
{
    public enum eTipoPagamento
    {
        [Description("Pagamento em Banco")]
        PagamentoEmBanco = 1,

        [Description("Baixa Manual Agência")]
        BaixaManualAgencia = 2,

        [Description("Baixa Manual Administradora")]
        BaixaManualAdministradora = 3,

        [Description("Depósito em Conta Agência")]
        DepositoEmContaAgencia = 4,

        [Description("Depósito em Conta Administradora")]
        DepositoEmContaAdministradora = 5,
    }
}