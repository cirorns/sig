﻿using System.ComponentModel;

namespace Sig.Web.Models.Enums
{
    public enum eTipoTelefone
    {
        Celular = 1,

        [Description("Telefone Residencial")]
        TelefoneResidencial = 2,

        [Description("Telefone Comercial")]
        TelefoneComercial = 3,

        Fax = 4
    }
}