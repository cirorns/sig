﻿using System.ComponentModel;

namespace Sig.Web.Models.Enums
{
    public enum eTipoCliente
    {
        Administradora = 1,
        [Description("Agência")]
        Agencia = 2,
        Cliente = 3,
        Colaborador = 4,
        [Description("Pré-Pago")]
        PrePago = 5,
        Prospect = 6,
        Revenda = 7
    }
}