﻿namespace Sig.Web.Models.Enums
{
    public enum eTipoEndereco
    {
        Comercial = 1,
        Residencial = 2,
        Outros = 3
    }
}