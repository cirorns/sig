﻿using System.ComponentModel;

namespace Sig.Web.Models.Enums
{
    public enum eMeio
    {
        Internet = 1,
        [Description("Balcão")]
        Balcao = 2,
        Local = 3,
        Telefone = 4
    }
}