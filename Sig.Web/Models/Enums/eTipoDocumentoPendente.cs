﻿namespace Sig.Web.Models.Enums
{
    public enum eTipoDocumentoPendente
    {
        Contrato = 1,
        Xerox = 2,
        Foto = 3,
    }
}