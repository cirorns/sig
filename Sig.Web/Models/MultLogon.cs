﻿using Sig.Web.Helpers;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sig.Web.Models
{
    [Table(nameof(MultLogon))]
    public class MultLogon
    {
        [Key]
        public int MultLogonId{ get; set; }

        public int? ClienteId { get; set; }

        [ForeignKey(nameof(ClienteId))]
        public virtual Cliente Cliente { get; set; }

        [StringLength(10, ErrorMessage = "O campo não pode exceder 10 caracteres.")]
        [Required(ErrorMessage = Mensagens.Requerido)]
        public string Logon { get; set; }

        [StringLength(10, ErrorMessage = "O campo não pode exceder 10 caracteres.")]
        [Required(ErrorMessage = Mensagens.Requerido)]
        public string Senha { get; set; }

        public bool IsAtivo { get; set; }
    }
}