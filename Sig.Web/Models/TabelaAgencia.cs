﻿using Sig.Web.Helpers;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sig.Web.Models
{
    [Table(nameof(TabelaAgencia))]
    public class TabelaAgencia
    {
        [Key, Column(Order = 0)]
        [Required(ErrorMessage = Mensagens.Requerido)]
        public int AgenciaId { get; set; }

        [ForeignKey(nameof(AgenciaId))]
        public virtual Cliente Agencia { get; set; }

        [Key, Column(Order = 1)]
        [Required(ErrorMessage = Mensagens.Requerido)]
        public int TabelaId { get; set; }

        [ForeignKey(nameof(TabelaId))]
        public virtual Tabela Tabela { get; set; }
    }
}