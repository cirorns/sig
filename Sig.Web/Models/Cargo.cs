﻿using Sig.Web.Helpers;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sig.Web.Models
{

    [Table(nameof(Cargo))]
    public class Cargo
    {
        [Key]
        public int CargoId { get; set; }

        public int AgenciaId { get; set; }

        [ForeignKey(nameof(AgenciaId))]
        public virtual Cliente Agencia { get; set; }

        [StringLength(250, ErrorMessage = Mensagens.Maximo)]
        [Required(ErrorMessage = Mensagens.Requerido)]
        public string Descricao { get; set; }
        
    }
}
