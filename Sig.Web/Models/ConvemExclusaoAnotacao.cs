﻿using Sig.Web.Helpers;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sig.Web.Models
{
    [Table(nameof(ConvemExclusaoAnotacao))]
    public class ConvemExclusaoAnotacao
    {
        [Key]
        public int ConvemExclusaoAnotacaoId { get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        public int ConvemInclusaoAnotacaoId { get; set; }

        [ForeignKey("ConvemInclusaoAnotacaoId")]
        public virtual ConvemInclusaoAnotacao ConvemInclusaoAnotacao { get; set; }

        [Required(ErrorMessage="O Campo é requerido")]
        [StringLength(2, ErrorMessage = "O campo não pode exceder 2 caracteres.")]
        public string MotivoBaixa { get; set; }

        //Número da Remessa Fins de Relatório
        public int? NumeroRemessa { get; set; }

        //Data de Confirmação da Remessa
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? DataAlteracao { get; set; }

        public string Status { get; set; }

        //Aplicar na geração do arquivo.
        [NotMapped]
        public bool IsAplicar { get; set; }
    }
}