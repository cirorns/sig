﻿using Sig.Web.Helpers;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sig.Web.Models
{
    [Table(nameof(Contrato))]
    public class Contrato
    {
        [Key]
        public int ContratoId { get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        public int ClienteId { get; set; }

        [ForeignKey(nameof(ClienteId))]
        public virtual Cliente Cliente { get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        public int VendedorId { get; set; }

        [ForeignKey(nameof(VendedorId))]
        public virtual Colaborador Vendedor { get; set; }
        
        [Required(ErrorMessage = Mensagens.Requerido)]
        public int TabelaId { get; set; }

        [ForeignKey(nameof(TabelaId))]
        public virtual Tabela Tabela { get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        public int DiaCobrancaId { get; set; }

        [ForeignKey(nameof(DiaCobrancaId))]
        public virtual DiaCobranca DiaCobranca { get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        public DateTime DataInicial{ get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        public DateTime DataFinal { get; set; }

        public DateTime? DataCancelamento { get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        public DateTime DataCadastro { get; set; }

        //Caso seja Revenda
        public decimal? Comissao { get; set; }

        public bool IsRenovacaoAutomatica { get; set; }

        public bool IsTaxaExtra { get; set; }

        public bool IsAniversario { get; set; }

        public bool IsEnviaEmail { get; set; }

        public bool IsEnviaSms { get; set; }

        public bool IsDespesaBancaria { get; set; }


    }
}