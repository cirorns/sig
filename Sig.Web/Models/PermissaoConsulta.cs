﻿using Sig.Web.Helpers;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sig.Web.Models
{
    [Table(nameof(PermissaoConsulta))]
    public class PermissaoConsulta
    {
        [Key, Column(Order = 0)]
        [Required(ErrorMessage = Mensagens.Requerido)]
        public int UsuarioConsultaId { get; set; }

        [ForeignKey(nameof(UsuarioConsultaId))]
        public virtual UsuarioConsulta UsuarioConsulta { get; set; }

        [Key, Column(Order = 1)]
        [Required(ErrorMessage = Mensagens.Requerido)]
        public int ItemFaturavelId { get; set; }

        [ForeignKey(nameof(ItemFaturavelId))]
        public virtual ItemFaturavel ItemFaturavel { get; set; }

        public bool IsPermitido { get; set; }
    }
}