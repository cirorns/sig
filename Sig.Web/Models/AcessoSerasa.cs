﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Sig.Web.Models
{
    [Table(nameof(AcessoSerasa))]
    public class AcessoSerasa
    {
        public string Logon { get; set; }
        public string Senha { get; set; }
    }
}