﻿using Sig.Web.Helpers;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sig.Web.Models
{
    [Table(nameof(TabelaItem))]
    public class TabelaItem
    {
        [Key, Column(Order = 0)]
        [Required(ErrorMessage = Mensagens.Requerido)]
        public int TabelaId { get; set; }

        [ForeignKey(nameof(TabelaId))]
        public virtual Tabela Tabela { get; set; }

        [Key, Column(Order = 1)]
        [Required(ErrorMessage = Mensagens.Requerido)]
        public int ItemFaturavelId { get; set; }

        [ForeignKey(nameof(ItemFaturavelId))]
        public virtual ItemFaturavel ItemFaturavel { get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        public decimal ValorUnitario { get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        public int QuantidadePacote { get; set; }
    }
}
