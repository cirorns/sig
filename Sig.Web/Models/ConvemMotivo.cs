﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sig.Web.Models
{
    public class Motivo
    {
        public string Sigla { get; set; }
        public string Descricao { get; set; }
    }

    public class ConvemMotivo
    {
        public static List<Motivo> Motivos = new List<Motivo>
        {
            new Motivo { Descricao = "Pagamento da dívida", Sigla = "01" },
            new Motivo { Descricao = "Renegociação da dívida", Sigla = "02" },
            new Motivo { Descricao = "Por solicitação do cliente", Sigla = "03" },
            new Motivo { Descricao = "Ordem judicial", Sigla = "04" },
            new Motivo { Descricao = "Correção de endereço", Sigla = "05" },
            new Motivo { Descricao = "Atualização do valor – valorização", Sigla = "06" },
            new Motivo { Descricao = "Atualização do valor–pagamento parcial", Sigla = "07" },
            new Motivo { Descricao = "Atualização de data", Sigla = "08" },
            new Motivo { Descricao = "Correção do nome", Sigla = "09" },
            new Motivo { Descricao = "Correção do número do contrato", Sigla = "10" },
            new Motivo { Descricao = "Correção de varios dados (valor+datas+etc)", Sigla = "11" },
            new Motivo { Descricao = "Baixa por perda de controle de base", Sigla = "12" },
            new Motivo { Descricao = "Motivo não identificado", Sigla = "13" },
            new Motivo { Descricao = "Pontualização da dívida", Sigla = "14" },
            new Motivo { Descricao = "Baixa por concessão de crédito", Sigla = "15" },
            new Motivo { Descricao = "Incorporação / mudança de titularidade", Sigla = "16" },
            new Motivo { Descricao = "Comunicado devolvido dos correios", Sigla = "17" },
            new Motivo { Descricao = "Correção de dados do coobrigado / avalista.", Sigla = "18" },
            new Motivo { Descricao = "Renegociação da dívida por acordo.", Sigla = "19" },
            new Motivo { Descricao = "Pagamento da dívida por pagamento bancário.", Sigla = "20" },
            new Motivo { Descricao = "Analise de documentos.", Sigla = "21" },
            new Motivo { Descricao = "Correção de dados pela loja / filial.", Sigla = "22" },
            new Motivo { Descricao = "Pagamento da dívida por emissão de Nota Promissória.", Sigla = "23" },
            new Motivo { Descricao = "Análise de documento por seguro.", Sigla = "24" },
            new Motivo { Descricao = "Devolução ou troca de bem financiado.", Sigla = "25" },
        };
    }
}