﻿namespace Sig.Web.Interfaces
{
    public interface IUsuarioLogado
    {
        string ObterNomeCliente();
        string ObterNomeClienteTruncado();
        string ObterNomeColaborador();
        int ObterClienteId();
        int ObterAgenciaId();
        int ObterUsuarioId();
        int ObterColaboradorId();
        int ObterContratoId();
        bool EhSuperUsuario();
        bool EhAdministradora();
        string ObterPagina();

    }
}
