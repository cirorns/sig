﻿using Microsoft.AspNetCore.Http;
using System;
using System.ComponentModel;
using System.Text;
using System.Text.RegularExpressions;

namespace Sig.Web.Helpers
{
    public static class Util
    {
        private const string CARACTERES_PARA_SENHA = "abcdefghijklmnopqrstuvwxyz1234567890@";

        public static string SomenteNumeros(this string texto)
        {
            Regex reg = new(@"[^0-9]");
            string retorno = reg.Replace(texto, string.Empty)?.Trim();
            return retorno;
        }

        public static bool CnpjValido(this string cnpj)
        {
            cnpj = cnpj.SomenteNumeros();

            int[] multiplicador1 = new int[12] { 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2 };

            int[] multiplicador2 = new int[13] { 6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2 };

            int soma;
            int resto;
            string digito;
            string tempCnpj;

            cnpj = cnpj.Trim();
            cnpj = cnpj.Replace(".", "").Replace("-", "").Replace("/", "");

            if (cnpj.Length != 14)
                return false;

            tempCnpj = cnpj.Substring(0, 12);

            soma = 0;

            for (int i = 0; i < 12; i++)
                soma += int.Parse(tempCnpj[i].ToString()) * multiplicador1[i];

            resto = (soma % 11);

            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;

            digito = resto.ToString();
            tempCnpj += digito;

            soma = 0;

            for (int i = 0; i < 13; i++)
                soma += int.Parse(tempCnpj[i].ToString()) * multiplicador2[i];

            resto = (soma % 11);

            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;

            digito += resto.ToString();

            return cnpj.EndsWith(digito);
        }

        public static bool CpfValido(this string cpf)
        {
            cpf = cpf.SomenteNumeros();

            if (cpf.Length > 11)
                return false;

            while (cpf.Length != 11)
                cpf = '0' + cpf;

            bool igual = true;
            for (int i = 1; i < 11 && igual; i++)
                if (cpf[i] != cpf[0])
                    igual = false;

            if (igual || cpf == "12345678909")
                return false;

            int[] numeros = new int[11];

            for (int i = 0; i < 11; i++)
                numeros[i] = int.Parse(cpf[i].ToString());

            int soma = 0;
            for (int i = 0; i < 9; i++)
                soma += (10 - i) * numeros[i];

            int resultado = soma % 11;

            if (resultado == 1 || resultado == 0)
            {
                if (numeros[9] != 0)
                    return false;
            }
            else if (numeros[9] != 11 - resultado)
                return false;

            soma = 0;
            for (int i = 0; i < 10; i++)
                soma += (11 - i) * numeros[i];

            resultado = soma % 11;

            if (resultado == 1 || resultado == 0)
            {
                if (numeros[10] != 0)
                    return false;
            }
            else
                if (numeros[10] != 11 - resultado)
                return false;

            return true;
        }

        public static bool CpfOuCnpjValido(this string cpfCnpj)
        {
            cpfCnpj = cpfCnpj.SomenteNumeros();

            if (cpfCnpj.Length == 11)
                return cpfCnpj.CpfValido();

            return cpfCnpj.CnpjValido();
        }

        public static string CortaTexto(this string texto, int quantidade)
        {
            string retorno = texto;

            if (texto.Length > quantidade)
                retorno = texto.Substring(0, quantidade);

            return retorno;
        }

        public static string RemoveAcentos(this string texto)
        {
            string textoNovo = string.Empty;

            for (int i = 0; i < texto.Length; i++)
            {
                if (texto[i].ToString() == "ã") textoNovo += "a";
                else if (texto[i].ToString() == "á") textoNovo += "a";
                else if (texto[i].ToString() == "à") textoNovo += "a";
                else if (texto[i].ToString() == "â") textoNovo += "a";
                else if (texto[i].ToString() == "ä") textoNovo += "a";
                else if (texto[i].ToString() == "é") textoNovo += "e";
                else if (texto[i].ToString() == "è") textoNovo += "e";
                else if (texto[i].ToString() == "ê") textoNovo += "e";
                else if (texto[i].ToString() == "ë") textoNovo += "e";
                else if (texto[i].ToString() == "í") textoNovo += "i";
                else if (texto[i].ToString() == "ì") textoNovo += "i";
                else if (texto[i].ToString() == "ï") textoNovo += "i";
                else if (texto[i].ToString() == "õ") textoNovo += "o";
                else if (texto[i].ToString() == "ó") textoNovo += "o";
                else if (texto[i].ToString() == "ò") textoNovo += "o";
                else if (texto[i].ToString() == "ö") textoNovo += "o";
                else if (texto[i].ToString() == "ú") textoNovo += "u";
                else if (texto[i].ToString() == "ù") textoNovo += "u";
                else if (texto[i].ToString() == "ü") textoNovo += "u";
                else if (texto[i].ToString() == "ç") textoNovo += "c";
                else if (texto[i].ToString() == "Ã") textoNovo += "A";
                else if (texto[i].ToString() == "Á") textoNovo += "A";
                else if (texto[i].ToString() == "À") textoNovo += "A";
                else if (texto[i].ToString() == "Â") textoNovo += "A";
                else if (texto[i].ToString() == "Ä") textoNovo += "A";
                else if (texto[i].ToString() == "É") textoNovo += "E";
                else if (texto[i].ToString() == "È") textoNovo += "E";
                else if (texto[i].ToString() == "Ê") textoNovo += "E";
                else if (texto[i].ToString() == "Ë") textoNovo += "E";
                else if (texto[i].ToString() == "Í") textoNovo += "I";
                else if (texto[i].ToString() == "Ì") textoNovo += "I";
                else if (texto[i].ToString() == "Ï") textoNovo += "I";
                else if (texto[i].ToString() == "Õ") textoNovo += "O";
                else if (texto[i].ToString() == "Ó") textoNovo += "O";
                else if (texto[i].ToString() == "Ò") textoNovo += "O";
                else if (texto[i].ToString() == "Ö") textoNovo += "O";
                else if (texto[i].ToString() == "Ú") textoNovo += "U";
                else if (texto[i].ToString() == "Ù") textoNovo += "U";
                else if (texto[i].ToString() == "Ü") textoNovo += "U";
                else if (texto[i].ToString() == "Ç") textoNovo += "C";
                else if (texto[i].ToString() == "º") textoNovo += " ";
                else if (texto[i].ToString() == "ª") textoNovo += " ";
                else textoNovo += texto[i];
            }
            return textoNovo;
        }


        public static string ObterSenhaAleatoria(int tamanho)
        {
            var valormaximo = CARACTERES_PARA_SENHA.Length;
            var randomico = new Random(DateTime.Now.Millisecond);
            var senha = new StringBuilder(tamanho);

            for (var indice = 0; indice < tamanho; indice++)
                senha.Append(CARACTERES_PARA_SENHA[randomico.Next(0, (valormaximo - 1))]);

            return senha.ToString();
        }

        
        public static bool EhRequisicaoAjax(this HttpRequest request)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            if (request.Headers != null)
                return request.Headers["X-Requested-With"] == "XMLHttpRequest";
            return false;
        }

        public static string ObterDescricaoDoEnum(this Enum value)
        {
            if (value != null)
            {
                var fi = value.GetType().GetField(value.ToString());

                DescriptionAttribute[] attributes = null;

                if (fi != null)
                {
                    attributes =
                    (DescriptionAttribute[])fi.GetCustomAttributes(
                    typeof(DescriptionAttribute),
                    false);
                }

                return (attributes != null && attributes.Length > 0) ? attributes[0].Description : value.ToString();
            }
            else
            {
                return string.Empty;
            }
        }

        public static string Truncar(this string valor, int tamanhoMaximo)
        {
            if (string.IsNullOrEmpty(valor)) return valor;
            return valor.Length <= tamanhoMaximo ? valor : valor.Substring(0, tamanhoMaximo);
        }
    }
}
