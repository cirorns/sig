﻿using System.ComponentModel.DataAnnotations;

namespace Sig.Web.Helpers.ValidadoresCustomizados
{
    public class CpfValidador : ValidationAttribute
    {
        public override bool IsValid(object value) => value != null ? value.ToString().CpfValido() : false;
    }
}
