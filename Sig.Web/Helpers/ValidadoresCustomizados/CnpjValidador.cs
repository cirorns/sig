﻿using System.ComponentModel.DataAnnotations;

namespace Sig.Web.Helpers.ValidadoresCustomizados
{
    public class CnpjValidador : ValidationAttribute
    {
        public override bool IsValid(object value) => value.ToString().CnpjValido();
    }
}
