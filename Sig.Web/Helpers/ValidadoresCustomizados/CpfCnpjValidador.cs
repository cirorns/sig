﻿using System.ComponentModel.DataAnnotations;

namespace Sig.Web.Helpers.ValidadoresCustomizados
{
    public class CpfCnpjValidador : ValidationAttribute
    {
        public override bool IsValid(object value) => value?.ToString()?.CpfOuCnpjValido() ?? false;
    }
}
