﻿using SendGrid;
using SendGrid.Helpers.Mail;
using Sig.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Sig.Web.Helpers
{
    public class SendGridEmail
    {
        public static bool IsEmail(string email)
        {
            return Regex.IsMatch(email, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);
        }

        public static void EnviarEmailBoleto(string email, string nome, string linha, string link, string itens, string dataReferencia, string dataVencimento)
        {
            if (!string.IsNullOrWhiteSpace(email) && IsEmail(email))
            {
                try
                {
                    ConfigSendGrid configSendGrid = RetornaConfiguracao();

                    if (configSendGrid != null)
                    {
                        var msg = new SendGridMessage();

                        msg.AddTo(email);
                        msg.From = new EmailAddress(configSendGrid.Email, configSendGrid.Nome);

                        if (!string.IsNullOrWhiteSpace(configSendGrid.EmailBcc))
                            msg.AddBcc(configSendGrid.EmailBcc);

                        msg.Subject = $"{configSendGrid.Nome} - Boleto";
                        msg.HtmlContent = $"Prezado(a) Cliente {nome},<br/><br/>Segue o link para a retirada do boleto referente ao consumo realizado em {dataReferencia}.<br/>Desde já, agradecemos a confiança e parceria.<br/><br/>Boleto: {linha}<br/><br/><br/>Link: {link}<br/><br/><br/><b>Demonstrativo das consultas:</b><br/><br/>{itens}";

                        if (!string.IsNullOrWhiteSpace(configSendGrid.TemplateIdPadrao))
                            msg.TemplateId = configSendGrid.TemplateIdPadrao;

                        var credenciaisSendgrid = new NetworkCredential(configSendGrid.Usuario, configSendGrid.Senha);

                        var client = new SendGridClient(configSendGrid.Usuario);

                        client.SendEmailAsync(msg).Wait();
                    }
                    else
                    {
                        throw new Exception("Configuração SendGrid não encontrada.");
                    }

                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        //public static void EnviarEmailRecuperarSenha(string email, string html)
        //{
        //    if (!string.IsNullOrWhiteSpace(email) && IsEmail(email))
        //    {
        //        try
        //        {
        //            ConfigSendGrid configSendGrid = RetornaConfiguracao();

        //            if (configSendGrid != null)
        //            {
        //                var msg = new SendGridMessage();

        //                msg.AddTo(email);
        //                msg.From = new MailAddress(configSendGrid.Email, configSendGrid.Nome);

        //                if (!string.IsNullOrWhiteSpace(configSendGrid.EmailBcc))
        //                    msg.AddBcc(configSendGrid.EmailBcc);

        //                msg.Subject = $"{configSendGrid.Nome} - Recuperar Senha";

        //                msg.Html = html;

        //                if (!string.IsNullOrWhiteSpace(configSendGrid.TemplateIdRecuperarSenha))
        //                    msg.EnableTemplateEngine(configSendGrid.TemplateIdRecuperarSenha);

        //                var credenciaisSendgrid = new NetworkCredential(configSendGrid.Usuario, configSendGrid.Senha);

        //                var transportWeb = new SendGrid.Web(credenciaisSendgrid);

        //                transportWeb.DeliverAsync(msg);

        //            }
        //            else
        //            {
        //                throw new Exception("Configuração SendGrid não encontrada.");
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            throw ex;
        //        }
        //    }
        //}

        //public static void EnviarEmailMensagemPadrao(string email, string html)
        //{
        //    if (!string.IsNullOrWhiteSpace(email) && IsEmail(email))
        //    {
        //        try
        //        {
        //            ConfigSendGrid configSendGrid = RetornaConfiguracao();

        //            if (configSendGrid != null)
        //            {
        //                var msg = new SendGridMessage();

        //                msg.AddTo(email);
        //                msg.From = new MailAddress(configSendGrid.Email, configSendGrid.Nome);

        //                if (!string.IsNullOrWhiteSpace(configSendGrid.EmailBcc))
        //                    msg.AddBcc(configSendGrid.EmailBcc);

        //                msg.Subject = $"{configSendGrid.Nome} - Mensagens";

        //                msg.Html = html;

        //                if (!string.IsNullOrWhiteSpace(configSendGrid.TemplateIdPadrao))
        //                    msg.EnableTemplateEngine(configSendGrid.TemplateIdPadrao);

        //                var credenciaisSendgrid = new NetworkCredential(configSendGrid.Usuario, configSendGrid.Senha);

        //                var transportWeb = new SendGrid.Web(credenciaisSendgrid);

        //                transportWeb.DeliverAsync(msg);

        //            }
        //            else
        //            {
        //                throw new Exception("Configuração SendGrid não encontrada.");
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            throw ex;
        //        }
        //    }
        //}

        public static ConfigSendGrid RetornaConfiguracao()
        {
            ConfigSendGrid configSendGrid = null;

            using (var _context = new ContextoDados())
            {
                configSendGrid = _context.ConfigSendGrids.FirstOrDefault();
            }

            return configSendGrid;
        }

    }
}
