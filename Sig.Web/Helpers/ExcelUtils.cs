﻿using OfficeOpenXml;
using System.Collections.Generic;
using System.Linq;

namespace Sig.Web.Helpers
{
    public static class ExcelUtils
    {
        public static string[] CellsToArray(ExcelWorksheet worksheet, int row, int colCount)
        {
            var array = new string[colCount];
            for (int col = 0; col < colCount; col++) array[col] = worksheet.Cells[row, (col + 1)].Value?.ToString();
            return array;
        }

        public static string GetData(string prop, string[] header, string[] data) => header
            .Zip(data, (h, d) => new KeyValuePair<string, string>(h, d))
            .FirstOrDefault(d => d.Key.Equals(prop))
            .Value;
    }
}
