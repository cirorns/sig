﻿using System.Text.Json;

namespace Sig.Web.Helpers
{
    public class Mensagens
    {
        public const string Requerido = "O campo é obrigatório.";
        public const string Email = "Email inválido.";
        public const string Maximo = "O campo ultrapassou o máximo permitido, {1} caracteres.";
        public const string MaximoMinimo = "O campo aceita entre {1} a {2} caracteres.";
        public const string Senha = "O campo senha deverá possuir entre 6 a 8 caracteres.";
        public const string SenhaConfirmacao = "O campo confirmar senha não confere.";
        public const string Imagem = "Extensões permitidas: .png, .jpg, .jpeg, e .gif";
        public const string Incluido = "Registro incluído com sucesso.";
        public const string Alterado = "Registro alterado com sucesso.";
        public const string NaoEncontrado = "Registro não encontrado.";
        public const string EmailExiste = "Este email já está cadastrado para esta empresa.";
        public const string Cpf = "Este CPF não é válido.";
        public const string Cnpj = "Este CNPJ não é válido.";
        public const string CpfCnpj = "Cpf ou Cnpj inválido.";


        public static string OnSuccess(string mensagem, string pagina)
        {
            pagina = JsonSerializer.Serialize(pagina);
            mensagem = JsonSerializer.Serialize(mensagem);

            return $"OnSuccess('{mensagem}', '{pagina}')";
        }

        public static string OnFailure(string titulo, string mensagem)
        {
            titulo = JsonSerializer.Serialize(titulo);
            mensagem = JsonSerializer.Serialize(mensagem);

            return $"OnFailure('{titulo}', '{mensagem}')";
        }
    }
}
