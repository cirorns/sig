﻿using Microsoft.AspNetCore.Mvc;

namespace Sig.Web.Helpers.Extensoes
{
    public class JavaScriptResult : ContentResult
    {
        public JavaScriptResult(string script)
        {
            Content = script;
            ContentType = "application/javascript";
        }
    }
}
