﻿using System.Text.Json;

namespace Sig.Web.Helpers
{
    public class Javascript
    {
        public static string OnSuccess(string mensagem, string pagina)
        {
            pagina = JsonSerializer.Serialize(pagina);
            mensagem = JsonSerializer.Serialize(mensagem);

            return $"OnSuccess('{mensagem}', '{pagina}')";
        }

        public static string OnFailure(string titulo, string mensagem)
        {
            titulo = JsonSerializer.Serialize(titulo);
            mensagem = JsonSerializer.Serialize(mensagem);

            return $"OnFailure('{titulo}', '{mensagem}')";
        }
    }
}
