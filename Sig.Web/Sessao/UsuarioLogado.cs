﻿using Microsoft.AspNetCore.Http;
using Sig.Web.Helpers;
using Sig.Web.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;

namespace Sig.Web.Sessao
{
    public class UsuarioLogado : IUsuarioLogado
    {
        private readonly IHttpContextAccessor _accessor;

        public UsuarioLogado(IHttpContextAccessor acessor)
        {
            _accessor = acessor;
        }

        public string ObterNomeCliente()
        {
            if (EstaAutenticado())
                return ObterDados()[0];

            return string.Empty;
        }

        public string ObterNomeClienteTruncado()
        {
            if (EstaAutenticado())
                return ObterDados()[0].Truncar(15);

            return string.Empty;
        }

        public string ObterNomeColaborador()
        {
            if (EstaAutenticado())
                return ObterDados()[1];

            return string.Empty;
        }

        public int ObterClienteId()
        {
            if (EstaAutenticado())
                return int.Parse(ObterDados()[2]);

            return default;
        }

        public int ObterAgenciaId()
        {
            if (EstaAutenticado())
                return int.Parse(ObterDados()[3]);

            return default;
        }

        public int ObterUsuarioId()
        {
            if (EstaAutenticado())
                return int.Parse(ObterDados()[4]);

            return default;
        }

        public int ObterColaboradorId()
        {
            if (EstaAutenticado())
                return int.Parse(ObterDados()[5]);

            return default;
        }

        public bool EhSuperUsuario()
        {
            if (EstaAutenticado())
                return bool.Parse(ObterDados()[6]);

            return false;
        }

        public int ObterContratoId()
        {
            if (EstaAutenticado())
                return int.Parse(ObterDados()[7]);

            return default;
        }

        public string ObterPagina()
        {
            if (EstaAutenticado())
                return ObterDados()[8];

            return string.Empty;
        }

        public bool EhAdministradora()
        {
            if (EstaAutenticado())
                return ObterPapeis().Contains("Administradora");

            return false;
        }

        public IEnumerable<string> ObterPapeis()
        {
            var identity = _accessor.HttpContext.User.Identity as ClaimsIdentity;
            return identity.Claims.Where(c => c.Type == "http://schemas.microsoft.com/ws/2008/06/identity/claims/role").Select(c => c.Value);
        }

        private string[] ObterDados()
        {
            var identity = _accessor.HttpContext.User.Identity as ClaimsIdentity;
            return identity.Claims.SingleOrDefault(c => c.Type == "Dados")?.Value?.Split(';');
        }

        private bool EstaAutenticado()
        {
            var identidade = _accessor.HttpContext.User.Identity as ClaimsIdentity;
            return identidade.IsAuthenticated;
        }

        
    }
}
