﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace Sig.Web.Migrations
{
    public partial class MigracaoInicial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Assunto",
                columns: table => new
                {
                    AssuntoId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Descricao = table.Column<string>(type: "character varying(250)", maxLength: 250, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Assunto", x => x.AssuntoId);
                });

            migrationBuilder.CreateTable(
                name: "Atividade",
                columns: table => new
                {
                    AtividadeId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Descricao = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Atividade", x => x.AtividadeId);
                });

            migrationBuilder.CreateTable(
                name: "ConfigSendGrid",
                columns: table => new
                {
                    ConfigSendGridId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Email = table.Column<string>(type: "character varying(250)", maxLength: 250, nullable: false),
                    EmailBcc = table.Column<string>(type: "character varying(250)", maxLength: 250, nullable: true),
                    Nome = table.Column<string>(type: "character varying(250)", maxLength: 250, nullable: false),
                    Usuario = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: false),
                    Senha = table.Column<string>(type: "character varying(20)", maxLength: 20, nullable: false),
                    TemplateIdBoleto = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: true),
                    TemplateIdEnvioDeSenha = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: true),
                    TemplateIdRecuperarSenha = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: true),
                    TemplateIdPadrao = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ConfigSendGrid", x => x.ConfigSendGridId);
                });

            migrationBuilder.CreateTable(
                name: "Configuracao",
                columns: table => new
                {
                    ConfiguracaoId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Logomarca = table.Column<string>(type: "character varying(250)", maxLength: 250, nullable: true),
                    Empresa = table.Column<string>(type: "character varying(250)", maxLength: 250, nullable: true),
                    IsMultLogon = table.Column<bool>(type: "boolean", nullable: false),
                    CNPJInformante = table.Column<string>(type: "character varying(15)", maxLength: 15, nullable: true),
                    DDDInformante = table.Column<string>(type: "character varying(4)", maxLength: 4, nullable: true),
                    TelefoneInformante = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: true),
                    NomeContatoInformante = table.Column<string>(type: "character varying(70)", maxLength: 70, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Configuracao", x => x.ConfiguracaoId);
                });

            migrationBuilder.CreateTable(
                name: "Credito",
                columns: table => new
                {
                    CreditoId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Descricao = table.Column<string>(type: "character varying(250)", maxLength: 250, nullable: false),
                    Valor = table.Column<decimal>(type: "numeric", nullable: false),
                    IsAtivo = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Credito", x => x.CreditoId);
                });

            migrationBuilder.CreateTable(
                name: "DiaCobranca",
                columns: table => new
                {
                    DiaCobrancaId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Dia = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DiaCobranca", x => x.DiaCobrancaId);
                });

            migrationBuilder.CreateTable(
                name: "ItemFaturavel",
                columns: table => new
                {
                    ItemFaturavelId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Descricao = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: false),
                    CustoUnitario = table.Column<decimal>(type: "numeric", nullable: false),
                    ValorUnitario = table.Column<decimal>(type: "numeric", nullable: false),
                    ValorMinimo = table.Column<decimal>(type: "numeric", nullable: false),
                    ValorMaximo = table.Column<decimal>(type: "numeric", nullable: false),
                    IsAtivo = table.Column<bool>(type: "boolean", nullable: false),
                    IsPacote = table.Column<bool>(type: "boolean", nullable: false),
                    LinkItemFaturavel = table.Column<int>(type: "integer", nullable: true),
                    IsConsulta = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ItemFaturavel", x => x.ItemFaturavelId);
                });

            migrationBuilder.CreateTable(
                name: "PagarMes",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    DefaultApiKey = table.Column<string>(type: "character varying(250)", maxLength: 250, nullable: false),
                    PostBackUrl = table.Column<string>(type: "character varying(250)", maxLength: 250, nullable: false),
                    IsAtivo = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PagarMes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Pagina",
                columns: table => new
                {
                    PaginaId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Controller = table.Column<string>(type: "character varying(250)", maxLength: 250, nullable: false),
                    View = table.Column<string>(type: "character varying(250)", maxLength: 250, nullable: false),
                    Descricao = table.Column<string>(type: "character varying(250)", maxLength: 250, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Pagina", x => x.PaginaId);
                });

            migrationBuilder.CreateTable(
                name: "Tabela",
                columns: table => new
                {
                    TabelaId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Descricao = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: false),
                    IsConsumo = table.Column<bool>(type: "boolean", nullable: false),
                    IsAtivo = table.Column<bool>(type: "boolean", nullable: false),
                    IsEspecial = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tabela", x => x.TabelaId);
                });

            migrationBuilder.CreateTable(
                name: "ItemFaturavelRelacionado",
                columns: table => new
                {
                    ItemFaturavelRelacionadoId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    ItemFaturavelId = table.Column<int>(type: "integer", nullable: false),
                    Descricao = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ItemFaturavelRelacionado", x => x.ItemFaturavelRelacionadoId);
                    table.ForeignKey(
                        name: "FK_ItemFaturavelRelacionado_ItemFaturavel_ItemFaturavelId",
                        column: x => x.ItemFaturavelId,
                        principalTable: "ItemFaturavel",
                        principalColumn: "ItemFaturavelId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TabelaItem",
                columns: table => new
                {
                    TabelaId = table.Column<int>(type: "integer", nullable: false),
                    ItemFaturavelId = table.Column<int>(type: "integer", nullable: false),
                    ValorUnitario = table.Column<decimal>(type: "numeric", nullable: false),
                    QuantidadePacote = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TabelaItem", x => new { x.TabelaId, x.ItemFaturavelId });
                    table.ForeignKey(
                        name: "FK_TabelaItem_ItemFaturavel_ItemFaturavelId",
                        column: x => x.ItemFaturavelId,
                        principalTable: "ItemFaturavel",
                        principalColumn: "ItemFaturavelId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TabelaItem_Tabela_TabelaId",
                        column: x => x.TabelaId,
                        principalTable: "Tabela",
                        principalColumn: "TabelaId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Atendimento",
                columns: table => new
                {
                    AtendimentoId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    AssuntoId = table.Column<int>(type: "integer", nullable: false),
                    ClienteId = table.Column<int>(type: "integer", nullable: false),
                    ContratoId = table.Column<int>(type: "integer", nullable: true),
                    ColaboradorId = table.Column<int>(type: "integer", nullable: false),
                    Meio = table.Column<int>(type: "integer", nullable: false),
                    Descricao = table.Column<string>(type: "text", nullable: false),
                    Data = table.Column<DateTime>(type: "timestamp without time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Atendimento", x => x.AtendimentoId);
                    table.ForeignKey(
                        name: "FK_Atendimento_Assunto_AssuntoId",
                        column: x => x.AssuntoId,
                        principalTable: "Assunto",
                        principalColumn: "AssuntoId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Cliente",
                columns: table => new
                {
                    ClienteId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    AgenciaId = table.Column<int>(type: "integer", nullable: true),
                    AtividadeId = table.Column<int>(type: "integer", nullable: false),
                    RegiaoId = table.Column<int>(type: "integer", nullable: true),
                    TipoCliente = table.Column<int>(type: "integer", nullable: false),
                    IsAtivo = table.Column<bool>(type: "boolean", nullable: false),
                    Rg = table.Column<string>(type: "character varying(14)", maxLength: 14, nullable: true),
                    RgOrgaoExpedidor = table.Column<string>(type: "character varying(10)", maxLength: 10, nullable: true),
                    RgUf = table.Column<string>(type: "character varying(2)", maxLength: 2, nullable: true),
                    RgDataExpedicao = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    CPFCNPJ = table.Column<string>(type: "character varying(14)", maxLength: 14, nullable: false),
                    InscricaoEstadual = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: true),
                    InscricaoMunicipal = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: true),
                    Nome = table.Column<string>(type: "character varying(50)", maxLength: 50, nullable: false),
                    RazaoSocial = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: true),
                    Email = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: false),
                    Website = table.Column<string>(type: "character varying(250)", maxLength: 250, nullable: true),
                    IsRevenda = table.Column<bool>(type: "boolean", nullable: false),
                    ComissaoAgente = table.Column<decimal>(type: "numeric", nullable: true),
                    IsComissionamento = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cliente", x => x.ClienteId);
                    table.ForeignKey(
                        name: "FK_Cliente_Atividade_AtividadeId",
                        column: x => x.AtividadeId,
                        principalTable: "Atividade",
                        principalColumn: "AtividadeId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Cliente_Cliente_AgenciaId",
                        column: x => x.AgenciaId,
                        principalTable: "Cliente",
                        principalColumn: "ClienteId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "BoletoAvulso",
                columns: table => new
                {
                    BoletoAvulsoId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    ClienteId = table.Column<int>(type: "integer", nullable: false),
                    NossoNumero = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: false),
                    DataVencimento = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    ValorOriginal = table.Column<decimal>(type: "numeric", nullable: false),
                    ValorPago = table.Column<decimal>(type: "numeric", nullable: true),
                    ValorJuro = table.Column<decimal>(type: "numeric", nullable: true),
                    ValorDesconto = table.Column<decimal>(type: "numeric", nullable: true),
                    Demonstrativo = table.Column<string>(type: "text", nullable: false),
                    DataPagamento = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    DataBaixa = table.Column<DateTime>(type: "timestamp without time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BoletoAvulso", x => x.BoletoAvulsoId);
                    table.ForeignKey(
                        name: "FK_BoletoAvulso_Cliente_ClienteId",
                        column: x => x.ClienteId,
                        principalTable: "Cliente",
                        principalColumn: "ClienteId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Cargo",
                columns: table => new
                {
                    CargoId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    AgenciaId = table.Column<int>(type: "integer", nullable: false),
                    Descricao = table.Column<string>(type: "character varying(250)", maxLength: 250, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cargo", x => x.CargoId);
                    table.ForeignKey(
                        name: "FK_Cargo_Cliente_AgenciaId",
                        column: x => x.AgenciaId,
                        principalTable: "Cliente",
                        principalColumn: "ClienteId");
                });

            migrationBuilder.CreateTable(
                name: "Consulta",
                columns: table => new
                {
                    AgenciaId = table.Column<int>(type: "integer", nullable: false),
                    ItemFaturavelId = table.Column<int>(type: "integer", nullable: false),
                    Descricao = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: false),
                    Detalhe = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Consulta", x => new { x.AgenciaId, x.ItemFaturavelId });
                    table.ForeignKey(
                        name: "FK_Consulta_Cliente_AgenciaId",
                        column: x => x.AgenciaId,
                        principalTable: "Cliente",
                        principalColumn: "ClienteId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Consulta_ItemFaturavel_ItemFaturavelId",
                        column: x => x.ItemFaturavelId,
                        principalTable: "ItemFaturavel",
                        principalColumn: "ItemFaturavelId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ConvemCredor",
                columns: table => new
                {
                    ConvemCredorId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    ClienteId = table.Column<int>(type: "integer", nullable: false),
                    CredorTipo = table.Column<string>(type: "character varying(1)", maxLength: 1, nullable: false),
                    CredorRazaoSocial = table.Column<string>(type: "character varying(27)", maxLength: 27, nullable: false),
                    CredorContato = table.Column<string>(type: "character varying(30)", maxLength: 30, nullable: true),
                    CredorEmail = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: false),
                    CredorNomeFantasia = table.Column<string>(type: "character varying(23)", maxLength: 23, nullable: false),
                    CredorCNPJ = table.Column<string>(type: "character varying(14)", maxLength: 14, nullable: false),
                    CredorEndereco = table.Column<string>(type: "character varying(35)", maxLength: 35, nullable: false),
                    CredorBairro = table.Column<string>(type: "character varying(15)", maxLength: 15, nullable: false),
                    CredorCidade = table.Column<string>(type: "character varying(15)", maxLength: 15, nullable: false),
                    CredorCEP = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: false),
                    CredorUF = table.Column<string>(type: "text", nullable: false),
                    CredorDDD = table.Column<string>(type: "character varying(2)", maxLength: 2, nullable: false),
                    CredorTelefone = table.Column<string>(type: "character varying(9)", maxLength: 9, nullable: false),
                    IsConfirmado = table.Column<bool>(type: "boolean", nullable: false),
                    NumeroRemessa = table.Column<int>(type: "integer", nullable: true),
                    DataConfirmacao = table.Column<DateTime>(type: "timestamp without time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ConvemCredor", x => x.ConvemCredorId);
                    table.ForeignKey(
                        name: "FK_ConvemCredor_Cliente_ClienteId",
                        column: x => x.ClienteId,
                        principalTable: "Cliente",
                        principalColumn: "ClienteId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CreditoConsumo",
                columns: table => new
                {
                    CreditoConsumoId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    ClienteId = table.Column<int>(type: "integer", nullable: false),
                    Valor = table.Column<decimal>(type: "numeric", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CreditoConsumo", x => x.CreditoConsumoId);
                    table.ForeignKey(
                        name: "FK_CreditoConsumo_Cliente_ClienteId",
                        column: x => x.ClienteId,
                        principalTable: "Cliente",
                        principalColumn: "ClienteId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Endereco",
                columns: table => new
                {
                    EnderecoId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    ClienteId = table.Column<int>(type: "integer", nullable: false),
                    TipoEndereco = table.Column<int>(type: "integer", nullable: false),
                    Logradouro = table.Column<string>(type: "character varying(250)", maxLength: 250, nullable: false),
                    Numero = table.Column<string>(type: "character varying(20)", maxLength: 20, nullable: false),
                    Complemento = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: true),
                    Bairro = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: false),
                    Cidade = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: false),
                    CEP = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: false),
                    UF = table.Column<string>(type: "character varying(2)", maxLength: 2, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Endereco", x => x.EnderecoId);
                    table.ForeignKey(
                        name: "FK_Endereco_Cliente_ClienteId",
                        column: x => x.ClienteId,
                        principalTable: "Cliente",
                        principalColumn: "ClienteId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Faturamento",
                columns: table => new
                {
                    FaturamentoId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    ClienteId = table.Column<int>(type: "integer", nullable: false),
                    TipoPagamento = table.Column<int>(type: "integer", nullable: true),
                    DataEmissao = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    DataVencimento = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    DataPagamento = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    ValorOriginal = table.Column<decimal>(type: "numeric", nullable: false),
                    ValorPago = table.Column<decimal>(type: "numeric", nullable: true),
                    ValorDesconto = table.Column<decimal>(type: "numeric", nullable: true),
                    ValorJuros = table.Column<decimal>(type: "numeric", nullable: true),
                    ValorCusto = table.Column<decimal>(type: "numeric", nullable: false),
                    ValorRepasseAgencia = table.Column<decimal>(type: "numeric", nullable: true),
                    DataInicialFaturamento = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    DataFinalFaturamento = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    Tid = table.Column<int>(type: "integer", nullable: true),
                    LinhaDigitavel = table.Column<string>(type: "character varying(250)", maxLength: 250, nullable: true),
                    LinkBoleto = table.Column<string>(type: "character varying(250)", maxLength: 250, nullable: true),
                    ValorDespesa = table.Column<decimal>(type: "numeric", nullable: true),
                    IsEnviar = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Faturamento", x => x.FaturamentoId);
                    table.ForeignKey(
                        name: "FK_Faturamento_Cliente_ClienteId",
                        column: x => x.ClienteId,
                        principalTable: "Cliente",
                        principalColumn: "ClienteId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "MultLogon",
                columns: table => new
                {
                    MultLogonId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    ClienteId = table.Column<int>(type: "integer", nullable: true),
                    Logon = table.Column<string>(type: "character varying(10)", maxLength: 10, nullable: false),
                    Senha = table.Column<string>(type: "character varying(10)", maxLength: 10, nullable: false),
                    IsAtivo = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MultLogon", x => x.MultLogonId);
                    table.ForeignKey(
                        name: "FK_MultLogon_Cliente_ClienteId",
                        column: x => x.ClienteId,
                        principalTable: "Cliente",
                        principalColumn: "ClienteId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PedidoCredito",
                columns: table => new
                {
                    PedidoCreditoId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    ClienteId = table.Column<int>(type: "integer", nullable: false),
                    CreditoId = table.Column<int>(type: "integer", nullable: false),
                    Data = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    DataConfirmacao = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    DataVencimento = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    CodigoTransacao = table.Column<string>(type: "character varying(250)", maxLength: 250, nullable: true),
                    Referencia = table.Column<string>(type: "character varying(250)", maxLength: 250, nullable: true),
                    Token = table.Column<string>(type: "character varying(250)", maxLength: 250, nullable: true),
                    IsConfirmado = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PedidoCredito", x => x.PedidoCreditoId);
                    table.ForeignKey(
                        name: "FK_PedidoCredito_Cliente_ClienteId",
                        column: x => x.ClienteId,
                        principalTable: "Cliente",
                        principalColumn: "ClienteId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PedidoCredito_Credito_CreditoId",
                        column: x => x.CreditoId,
                        principalTable: "Credito",
                        principalColumn: "CreditoId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Permissao",
                columns: table => new
                {
                    PermissaoId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    ClienteId = table.Column<int>(type: "integer", nullable: false),
                    Descricao = table.Column<string>(type: "character varying(250)", maxLength: 250, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Permissao", x => x.PermissaoId);
                    table.ForeignKey(
                        name: "FK_Permissao_Cliente_ClienteId",
                        column: x => x.ClienteId,
                        principalTable: "Cliente",
                        principalColumn: "ClienteId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Regiao",
                columns: table => new
                {
                    RegiaoId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    AgenciaId = table.Column<int>(type: "integer", nullable: true),
                    Descricao = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Regiao", x => x.RegiaoId);
                    table.ForeignKey(
                        name: "FK_Regiao_Cliente_AgenciaId",
                        column: x => x.AgenciaId,
                        principalTable: "Cliente",
                        principalColumn: "ClienteId");
                });

            migrationBuilder.CreateTable(
                name: "Revenda",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    ClienteId = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Revenda", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Revenda_Cliente_ClienteId",
                        column: x => x.ClienteId,
                        principalTable: "Cliente",
                        principalColumn: "ClienteId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Serasa",
                columns: table => new
                {
                    SerasaId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    ClienteId = table.Column<int>(type: "integer", nullable: false),
                    Logon = table.Column<string>(type: "character varying(50)", maxLength: 50, nullable: false),
                    Senha = table.Column<string>(type: "character varying(50)", maxLength: 50, nullable: false),
                    Observacao = table.Column<string>(type: "text", nullable: true),
                    IsAtivo = table.Column<bool>(type: "boolean", nullable: false),
                    IsInclusao = table.Column<bool>(type: "boolean", nullable: false),
                    IsInterno = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Serasa", x => x.SerasaId);
                    table.ForeignKey(
                        name: "FK_Serasa_Cliente_ClienteId",
                        column: x => x.ClienteId,
                        principalTable: "Cliente",
                        principalColumn: "ClienteId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Siscovem",
                columns: table => new
                {
                    SiscovemId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    ClienteId = table.Column<int>(type: "integer", nullable: false),
                    Logon = table.Column<string>(type: "text", nullable: false),
                    Senha = table.Column<string>(type: "text", nullable: false),
                    CredorDocumento = table.Column<string>(type: "text", nullable: false),
                    CredorRazaoSocial = table.Column<string>(type: "character varying(70)", maxLength: 70, nullable: false),
                    CredorNomeFantasia = table.Column<string>(type: "character varying(25)", maxLength: 25, nullable: false),
                    CredorEndereco = table.Column<string>(type: "character varying(45)", maxLength: 45, nullable: false),
                    CredorBairro = table.Column<string>(type: "character varying(20)", maxLength: 20, nullable: false),
                    CredorMunicipio = table.Column<string>(type: "character varying(25)", maxLength: 25, nullable: false),
                    CredorUF = table.Column<string>(type: "text", nullable: false),
                    CredorCEP = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: false),
                    CredorTelefone = table.Column<string>(type: "character varying(11)", maxLength: 11, nullable: false),
                    DistribuidorDocumento = table.Column<string>(type: "text", nullable: false),
                    DevedorDocumento = table.Column<string>(type: "text", nullable: false),
                    DevedorNome = table.Column<string>(type: "character varying(70)", maxLength: 70, nullable: false),
                    DevedorEndereco = table.Column<string>(type: "character varying(45)", maxLength: 45, nullable: false),
                    DevedorComplemento = table.Column<string>(type: "character varying(25)", maxLength: 25, nullable: true),
                    DevedorBairro = table.Column<string>(type: "character varying(20)", maxLength: 20, nullable: false),
                    DevedorMunicipio = table.Column<string>(type: "character varying(25)", maxLength: 25, nullable: false),
                    DevedorUF = table.Column<string>(type: "text", nullable: false),
                    DevedorCEP = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: false),
                    DevedorTelefone = table.Column<string>(type: "text", nullable: true),
                    DevedorDataNascimento = table.Column<string>(type: "text", nullable: true),
                    DevedorRG = table.Column<string>(type: "character varying(15)", maxLength: 15, nullable: true),
                    DevedorRGUF = table.Column<string>(type: "text", nullable: true),
                    DevedorNomePai = table.Column<string>(type: "character varying(70)", maxLength: 70, nullable: true),
                    DevedorNomeMae = table.Column<string>(type: "character varying(70)", maxLength: 70, nullable: true),
                    AnotacaoNossoNumero = table.Column<string>(type: "character varying(15)", maxLength: 15, nullable: false),
                    AnotacaoEspecieTitulo = table.Column<string>(type: "text", nullable: false),
                    AnotacaoNumeroTitulo = table.Column<string>(type: "character varying(16)", maxLength: 16, nullable: false),
                    AnotacaoUF = table.Column<string>(type: "text", nullable: false),
                    AnotacaoCidade = table.Column<string>(type: "text", nullable: false),
                    AnotacaoDataEmissao = table.Column<string>(type: "text", nullable: false),
                    AnotacaoDataVencimento = table.Column<string>(type: "text", nullable: false),
                    AnotacaoValorTitulo = table.Column<string>(type: "text", nullable: false),
                    AnotacaoValorSaldoTitulo = table.Column<string>(type: "text", nullable: false),
                    AnotacaoTipoEndosso = table.Column<string>(type: "text", nullable: false),
                    AnotacaoInformacaoAceite = table.Column<string>(type: "text", nullable: false),
                    AnotacaoNumeroControle = table.Column<string>(type: "text", nullable: false),
                    AnotacaoUsoInstituicaoConveniada = table.Column<string>(type: "text", nullable: true),
                    IsCredorSerasa = table.Column<bool>(type: "boolean", nullable: false),
                    DadosEnvio = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Siscovem", x => x.SiscovemId);
                    table.ForeignKey(
                        name: "FK_Siscovem_Cliente_ClienteId",
                        column: x => x.ClienteId,
                        principalTable: "Cliente",
                        principalColumn: "ClienteId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TabelaAgencia",
                columns: table => new
                {
                    AgenciaId = table.Column<int>(type: "integer", nullable: false),
                    TabelaId = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TabelaAgencia", x => new { x.AgenciaId, x.TabelaId });
                    table.ForeignKey(
                        name: "FK_TabelaAgencia_Cliente_AgenciaId",
                        column: x => x.AgenciaId,
                        principalTable: "Cliente",
                        principalColumn: "ClienteId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TabelaAgencia_Tabela_TabelaId",
                        column: x => x.TabelaId,
                        principalTable: "Tabela",
                        principalColumn: "TabelaId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TabelaRevenda",
                columns: table => new
                {
                    ClienteId = table.Column<int>(type: "integer", nullable: false),
                    TabelaId = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TabelaRevenda", x => new { x.ClienteId, x.TabelaId });
                    table.ForeignKey(
                        name: "FK_TabelaRevenda_Cliente_ClienteId",
                        column: x => x.ClienteId,
                        principalTable: "Cliente",
                        principalColumn: "ClienteId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TabelaRevenda_Tabela_TabelaId",
                        column: x => x.TabelaId,
                        principalTable: "Tabela",
                        principalColumn: "TabelaId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Telefone",
                columns: table => new
                {
                    TelefoneId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    ClienteId = table.Column<int>(type: "integer", nullable: false),
                    TipoTelefone = table.Column<int>(type: "integer", nullable: false),
                    DDD = table.Column<string>(type: "character varying(2)", maxLength: 2, nullable: false),
                    Numero = table.Column<string>(type: "character varying(9)", maxLength: 9, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Telefone", x => x.TelefoneId);
                    table.ForeignKey(
                        name: "FK_Telefone_Cliente_ClienteId",
                        column: x => x.ClienteId,
                        principalTable: "Cliente",
                        principalColumn: "ClienteId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Colaborador",
                columns: table => new
                {
                    ColaboradorId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    AgenciaId = table.Column<int>(type: "integer", nullable: false),
                    CargoId = table.Column<int>(type: "integer", nullable: false),
                    CPF = table.Column<string>(type: "character varying(11)", maxLength: 11, nullable: false),
                    Rg = table.Column<string>(type: "character varying(14)", maxLength: 14, nullable: true),
                    RgOrgaoExpedidor = table.Column<string>(type: "character varying(10)", maxLength: 10, nullable: true),
                    RgUf = table.Column<string>(type: "character varying(2)", maxLength: 2, nullable: true),
                    RgDataExpedicao = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    Nome = table.Column<string>(type: "character varying(50)", maxLength: 50, nullable: false),
                    DataNascimento = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    DDDTelefone = table.Column<string>(type: "character varying(2)", maxLength: 2, nullable: false),
                    Telefone = table.Column<string>(type: "character varying(9)", maxLength: 9, nullable: false),
                    DDDCelular = table.Column<string>(type: "character varying(2)", maxLength: 2, nullable: false),
                    Celular = table.Column<string>(type: "character varying(9)", maxLength: 9, nullable: false),
                    Logradouro = table.Column<string>(type: "character varying(250)", maxLength: 250, nullable: false),
                    Complemento = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: true),
                    Bairro = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: false),
                    Cidade = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: false),
                    CEP = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: false),
                    UF = table.Column<string>(type: "character varying(2)", maxLength: 2, nullable: false),
                    Email = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Colaborador", x => x.ColaboradorId);
                    table.ForeignKey(
                        name: "FK_Colaborador_Cargo_CargoId",
                        column: x => x.CargoId,
                        principalTable: "Cargo",
                        principalColumn: "CargoId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Colaborador_Cliente_AgenciaId",
                        column: x => x.AgenciaId,
                        principalTable: "Cliente",
                        principalColumn: "ClienteId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Contato",
                columns: table => new
                {
                    ContatoId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    ClienteId = table.Column<int>(type: "integer", nullable: false),
                    CargoId = table.Column<int>(type: "integer", nullable: false),
                    CPF = table.Column<string>(type: "character varying(11)", maxLength: 11, nullable: false),
                    Rg = table.Column<string>(type: "character varying(14)", maxLength: 14, nullable: true),
                    RgOrgaoExpedidor = table.Column<string>(type: "character varying(10)", maxLength: 10, nullable: true),
                    RgUf = table.Column<string>(type: "character varying(2)", maxLength: 2, nullable: true),
                    RgDataExpedicao = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    Nome = table.Column<string>(type: "character varying(50)", maxLength: 50, nullable: false),
                    DataNascimento = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    DDDTelefone = table.Column<string>(type: "character varying(2)", maxLength: 2, nullable: true),
                    Telefone = table.Column<string>(type: "character varying(9)", maxLength: 9, nullable: true),
                    DDDCelular = table.Column<string>(type: "character varying(2)", maxLength: 2, nullable: true),
                    Celular = table.Column<string>(type: "character varying(9)", maxLength: 9, nullable: true),
                    Logradouro = table.Column<string>(type: "character varying(250)", maxLength: 250, nullable: true),
                    Complemento = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: true),
                    Bairro = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: true),
                    Cidade = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: true),
                    CEP = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: true),
                    UF = table.Column<string>(type: "character varying(2)", maxLength: 2, nullable: true),
                    Email = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Contato", x => x.ContatoId);
                    table.ForeignKey(
                        name: "FK_Contato_Cargo_CargoId",
                        column: x => x.CargoId,
                        principalTable: "Cargo",
                        principalColumn: "CargoId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Contato_Cliente_ClienteId",
                        column: x => x.ClienteId,
                        principalTable: "Cliente",
                        principalColumn: "ClienteId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ConvemInclusaoAnotacao",
                columns: table => new
                {
                    ConvemInclusaoAnotacaoId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    ConvemCredorId = table.Column<int>(type: "integer", nullable: false),
                    DevedorNome = table.Column<string>(type: "character varying(70)", maxLength: 70, nullable: false),
                    DevedorCPFCNPJ = table.Column<string>(type: "character varying(14)", maxLength: 14, nullable: false),
                    DevedorNomeMae = table.Column<string>(type: "character varying(70)", maxLength: 70, nullable: true),
                    DevedorCEP = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: false),
                    DevedorEndereco = table.Column<string>(type: "character varying(45)", maxLength: 45, nullable: false),
                    DevedorBairro = table.Column<string>(type: "character varying(20)", maxLength: 20, nullable: false),
                    DevedorCidade = table.Column<string>(type: "character varying(25)", maxLength: 25, nullable: false),
                    DevedorUF = table.Column<string>(type: "character varying(2)", maxLength: 2, nullable: false),
                    DevedorDataNascimento = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    DevedorDDD = table.Column<string>(type: "character varying(2)", maxLength: 2, nullable: true),
                    DevedorTelefone = table.Column<string>(type: "character varying(9)", maxLength: 9, nullable: true),
                    AnotacaoDataCompromisso = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    AnotacaoDataVencimento = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    AnotacaoNatureza = table.Column<string>(type: "character varying(2)", maxLength: 2, nullable: false),
                    AnotacaoNumeroContrato = table.Column<string>(type: "character varying(16)", maxLength: 16, nullable: false),
                    AnotacaoNossoNumero = table.Column<string>(type: "character varying(9)", maxLength: 9, nullable: false),
                    AnotacaoValor = table.Column<decimal>(type: "numeric", nullable: false),
                    ChequeNumeroBanco = table.Column<string>(type: "character varying(4)", maxLength: 4, nullable: true),
                    ChequeNumeroAgencia = table.Column<string>(type: "character varying(4)", maxLength: 4, nullable: true),
                    ChequeNumero = table.Column<string>(type: "character varying(6)", maxLength: 6, nullable: true),
                    ChequeNumeroContaCorrente = table.Column<string>(type: "character varying(9)", maxLength: 9, nullable: true),
                    ChequeAlinea = table.Column<int>(type: "integer", nullable: false),
                    NumeroRemessa = table.Column<int>(type: "integer", nullable: true),
                    DataAlteracao = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    ConvemErroPefinId = table.Column<int>(type: "integer", nullable: true),
                    Status = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ConvemInclusaoAnotacao", x => x.ConvemInclusaoAnotacaoId);
                    table.ForeignKey(
                        name: "FK_ConvemInclusaoAnotacao_ConvemCredor_ConvemCredorId",
                        column: x => x.ConvemCredorId,
                        principalTable: "ConvemCredor",
                        principalColumn: "ConvemCredorId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "FaturamentoItem",
                columns: table => new
                {
                    FaturamentoId = table.Column<int>(type: "integer", nullable: false),
                    ItemFaturavelId = table.Column<int>(type: "integer", nullable: false),
                    Quantidade = table.Column<int>(type: "integer", nullable: false),
                    CustoUnitario = table.Column<decimal>(type: "numeric", nullable: true),
                    ValorUnitario = table.Column<decimal>(type: "numeric", nullable: false),
                    RepasseAgencia = table.Column<decimal>(type: "numeric", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FaturamentoItem", x => new { x.FaturamentoId, x.ItemFaturavelId });
                    table.ForeignKey(
                        name: "FK_FaturamentoItem_Faturamento_FaturamentoId",
                        column: x => x.FaturamentoId,
                        principalTable: "Faturamento",
                        principalColumn: "FaturamentoId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_FaturamentoItem_ItemFaturavel_ItemFaturavelId",
                        column: x => x.ItemFaturavelId,
                        principalTable: "ItemFaturavel",
                        principalColumn: "ItemFaturavelId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PermissaoPagina",
                columns: table => new
                {
                    PermissaoId = table.Column<int>(type: "integer", nullable: false),
                    PaginaId = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PermissaoPagina", x => new { x.PermissaoId, x.PaginaId });
                    table.ForeignKey(
                        name: "FK_PermissaoPagina_Pagina_PaginaId",
                        column: x => x.PaginaId,
                        principalTable: "Pagina",
                        principalColumn: "PaginaId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PermissaoPagina_Permissao_PermissaoId",
                        column: x => x.PermissaoId,
                        principalTable: "Permissao",
                        principalColumn: "PermissaoId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "RevendaItem",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    RevendaId = table.Column<int>(type: "integer", nullable: false),
                    ItemFaturavelId = table.Column<int>(type: "integer", nullable: false),
                    Valor = table.Column<decimal>(type: "numeric", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RevendaItem", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RevendaItem_ItemFaturavel_ItemFaturavelId",
                        column: x => x.ItemFaturavelId,
                        principalTable: "ItemFaturavel",
                        principalColumn: "ItemFaturavelId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_RevendaItem_Revenda_RevendaId",
                        column: x => x.RevendaId,
                        principalTable: "Revenda",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Contrato",
                columns: table => new
                {
                    ContratoId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    ClienteId = table.Column<int>(type: "integer", nullable: false),
                    VendedorId = table.Column<int>(type: "integer", nullable: false),
                    TabelaId = table.Column<int>(type: "integer", nullable: false),
                    DiaCobrancaId = table.Column<int>(type: "integer", nullable: false),
                    DataInicial = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    DataFinal = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    DataCancelamento = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    DataCadastro = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    Comissao = table.Column<decimal>(type: "numeric", nullable: true),
                    IsRenovacaoAutomatica = table.Column<bool>(type: "boolean", nullable: false),
                    IsTaxaExtra = table.Column<bool>(type: "boolean", nullable: false),
                    IsAniversario = table.Column<bool>(type: "boolean", nullable: false),
                    IsEnviaEmail = table.Column<bool>(type: "boolean", nullable: false),
                    IsEnviaSms = table.Column<bool>(type: "boolean", nullable: false),
                    IsDespesaBancaria = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Contrato", x => x.ContratoId);
                    table.ForeignKey(
                        name: "FK_Contrato_Cliente_ClienteId",
                        column: x => x.ClienteId,
                        principalTable: "Cliente",
                        principalColumn: "ClienteId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Contrato_Colaborador_VendedorId",
                        column: x => x.VendedorId,
                        principalTable: "Colaborador",
                        principalColumn: "ColaboradorId");
                    table.ForeignKey(
                        name: "FK_Contrato_DiaCobranca_DiaCobrancaId",
                        column: x => x.DiaCobrancaId,
                        principalTable: "DiaCobranca",
                        principalColumn: "DiaCobrancaId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Contrato_Tabela_TabelaId",
                        column: x => x.TabelaId,
                        principalTable: "Tabela",
                        principalColumn: "TabelaId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Usuario",
                columns: table => new
                {
                    UsuarioId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    ColaboradorId = table.Column<int>(type: "integer", nullable: false),
                    Senha = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: false),
                    PermissaoId = table.Column<int>(type: "integer", nullable: true),
                    IsSuperUsuario = table.Column<bool>(type: "boolean", nullable: false),
                    IsAtivo = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Usuario", x => x.UsuarioId);
                    table.ForeignKey(
                        name: "FK_Usuario_Colaborador_ColaboradorId",
                        column: x => x.ColaboradorId,
                        principalTable: "Colaborador",
                        principalColumn: "ColaboradorId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Usuario_Permissao_PermissaoId",
                        column: x => x.PermissaoId,
                        principalTable: "Permissao",
                        principalColumn: "PermissaoId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ConvemExclusaoAnotacao",
                columns: table => new
                {
                    ConvemExclusaoAnotacaoId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    ConvemInclusaoAnotacaoId = table.Column<int>(type: "integer", nullable: false),
                    MotivoBaixa = table.Column<string>(type: "character varying(2)", maxLength: 2, nullable: false),
                    NumeroRemessa = table.Column<int>(type: "integer", nullable: true),
                    DataAlteracao = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    Status = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ConvemExclusaoAnotacao", x => x.ConvemExclusaoAnotacaoId);
                    table.ForeignKey(
                        name: "FK_ConvemExclusaoAnotacao_ConvemInclusaoAnotacao_ConvemInclusa~",
                        column: x => x.ConvemInclusaoAnotacaoId,
                        principalTable: "ConvemInclusaoAnotacao",
                        principalColumn: "ConvemInclusaoAnotacaoId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UsuarioConsulta",
                columns: table => new
                {
                    UsuarioConsultaId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    ContratoId = table.Column<int>(type: "integer", nullable: true),
                    ClienteId = table.Column<int>(type: "integer", nullable: false),
                    Email = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: false),
                    Senha = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: false),
                    IsSuperUsuario = table.Column<bool>(type: "boolean", nullable: false),
                    IsAtivo = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UsuarioConsulta", x => x.UsuarioConsultaId);
                    table.ForeignKey(
                        name: "FK_UsuarioConsulta_Cliente_ClienteId",
                        column: x => x.ClienteId,
                        principalTable: "Cliente",
                        principalColumn: "ClienteId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UsuarioConsulta_Contrato_ContratoId",
                        column: x => x.ContratoId,
                        principalTable: "Contrato",
                        principalColumn: "ContratoId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "DocumentoPendente",
                columns: table => new
                {
                    DocumentoPendenteId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    ContratoId = table.Column<int>(type: "integer", nullable: false),
                    UsuarioId = table.Column<int>(type: "integer", nullable: false),
                    TipoDocumento = table.Column<int>(type: "integer", nullable: false),
                    DataBaixa = table.Column<DateTime>(type: "timestamp without time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DocumentoPendente", x => x.DocumentoPendenteId);
                    table.ForeignKey(
                        name: "FK_DocumentoPendente_Contrato_ContratoId",
                        column: x => x.ContratoId,
                        principalTable: "Contrato",
                        principalColumn: "ContratoId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DocumentoPendente_Usuario_UsuarioId",
                        column: x => x.UsuarioId,
                        principalTable: "Usuario",
                        principalColumn: "UsuarioId");
                });

            migrationBuilder.CreateTable(
                name: "PermissaoConsulta",
                columns: table => new
                {
                    UsuarioConsultaId = table.Column<int>(type: "integer", nullable: false),
                    ItemFaturavelId = table.Column<int>(type: "integer", nullable: false),
                    IsPermitido = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PermissaoConsulta", x => new { x.UsuarioConsultaId, x.ItemFaturavelId });
                    table.ForeignKey(
                        name: "FK_PermissaoConsulta_ItemFaturavel_ItemFaturavelId",
                        column: x => x.ItemFaturavelId,
                        principalTable: "ItemFaturavel",
                        principalColumn: "ItemFaturavelId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PermissaoConsulta_UsuarioConsulta_UsuarioConsultaId",
                        column: x => x.UsuarioConsultaId,
                        principalTable: "UsuarioConsulta",
                        principalColumn: "UsuarioConsultaId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TransacaoConsulta",
                columns: table => new
                {
                    NSU = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    ItemFaturavelId = table.Column<int>(type: "integer", nullable: false),
                    ClienteId = table.Column<int>(type: "integer", nullable: false),
                    UsuarioConsultaId = table.Column<int>(type: "integer", nullable: true),
                    DataEnvio = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    DataResposta = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    DadosEnvio = table.Column<string>(type: "text", nullable: true),
                    DadosResposta = table.Column<string>(type: "text", nullable: true),
                    StringResposta = table.Column<string>(type: "text", nullable: true),
                    IsConfirmado = table.Column<bool>(type: "boolean", nullable: false),
                    NSUOrigem = table.Column<int>(type: "integer", nullable: true),
                    DataInicial = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    DataFinal = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    ChaveConsulta = table.Column<string>(type: "character varying(250)", maxLength: 250, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TransacaoConsulta", x => x.NSU);
                    table.ForeignKey(
                        name: "FK_TransacaoConsulta_Cliente_ClienteId",
                        column: x => x.ClienteId,
                        principalTable: "Cliente",
                        principalColumn: "ClienteId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TransacaoConsulta_ItemFaturavel_ItemFaturavelId",
                        column: x => x.ItemFaturavelId,
                        principalTable: "ItemFaturavel",
                        principalColumn: "ItemFaturavelId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TransacaoConsulta_UsuarioConsulta_UsuarioConsultaId",
                        column: x => x.UsuarioConsultaId,
                        principalTable: "UsuarioConsulta",
                        principalColumn: "UsuarioConsultaId");
                });

            migrationBuilder.CreateIndex(
                name: "IX_Atendimento_AssuntoId",
                table: "Atendimento",
                column: "AssuntoId");

            migrationBuilder.CreateIndex(
                name: "IX_Atendimento_ClienteId",
                table: "Atendimento",
                column: "ClienteId");

            migrationBuilder.CreateIndex(
                name: "IX_Atendimento_ColaboradorId",
                table: "Atendimento",
                column: "ColaboradorId");

            migrationBuilder.CreateIndex(
                name: "IX_Atendimento_ContratoId",
                table: "Atendimento",
                column: "ContratoId");

            migrationBuilder.CreateIndex(
                name: "IX_BoletoAvulso_ClienteId",
                table: "BoletoAvulso",
                column: "ClienteId");

            migrationBuilder.CreateIndex(
                name: "IX_Cargo_AgenciaId",
                table: "Cargo",
                column: "AgenciaId");

            migrationBuilder.CreateIndex(
                name: "IX_Cliente_AgenciaId",
                table: "Cliente",
                column: "AgenciaId");

            migrationBuilder.CreateIndex(
                name: "IX_Cliente_AtividadeId",
                table: "Cliente",
                column: "AtividadeId");

            migrationBuilder.CreateIndex(
                name: "IX_Cliente_RegiaoId",
                table: "Cliente",
                column: "RegiaoId");

            migrationBuilder.CreateIndex(
                name: "IX_Colaborador_AgenciaId",
                table: "Colaborador",
                column: "AgenciaId");

            migrationBuilder.CreateIndex(
                name: "IX_Colaborador_CargoId",
                table: "Colaborador",
                column: "CargoId");

            migrationBuilder.CreateIndex(
                name: "IX_Consulta_ItemFaturavelId",
                table: "Consulta",
                column: "ItemFaturavelId");

            migrationBuilder.CreateIndex(
                name: "IX_Contato_CargoId",
                table: "Contato",
                column: "CargoId");

            migrationBuilder.CreateIndex(
                name: "IX_Contato_ClienteId",
                table: "Contato",
                column: "ClienteId");

            migrationBuilder.CreateIndex(
                name: "IX_Contrato_ClienteId",
                table: "Contrato",
                column: "ClienteId");

            migrationBuilder.CreateIndex(
                name: "IX_Contrato_DiaCobrancaId",
                table: "Contrato",
                column: "DiaCobrancaId");

            migrationBuilder.CreateIndex(
                name: "IX_Contrato_TabelaId",
                table: "Contrato",
                column: "TabelaId");

            migrationBuilder.CreateIndex(
                name: "IX_Contrato_VendedorId",
                table: "Contrato",
                column: "VendedorId");

            migrationBuilder.CreateIndex(
                name: "IX_ConvemCredor_ClienteId",
                table: "ConvemCredor",
                column: "ClienteId");

            migrationBuilder.CreateIndex(
                name: "IX_ConvemExclusaoAnotacao_ConvemInclusaoAnotacaoId",
                table: "ConvemExclusaoAnotacao",
                column: "ConvemInclusaoAnotacaoId");

            migrationBuilder.CreateIndex(
                name: "IX_ConvemInclusaoAnotacao_ConvemCredorId",
                table: "ConvemInclusaoAnotacao",
                column: "ConvemCredorId");

            migrationBuilder.CreateIndex(
                name: "IX_CreditoConsumo_ClienteId",
                table: "CreditoConsumo",
                column: "ClienteId");

            migrationBuilder.CreateIndex(
                name: "IX_DocumentoPendente_ContratoId",
                table: "DocumentoPendente",
                column: "ContratoId");

            migrationBuilder.CreateIndex(
                name: "IX_DocumentoPendente_UsuarioId",
                table: "DocumentoPendente",
                column: "UsuarioId");

            migrationBuilder.CreateIndex(
                name: "IX_Endereco_ClienteId",
                table: "Endereco",
                column: "ClienteId");

            migrationBuilder.CreateIndex(
                name: "IX_Faturamento_ClienteId",
                table: "Faturamento",
                column: "ClienteId");

            migrationBuilder.CreateIndex(
                name: "IX_FaturamentoItem_ItemFaturavelId",
                table: "FaturamentoItem",
                column: "ItemFaturavelId");

            migrationBuilder.CreateIndex(
                name: "IX_ItemFaturavelRelacionado_ItemFaturavelId",
                table: "ItemFaturavelRelacionado",
                column: "ItemFaturavelId");

            migrationBuilder.CreateIndex(
                name: "IX_MultLogon_ClienteId",
                table: "MultLogon",
                column: "ClienteId");

            migrationBuilder.CreateIndex(
                name: "IX_PedidoCredito_ClienteId",
                table: "PedidoCredito",
                column: "ClienteId");

            migrationBuilder.CreateIndex(
                name: "IX_PedidoCredito_CreditoId",
                table: "PedidoCredito",
                column: "CreditoId");

            migrationBuilder.CreateIndex(
                name: "IX_Permissao_ClienteId",
                table: "Permissao",
                column: "ClienteId");

            migrationBuilder.CreateIndex(
                name: "IX_PermissaoConsulta_ItemFaturavelId",
                table: "PermissaoConsulta",
                column: "ItemFaturavelId");

            migrationBuilder.CreateIndex(
                name: "IX_PermissaoPagina_PaginaId",
                table: "PermissaoPagina",
                column: "PaginaId");

            migrationBuilder.CreateIndex(
                name: "IX_Regiao_AgenciaId",
                table: "Regiao",
                column: "AgenciaId");

            migrationBuilder.CreateIndex(
                name: "IX_Revenda_ClienteId",
                table: "Revenda",
                column: "ClienteId");

            migrationBuilder.CreateIndex(
                name: "IX_RevendaItem_ItemFaturavelId",
                table: "RevendaItem",
                column: "ItemFaturavelId");

            migrationBuilder.CreateIndex(
                name: "IX_RevendaItem_RevendaId",
                table: "RevendaItem",
                column: "RevendaId");

            migrationBuilder.CreateIndex(
                name: "IX_Serasa_ClienteId",
                table: "Serasa",
                column: "ClienteId");

            migrationBuilder.CreateIndex(
                name: "IX_Siscovem_ClienteId",
                table: "Siscovem",
                column: "ClienteId");

            migrationBuilder.CreateIndex(
                name: "IX_TabelaAgencia_TabelaId",
                table: "TabelaAgencia",
                column: "TabelaId");

            migrationBuilder.CreateIndex(
                name: "IX_TabelaItem_ItemFaturavelId",
                table: "TabelaItem",
                column: "ItemFaturavelId");

            migrationBuilder.CreateIndex(
                name: "IX_TabelaRevenda_TabelaId",
                table: "TabelaRevenda",
                column: "TabelaId");

            migrationBuilder.CreateIndex(
                name: "IX_Telefone_ClienteId",
                table: "Telefone",
                column: "ClienteId");

            migrationBuilder.CreateIndex(
                name: "IX_TransacaoConsulta_ClienteId",
                table: "TransacaoConsulta",
                column: "ClienteId");

            migrationBuilder.CreateIndex(
                name: "IX_TransacaoConsulta_ItemFaturavelId",
                table: "TransacaoConsulta",
                column: "ItemFaturavelId");

            migrationBuilder.CreateIndex(
                name: "IX_TransacaoConsulta_UsuarioConsultaId",
                table: "TransacaoConsulta",
                column: "UsuarioConsultaId");

            migrationBuilder.CreateIndex(
                name: "IX_Usuario_ColaboradorId",
                table: "Usuario",
                column: "ColaboradorId");

            migrationBuilder.CreateIndex(
                name: "IX_Usuario_PermissaoId",
                table: "Usuario",
                column: "PermissaoId");

            migrationBuilder.CreateIndex(
                name: "IX_UsuarioConsulta_ClienteId",
                table: "UsuarioConsulta",
                column: "ClienteId");

            migrationBuilder.CreateIndex(
                name: "IX_UsuarioConsulta_ContratoId",
                table: "UsuarioConsulta",
                column: "ContratoId");

            migrationBuilder.AddForeignKey(
                name: "FK_Atendimento_Cliente_ClienteId",
                table: "Atendimento",
                column: "ClienteId",
                principalTable: "Cliente",
                principalColumn: "ClienteId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Atendimento_Colaborador_ColaboradorId",
                table: "Atendimento",
                column: "ColaboradorId",
                principalTable: "Colaborador",
                principalColumn: "ColaboradorId");

            migrationBuilder.AddForeignKey(
                name: "FK_Atendimento_Contrato_ContratoId",
                table: "Atendimento",
                column: "ContratoId",
                principalTable: "Contrato",
                principalColumn: "ContratoId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Cliente_Regiao_RegiaoId",
                table: "Cliente",
                column: "RegiaoId",
                principalTable: "Regiao",
                principalColumn: "RegiaoId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Regiao_Cliente_AgenciaId",
                table: "Regiao");

            migrationBuilder.DropTable(
                name: "Atendimento");

            migrationBuilder.DropTable(
                name: "BoletoAvulso");

            migrationBuilder.DropTable(
                name: "ConfigSendGrid");

            migrationBuilder.DropTable(
                name: "Configuracao");

            migrationBuilder.DropTable(
                name: "Consulta");

            migrationBuilder.DropTable(
                name: "Contato");

            migrationBuilder.DropTable(
                name: "ConvemExclusaoAnotacao");

            migrationBuilder.DropTable(
                name: "CreditoConsumo");

            migrationBuilder.DropTable(
                name: "DocumentoPendente");

            migrationBuilder.DropTable(
                name: "Endereco");

            migrationBuilder.DropTable(
                name: "FaturamentoItem");

            migrationBuilder.DropTable(
                name: "ItemFaturavelRelacionado");

            migrationBuilder.DropTable(
                name: "MultLogon");

            migrationBuilder.DropTable(
                name: "PagarMes");

            migrationBuilder.DropTable(
                name: "PedidoCredito");

            migrationBuilder.DropTable(
                name: "PermissaoConsulta");

            migrationBuilder.DropTable(
                name: "PermissaoPagina");

            migrationBuilder.DropTable(
                name: "RevendaItem");

            migrationBuilder.DropTable(
                name: "Serasa");

            migrationBuilder.DropTable(
                name: "Siscovem");

            migrationBuilder.DropTable(
                name: "TabelaAgencia");

            migrationBuilder.DropTable(
                name: "TabelaItem");

            migrationBuilder.DropTable(
                name: "TabelaRevenda");

            migrationBuilder.DropTable(
                name: "Telefone");

            migrationBuilder.DropTable(
                name: "TransacaoConsulta");

            migrationBuilder.DropTable(
                name: "Assunto");

            migrationBuilder.DropTable(
                name: "ConvemInclusaoAnotacao");

            migrationBuilder.DropTable(
                name: "Usuario");

            migrationBuilder.DropTable(
                name: "Faturamento");

            migrationBuilder.DropTable(
                name: "Credito");

            migrationBuilder.DropTable(
                name: "Pagina");

            migrationBuilder.DropTable(
                name: "Revenda");

            migrationBuilder.DropTable(
                name: "ItemFaturavel");

            migrationBuilder.DropTable(
                name: "UsuarioConsulta");

            migrationBuilder.DropTable(
                name: "ConvemCredor");

            migrationBuilder.DropTable(
                name: "Permissao");

            migrationBuilder.DropTable(
                name: "Contrato");

            migrationBuilder.DropTable(
                name: "Colaborador");

            migrationBuilder.DropTable(
                name: "DiaCobranca");

            migrationBuilder.DropTable(
                name: "Tabela");

            migrationBuilder.DropTable(
                name: "Cargo");

            migrationBuilder.DropTable(
                name: "Cliente");

            migrationBuilder.DropTable(
                name: "Atividade");

            migrationBuilder.DropTable(
                name: "Regiao");
        }
    }
}
