﻿using Sig.Web.Models;
using System.Collections.Generic;

namespace Sig.Web.ViewModels.Usuarios
{
    public class UsuarioViewModel
    {
        public Usuario Usuario { get; set; }
        public bool Inativos { get; set; }
        public int? PerfilId { get; set; }
        public IEnumerable<Usuario> Usuarios { get; set; }
    }
}
