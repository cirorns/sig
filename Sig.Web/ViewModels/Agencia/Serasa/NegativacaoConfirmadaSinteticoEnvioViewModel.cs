﻿namespace Sig.Web.ViewModels.Agencia.Serasa
{
    public class NegativacaoConfirmadaSinteticoEnvioViewModel
    {
        public string DataInicial { get; set; }
        public string DataFinal { get; set; }
    }
}