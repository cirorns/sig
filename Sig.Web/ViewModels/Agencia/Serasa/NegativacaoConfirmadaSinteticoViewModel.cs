﻿namespace Sig.Web.ViewModels.Agencia.Serasa
{
    public class NegativacaoConfirmadaSinteticoViewModel
    {
        public int ClienteId { get; set; }
        public string CredorRazaoSocial { get; set; }
        public string Periodo { get; set; }
        public string TipoDocumento { get; set; }
        public string DevedorUF { get; set; }
        public int Quantidade { get; set; }
    }
}