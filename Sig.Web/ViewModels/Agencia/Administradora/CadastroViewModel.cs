﻿using Sig.Web.Models;
using System.Collections.Generic;

namespace Sig.Web.ViewModels.Agencia.Administradora
{
    public class CadastroViewModel
    {
        public Cliente Cliente { get; set; }
        public List<Endereco> Enderecos { get; set; }
        public List<Telefone> Telefones { get; set; }
        public Colaborador Colaborador { get; set; }
        
        public CadastroViewModel()
        {
            Enderecos = new List<Endereco>();
            Telefones = new List<Telefone>();
        }
    }
}