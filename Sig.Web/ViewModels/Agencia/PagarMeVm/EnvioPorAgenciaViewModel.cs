﻿using Sig.Web.Helpers;
using System;
using System.ComponentModel.DataAnnotations;

namespace Sig.Web.ViewModels.Agencia.PagarMeVm
{
    public class EnvioPorAgenciaViewModel
    {
        [Required(ErrorMessage = Mensagens.Requerido)]
        public int AgenciaId { get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        public DateTime DataInicial { get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        public DateTime DataFinal { get; set; }
    }
}