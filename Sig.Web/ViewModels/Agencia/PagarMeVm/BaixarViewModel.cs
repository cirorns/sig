﻿using Sig.Web.Helpers;
using System;
using System.ComponentModel.DataAnnotations;

namespace SIG.Web.ViewModels.Agencia.PagarMeVm
{
    public class BaixarViewModel
    {
        [Required(ErrorMessage = Mensagens.Requerido)]
        public DateTime DataInicial { get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        public DateTime DataFinal { get; set; }
    }
}