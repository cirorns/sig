﻿using Sig.Web.Helpers;
using System;
using System.ComponentModel.DataAnnotations;

namespace Sig.Web.ViewModels.Agencia.PagarMeVm
{
    public class AtualizarFaturaViewModel
    {
        [Required(ErrorMessage = Mensagens.Requerido)]
        public int FaturamentoId { get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        public DateTime DataVencimento { get; set; }

        public decimal? ValorJuros { get; set; }

        public decimal? ValorDesconto { get; set; }

        public bool IsTaxaBoleto { get; set; }

        public string Observacao { get; set; }
    }
}