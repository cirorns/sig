﻿using System.ComponentModel.DataAnnotations;

namespace Sig.Web.ViewModels.Agencia.TabelaVm
{
    public class ReajusteViewModel
    {
        [Required(ErrorMessage = "O campo é obrigatório.")]
        public int TabelaId { get; set; }

        [Required(ErrorMessage = "O campo é obrigatório.")]
        public int Percentual { get; set; }
    }
}