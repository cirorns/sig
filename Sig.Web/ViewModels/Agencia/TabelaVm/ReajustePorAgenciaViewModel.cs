﻿using System.ComponentModel.DataAnnotations;

namespace Sig.Web.ViewModels.Agencia.TabelaVm
{
    public class ReajustePorAgenciaViewModel
    {
        [Required(ErrorMessage = "O campo é obrigatório.")]
        public int AgenciaId { get; set; }

        [Required(ErrorMessage = "O campo é obrigatório.")]
        public int Percentual { get; set; }
    }
}