﻿using Sig.Web.Helpers;
using System.ComponentModel.DataAnnotations;

namespace Sig.Web.ViewModels.Agencia.ClienteVm
{
    public class MigrarViewModel
    {
        [Required(ErrorMessage = Mensagens.Requerido)]
        public int ClienteId { get; set; }

        [Required(ErrorMessage = Mensagens.Requerido)]
        public int AgenciaId { get; set; }
    }
}