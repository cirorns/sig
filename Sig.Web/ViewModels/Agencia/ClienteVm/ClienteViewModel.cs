﻿using System.Collections.Generic;
using Sig.Web.Models;

namespace Sig.Web.ViewModels.Agencia.ClienteVm
{
    public class ClienteViewModel
    {
        public Cliente Cliente { get; set; }
        public Contrato Contrato { get; set; }
        public Contato Contato { get; set; }
        public IList<Endereco> Enderecos { get; set; }
        public IList<Telefone> Telefones { get; set; }
    }
}