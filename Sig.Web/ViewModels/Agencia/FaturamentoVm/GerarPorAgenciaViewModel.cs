﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Sig.Web.ViewModels.Agencia.FaturamentoVm
{
    public class GerarPorAgenciaViewModel
    {
        [Required(ErrorMessage = "O campo é obrigatório.")]
        public int AgenciaId { get; set; }

        [Required(ErrorMessage = "O campo é obrigatório.")]
        public DateTime DataInicial { get; set; }

        [Required(ErrorMessage = "O campo é obrigatório.")]
        public DateTime DataFinal { get; set; }

        public DateTime? DataVencimento { get; set; }
    }
}