﻿using Sig.Web.Helpers;
using System.ComponentModel.DataAnnotations;

namespace Sig.Web.ViewModels.Agencia.Usuario
{
    public class RecuperarSenhaViewModel
    {
        [StringLength(14, ErrorMessage = Mensagens.Maximo)]
        public string CpfCnpj { get; set; }

        [StringLength(100, ErrorMessage = Mensagens.Maximo)]
        [EmailAddress(ErrorMessage = Mensagens.Email)]
        public string Email { get; set; }
    }
}