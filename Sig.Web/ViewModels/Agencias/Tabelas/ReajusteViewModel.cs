﻿using System.ComponentModel.DataAnnotations;

namespace Sig.Web.ViewModels.Agencias.Tabelas
{
    public class ReajusteViewModel
    {
        [Required(ErrorMessage = "O campo é obrigatório.")]
        public int TabelaId { get; set; }

        public int AgenciaId { get; set; }

        [Required(ErrorMessage = "O campo é obrigatório.")]
        public int Percentual { get; set; }
    }
}
