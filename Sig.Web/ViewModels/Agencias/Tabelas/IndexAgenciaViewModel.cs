﻿using Sig.Web.Models;
using System.Collections.Generic;

namespace Sig.Web.ViewModels.Agencias.Tabelas
{
    public class IndexAgenciaViewModel
    {
        public Tabela Tabela { get; set; }
        public IEnumerable<TabelaAgencia> TabelasAgencia { get; set; }
    }
}
